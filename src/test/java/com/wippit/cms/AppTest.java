package com.wippit.cms;

import com.wippit.cms.backend.utils.color.Color;
import com.wippit.cms.backend.utils.color.RGBColor;
import com.wippit.cms.backend.utils.color.Solver;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Slf4j
//@SpringBootTest
public class AppTest {

    @Test
    public void colorTest() {

        Color color = Color.hex("#DD2C10");
        log.debug("Doing color test... {}", color.getHex());

        RGBColor rgbColor = new RGBColor(color.getRed(),color.getGreen(),color.getBlue());
        Solver solver = new Solver(rgbColor);
        Solver.Result result = solver.solve();

        log.debug("Color[{}] --> RGB: {} ---> Filter: {}", color.getHex(), rgbColor, result.getFilter());

    }

    @Test
    public void testConversions(){

        Timestamp now = AppUtils.timestampNow();
        Timestamp tomorrow = AppUtils.getNowPlusDays(1);
        log.debug("TS now: {}", now);
        log.debug("TS tomorrow: {}", tomorrow);

        LocalDateTime ldtNow = AppUtils.timestampToSiteDateTime(now);
        LocalDateTime ldtTomorrow = AppUtils.timestampToSiteDateTime(tomorrow);

        log.debug("DT now: {}", ldtNow);
        log.debug("DT tomorrow: {}", ldtTomorrow);

        log.debug("DT format now:      {}", AppUtils.timestampSiteDateTimeString(now));
        log.debug("DT format tomorrow: {}", AppUtils.timestampSiteDateTimeString(tomorrow));

    }

    public static String fixAccountName(String original) {
        String data = StringUtils.stripAccents(original).toLowerCase();
        StringBuilder account = new StringBuilder();
        String validCharacters = "abcdefghijklmnopqrstuvwxyz1234567890_.";

        for (int index = 0; index < data.length(); index++) {
            if (validCharacters.contains(String.valueOf(data.charAt(index)))) {
                account.append(data.charAt(index));
            }
        }

        return account.toString();
    }

    @Test
    public void testAccount() {

        System.out.println("Account: " + fixAccountName("āăąēîïĩíĝġńñšŝśûůŷ"));
        System.out.println("Account: "+ fixAccountName("'¡!}\"#%$%%&$%&%/Chícágo-.-Pérez"));
        System.out.println("Account: " + fixAccountName("ʲLos Pájaros !}\"#$%&/()=?¡¿"));
    }
}
