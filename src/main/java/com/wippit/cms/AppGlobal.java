/***********************************************************************
 Copyright 2024 Wippit.Systems

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ************************************************************************/

package com.wippit.cms;

import com.wippit.cms.backend.data.bean.site.SiteInfo;
import com.wippit.cms.backend.data.bean.site.SiteKeys;
import com.wippit.cms.backend.data.services.SiteDataService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
public class AppGlobal {

    @Getter
    private String path = "";
    @Getter
    private String url = "";
    @Getter
    @Setter
    private SiteInfo siteInfo = new SiteInfo();
    @Getter
    private String templatePath = "";
    @Getter
    private String storagePath = "";
    @Getter
    private String tempPath = "";
    @Getter
    private String emailPath = "";
    @Getter
    @Setter
    private int cmsPort = 0;
    @Getter
    @Setter
    private SiteKeys keys = new SiteKeys();
    @Getter
    @Setter
    private boolean setupRequired = false;
    @Getter
    @Setter
    private String cryptoKey;

    private static final AppGlobal shared = new AppGlobal();
    public static AppGlobal getInstance() {
        return shared;
    }

    private AppGlobal(){
    }

    public static void setup(String newPath, String url, SiteDataService siteService) {

        log.debug("Setup request path: {}", newPath);
        log.debug("Setup request url: {}", url);

        //---
        //Paths
        //---
        String workPath = "";
        if (!StringUtils.isBlank(newPath)) {
            if (FileUtils.isDirectory(new File(newPath))) {
                workPath = newPath;
            }
            else {
                log.warn("Work path is invalid: {}", newPath);
                workPath = "";
            }
        }

        if (StringUtils.isBlank(workPath)) {
            log.warn("Moving to current dir. Work path is invalid.");
            workPath = Path.of("").toAbsolutePath().toAbsolutePath().toString() + "/work";
        }

        log.info("Server URL: {}", url);
        log.info("Server Port: {}", AppGlobal.shared.cmsPort);

        AppGlobal.shared.url = url;
        AppGlobal.shared.path = workPath;
        AppGlobal.shared.templatePath = workPath + "/templates";
        AppGlobal.shared.emailPath = workPath + "/email";
        AppGlobal.shared.storagePath = workPath + "/storage";
        AppGlobal.shared.tempPath = workPath + "/tmp";

//        int setup = keyService.getKeyAsInt("setup.required",0);
//        log.info("Setup required = {}", setup);
//        shared.setupRequired = (setup>0);

        shared.setupRequired = siteService.setupRequired();
        log.info("Setup required = {}", shared.setupRequired);

        try {
            AppGlobal.getInstance().createDefaultDirs();
        }
        catch (Exception ex) {
            log.error("Creating work directories: {}", ex.toString());
        }


        readSettings(siteService);
    }

    public String getDomain() {
        URI uri = URI.create(url);
        return uri.getHost();
    }

    public static void saveSettings(SiteDataService siteService) {
        siteService.save(AppGlobal.shared.getSiteInfo());
        siteService.save(AppGlobal.shared.getKeys());
    }

    private static void readSettings(SiteDataService siteService) {
        AppGlobal.shared.siteInfo = siteService.getSiteInfo();
        AppGlobal.shared.keys = siteService.getSiteKeys();

    }

    private void createDefaultDirs() throws Exception {
        log.debug("Creating work directories");
        if (!FileUtils.isDirectory(new File(AppGlobal.getInstance().getTemplatePath()))) {
            Files.createDirectories(Paths.get(AppGlobal.getInstance().getTemplatePath()));
        }
        if (!FileUtils.isDirectory(new File(AppGlobal.getInstance().getEmailPath()))) {
            Files.createDirectories(Paths.get(AppGlobal.getInstance().getEmailPath()));
        }
        if (!FileUtils.isDirectory(new File(AppGlobal.getInstance().getStoragePath()))) {
            Files.createDirectories(Paths.get(AppGlobal.getInstance().getStoragePath()));
        }
        if (!FileUtils.isDirectory(new File(AppGlobal.getInstance().getTempPath()))) {
            Files.createDirectories(Paths.get(AppGlobal.getInstance().getTempPath()));
        }

        String geoPath = AppGlobal.getInstance().getPath() + "/geo";
        if (!FileUtils.isDirectory(new File(geoPath))) {
            Files.createDirectories(Paths.get(geoPath));
        }

    }

}
