/***********************************************************************
 Copyright 2024 Wippit.Systems

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ************************************************************************/

package com.wippit.cms;

import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class AppRedirect {


    @Value("${spring.profiles.active}")
    private String activeProfile;

    @Value("${server.port}")
    private int serverPort;

    @Bean
    public ServletWebServerFactory servletContainer() {

        log.info("Server port: {}", serverPort);

        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory() {
            @Override
            protected void postProcessContext(Context context) {
                    SecurityConstraint securityConstraint = new SecurityConstraint();
                    securityConstraint.setUserConstraint("CONFIDENTIAL");
                    SecurityCollection collection = new SecurityCollection();
                    collection.addPattern("/*");
                    securityConstraint.addCollection(collection);
                    context.addConstraint(securityConstraint);
            }
        };
        tomcat.addAdditionalTomcatConnectors(redirectConnector());
        return tomcat;
    }

    private Connector redirectConnector() {

        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        connector.setScheme("http");
        connector.setSecure(false);

        if (activeProfile.equalsIgnoreCase("production")) {
            log.info("Setup redirect 80 -> {}", serverPort);
            connector.setPort(80);
            connector.setRedirectPort(serverPort);
            AppGlobal.getInstance().setCmsPort(serverPort);
        }
        else if (activeProfile.equalsIgnoreCase("beta")) {
            log.info("Setup redirect 8080 -> {}", serverPort);
            connector.setPort(8080);
            connector.setRedirectPort(serverPort);
            AppGlobal.getInstance().setCmsPort(serverPort);
        }
        else {
            //---
            //devel
            //---
            log.info("Setup redirect 8080 -> {}", serverPort);
            connector.setPort(8080);
            connector.setRedirectPort(serverPort);
            AppGlobal.getInstance().setCmsPort(serverPort);
        }
        return connector;
    }

}
