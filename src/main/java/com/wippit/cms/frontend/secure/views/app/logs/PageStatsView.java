package com.wippit.cms.frontend.secure.views.app.logs;

import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wippit.cms.backend.data.bean.stats.HitStatItem;
import com.wippit.cms.backend.data.services.HitStatDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.types.CMSItemTypes;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.secure.views.MainLayout;
import jakarta.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Autowired;

@PageTitle("Page Stats")
@Route(value = "cms/pagestats", layout = MainLayout.class)
@RolesAllowed({"ADMIN", "MANAGER", "DEVELOPER"})
@Uses(Icon.class)
public class PageStatsView extends OWSecureAdapterView {

    @Autowired
    private HitStatDataService dataService;
    private Grid<HitStatItem> grid;



    @Override
    public boolean setup() {
        dataService.setType(CMSItemTypes.ALL);
        return true;
    }

    public PageStatsView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {
        DataProvider<HitStatItem, Void> dataProvider;
        dataProvider = DataProvider.fromCallbacks(
                query -> dataService.getPage(query.getPage(), query.getLimit()).stream(),
                query -> (int)dataService.getCount());

        grid = new Grid<>();
        grid.setId("grid");
        grid.addColumn(HitStatItem::getUrl).setHeader("URL").setWidth("90%");
        grid.addColumn(HitStatItem::getCount).setHeader("Requests").setWidth("10%");
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setDataProvider(dataProvider);

        grid.setWidthFull();
        grid.setHeight((size.getHeight()-200)+"px");
        add(grid);
    }

    @Override
    public void resizeUI(Size size) {
        grid.setHeight((size.getHeight()-200)+"px");
    }

}
