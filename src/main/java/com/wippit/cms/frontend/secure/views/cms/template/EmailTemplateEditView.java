package com.wippit.cms.frontend.secure.views.cms.template;

import com.hilerio.ace.AceEditor;
import com.hilerio.ace.AceMode;
import com.hilerio.ace.AceTheme;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.TabSheet;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.backend.data.services.EmailTemplateDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.services.email.EmailAttachment;
import com.wippit.cms.backend.services.email.EmailTemplate;
import com.wippit.cms.backend.services.email.EmailTemplateManager;
import com.wippit.cms.backend.types.ApiEmailTypes;
import com.wippit.cms.backend.types.AppIcons;
import com.wippit.cms.backend.types.ToolbarTypes;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.components.DialogManager;
import com.wippit.cms.frontend.secure.views.MainLayout;
import io.overcoded.vaadin.tinymce.TinyMceField;
import io.overcoded.vaadin.tinymce.config.TinyMceConfig;
import jakarta.annotation.security.RolesAllowed;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@PageTitle("Email Template Edit")
@Route(value = "cms/emails/edit", layout = MainLayout.class)
@RolesAllowed({"ADMIN", "DEVELOPER"})
@Uses(Icon.class)
public class EmailTemplateEditView extends OWSecureAdapterView {

    private Tabs tabs;
    private AceEditor editorDefault;
    private Grid<EmailAttachment> grid;
    private final int MARGIN_W = 30;
    private final int MARGIN_H = 170;
    private List<EmailAttachment> templateAttachments = new ArrayList<>();
    private String contentHTML = "";
    private String contentPlain = "";
    private boolean flagHTMLSelected = true;
    private VerticalLayout editorLayout;

    private EmailTemplate template;

    @Autowired
    private EmailTemplateDataService dataService;

    public EmailTemplateEditView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public boolean setup() {

        template = (EmailTemplate) UI.getCurrent().getSession().getAttribute("template");
        if (template == null) {
            return false;
        }

        return true;
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {

        Button buttonAddAttachment = ViewUtils.getToolbarButton(AppIcons.CLOUD_UPLOAD, "Add attachment");
        Button buttonEditHTML = ViewUtils.getToolbarButton(AppIcons.EDIT_HTML, "Edit HTML");

        HorizontalLayout toolbar = ViewUtils.getToolbar(ToolbarTypes.HEADER);
        toolbar.add(
                buttonAddAttachment,
                buttonEditHTML
        );
        add(toolbar);
        add(ViewUtils.getLineSecondary());

        TabSheet tabSheet = new TabSheet();
        tabSheet.add("Settings", getSettingsLayout(size));
        tabSheet.add("Content", getContentLayout(size));
        tabSheet.add("Attachments", getGridFiles(size));
        tabSheet.setWidth((size.getWidth()-MARGIN_W) + "px");
        tabSheet.setHeight((size.getHeight() - MARGIN_H) + "px");
        add(tabSheet);

        buttonAddAttachment.addClickListener(e->{
            log.debug("Add attachment");
            addFileToTemplate();
        });

        buttonEditHTML.addClickListener(e->editHTML());

        loadSelectedTemplate();
    }

    @Override
    public void resizeUI(Size size) {

        int h = size.getHeight() - MARGIN_H;
        tabs.setWidth((size.getWidth()-MARGIN_W) + "px");
        tabs.setHeight(h + "px");

        h = h - 60;
        grid.setHeight(h + "px");
        editorLayout.setHeight(h + "px");
        h = h - 60;
        editorDefault.setHeight(h + "px");
    }

    private VerticalLayout getSettingsLayout(Size size) {
        VerticalLayout layout = new VerticalLayout();

        FormLayout formLayout = new FormLayout();

        TextField idField = new TextField("ID");
        idField.setValue(template.getEtid());
        idField.setReadOnly(true);

        TextField nameField = new TextField("Name");
        nameField.setValue(template.getName());
        nameField.setRequired(true);

        ComboBox<ApiEmailTypes> templateCombo = new ComboBox<>("Type");
        templateCombo.setItems(ApiEmailTypes.toList());
        templateCombo.setValue(ApiEmailTypes.getType(template.getType()));

        TextArea dataField = new TextArea("Data");
        dataField.setValue(template.getData());

        TextArea commentsField = new TextArea("Comments");
        commentsField.setValue(template.getComments());

        if (isMobile()) {
            formLayout.setWidth("90%");
        }
        else {
            formLayout.setWidth("40em");
        }

        formLayout.add(
                idField,
                nameField,
                templateCombo,
                dataField,
                commentsField
        );
        formLayout.setColspan(idField, 2);
        layout.add(formLayout);

        HorizontalLayout footer = ViewUtils.getToolbar(ToolbarTypes.FOOTER);
        Button saveButton = ViewUtils.getActiveButton("Save");
        footer.add(saveButton);
        layout.add(footer);

        templateCombo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                return;
            }
            template.setType(e.getValue().getType());
        });

        saveButton.addClickListener(e->{

            String name = nameField.getValue();
            String data = dataField.getValue();
            String comments = commentsField.getValue();

            if (StringUtils.isBlank(name) || templateCombo.getValue() == null) {
                ViewUtils.showNotificationError("Name & type are required");
                return;
            }
            if (StringUtils.isBlank(data)){
                data = "";
            }
            if (StringUtils.isBlank(comments)) {
                comments = "";
            }

            template.setName(name);
            template.setData(data);
            template.setComments(comments);
            template.setType(templateCombo.getValue().getType());
            template.setHtml(contentHTML);
            template.setPlain(contentPlain);

            saveTemplate();
        });

        return layout;
    }

    private VerticalLayout getContentLayout(Size size) {
        editorLayout = new VerticalLayout();
        editorLayout.setMargin(false);
        editorLayout.setPadding(false);
        editorLayout.setWidthFull();
        editorLayout.setHeight((size.getHeight() - MARGIN_H - 60) + "px");
        //editorLayout.getStyle().set("background-color", "gray");

        Tab tabHTML = new Tab("HTML");
        Tab tabPlain = new Tab("Plain");
        tabs = new Tabs(tabHTML, tabPlain);
        tabs.setWidthFull();
        tabs.addSelectedChangeListener(e->{
            flagHTMLSelected = e.getSource().getSelectedIndex() == 0;
            if (flagHTMLSelected) {
                editorDefault.setValue(contentHTML);
            }
            else {
                editorDefault.setValue(contentPlain);
            }
        });
        editorLayout.add(tabs);
        editorLayout.add(getEditor(size));
        return editorLayout;
    }

    private AceEditor getEditor(Size size) {

        editorDefault = new AceEditor();
        editorDefault.setBasePath("/static/editor");
        editorDefault.setTheme(AceTheme.xcode);
        editorDefault.setMode(AceMode.ftl);
        editorDefault.setFontSize(15);
        editorDefault.setReadOnly(false);
        editorDefault.setShowInvisibles(true);
        editorDefault.setShowGutter(true);
        editorDefault.setShowPrintMargin(true);
        editorDefault.setDisplayIndentGuides(false);
        editorDefault.setUseWorker(false);

        editorDefault.addValueChangeListener(e -> {
            log.debug("Editor changed...");
            if (flagHTMLSelected) {
                contentHTML = e.getValue();
            }
            else {
                contentPlain = e.getValue();
            }
        });

        editorDefault.setSofttabs(false);
        editorDefault.setTabSize(4);
        editorDefault.setWrap(false);
        editorDefault.setWidthFull();
        editorDefault.setHeightFull();

        editorDefault.setWidthFull();
        editorDefault.setHeight((size.getHeight() - MARGIN_H - 120) + "px");

        return editorDefault;
    }

    private Grid<EmailAttachment> getGridFiles(Size size) {

        grid = new Grid<>();
        grid.setId("grid");
        grid.addColumn(EmailAttachment::getCid).setHeader("File").setWidth("80%");
        grid.addComponentColumn(this::getDeleteButton).setWidth("5%");
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setWidthFull();
        grid.setHeight((size.getHeight() - MARGIN_H - 60) + "px");

        return grid;
    }

    private Button getDeleteButton(EmailAttachment item){
        Button button = ViewUtils.getToolbarButton(AppIcons.TRASH, "Delete file " + item.getCid());
        button.addClickListener(e->{
            log.debug("Delete file: {}", item.getCid());
            deleteFileInTemplate(item);
        });
        return button;
    }

    private void loadSelectedTemplate(){

        editorDefault.clear();
        contentHTML = "";
        contentPlain = "";

        try {

            //---
            //Template file HTML
            //---
            log.debug("Loading template: {}", template.getName());
            contentHTML = template.getHtml();
            contentPlain = template.getPlain();

            //---
            //Files in directory
            //---
            templateAttachments = dataService.getAttachments(template);
            grid.setItems(templateAttachments);

            if (flagHTMLSelected) {
                editorDefault.setValue(contentHTML);
            }
            else {
                editorDefault.setValue(contentPlain);
            }
        }
        catch (Exception ex) {
            log.error("Loading template files: {}", ex.toString());
            ViewUtils.showNotificationError("Problems loading template");
        }
    }

    private Set<String> listFilesUsingFilesList(String dir) throws IOException {
        try (Stream<Path> stream = Files.list(Paths.get(dir))) {
            return stream
                    .filter(file -> !Files.isDirectory(file))
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .collect(Collectors.toSet());
        }
    }

    private void deleteFileInTemplate(EmailAttachment item) {
//        String pathDir = AppGlobal.getInstance().getEmailPath() + "/" + template.getTemplate();
//        String fileToDeletePath = pathDir + "/" + templateAttachmentItem.getFilename();
//
//        try {
//            FileUtils.delete(new File(fileToDeletePath));
//            log.debug("File deleted: {}", fileToDeletePath);
//            loadFilesForTemplate();
//        }
//        catch (Exception ex) {
//            log.error("Removing file: {}", ex.toString());
//            ViewUtils.showNotificationError("Can't remove file " + templateAttachmentItem.getFilename());
//        }
    }

    private void saveTemplate() {

        if (template == null) {
            return;
        }

        if (StringUtils.isBlank(contentHTML.trim()) || StringUtils.isBlank(contentPlain.trim())) {
            ViewUtils.showNotificationWarning("Template HTML & Plain are required");
            return;
        }

        template.setHtml(contentHTML.trim());
        template.setPlain(contentPlain.trim());

        if (dataService.save(template)) {
            ViewUtils.showNotification("Template saved");
            EmailTemplateManager.current.setFlag();
            UI.getCurrent().navigate(EmailTemplatesView.class);
        }
        else {
            ViewUtils.showNotificationError("Can't save template");
        }

    }

    private void addFileToTemplate() {

    }

    private void editHTML() {
        DialogManager dialog;

        Size size = new Size(getCurrentSize().getWidth(), getCurrentSize().getHeight());
        if (isMobile()) {
            size = size.getInnerSize(50);
            dialog = new DialogManager(size.getWidth(), size.getHeight(), "Edit HTML");
        }
        else {
            size = size.getInnerSize(100);
            dialog = new DialogManager(size.getWidth(), size.getHeight(), "Edit HTML");
        }

        dialog.setResizableAndDraggable();
        TinyMceConfig config;
            config = TinyMceConfig.builder()
                    .height((size.getHeight() - 150) + "px")
                    .apiKey(AppGlobal.getInstance().getKeys().getEditor())
                    .menubar(List.of("false"))
                    .plugins(List.of("anchor", "lists", "link", "code", "fullscreen", "image", "table", "accordion", "emoticons", "charmap", "preview"))
                    .toolbar(List.of("undo", "redo", "|", "blocks", "|", "styleselect", "removeformat", "|", "bold", "italic", "underline", "bullist", "numlist", "|", "alignleft", "aligncenter", "alignright", "alignjustify", "|", "outdent", "indent", "|", "forecolor","backcolor","|", "table", "accordion", "charmap", "emoticons","link", "image", "code", "preview", "fullscreen"))
                    .build();

        TinyMceField editorHTML = new TinyMceField(config);
        editorHTML.setWidthFull();
        editorHTML.setHeight((size.getHeight()-150)  + "px");
        editorHTML.setContent(contentHTML);
        dialog.getContainer().add(editorHTML);

        Button saveButton = dialog.addMainButton("Set");
        saveButton.addClickListener(e->{
            template.setHtml(editorHTML.getContent());
            contentHTML = editorHTML.getContent();
            if(flagHTMLSelected) {
                editorDefault.setValue(contentHTML);
            }
            dialog.close();
        });

        dialog.open();
    }
}
