package com.wippit.cms.frontend.secure.views.cms.translations;

import com.deepl.api.TextResult;
import com.deepl.api.Translator;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.properties.AppProperties;
import com.wippit.cms.backend.data.services.LanguageDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.types.AppIcons;
import com.wippit.cms.backend.types.ToolbarTypes;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.backend.utils.i18n.LanguageItem;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.components.DialogManager;
import com.wippit.cms.frontend.components.dialog.DialogBox;
import com.wippit.cms.frontend.components.dialog.DialogBoxAdapter;
import com.wippit.cms.frontend.secure.views.MainLayout;
import jakarta.annotation.security.RolesAllowed;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@PageTitle("Translations")
@Route(value = "cms/translations", layout = MainLayout.class)
@RolesAllowed({"ADMIN", "MANAGER"})
public class TranslationsView  extends OWSecureAdapterView {

    private ComboBox<String> pagesCombo;
    private String template;
    private Translator translator;
    private LocalCountry selectedCountry;
    private boolean flagValidKey = false;

    private DataProvider<LanguageItem, Void> dataProvider;

    @Autowired
    private LanguageDataService languageDataService;

    @Autowired
    private AppProperties appProperties;

    private Grid<LanguageItem> grid;
    public TranslationsView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public boolean setup() {
        template = AppGlobal.getInstance().getSiteInfo().getSiteTemplate().getName();
        languageDataService.setTemplate(template);
        log.debug("Template: {}", template);

        String key = AppGlobal.getInstance().getKeys().getTranslator();
        if (!StringUtils.isBlank(key)) {
            flagValidKey = true;
            try {
                translator = new Translator(AppGlobal.getInstance().getKeys().getTranslator());
            }
            catch (Exception ex) {
                setMessageError("Invalid translator key. Please set a valid translator key in settings.");
                return false;
            }
        }

        selectedCountry = (LocalCountry) UI.getCurrent().getSession().getAttribute("country");
        if (selectedCountry == null) {
            List<LocalCountry> countries = languageDataService.getCountries();
            for (LocalCountry country : countries) {
                if (country.getIso().equalsIgnoreCase(AppGlobal.getInstance().getSiteInfo().getCountry_iso())) {
                    selectedCountry = country;
                    break;
                }
            }
        }

        languageDataService.setIso(selectedCountry.getIso());

        dataProvider = DataProvider.fromCallbacks(
                query -> languageDataService.getPage(query.getPage(), query.getLimit()).stream(),
                query -> languageDataService.getCount());

        return true;
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {

        ComboBox<LocalCountry> countryCombo = new ComboBox<>();
        countryCombo.setWidth("20em");
        countryCombo.setItems(languageDataService.getCountries());
        countryCombo.setValue(selectedCountry);

        pagesCombo = new ComboBox<>();
        ComboBox<String> templateCombo = new ComboBox<>();

        List<String> templateList = new ArrayList<>();
        templateList.add("email");
        templateList.add(template);
        templateCombo.setItems(templateList);
        templateCombo.setValue(template);


        templateCombo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                return;
            }

            template = e.getValue();
            languageDataService.setTemplate(template);
            languageDataService.setIso(selectedCountry.getIso());
            languageDataService.setPage("");
            pagesCombo.setItems(languageDataService.getPages());
            reloadGrid();
        });

        countryCombo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                return;
            }

            selectedCountry = e.getValue();
            UI.getCurrent().getSession().setAttribute("country", selectedCountry);
            languageDataService.setIso(selectedCountry.getIso());
            languageDataService.setPage("");

            pagesCombo.setItems(languageDataService.getPages());
            reloadGrid();
        });

        pagesCombo.setItems(languageDataService.getPages());
        pagesCombo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                return;
            }
            languageDataService.setPage(e.getValue());
            reloadGrid();
        });

        Button buttonResetLocalization = ViewUtils.getToolbarButton(AppIcons.PAGE_TRASH, "Reset localization");
        Button buttonResetPage = ViewUtils.getToolbarButton(AppIcons.DOCUMENT_TRASH, "Reset page");


        HorizontalLayout toolbar = ViewUtils.getToolbar(ToolbarTypes.HEADER);
        toolbar.add(
                ViewUtils.getToolbarLabel("Template"),
                templateCombo,
                ViewUtils.getToolbarLabel("Locale"),
                countryCombo,
                ViewUtils.getToolbarLabel("Page"),
                pagesCombo,
                ViewUtils.getToolbarLabel("Actions"),
                buttonResetLocalization,
                buttonResetPage
        );
        add(toolbar);
        add(ViewUtils.getLineSecondary());
        grid = new Grid<>();
        grid.setId("grid");
        grid.addColumn(LanguageItem::getKey).setHeader("Key").setWidth("45%");
        grid.addColumn(LanguageItem::getContent).setHeader("Content").setWidth("50%");
        grid.addComponentColumn(this::getEditButton).setWidth("5%");
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setWidthFull();
        grid.setHeight((size.getHeight()  - 200) + "px");
        grid.setDataProvider(dataProvider);
        add(grid);

        reloadGrid();

        buttonResetLocalization.addClickListener(e->resetLocalizationDialog());
        buttonResetPage.addClickListener(e->resetPageDialog());
    }

    @Override
    public void resizeUI(Size size) {
        grid.setHeight((size.getHeight()  - 200) + "px");
    }

    @Override
    public void buildErrorUI() {

    }

    private void reloadGrid() {
        dataProvider.refreshAll();
    }

    private Button getEditButton(LanguageItem item) {
        Button button = ViewUtils.getToolbarButton(VaadinIcon.EDIT.create(), "Edit text");
        button.addClickListener(e->{
            editItemDialog(item);
        });
        return button;
    }

    private void editItemDialog(LanguageItem item) {

        DialogManager dialog = new DialogManager("Edit text");
        dialog.setResizableAndDraggable();

        TextArea keyField = new TextArea("Key");
        keyField.setValue(item.getKey());
        keyField.setReadOnly(true);
        if (item.getKey().length()<140) {
            keyField.setHeight("6em");
        }
        else if (item.getKey().length()<280) {
            keyField.setHeight("10em");
        }
        else if (item.getKey().length()<420) {
            keyField.setHeight("15em");
        }
        else {
            keyField.setHeight("21em");
        }

        TextArea valueField = new TextArea("Value");
        valueField.setValue(item.getContent());
        if (item.getContent().length()<140) {
            valueField.setHeight("6em");
        }
        else if (item.getContent().length()<280) {
            valueField.setHeight("10em");
        }
        else if (item.getContent().length()<420) {
            valueField.setHeight("15em");
        }
        else {
            valueField.setHeight("21em");
        }
        dialog.addToForm(keyField);
        dialog.addToForm(valueField);

        if (flagValidKey) {
            Button translateButton = dialog.addSecondaryButton("Translate");
            translateButton.addClickListener(e -> {

                if (StringUtils.isBlank(keyField.getValue())) {
                    return;
                }

                try {
                    TextResult result = translator.translateText(keyField.getValue(), null, selectedCountry.getLocale());
                    valueField.setValue(result.getText());
                } catch (Exception ex) {
                    log.error("On translation: {}", ex.toString());
                    ViewUtils.showNotificationError("Can't translate");
                }
            });
        }

        Button resetButton = dialog.addSecondaryButton("Reset");
        resetButton.addClickListener(e->{
            valueField.setValue(keyField.getValue());
        });


        Button rawSaveButton = dialog.addSecondaryButton("Raw Save");
        rawSaveButton.addClickListener(e->{
           if (saveItem(true, valueField.getValue(), item)) {
               reloadGrid();
               dialog.close();
           }
        });

        Button saveButton = dialog.addMainButton("Save");
        saveButton.addClickListener(e->{
            if (saveItem(false, valueField.getValue(), item)) {
                reloadGrid();
                dialog.close();
            }
        });

        dialog.open();
    }

    private boolean saveItem(boolean flagRaw, String original, LanguageItem item) {
        if (StringUtils.isBlank(original)) {
            return false;
        }

        String value = original.trim();

        if (!flagRaw) {
            value = StringEscapeUtils.escapeHtml4(value);
        }

        if (languageDataService.addItem(template, item.getPage(), item.getIso(), item.getKey(), value)) {
            return true;
        }

        ViewUtils.showNotificationError("Can't save item");
        return  false;
    }

    private void resetLocalizationDialog() {

        StringBuilder sb = new StringBuilder();
        sb.append("Confirm to reset localization. ");
        sb.append("This will remove all items for localization '");
        sb.append(selectedCountry.getIso());
        sb.append("'.");

        DialogBox dialog = new DialogBox();
        dialog.showConfirm("Reset Localization", sb.toString());
        dialog.addListener(new DialogBoxAdapter() {
            @Override
            public void dialogConfirm() {
                if (languageDataService.resetLocalization(template, selectedCountry.getIso())) {
                    ViewUtils.showNotification("Localization cleared");
                    reloadGrid();
                }
                else {
                    ViewUtils.showNotificationError("Can't clear localization");
                }
            }

        });
    }


    private void resetPageDialog() {

        if (pagesCombo.getValue() == null) {
            return;
        }

        StringBuilder sb = new StringBuilder();
        sb.append("Confirm to reset page. ");
        sb.append("This will remove all items in page '");
        sb.append(pagesCombo.getValue());
        sb.append("' for localization '");
        sb.append(selectedCountry.getIso());
        sb.append("'.");

        DialogBox dialog = new DialogBox();
        dialog.showConfirm("Reset Page", sb.toString());
        dialog.addListener(new DialogBoxAdapter() {
            @Override
            public void dialogConfirm() {
                if (languageDataService.resetLocalizationByPage(template, selectedCountry.getIso(), pagesCombo.getValue())) {
                    ViewUtils.showNotification("Page cleared");
                    reloadGrid();
                }
                else {
                    ViewUtils.showNotificationError("Can't clear page");
                }
            }

        });
    }
}
