package com.wippit.cms.frontend.secure.views.app.users;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.services.UserDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.types.AppIcons;
import com.wippit.cms.backend.types.StorageTypes;
import com.wippit.cms.backend.types.ToolbarTypes;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.components.DialogManager;
import com.wippit.cms.frontend.components.ResourceListener;
import com.wippit.cms.frontend.components.ResourceUploadDialog;
import com.wippit.cms.frontend.secure.views.MainLayout;
import jakarta.annotation.security.PermitAll;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@PageTitle("Profile")
@Route(value = "profile", layout = MainLayout.class)
@PermitAll
@Uses(Icon.class)
public class ProfileView extends OWSecureAdapterView implements ResourceListener {

    @Autowired
    private UserDataService dataService;

    @Autowired
    private ResourceUploadDialog resourceUploadDialog;

    private boolean flagAvatar = true;

    private FormLayout  formLayout;
    private Image       avatarImage;
    private Image       bannerImage;
    private TextField   usernameField;
    private TextField   fullNameField;
    private EmailField  emailField;
    private TextArea    aboutField;
    private Account     currentAccount = null;

    public ProfileView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public boolean setup() {
        resourceUploadDialog.addResourceAddedListener(this);
        currentAccount = getAccount();
        log.debug("Current account: {}", currentAccount);
        setTitle("Profile: " + currentAccount.getAlias());
        return true;
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {

        HorizontalLayout header = ViewUtils.getToolbar(ToolbarTypes.HEADER);

        Button editAvatarButton = ViewUtils.getToolbarButton(AppIcons.AVATAR, "Set avatar");
        Button editBannerButton = ViewUtils.getToolbarButton(AppIcons.BANNER, "Set banner");
        Button passwordButton = ViewUtils.getToolbarButton(AppIcons.PASSWORD, "Change password");
        header.add(
                editAvatarButton,
                editBannerButton,
                passwordButton
        );
        add(header);
        add(ViewUtils.getLineSecondary());

        formLayout = new FormLayout();

        usernameField = new TextField("Account");
        usernameField.setValue(currentAccount.getAlias());
        usernameField.setReadOnly(true);

        fullNameField = new TextField("Full name");
        fullNameField.setValue(currentAccount.getFull_name());
        fullNameField.setRequired(true);
        fullNameField.setWidth("30em");

        emailField = new EmailField("Email");
        emailField.setRequired(true);
        emailField.setValue(currentAccount.getEmail());

        aboutField = new TextArea("About");
        aboutField.setValue(currentAccount.getAbout());
        aboutField.setHeight("6em");

        if (isMobile) {
            formLayout.setWidth("90%");
        }
        else {
            formLayout.setWidth("40em");
        }

        avatarImage = new Image();
        avatarImage.setClassName("image_box_medium");
        if (!StringUtils.isBlank(currentAccount.getAvatar_url())) {
            avatarImage.setSrc(currentAccount.getAvatar_url());
        }
        Details detailsAvatar = new Details("Avatar", avatarImage);
        detailsAvatar.setOpened(true);

        bannerImage = new Image();
        bannerImage.setClassName("image_box_medium");
        if (!StringUtils.isBlank(currentAccount.getBanner_url())) {
            bannerImage.setSrc(currentAccount.getBanner_url());
        }
        Details detailsBanner = new Details("Banner", bannerImage);
        detailsBanner.setOpened(true);

        formLayout.add(
                usernameField,
                fullNameField,
                emailField,
                aboutField,
                detailsAvatar,
                detailsBanner
        );

        formLayout.setColspan(usernameField,2);
        formLayout.setColspan(fullNameField,2);
        formLayout.setColspan(emailField,2);
        formLayout.setColspan(aboutField,2);
        formLayout.setColspan(detailsAvatar,1);
        formLayout.setColspan(detailsBanner,1);

        add(formLayout);

        HorizontalLayout footer = ViewUtils.getToolbar(ToolbarTypes.FOOTER);
        Button saveButton = ViewUtils.getActiveButton("Save");
        footer.add(saveButton);
        add(footer);

        editAvatarButton.addClickListener(e->{
            flagAvatar = true;
            resourceUploadDialog.setAllowedTypes(StorageTypes.IMAGE_AVATAR);
            resourceUploadDialog.setAlias(currentAccount.getAlias());
            resourceUploadDialog.setTitle("Avatar");
            resourceUploadDialog.open();
        });

        editBannerButton.addClickListener(e->{
            flagAvatar = false;
            resourceUploadDialog.setAllowedTypes(StorageTypes.IMAGE_BANNER);
            resourceUploadDialog.setAlias(currentAccount.getAlias());
            resourceUploadDialog.setTitle("Banner");
            resourceUploadDialog.open();
        });

        saveButton.addClickListener(e->{
            log.debug("Save profile");
            saveProfile();
        });

        passwordButton.addClickListener(e->{
            passwordChange(currentAccount);
        });
    }

    @Override
    public void resourceUploaded(Storage storage) {
        if (flagAvatar) {
            avatarImage.setSrc(storage.getCompleteURL());
            currentAccount.setAvatar_url(storage.getCompleteURL());
        }
        else {
            bannerImage.setSrc(storage.getCompleteURL());
            currentAccount.setBanner_url(storage.getCompleteURL());
        }
    }

    @Override
    public void resourceSelected(Storage storage) {}

    private void saveProfile() {
        if (StringUtils.isBlank(fullNameField.getValue())) {
            ViewUtils.showNotificationError("Full name required");
            return;
        }

        if (emailField.isInvalid() || StringUtils.isBlank(emailField.getValue())) {
            ViewUtils.showNotificationError("Email required");
            return;
        }
        String about = aboutField.getValue();
        if (StringUtils.isBlank(about)) {
            about = "";
        }

        currentAccount.setFull_name(fullNameField.getValue());
        currentAccount.setEmail(emailField.getValue());
        currentAccount.setAbout(about);
        dataService.save(currentAccount);
        ViewUtils.showNotification("Profile updated");
    }

    private void passwordChange(Account account) {

        DialogManager dialog = new DialogManager("Change password: " + account.getAlias());

        PasswordField passwordField1 = new PasswordField("New password");
        passwordField1.setRequired(true);
        passwordField1.setPlaceholder("At least 8 characters");
        passwordField1.setMinLength(8);

        PasswordField passwordField2 = new PasswordField("Confirm new password");
        passwordField2.setRequired(true);
        passwordField2.setMinLength(8);

        dialog.addToForm(passwordField1);
        dialog.addToForm(passwordField2);

        Button saveButton = dialog.addMainButton("Change");
        saveButton.addClickListener(e->{

            String secret1 = passwordField1.getValue();
            String secret2 = passwordField2.getValue();

            if (passwordField1.isInvalid() || passwordField2.isInvalid() || StringUtils.isBlank(secret1) || StringUtils.isBlank(secret2)) {
                ViewUtils.showNotificationError("Invalid password");
                return;
            }

            if (!secret1.contentEquals(secret2)) {
                ViewUtils.showNotificationError("Password doesn't match");
                return;
            }

            if (dataService.changeSecret(account.getAlias(), secret1)) {
                ViewUtils.showNotification("Password changed for " + account.getAlias());
                dialog.close();
            }
            else {
                ViewUtils.showNotificationError("Can't change password");
            }

        });
        dialog.open();
    }
}
