package com.wippit.cms.frontend.secure.views.app.users;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.services.UserDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.types.*;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.components.DialogManager;
import com.wippit.cms.frontend.secure.views.MainLayout;
import jakarta.annotation.security.RolesAllowed;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@PageTitle("Accounts")
@Route(value = "accounts", layout = MainLayout.class)
@RolesAllowed("ADMIN")
@Uses(Icon.class)
public class UsersView  extends OWSecureAdapterView {

    @Autowired
    private UserDataService dataService;
    private Grid<Account> grid;

    public UsersView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public boolean setup() {
        dataService.setSelectedRole(RoleTypes.ALL);
        return true;
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {

        ComboBox<RoleTypes> roleCombo = new ComboBox<>();
        roleCombo.setItems(RoleTypes.toFilterList());
        roleCombo.setValue(dataService.getSelectedRole());
        roleCombo.setWidth("30em");

        HorizontalLayout toolbar = ViewUtils.getToolbar(ToolbarTypes.HEADER);

        Button addButton = ViewUtils.getToolbarButton(AppIcons.ADD, "Add account");

        addButton.addClickListener(e->{
            Account account = new Account();
            editAccount(account);
        });

        toolbar.add(
                ViewUtils.getToolbarLabel("Filter By Role"),
                roleCombo,
                ViewUtils.getToolbarLabel("Account"),
                addButton
        );
        add(toolbar);
        add(ViewUtils.getLineSecondary());

        DataProvider<Account, Void> dataProvider;
        dataProvider = DataProvider.fromCallbacks(
                query -> dataService.getPage(query.getPage(), query.getLimit()).stream(),
                query -> dataService.getCount());

        grid = new Grid<>();
        grid.setId("grid");
        grid.addColumn(Account::getAlias).setHeader("Account").setWidth("20%");
        grid.addColumn(Account::getFull_name).setHeader("Name").setWidth("20%");
        grid.addColumn(Account::getEmail).setHeader("Email").setWidth("20%");
        grid.addComponentColumn(item->getStatusIcon(item)).setHeader("Status").setWidth("5%");
        grid.addComponentColumn(item->getRoleIcon(item.isAdmin())).setHeader("Admin").setWidth("5%");
        grid.addComponentColumn(storage -> getEditButton(storage)).setWidth("5%");
        grid.addComponentColumn(storage -> getPasswordButton(storage)).setWidth("5%");
        grid.addComponentColumn(storage -> getDeleteButton(storage)).setWidth("5%");

        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setDataProvider(dataProvider);

        grid.setWidthFull();
        grid.setHeight((size.getHeight()-200)+"px");

        add(grid);


        roleCombo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                dataService.setSelectedRole(RoleTypes.ALL);
            }
            else {
                dataService.setSelectedRole(e.getValue());
            }
            grid.getLazyDataView().refreshAll();
        });

    }

    @Override
    public void resizeUI(Size size) {
        grid.setHeight((size.getHeight()-200)+"px");
    }

    private void editAccount(Account account) {

        boolean flagNew = (account.getId() == 0);

        String title = "New account";
        if (!flagNew) {
            title = "Account: " + account.getAlias();
        }
        DialogManager dialog = new DialogManager(title);

        TextField aliasField = new TextField("Account");
        aliasField.setValue(account.getAlias());
        aliasField.setMinLength(6);
        aliasField.setPlaceholder("Account name (at least 6 characters)");
        if (!flagNew) {
            aliasField.setReadOnly(true);
        }
        else {
            aliasField.setRequired(true);
        }

        TextField nameField = new TextField("Full name");
        nameField.setValue(account.getFull_name());
        nameField.setRequired(true);

        EmailField emailField = new EmailField("Email");
        emailField.setValue(account.getEmail());
        emailField.setRequired(true);

        ComboBox<StatusTypes> statusCombo = new ComboBox<>("Status");
        statusCombo.setItems(StatusTypes.toEditList());
        statusCombo.setValue(StatusTypes.getType(account.getStatus()));

        Checkbox adminCheck = new Checkbox("Admin");
        adminCheck.setValue(account.isAdmin());

        Checkbox editorCheck = new Checkbox("Editor");
        editorCheck.setValue(account.isManager());

        Checkbox authorCheck = new Checkbox("Author");
        authorCheck.setValue(account.isAuthor());

        Checkbox devCheck = new Checkbox("Developer");
        devCheck.setValue(account.isDeveloper());

        dialog.addToForm(aliasField);
        dialog.addToForm(nameField);
        dialog.addToForm(emailField);
        dialog.addToForm(statusCombo);
        dialog.addToForm(adminCheck);
        dialog.addToForm(editorCheck);
        dialog.addToForm(authorCheck);
        dialog.addToForm(devCheck);

        //---
        //Save
        //---
        Button saveButton = dialog.addMainButton("Save");
        saveButton.addClickListener(e->{

            String alias = aliasField.getValue();
            String name = nameField.getValue();
            String email = emailField.getValue();

            if (emailField.isInvalid() || aliasField.isInvalid() || StringUtils.isBlank(alias) || StringUtils.isBlank(email) || StringUtils.isBlank(name)) {
                ViewUtils.showNotificationError("Invalid data");
                return;
            }

            if (flagNew) {
                Account current = dataService.getAccountByAlias(alias);
                if (current != null) {
                    ViewUtils.showNotificationError("Account already exists");
                    return;
                }

                account.setAlias(alias);
            }

            Account toSave = new Account(account);
            toSave.setFull_name(name);
            toSave.setEmail(email);
            toSave.setStatus(statusCombo.getValue().value());
            toSave.setRole(RoleTypes.ADMIN, adminCheck.getValue());
            toSave.setRole(RoleTypes.EDITOR, editorCheck.getValue());
            toSave.setRole(RoleTypes.AUTHOR, authorCheck.getValue());
            toSave.setRole(RoleTypes.DEVELOPER, devCheck.getValue());

            log.warn("To Save Account[{}] \tAdmin={} \tEditor={} \tAuthor={} \tDev={}", toSave.getAlias(), toSave.isAdmin(), toSave.isManager(), toSave.isAuthor(), toSave.isDeveloper());

            if (dataService.save(toSave)) {
                ViewUtils.showNotification("Account saved: " + alias);
                grid.getLazyDataView().refreshAll();
                dialog.close();
            }
            else {
                ViewUtils.showNotificationError("Can't save account");
            }
        });
        dialog.open();

    }

    private void editAccountExtended(Account account) {
        //---
        //TODO: customize your edit dialog
        //create a custom dialog to edit an extended account
        //---
    }

    private void removeAccount(Account account) {

        if (account.getAlias().equalsIgnoreCase("admin")) {
            return;
        }

        DialogManager dialog = new DialogManager("Remove account");
        dialog.addToForm(ViewUtils.getDivLabel("Confirm to remove account: " + account.getAlias()));

        Button buttonRemove = dialog.addDangerButton("Remove");
        buttonRemove.addClickListener(e->{
            if (dataService.remove(account)) {
                ViewUtils.showNotification("Account removed: " + account.getAlias());
                grid.getLazyDataView().refreshAll();
                dialog.close();
            }
            else {
                ViewUtils.showNotificationError("Can't remove account");
            }
        });
        Button buttonCancel = dialog.addMainButton("Cancel");
        buttonCancel.addClickListener(e->dialog.close());

        dialog.open();
    }

    private void passwordChange(Account account) {

        DialogManager dialog = new DialogManager("Change password: " + account.getAlias());

        PasswordField passwordField1 = new PasswordField("New password");
        passwordField1.setRequired(true);
        passwordField1.setPlaceholder("At least 8 characters");
        passwordField1.setMinLength(8);

        PasswordField passwordField2 = new PasswordField("Confirm new password");
        passwordField2.setRequired(true);
        passwordField2.setMinLength(8);

        dialog.addToForm(passwordField1);
        dialog.addToForm(passwordField2);

        Button saveButton = dialog.addMainButton("Change");
        saveButton.addClickListener(e->{

            String secret1 = passwordField1.getValue();
            String secret2 = passwordField2.getValue();

            if (passwordField1.isInvalid() || passwordField2.isInvalid() || StringUtils.isBlank(secret1) || StringUtils.isBlank(secret2)) {
                ViewUtils.showNotificationError("Invalid password");
                return;
            }

            if (!secret1.contentEquals(secret2)) {
                ViewUtils.showNotificationError("Password doesn't match");
                return;
            }

            if (dataService.changeSecret(account.getAlias(), secret1)) {
                ViewUtils.showNotification("Password changed for " + account.getAlias());
                dialog.close();
            }
            else {
                ViewUtils.showNotificationError("Can't change password");
            }

        });
        dialog.open();
    }

    private Button getDeleteButton(Account item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.TRASH,"Delete: " + item.getAlias());
        button.addClickListener(e->removeAccount(item));
        return button;
    }

    private Button getEditButton(Account item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.EDIT,"Edit: " + item.getAlias());
        button.addClickListener(e->{
            if (item.getAccountType()  != AccountTypes.SYSTEM) {
                editAccountExtended(item);
            }
            else  {
                editAccount(item);
            }
        });
        return button;
    }

    private Button getPasswordButton(Account item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.PASSWORD,"Change password: " + item.getAlias());
        button.addClickListener(e->passwordChange(item));
        return button;
    }

    private Icon getStatusIcon(Account item) {
        StatusTypes status = StatusTypes.getType(item.getStatus());
        Icon icon;
        if (status == StatusTypes.ACTIVE) {
            icon = VaadinIcon.CHECK_CIRCLE_O.create();
        }
        else {
            icon = VaadinIcon.CIRCLE_THIN.create();
        }
        return icon;
    }

    private Icon getRoleIcon(boolean hasRole) {
        Icon icon;
        if (hasRole) {
            icon = VaadinIcon.CHECK_CIRCLE_O.create();
        }
        else {
            icon = VaadinIcon.CIRCLE_THIN.create();
        }
        return icon;
    }
}
