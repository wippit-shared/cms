package com.wippit.cms.frontend.secure.views.cms.template;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wippit.cms.backend.data.dao.SystemDao;
import com.wippit.cms.backend.data.services.EmailTemplateDataService;
import com.wippit.cms.backend.data.services.KeyDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.services.email.EmailTemplate;
import com.wippit.cms.backend.types.ApiEmailTypes;
import com.wippit.cms.backend.types.AppIcons;
import com.wippit.cms.backend.types.ToolbarTypes;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.components.DialogManager;
import com.wippit.cms.frontend.secure.views.MainLayout;
import jakarta.annotation.security.RolesAllowed;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@PageTitle("Email Templates")
@Route(value = "cms/emails", layout = MainLayout.class)
@RolesAllowed({"ADMIN", "DEVELOPER"})
@Uses(Icon.class)
public class EmailTemplatesView extends OWSecureAdapterView {

    @Autowired
    private EmailTemplateDataService dataService;

    @Autowired
    private SystemDao systemDao;
    private KeyDataService keyDataService;

    private DataProvider<EmailTemplate, Void> dataProvider;

    private Grid<EmailTemplate> grid;
    private final int MARGIN_H = 170;

    public EmailTemplatesView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public boolean setup() {

        dataProvider = DataProvider.fromCallbacks(
                query -> dataService.getPage(query.getPage(), query.getLimit()).stream(),
                query -> dataService.getCount());

        keyDataService = new KeyDataService(systemDao);
        return true;
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {

        ComboBox<ApiEmailTypes> templateCombo = new ComboBox<>();
        templateCombo.setItems(ApiEmailTypes.toList());
        templateCombo.setValue(ApiEmailTypes.ALL);

        Button buttonAddTemplate = ViewUtils.getToolbarButton(AppIcons.ADD, "Add template");

        HorizontalLayout toolbar = ViewUtils.getToolbar(ToolbarTypes.HEADER);
        toolbar.add(
                ViewUtils.getToolbarLabel("Type"),
                templateCombo,
                ViewUtils.getToolbarLabel("Add"),
                buttonAddTemplate
        );
        add(toolbar);
        add(ViewUtils.getLineSecondary());

        grid = new Grid<>();
        grid.setId("grid");
        grid.addColumn(EmailTemplate::getName).setHeader("Name").setWidth("30%");
        grid.addColumn(item -> ApiEmailTypes.getType(item.getType()).getLabel()).setHeader("Type").setWidth("25%");
        grid.addComponentColumn(this::getDefaultIcon).setHeader("Default").setWidth("5%");
        grid.addColumn(EmailTemplate::getComments).setHeader("Comments").setWidth("30%");
        grid.addComponentColumn(this::getDefaultButton).setWidth("5%");
        grid.addComponentColumn(this::getEditButton).setWidth("5%");
        grid.addComponentColumn(this::getDeleteButton).setWidth("5%");
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setWidthFull();
        grid.setHeight((size.getHeight()  - 200) + "px");
        grid.setDataProvider(dataProvider);
        add(grid);

        templateCombo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                dataService.setType(ApiEmailTypes.ALL);
            }
            else {
                dataService.setType(e.getValue());
            }
            grid.getLazyDataView().refreshAll();
        });

        buttonAddTemplate.addClickListener(e->{
            log.debug("Add template");
            EmailTemplate template = new EmailTemplate();
            template.setName("New template");
            if (templateCombo.getValue() == ApiEmailTypes.ALL) {
                template.setType(ApiEmailTypes.EMAIL_CONFIRM.getType());
            }
            else {
                template.setType(templateCombo.getValue().getType());
            }
            UI.getCurrent().getSession().setAttribute("template", template);
            UI.getCurrent().navigate(EmailTemplateEditView.class);
        });
    }

    @Override
    public void resizeUI(Size size) {
        int h = size.getHeight() - MARGIN_H;
        h = h - 60;
        grid.setHeight(h + "px");
    }

    private Button getEditButton(EmailTemplate item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.EDIT, "Edit template");
        button.addClickListener(e->{
            UI.getCurrent().getSession().setAttribute("template", item);
            UI.getCurrent().navigate(EmailTemplateEditView.class);
        });
        return button;
    }

    private Button getDefaultButton(EmailTemplate item) {
        ApiEmailTypes type = ApiEmailTypes.getType(item.getType());
        String value = keyDataService.getKey(type.key());
        boolean showIcon = !value.equalsIgnoreCase(item.getEtid());

        Button button = ViewUtils.getToolbarButton(AppIcons.OK, "Set as default for " + type.getLabel());
        if (!showIcon) {
            button.setVisible(false);
        }

        button.addClickListener(e->{

            if (StringUtils.isBlank(type.getTemplate())) {
                return;
            }

            boolean result = keyDataService.setKey(type.key(), item.getEtid());
            if (result) {
                grid.getLazyDataView().refreshAll();
                ViewUtils.showNotification("Template " + item.getName() + " set as default for " + type.getLabel());
            }
            else {
                ViewUtils.showNotificationError("Can't set default");
            }

        });
        return button;
    }

    private Icon getDefaultIcon(EmailTemplate item) {
        ApiEmailTypes type = ApiEmailTypes.getType(item.getType());
        String value = keyDataService.getKey(type.key());

        if (value.equalsIgnoreCase(item.getEtid())) {
            return VaadinIcon.CHECK_CIRCLE_O.create();
        }
        else {
            return VaadinIcon.CIRCLE_THIN.create();
        }
    }

    private Button getDeleteButton(EmailTemplate item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.TRASH, "Delete template");
        button.addClickListener(e->{
            DialogManager dialog = new DialogManager("Confirm to Delete");
            dialog.getContainer().add(ViewUtils.getDivLabel("Confirm to delete template: " + item.getName()));
            Button deleteButton = dialog.addDangerButton("Delete");
            Button cancelButton = dialog.addMainButton("Cancel");
            deleteButton.addClickListener(e1->{
                if (dataService.remove(item)) {
                    ViewUtils.showNotification("Template removed");
                    grid.getLazyDataView().refreshAll();
                    dialog.close();
                    return;
                }
                else {
                    ViewUtils.showNotificationError("Can't remove template");
                }
            });
            cancelButton.addClickListener(e2->dialog.close());

            dialog.open();
        });
        return button;
    }


}
