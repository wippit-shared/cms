package com.wippit.cms.frontend.secure.views.app.logs;

import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wippit.cms.backend.data.bean.log.ActivityItem;
import com.wippit.cms.backend.data.services.ActivityDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.types.LogTypes;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.secure.views.MainLayout;
import jakarta.annotation.security.PermitAll;
import org.springframework.beans.factory.annotation.Autowired;

@PageTitle("Activity Logs")
@Route(value = "cms/activitylogs", layout = MainLayout.class)
@PermitAll
@Uses(Icon.class)
public class ActivityLogsView  extends OWSecureAdapterView {

    private Grid<ActivityItem> grid;

    @Autowired
    private ActivityDataService dataService;

    public ActivityLogsView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public boolean setup() {
        dataService.setType(LogTypes.ALL);
        dataService.setAlias("ALL");
        return true;
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {
        DataProvider<ActivityItem, Void> dataProvider;
        dataProvider = DataProvider.fromCallbacks(
                query -> dataService.getPage(query.getPage(), query.getLimit()).stream(),
                query -> (int)dataService.getCount());

        grid = new Grid<>();
        grid.setId("grid");
        grid.addColumn(ActivityItem::getAlias).setHeader("Account").setWidth("15%");
        grid.addColumn(ActivityItem::getDetail).setHeader("Detail").setWidth("60%");
        grid.addColumn(ActivityItem::getCreated_date).setHeader("When").setWidth("25%");
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setDataProvider(dataProvider);
        grid.setWidthFull();
        grid.setHeight((size.getHeight()-200)+"px");
        add(grid);
    }

    @Override
    public void resizeUI(Size size) {
        grid.setHeight((size.getHeight()-200)+"px");
    }
}
