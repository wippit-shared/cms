package com.wippit.cms.frontend.secure.views.cms.resources;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.services.StorageDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.services.storage.StorageService;
import com.wippit.cms.backend.types.AppIcons;
import com.wippit.cms.backend.types.StorageTypes;
import com.wippit.cms.backend.types.ToolbarTypes;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.components.ResourceListener;
import com.wippit.cms.frontend.components.ResourceUploadDialog;
import com.wippit.cms.frontend.secure.views.MainLayout;
import jakarta.annotation.security.RolesAllowed;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

@PageTitle("Resources")
@Route(value = "cms/resources", layout = MainLayout.class)
@RolesAllowed({"AUTHOR", "MANAGER", "EDITOR", "ADMIN", "DEVELOPER"})
public class ResourcesView  extends OWSecureAdapterView implements ResourceListener {

    @Autowired
    private StorageService storageService;

    @Autowired
    private StorageDataService storageDataService;

    @Autowired
    private ResourceUploadDialog resourceUploadDialog;

    private Grid<Storage> grid;

    public ResourcesView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public boolean setup() {
        resourceUploadDialog.setAlias(getAccount().getAlias());
        return true;
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {

        HorizontalLayout toolbar = ViewUtils.getToolbar(ToolbarTypes.HEADER);

        Button uploadResourceButton = ViewUtils.getToolbarButton(AppIcons.CLOUD_UPLOAD, "Upload resource");
        uploadResourceButton.addClickListener(e->uploadResource());

        ComboBox<StorageTypes> storageTypeCombo = new ComboBox<>();
        storageTypeCombo.setItems(StorageTypes.toLinkList());
        storageTypeCombo.setValue(StorageTypes.ALL);
        storageTypeCombo.setWidth("30em");

        storageTypeCombo.addValueChangeListener(e -> {
            if (e.getValue() == null) {
                storageDataService.setType(StorageTypes.ALL);
            } else {
                storageDataService.setType(e.getValue());
            }
            grid.getLazyDataView().refreshAll();
        });

        toolbar.add(
                uploadResourceButton,
                ViewUtils.getToolbarLabel("Filter"),
                storageTypeCombo
        );
        add(toolbar);
        add(ViewUtils.getLinePrimary());

        DataProvider<Storage, Void> dataProvider;
        dataProvider = DataProvider.fromCallbacks(
                query -> storageDataService.getPage(query.getPage(), query.getLimit()).stream(),
                query -> storageDataService.getCount());

        grid = new Grid<>();
        grid.setId("grid");
        //grid.addColumn(Storage::getOriginal_name).setHeader("File").setWidth("50%");
        grid.addComponentColumn(storage -> getDownloadLink(storage)).setHeader("File").setWidth("50%");
        grid.addColumn(storage -> ViewUtils.getFancyFileSize(storage.getByte_size())).setHeader("Size").setWidth("15%");
        grid.addColumn(storage -> getImageProperties(storage)).setWidth("15%");
        grid.addComponentColumn(storage -> getStorageImage(storage)).setWidth("10%");
        grid.addComponentColumn(storage -> deleteButton(storage)).setWidth("10%");

        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setDataProvider(dataProvider);

        grid.setWidthFull();
        grid.setHeight((size.getHeight()-200)+"px");

        add(grid);
    }

    @Override
    public void resizeUI(Size size) {
        grid.setHeight((size.getHeight()-200)+"px");
    }

    private Anchor getDownloadLink(Storage item) {
        return new Anchor(item.getCompleteURL(), item.getOriginal_name(), AnchorTarget.BLANK);
    }

    private Button deleteButton(Storage item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.TRASH, "Delete resource: " + item.getOriginal_name());
        button.addClickListener(e->{
        });
        return button;
    }

    private String getImageProperties(Storage storage) {
        if (StringUtils.isBlank(storage.getProperties())) {
            return "";
        }

        Size size = Size.fromJSON(storage.getProperties());
        if (size == null) {
            return  "";
        }

        return size.toDisplay();
    }

    private Image getStorageImage(Storage item) {

        Image image;
        StorageTypes type = StorageTypes.getType(item.getType());

        if (!type.isImage()) {
            image = type.getImage();
        }
        else {
            image = new Image(item.getCompleteURL(), item.getOriginal_name());
        }

        image.addClassName("image_box");
        image.getStyle().set("width","50px");
        image.getStyle().set("height","50px");
        return image;
    }

    private void uploadResource() {
        resourceUploadDialog.setTitle("Upload resource");
        resourceUploadDialog.setAllowedTypes(StorageTypes.ALL);
        resourceUploadDialog.setSize(getCurrentSize().getSmallerSize(250));
        resourceUploadDialog.removeResourceListener(this);
        resourceUploadDialog.open();
    }

    @Override
    public void resourceUploaded(Storage storage) {
        grid.getLazyDataView().refreshAll();
    }

    @Override
    public void resourceSelected(Storage storage) {

    }
}
