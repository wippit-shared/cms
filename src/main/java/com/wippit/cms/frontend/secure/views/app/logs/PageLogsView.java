package com.wippit.cms.frontend.secure.views.app.logs;

import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wippit.cms.backend.data.bean.stats.HitItem;
import com.wippit.cms.backend.data.services.HitDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.types.CMSHitTypes;
import com.wippit.cms.backend.types.CMSItemTypes;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.secure.views.MainLayout;
import jakarta.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Autowired;

@PageTitle("Page Logs")
@Route(value = "cms/pagelogs", layout = MainLayout.class)
@RolesAllowed({"ADMIN", "MANAGER", "DEVELOPER"})
@Uses(Icon.class)
public class PageLogsView  extends OWSecureAdapterView {

    private Grid<HitItem> grid;
    @Autowired
    private HitDataService dataService;

    @Override
    public boolean setup() {
        dataService.setType(CMSItemTypes.ALL);
        return true;
    }

    public PageLogsView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {
        DataProvider<HitItem, Void> dataProvider;
        dataProvider = DataProvider.fromCallbacks(
                query -> dataService.getPage(query.getPage(), query.getLimit()).stream(),
                query -> (int)dataService.getCount());

        grid = new Grid<>();
        grid.setId("grid");
        grid.addComponentColumn(this::getStatusIcon).setWidth("5%");
        grid.addColumn(HitItem::getUrl).setHeader("URL").setWidth("30%");
        grid.addColumn(item -> item.getCity()+"," + item.getCountry()).setHeader("From").setWidth("20%");
        grid.addColumn(HitItem::getAddress).setHeader("Address").setWidth("15%");
        grid.addColumn(HitItem::getAgent).setHeader("Agent").setWidth("35%");
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setDataProvider(dataProvider);

        grid.setWidthFull();
        grid.setHeight((size.getHeight()-200)+"px");
        add(grid);
    }

    @Override
    public void resizeUI(Size size) {
        grid.setHeight((size.getHeight()-200)+"px");
    }

    private Icon getStatusIcon(HitItem item) {
        CMSHitTypes status = CMSHitTypes.getType(item.getHit());
        Icon icon;
        if (status.isError()) {
            icon = VaadinIcon.CHECK_CIRCLE_O.create();
        }
        else {
            icon = VaadinIcon.CIRCLE_THIN.create();
        }
        return icon;
    }
}
