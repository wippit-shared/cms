package com.wippit.cms.frontend.secure.views.cms.template;

import com.hilerio.ace.AceEditor;
import com.hilerio.ace.AceMode;
import com.hilerio.ace.AceTheme;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileBuffer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import com.vladsch.flexmark.util.misc.FileUtil;
import com.wippit.cms.AppConstants;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.content.HomeIndex;
import com.wippit.cms.backend.data.bean.content.Section;
import com.wippit.cms.backend.data.bean.site.SiteTemplate;
import com.wippit.cms.backend.data.bean.template.TemplateSection;
import com.wippit.cms.backend.data.services.SiteDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.services.ActivityService;
import com.wippit.cms.backend.types.AppIcons;
import com.wippit.cms.backend.types.LogTypes;
import com.wippit.cms.backend.types.SectionTypes;
import com.wippit.cms.backend.types.ToolbarTypes;
import com.wippit.cms.backend.utils.ResourceFile;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.components.DialogManager;
import com.wippit.cms.frontend.components.filetree.FileTree;
import com.wippit.cms.frontend.components.filetree.FileTreeReloadListener;
import com.wippit.cms.frontend.open.controllers.CMSWriter;
import com.wippit.cms.frontend.secure.views.MainLayout;
import jakarta.annotation.security.RolesAllowed;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@PageTitle("Template")
@Route(value = "cms/templates/edit", layout = MainLayout.class)
@RolesAllowed({"ADMIN", "DEVELOPER"})
@Uses(Icon.class)
public class TemplateEditView extends OWSecureAdapterView implements FileTreeReloadListener {

    private VerticalLayout fileLayout;
    private VerticalLayout editorLayout;
    private SplitLayout splitLayout;
    private AceEditor editor;
    private Image currentImage;
    private String currentDirPath;
    private String currentFilePath;
    private FileBuffer fileBuffer;
    private Upload singleFileUpload;
    private TextField fileInfo;
    private TextField fileSizeInfo;
    private SiteTemplate siteTemplate;
    private boolean flagReload = true;
    private Button reloadButton;
    private Button makeDefaultButton;
    private Div fileLabel;
    private Div actionLabel;

    @Autowired
    private SiteDataService siteDataService;

    public TemplateEditView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public boolean setup() {

        siteTemplate = (SiteTemplate) UI.getCurrent().getSession().getAttribute("template");
        if (siteTemplate == null) {
            return false;
        }

        fileBuffer = new FileBuffer();

        setTitle("Template: " + siteTemplate.getName());
        return true;
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {

        reloadButton = ViewUtils.getToolbarButton(AppIcons.RELOAD, "Reload template on site");
        makeDefaultButton = ViewUtils.getToolbarButton(AppIcons.OK, "Make template default");
        Button editButton = ViewUtils.getToolbarButton(AppIcons.EDIT, "Edit template settings");
        Button editIndexButton = ViewUtils.getToolbarButton(AppIcons.HOME, "Edit home pages");
        Button editSectionsButton = ViewUtils.getToolbarButton(AppIcons.HISTORY, "Edit sections pages");
        Button addFileButton = ViewUtils.getToolbarButton(AppIcons.FILE_NEW, "Add file");
        Button addDirButton = ViewUtils.getToolbarButton(AppIcons.FOLDER_NEW, "Add folder");
        Button uploadFileButton = ViewUtils.getToolbarButton(AppIcons.CLOUD_UPLOAD, "Upload file");
        Button deleteFileButton = ViewUtils.getToolbarButton(AppIcons.TRASH, "Remove");
        Button renameButton = ViewUtils.getToolbarButton(AppIcons.RENAME, "Rename");
        Button copyFileButton = ViewUtils.getToolbarButton(AppIcons.FILE_COPY, "Copy file");
        Button saveButton = ViewUtils.getToolbarButton(AppIcons.FILE_SAVE, "Save this file");

        fileInfo = new TextField();
        fileInfo.setReadOnly(true);
        fileInfo.setWidth("30em");

        fileSizeInfo = new TextField();
        fileSizeInfo.setReadOnly(true);
        fileSizeInfo.setWidth("10em");

        HorizontalLayout toolbar = ViewUtils.getToolbar(ToolbarTypes.HEADER);

        fileLabel = ViewUtils.getToolbarLabel("File");
        actionLabel = ViewUtils.getToolbarLabel("Actions");
        toolbar.add(
                ViewUtils.getToolbarLabel("Template actions"),
                editButton,
                editIndexButton,
                editSectionsButton,
                reloadButton,
                makeDefaultButton,
                addFileButton,
                addDirButton,
                uploadFileButton,
                fileLabel,
                fileInfo,
                fileSizeInfo,
                actionLabel,
                saveButton,
                renameButton,
                copyFileButton,
                deleteFileButton
                );
        add(toolbar);
        add(ViewUtils.getLineSecondary());

        fileLayout = getFileLayout(size);
        editorLayout = getEditorLayout(size);

        splitLayout = new SplitLayout();
        splitLayout.setWidthFull();
        splitLayout.setHeight((size.getHeight() - 200) + "px");

        splitLayout.addToPrimary(fileLayout);
        splitLayout.addToSecondary(editorLayout);
        splitLayout.setSplitterPosition(30);
        add(splitLayout);

        currentDirPath = AppGlobal.getInstance().getTemplatePath() + "/" + siteTemplate.getName();
        currentFilePath = "";

        actionLoadTemplateFileTree();

        addFileButton.addClickListener(e->addFile());
        addDirButton.addClickListener(e->addDir());
        uploadFileButton.addClickListener(e->uploadFile());
        deleteFileButton.addClickListener(e->removeFile());
        reloadButton.addClickListener(e->reloadTemplateOnSite());
        saveButton.addClickListener(e->saveFile());
        makeDefaultButton.addClickListener(e->makeDefaultDialog());
        editButton.addClickListener(e->editTemplateDialog());
        editIndexButton.addClickListener(e->editTemplateHomeDialog());
        editSectionsButton.addClickListener(e->editTemplateSectionsDialog());
        copyFileButton.addClickListener(e-> fileCopyDialog());
        renameButton.addClickListener(e-> fileRenameDialog());
        flagReload = siteTemplate.getName().equalsIgnoreCase(AppGlobal.getInstance().getSiteInfo().getTemplate());
        reloadButton.setVisible(flagReload);
        makeDefaultButton.setVisible(!flagReload);
    }

    @Override
    public void resizeUI(Size size) {
        splitLayout.setHeight((size.getHeight() - 200) + "px");
    }

    private VerticalLayout getFileLayout(Size size) {

        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();

        return layout;
    }

    private VerticalLayout getEditorLayout(Size size) {

        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();

        currentImage = new Image();
        currentImage.getStyle().set("width","auto");
        currentImage.getStyle().set("height","auto");
        currentImage.getStyle().set("max-width","100%");
        currentImage.getStyle().set("max-height","100%");
        currentImage.getStyle().set("display","block");

        layout.add(
                getEditorHTML(size),
                currentImage
        );

//        editor.setVisible(false);
        currentImage.setVisible(false);

        return layout;
    }

    private void actionLoadTemplateFileTree() {

        currentDirPath = "";
        currentFilePath = "";
        fileInfo.setValue("");
        fileSizeInfo.setValue("");

        editor.clear();
        currentImage.setVisible(false);

        fileLayout.removeAll();
        String path = AppGlobal.getInstance().getTemplatePath() + AppConstants.PATH_SEPARATOR + siteTemplate.getName();
        FileTree fileTree = new FileTree(path, true, this::selectedFile, this);
        fileTree.setSizeFull();
        fileLayout.add(fileTree);
    }

    private void selectedFile(File file) {
        log.debug("File selected: {}", file.getName());

        currentFilePath = "";

        if (file.isDirectory()) {
            currentDirPath = file.getAbsolutePath();
            log.debug("Current dir: {}", currentDirPath);
            currentImage.setVisible(false);
            editor.setVisible(false);

            String serverPath = currentDirPath;
            fileInfo.setValue(serverPath.replace(AppGlobal.getInstance().getTemplatePath()+"/"+siteTemplate.getName(), ""));

            //int size = FileUtils.listFilesAndDirs(Paths.get(currentDirPath).toFile(), FileFileFilter.FILE, null).size();
            //long size = FileUtils.sizeOfDirectory(Paths.get(currentDirPath).toFile());
            long fileCount  = 0;
            long folderCount = 0;
            try {
                fileCount = Files.list(Paths.get(currentDirPath))
                        .filter(p -> (p.toFile().isFile()))
                        .count();

                folderCount = Files.list(Paths.get(currentDirPath))
                        .filter(p -> (p.toFile().isDirectory()))
                        .count();

            }
            catch (Exception ex) {
                log.error("Getting dir size: {}", ex.toString());
            }
            fileSizeInfo.setValue(fileCount + " files / "+ folderCount + " folders");

            fileLabel.getElement().setProperty("innerHTML", "Directory");
            actionLabel.getElement().setProperty("innerHTML", "Actions");
            return;
        }

        fileLabel.getElement().setProperty("innerHTML", "File");
        actionLabel.getElement().setProperty("innerHTML", "Actions");

        currentFilePath = file.getAbsolutePath();

        String serverPath = currentFilePath;
        fileInfo.setValue(serverPath.replace(AppGlobal.getInstance().getTemplatePath()+"/"+siteTemplate.getName(), ""));
        long size = FileUtils.sizeOf(Paths.get(currentFilePath).toFile());
        fileSizeInfo.setValue(ViewUtils.getFancyFileSize(size));

        String ext = FileUtil.getDotExtension(file);

        if (ext.equalsIgnoreCase(".html")
                || ext.equalsIgnoreCase(".ftl")
                || ext.equalsIgnoreCase(".css")
                || ext.equalsIgnoreCase(".htm")
                || ext.equalsIgnoreCase(".json")
                || ext.equalsIgnoreCase(".txt")
                || ext.equalsIgnoreCase(".js")
        ) {
            if (ext.equalsIgnoreCase(".html")) {
                editor.setMode(AceMode.html);
            } else if (ext.equalsIgnoreCase(".ftl")) {
                editor.setMode(AceMode.ftl);
            } else if (ext.equalsIgnoreCase(".css")) {
                editor.setMode(AceMode.css);
            } else if (ext.equalsIgnoreCase(".json")) {
                editor.setMode(AceMode.json);
            } else if (ext.equalsIgnoreCase(".txt")) {
                editor.setMode(AceMode.text);
            } else if (ext.equalsIgnoreCase(".js")) {
                editor.setMode(AceMode.javascript);
            } else {
                editor.setMode(AceMode.text);
            }

            String content = FileUtil.getFileContent(file);

            currentImage.setVisible(false);
            editor.setVisible(true);
            editor.setValue(content);
        }
        else if (ext.equalsIgnoreCase(".png")
                || ext.equalsIgnoreCase(".jpg")
                || ext.equalsIgnoreCase(".jpeg")
                || ext.equalsIgnoreCase(".webp")
                || ext.equalsIgnoreCase(".bmp")
        ){
            StreamResource resource = new StreamResource(file.getName(),
                    () -> new ByteArrayInputStream(FileUtil.getFileContentBytes(file)));

            editor.setVisible(false);

            currentImage.setAlt(file.getName());
            currentImage.setSrc(resource);
            currentImage.setVisible(true);
        }
        else if (ext.equalsIgnoreCase(".svg")){
            String path = file.getAbsolutePath().replace(AppGlobal.getInstance().getTemplatePath() + "/" + siteTemplate.getName(), "");
            editor.setVisible(false);
            currentImage.setAlt(file.getName());
            currentImage.setVisible(true);
            currentImage.setSrc(path);
        }
        else {
            currentImage.setVisible(false);
            editor.setVisible(false);
        }
    }

    private AceEditor getEditorHTML(Size size) {

        editor = new AceEditor();
        editor.setBasePath("/static/editor");
        editor.setTheme(AceTheme.xcode);
        editor.setMode(AceMode.ftl);
        editor.setFontSize(15);
        editor.setReadOnly(false);
        editor.setShowInvisibles(true);
        editor.setShowGutter(true);
        editor.setShowPrintMargin(true);
        editor.setDisplayIndentGuides(false);
        editor.setUseWorker(false);

        editor.addValueChangeListener(e -> {
            log.debug("Editor changed...");
        });

        editor.setSofttabs(false);
        editor.setTabSize(4);
        editor.setWrap(false);
        editor.setWidthFull();
        editor.setHeightFull();

        editor.setSizeFull();
        return editor;
    }

    private void addFile() {

        if (StringUtils.isBlank(currentDirPath)) {
            return;
        }

        int index = 1;
        boolean flagContinue = true;
        String fileName = "NewPage-" + index + ".html";
        Path newFilePath = Paths.get(currentDirPath + "/" + fileName);
        while (flagContinue) {
            fileName = "NewPage-" + index + ".html";
            newFilePath = Paths.get(currentDirPath + "/" + fileName);
            flagContinue = newFilePath.toFile().exists();
            index++;
        }

        ResourceFile resourceFile = new ResourceFile("defaults/page.html");
        try {
            FileUtils.writeStringToFile(newFilePath.toFile(), resourceFile.getContentAsString(), StandardCharsets.UTF_8);
            actionLoadTemplateFileTree();
            ViewUtils.showNotification(fileName + " created");
        }
        catch (Exception ex) {
            log.error("Can't create new file");
            ViewUtils.showNotificationError("Can't create new file");
        }
    }

    private void addDir() {

        if (StringUtils.isBlank(currentDirPath)) {
            return;
        }

        int index = 1;
        boolean flagContinue = true;
        String fileName = "NewFolder-" + index;
        Path newFilePath = Paths.get(currentDirPath + "/" + fileName);
        while (flagContinue) {
            fileName = "NewFolder-" + index;
            newFilePath = Paths.get(currentDirPath + "/" + fileName);
            flagContinue = newFilePath.toFile().exists();
            index++;
        }

        File newDir = newFilePath.toFile();
        if (newDir.mkdirs()) {
            ViewUtils.showNotification("Folder created: " + fileName);
            actionLoadTemplateFileTree();
        }
        else {
            ViewUtils.showNotificationError("Can't create folder");
        }
    }

    private void removeFile() {

        if (StringUtils.isBlank(currentFilePath) && StringUtils.isBlank(currentDirPath)) {
            return;
        }

        String nameToDelete = "";
        boolean flagFile;
        //---
        //Remove folder
        //---
        Path toDeletePath;
        if (StringUtils.isBlank(currentFilePath)) {
            flagFile = false;

            if (currentDirPath.equalsIgnoreCase(AppGlobal.getInstance().getTemplatePath() + "/" + siteTemplate.getName())) {
                log.debug("Can't delete template dir");
                return;
            }

            toDeletePath = Paths.get(currentDirPath);
        }
        //---
        //Remove file
        //---
        else {
            flagFile = true;
            toDeletePath = Paths.get(currentFilePath);
        }
        nameToDelete = toDeletePath.toFile().getName();

        log.debug("To delete {}: {}", flagFile?"file":"dir", nameToDelete);

        DialogManager dialog = new DialogManager("Confirm");
        String message = "Confirm to delete ";
        String display = "File deleted";
        if (flagFile) {
            message = message+"file";
        }
        else {
            message = message+"folder";
            display = "Folder deleted";
        }
        message = message + ": " + nameToDelete;
        display = display + ": " + nameToDelete;

        dialog.addToForm(ViewUtils.getWarningLabel(message));
        Button deleteButton = dialog.addSecondaryButton("Delete");
        Button cancelButton = dialog.addMainButton("Cancel");

        cancelButton.addClickListener(e->dialog.close());
        String finalDisplay = display;
        deleteButton.addClickListener(e->{
            try {
                if (flagFile) {
                    FileUtils.delete(toDeletePath.toFile());
                } else {
                    FileUtils.deleteDirectory(toDeletePath.toFile());
                }
                actionLoadTemplateFileTree();
                dialog.close();
                ViewUtils.showNotification(finalDisplay);
            }
            catch (Exception ex) {
                log.error("Deleting file/dir [{}]: {}", toDeletePath.toAbsolutePath(), ex.toString());
                ViewUtils.showNotificationError("Can't delete");
            }
        });

        dialog.open();
    }

    private void uploadFile() {

        DialogManager dialog = new DialogManager("Upload file");

        singleFileUpload = new Upload(fileBuffer);
        singleFileUpload.setAutoUpload(true);
        singleFileUpload.setMaxFileSize(1024*1024*50);
        singleFileUpload.setWidth("40em");
        dialog.getContainer().add(ViewUtils.getDivLabel("Select a file to upload (max 50Mb)"));
        dialog.getContainer().add(singleFileUpload);

        singleFileUpload.addFileRejectedListener(e->{
            ViewUtils.showNotificationError(e.getErrorMessage());
        });

        Button button = dialog.addMainButton("Add");
        button.addClickListener(e->{

            String destFilePath = currentDirPath + AppConstants.PATH_SEPARATOR + fileBuffer.getFileName();
            log.debug("File path: {}", destFilePath);

            try {
                File destFile = new File(destFilePath);
                FileUtils.writeByteArrayToFile(destFile, fileBuffer.getInputStream().readAllBytes());

                ViewUtils.showNotification("File added: " + fileBuffer.getFileName());
                ActivityService.addLog(getAccount().getAlias(), "File uploaded: " + fileBuffer.getFileName(), LogTypes.EDITION);

                actionLoadTemplateFileTree();

                dialog.close();
            }
            catch (Exception ex) {
                log.error("Moving uploaded file: {}", ex.toString());
                ViewUtils.showNotificationError("Can't move uploaded file");
            }
        });

        button.setEnabled(false);

        singleFileUpload.addSucceededListener(event -> {
            log.debug("Upload success");
            button.setEnabled(true);
        });

        singleFileUpload.addFailedListener(e->{
            log.debug("Upload failed: {}", e.getReason().toString());
        });

        dialog.open();
    }

    private void fileCopyDialog(){

        if (StringUtils.isBlank(currentFilePath)) {
            return;
        }

        log.debug("Current file: {}", currentFilePath);
        File source = new File(currentFilePath);
        Path sourcePath = source.toPath().getParent();
        String baseDir = sourcePath.toString();

        int index = 1;
        boolean flagContinue = true;

        String originalName = "";
        if (source.getName().startsWith("_")) {
            originalName = source.getName().substring(1);
        }
        else {
            originalName = source.getName();
        }
        log.debug("Original name: {}", originalName);


        String newFileName = "copy" + index + "-" + originalName;
        Path newFilePath = Path.of( baseDir + "/" + newFileName);
        while (flagContinue) {
            newFileName = "copy" + index + "-" + originalName;
            newFilePath = Paths.get(baseDir  + "/" + newFileName);
            flagContinue = newFilePath.toFile().exists();
            log.debug("newFilePath: {}", newFilePath.toString());
            index++;
        }

        DialogManager dialog = new DialogManager("Duplicate file");
        TextField sourceNameField = new TextField("From file");
        sourceNameField.setValue(originalName);
        sourceNameField.setReadOnly(true);
        sourceNameField.setWidth("30em");

        TextField newNameField = new TextField("To file");
        newNameField.setValue(newFileName);
        newNameField.setRequired(true);
        newNameField.setWidth("30em");

        dialog.addToForm(sourceNameField);
        dialog.addToForm(newNameField);

        Button saveButton = dialog.addMainButton("Duplicate");
        saveButton.addClickListener(e->{

            try {
                String newName = newNameField.getValue();
                if (StringUtils.isBlank(newName)) {
                    return;
                }

                Path newFileToSave = Paths.get(baseDir  + "/" + newName);
                FileUtils.copyFile(source, newFileToSave.toFile());
                actionLoadTemplateFileTree();
                ViewUtils.showNotification(source.getName() + " duplicated as: " + newName);
                dialog.close();
            }
            catch (Exception ex) {
                log.error("Can't duplicate file {}: {}", source.getName(), ex.toString());
                ViewUtils.showNotificationError("Can't duplicate file: " + source.getName());
            }

        });
        dialog.open();

    }

    private void fileRenameDialog(){

        if (StringUtils.isBlank(currentFilePath)) {
            return;
        }

        log.debug("Current file: {}", currentFilePath);
        File source = new File(currentFilePath);
        Path sourcePath = source.toPath().getParent();
        String baseDir = sourcePath.toString();

        DialogManager dialog = new DialogManager("Rename file");
        TextField sourceNameField = new TextField("From file");
        sourceNameField.setValue(source.getName());
        sourceNameField.setReadOnly(true);
        sourceNameField.setWidth("30em");

        TextField newNameField = new TextField("To file");
        newNameField.setValue("renamed " + source.getName());
        newNameField.setRequired(true);
        newNameField.setWidth("30em");

        dialog.addToForm(sourceNameField);
        dialog.addToForm(newNameField);

        Button saveButton = dialog.addMainButton("Rename");
        saveButton.addClickListener(e->{

            try {
                String newName = newNameField.getValue();
                if (StringUtils.isBlank(newName)) {
                    return;
                }

                Path newFileToSave = Paths.get(baseDir  + "/" + newName);
                File file2 = newFileToSave.toFile();
                if (file2.exists()) {
                    ViewUtils.showNotificationError("File already exists");
                    return;
                }

                boolean result = source.renameTo(file2);
                if (result) {
                    actionLoadTemplateFileTree();
                    ViewUtils.showNotification(source.getName() + " renamed as: " + newName);
                    dialog.close();
                }
                else {
                    ViewUtils.showNotificationError("Can't rename file");
                }
            }
            catch (Exception ex) {
                log.error("Can't rename file {}: {}", source.getName(), ex.toString());
                ViewUtils.showNotificationError("Can't rename file: " + source.getName());
            }

        });
        dialog.open();

    }

    @Override
    public void reloadRequired() {
        log.debug("Reloading file tree");
        actionLoadTemplateFileTree();
    }

    private void reloadTemplateOnSite() {
        log.debug("Reloading default template");
        siteDataService.reloadDefaultTemplate();
        ViewUtils.showNotification("Template " + siteTemplate.getName() + " reloaded with " + CMSWriter.getCurrent().itemCount() +  " files/folders on site");
    }

    public void saveFile() {
        if (StringUtils.isBlank(currentFilePath)) {
            return;
        }

        if (!editor.isVisible()) {
            return;
        }

        try {
            String content = editor.getValue();
            FileUtils.writeStringToFile(new File(currentFilePath), content, StandardCharsets.UTF_8);

            String file = currentFilePath;
            String path = file.replace(AppGlobal.getInstance().getTemplatePath() + "/" + siteTemplate.getName(), "");
            ViewUtils.showNotification("File saved: " + path);
        }
        catch (Exception ex) {
            log.error("Saving file[{}] error: {}", currentFilePath, ex.toString());
            ViewUtils.showNotificationError("Can't save file");
        }

    }

    public void makeDefaultDialog() {
        DialogManager dialog = new DialogManager("Make template default");
        dialog.getContainer().add(ViewUtils.getDivLabel("Confirm to make template " + siteTemplate.getName() + " default for site."));
        Button confirmButton = dialog.addMainButton("Make default");
        confirmButton.addClickListener(e->{
            if (siteDataService.makeTemplateDefault(siteTemplate)) {
                ViewUtils.showNotification("Template " + siteTemplate.getName() + " is default on site");
                flagReload = siteTemplate.getName().equalsIgnoreCase(AppGlobal.getInstance().getSiteInfo().getTemplate());
                reloadButton.setVisible(flagReload);
                makeDefaultButton.setVisible(!flagReload);
                dialog.close();
            }
            else {
                ViewUtils.showNotificationError("Can't set template as default");
            }
        });
        dialog.open();
    }

    private void editTemplateDialog() {
        DialogManager dialog = new DialogManager("Template settings: " + siteTemplate.getName());

        List<String> files = getFiles();

        TextField nameField = new TextField("Name");
        nameField.setValue(siteTemplate.getName());
        nameField.setReadOnly(true);

        ComboBox<String> rootCombo = new ComboBox<>("Index page");
        rootCombo.setItems(files);
        rootCombo.setValue(siteTemplate.getPage_root());
        rootCombo.setRequired(true);

        ComboBox<String> blogCombo = new ComboBox<>("Blog page");
        blogCombo.setItems(files);
        blogCombo.setValue(siteTemplate.getPage_blog());

        ComboBox<String> blogDetailCombo = new ComboBox<>("Blog item page");
        blogDetailCombo.setItems(files);
        blogDetailCombo.setValue(siteTemplate.getPage_item());

        ComboBox<String> error404Combo = new ComboBox<>("404 page");
        error404Combo.setItems(files);
        error404Combo.setValue(siteTemplate.getPage_404());

        ComboBox<String> error500Combo = new ComboBox<>("500 page");
        error500Combo.setItems(files);
        error500Combo.setValue(siteTemplate.getPage_500());

        ComboBox<String> searchCombo = new ComboBox<>("Search page");
        searchCombo.setItems(files);
        searchCombo.setValue(siteTemplate.getPage_search());

        ComboBox<String> authorCombo = new ComboBox<>("Author page");
        authorCombo.setItems(files);
        authorCombo.setValue(siteTemplate.getPage_author());

        dialog.addToForm(nameField);
        dialog.addToForm(rootCombo);
        dialog.addToForm(blogCombo);
        dialog.addToForm(blogDetailCombo);
        dialog.addToForm(searchCombo);
        dialog.addToForm(authorCombo);
        dialog.addToForm(error404Combo);
        dialog.addToForm(error500Combo);

        Button saveTemplateButton = dialog.addMainButton("Save");
        saveTemplateButton.addClickListener(e->{
            String root = rootCombo.getValue();
            if (StringUtils.isBlank(root)) {
                ViewUtils.showNotificationError("Root page required");
                return;
            }

            String blogPage = AppUtils.validData(blogCombo.getValue());
            String itemPage = AppUtils.validData(blogDetailCombo.getValue());
            String searchPage = AppUtils.validData(searchCombo.getValue());
            String authorPage = AppUtils.validData(authorCombo.getValue());
            String error404 = AppUtils.validData(error404Combo.getValue());
            String error500 = AppUtils.validData(error500Combo.getValue());

            siteTemplate.setPage_root(root);
            siteTemplate.setPage_blog(blogPage);
            siteTemplate.setPage_item(itemPage);
            siteTemplate.setPage_search(searchPage);
            siteTemplate.setPage_author(authorPage);
            siteTemplate.setPage_404(error404);
            siteTemplate.setPage_500(error500);

            if (siteDataService.save(siteTemplate)){
                if (flagReload) {
                    ViewUtils.showNotificationWarning("Template settings updated. Reload is required to apply changes.");
                }
                else {
                    ViewUtils.showNotification("Template settings updated");
                }
                dialog.close();
            }
            else {
                ViewUtils.showNotificationError("Can't save template settings");
            }
        });

        dialog.open();
    }

    private void editTemplateHomeDialog() {

        DialogManager dialog = new DialogManager("Home Pages");

        List<HomeIndex> templateHomeList = siteDataService.getHomeTemplate(siteTemplate.getName());
        List<String> files = getFiles();

        Grid<HomeIndex> gridSections = new Grid<>(HomeIndex.class, false);

        gridSections.addColumn(HomeIndex::getIso).setHeader("Localization").setWidth("30%");
        gridSections.addComponentColumn(item->getHomeEditor(item, files)).setHeader("Home page").setWidth("30%");
        gridSections.setItems(templateHomeList);
        gridSections.setSelectionMode(Grid.SelectionMode.SINGLE);
        gridSections.addThemeVariants(GridVariant.LUMO_NO_BORDER);

        if (isMobile()) {
            gridSections.setWidth("90%");
            gridSections.setHeight("90%");
        }
        else {
            gridSections.setWidth(getCurrentSize().getInnerSize(300).getWidth() + "px");
            gridSections.setHeight(getCurrentSize().getInnerSize(400).getHeight() + "px");
        }

        dialog.getContainer().add(gridSections);

        Button saveButton = dialog.addMainButton("Save");
        saveButton.addClickListener(e->{

            for (HomeIndex item:templateHomeList) {
                siteDataService.save(item);
            }

            ViewUtils.showNotificationWarning("Template home pages updated. Reload is required to apply changes.");
            dialog.close();
        });

        dialog.open();
    }

    private void editTemplateSectionsDialog() {

        DialogManager dialog = new DialogManager("Content sections");

        final List<TemplateSection> templateSectionList = new ArrayList<>();
        final List<Section> sections = new ArrayList<>();
        List<Section> allSections = siteDataService.getAllDistinctSections();

        Section selectedSection = null;

        for (Section section : allSections) {
            log.debug("Analizing section: {} = {}", section.getSecID(), SectionTypes.getType(section.getType()).getLabel());

            if (section.getType() != SectionTypes.NOTES.getType()) {
                sections.add(section);
                log.debug("Set selected section: {}", section.getSecID());
                selectedSection = section;
            }
        }

        final ComboBox<Section> sectionCombo = new ComboBox<>("Section");
        sectionCombo.setItems(sections);

        if (selectedSection != null) {
            log.debug("Loading content sections: {}", selectedSection.getSecID());

            sectionCombo.setValue(selectedSection);

            List<TemplateSection> list = siteDataService.getTemplateSectionsForTemplate(siteTemplate.getName(), selectedSection);
            if (list != null) {
                templateSectionList.addAll(list);
            }
        }

        dialog.getContainer().add(sectionCombo);

        final List<String> files = getFiles();

        Grid<TemplateSection> gridSections = new Grid<>(TemplateSection.class, false);
//        gridSections.addColumn(TemplateSection::getSecID).setHeader("Section").setWidth("20%");
        gridSections.addColumn(TemplateSection::getIso).setHeader("Localization").setWidth("20%");
        gridSections.addComponentColumn(item->getBlogEditor(item, files)).setHeader("List page").setWidth("30%");
        gridSections.addComponentColumn(item->getItemEditor(item, files)).setHeader("Detail page").setWidth("30%");
        gridSections.setItems(templateSectionList);
        gridSections.setSelectionMode(Grid.SelectionMode.SINGLE);
        gridSections.addThemeVariants(GridVariant.LUMO_NO_BORDER);

        if (isMobile()) {
            gridSections.setWidth("90%");
            gridSections.setHeight("90%");
        }
        else {
            gridSections.setWidth(getCurrentSize().getInnerSize(300).getWidth() + "px");
            gridSections.setHeight(getCurrentSize().getInnerSize(400).getHeight() + "px");
        }


        dialog.getContainer().add(gridSections);

        sectionCombo.addValueChangeListener(e->{
            log.debug("Selecting section");
            if (e == null) {
                return;
            }
            log.debug("Selecting section: {}", e.getValue());

            List<TemplateSection> list = siteDataService.getTemplateSectionsForTemplate(siteTemplate.getName(), e.getValue());
            templateSectionList.clear();
            if (list != null) {
                templateSectionList.addAll(list);
            }
            gridSections.setItems(templateSectionList);
            gridSections.getListDataView().refreshAll();
        });


        Button saveButton = dialog.addMainButton("Save");
        saveButton.addClickListener(e->{

            for (TemplateSection section:templateSectionList) {
                siteDataService.saveTemplateSection(
                        section.getName(),
                        section.getSecID(),
                        SectionTypes.HOME,
                        section.getIso(),
                        section.getSection_page(),
                        section.getItem_page()
                        );
            }

            ViewUtils.showNotificationWarning("Template sections updated. Reload is required to apply changes.");
        });

        dialog.open();
    }

    private List<String> getFiles() {
        List<String> fileList = new ArrayList<>();
        String templatePath = AppGlobal.getInstance().getTemplatePath() + "/" + siteTemplate.getName();

        log.debug("Getting files from path: {}", templatePath);

        try {
            Files.walkFileTree(Paths.get(templatePath), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                    if (!Files.isDirectory(file)) {
                        String absPath = file.toAbsolutePath().toString();
                        if (absPath.endsWith(".ftl")
                                || absPath.endsWith(".htm")
                                || absPath.endsWith(".html")
                        ) {
                            String filePath = absPath.replace(templatePath, "");
                            fileList.add(filePath);
                            //log.debug("Adding file: {}", filePath);
                        }
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        }
        catch (Exception ex) {
            log.error("Can't get files in template: {}", ex.toString());
        }

        log.debug("Adding {} files to list", fileList.size());
        return fileList;
    }

    private ComboBox<String> getBlogEditor(TemplateSection templateSection, List<String> pages) {
        ComboBox<String> combo = new ComboBox<>();
        combo.setItems(pages);
        combo.setWidth("20em");
        combo.setValue(templateSection.getSection_page());
        combo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                templateSection.setSection_page("");
            }
            else {
                templateSection.setSection_page(e.getValue());
            }
        });
        return combo;
    }

    private ComboBox<String> getHomeEditor(HomeIndex homeIndex, List<String> pages) {
        ComboBox<String> combo = new ComboBox<>();
        combo.setItems(pages);
        combo.setWidth("20em");
        combo.setValue(homeIndex.getPage());
        combo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                homeIndex.setPage("");
            }
            else {
                homeIndex.setPage(e.getValue());
            }
        });
        return combo;
    }

    private ComboBox<String> getItemEditor(TemplateSection templateSection, List<String> pages) {
        ComboBox<String> combo = new ComboBox<>();
        combo.setItems(pages);
        combo.setWidth("20em");
        combo.setValue(templateSection.getItem_page());
        combo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                templateSection.setItem_page("");
            }
            else {
                templateSection.setItem_page(e.getValue());
            }
        });
        return combo;
    }
}

