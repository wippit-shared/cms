package com.wippit.cms.frontend.secure.views.cms.dashboard;

import com.storedobject.chart.*;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.wippit.cms.AppConstants;
import com.wippit.cms.backend.data.bean.stats.VisitorsByCity;
import com.wippit.cms.backend.data.bean.stats.VisitorsByCountry;
import com.wippit.cms.backend.data.services.StatsDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.types.PasTimeTypes;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.secure.views.MainLayout;
import jakarta.annotation.security.PermitAll;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;


@Slf4j
@PageTitle("Dashboard")
@Route(value = "cms/dashboard", layout = MainLayout.class)
@RouteAlias(value = "", layout = MainLayout.class)
@PermitAll
@Uses(Icon.class)
public class DashboardView  extends OWSecureAdapterView {

    @Autowired
    private StatsDataService dataService;

    @Value("${spring.profiles.active}")
    private String activeProfile;

    private int chartWidth;
    private int chartHeight;

    private SplitLayout splitLayout;

    public DashboardView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public void buildUI(com.wippit.cms.backend.utils.Size size, boolean isMobile) {
        doDashboardCMS();
    }

    @Override
    public void resizeUI(com.wippit.cms.backend.utils.Size size) {

    }

    private void doDashboardCMS() {
        com.wippit.cms.backend.utils.Size size = getCurrentSize();

        if (isMobile()) {
            chartWidth = size.getWidth()-20;
            chartHeight = size.getHeight()/2-50;
        }
        else {
            chartWidth = size.getWidth()/2-40;
            chartHeight = size.getHeight()/2-100;
        }

        log.debug("Chart size: {} x {}", chartHeight, chartWidth);

        add(getCharts(PasTimeTypes.YESTERDAY));
        add(getCharts(PasTimeTypes.WEEK));
        add(getCharts(PasTimeTypes.MONTH));
        add(getTopCharts());
    }

    private HorizontalLayout getTopCharts() {

        HorizontalLayout layout = new HorizontalLayout();
        layout.setWidthFull();
        if (isMobile()) {
            layout.setHeight((2*chartHeight + 50) + "px");
        }
        else {
            layout.setHeight((chartHeight + 50) + "px");
        }

        layout.add(getChartByCity(PasTimeTypes.BEGINNING));
        layout.add(getChartByCountry(PasTimeTypes.BEGINNING));

        return layout;
    }

    private HorizontalLayout getCharts(PasTimeTypes pasTimeType) {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setWidthFull();
        if (isMobile()) {
            layout.setHeight((2*chartHeight + 50) + "px");
        }
        else {
            layout.setHeight((chartHeight + 50) + "px");
        }

        layout.add(getChartByCity(pasTimeType));
        layout.add(getChartByCountry(pasTimeType));

        return layout;
    }

    private SOChart getChartByCity(PasTimeTypes pasTimeType) {

        // Creating a chart display area
        SOChart soChart = new SOChart();
        soChart.setSize(chartWidth + "px", chartHeight +"px");

        // Just to demonstrate it, we are creating a "Download" and a "Zoom" toolbox button
        Toolbox toolbox = new Toolbox();
        toolbox.addButton(new Toolbox.Download(), new Toolbox.Zoom());

        // Switching off the default legend
        soChart.disableDefaultLegend();

        // Title & subtitle
        String titleValue = "Pages By City";
        if (pasTimeType != PasTimeTypes.BEGINNING) {
            titleValue = titleValue + " - Last " + pasTimeType.getDays() + " days";
        }
        Title title = new Title(titleValue);
        title.setSubtext("Top 15");
        Position posCenter = new Position();
        posCenter.justifyCenter();
        title.setPosition(posCenter);
        title.getTextStyle(true).setColor(new Color(AppConstants.COLOR_RED));
        title.getSubtextStyle(true).setColor(new Color(AppConstants.COLOR_RED));


        List<VisitorsByCity> visitorsByCity;
        if (activeProfile.equalsIgnoreCase("local")) {
            visitorsByCity = dataService.getVisitorsByCityDemo();
        }
        else {
            if (pasTimeType == PasTimeTypes.BEGINNING) {
                visitorsByCity = dataService.getVisitorsByCity();
            }
            else {
                visitorsByCity = dataService.getVisitorsByCityByDays(pasTimeType.getDays());
            }
        }

        CategoryData labels = new CategoryData();
        Data data = new Data();
        for (VisitorsByCity visitor: visitorsByCity) {
            data.add(visitor.getCount());
            labels.add(visitor.getCity() + ", " + visitor.getCountryName());
        }

        // Axes
        XAxis xAxis = new XAxis(data);
        YAxis yAxis = new YAxis(labels);

        BarChart barChart = new BarChart(data, labels); // First bar chart
        RectangularCoordinate coordinate = new RectangularCoordinate(xAxis, yAxis);
        barChart.plotOn(coordinate); // Bar chart needs to be plotted on a coordinate system
        barChart.getLabel(true).setColor(new Color(AppConstants.COLOR_WHITE));
        coordinate.getPosition(true).setLeft(Size.pixels(Math.round((float)chartWidth/(float)5))); // Leave space on the left

        soChart.add(barChart, toolbox, title);
        return soChart;
    }

    private SOChart getChartByCountry(PasTimeTypes pasTimeType){

        // Creating a chart display area
        SOChart soChart = new SOChart();
        soChart.setSize(chartWidth + "px", chartHeight +"px");

        Toolbox toolbox = new Toolbox();
        toolbox.addButton(new Toolbox.Download(), new Toolbox.Zoom());

        // Switching off the default legend
        soChart.disableDefaultLegend();

        //Title & subtitle
        String titleValue = "Pages By Country";
        if (pasTimeType != PasTimeTypes.BEGINNING) {
            titleValue = titleValue + " - Last " + pasTimeType.getDays() + " days";
        }
        Title title = new Title(titleValue);
        title.setSubtext("Top 15");
        Position posCenter = new Position();
        posCenter.justifyCenter();
        title.setPosition(posCenter);
        title.getTextStyle(true).setColor(new Color(AppConstants.COLOR_RED));
        title.getSubtextStyle(true).setColor(new Color(AppConstants.COLOR_RED));

        List<VisitorsByCountry> visitorsByCountry;
        if (activeProfile.equalsIgnoreCase("local")) {
            visitorsByCountry = dataService.getVisitorsByCountryDemo();
        }
        else {
            if (pasTimeType == PasTimeTypes.BEGINNING) {
                visitorsByCountry = dataService.getVisitorsByCountry();
            }
            else {
                visitorsByCountry = dataService.getVisitorsByCountryByDays(pasTimeType.getDays());
            }
        }

        CategoryData labels = new CategoryData();
        Data data = new Data();
        for (VisitorsByCountry visitor: visitorsByCountry) {
            data.add(visitor.getCount());
            labels.add(visitor.getCountryName());
        }

        XAxis xAxis = new XAxis(data);
        YAxis yAxis = new YAxis(labels);
//        yAxis.getLabel(true).setColor(new Color(AppConstants.COLOR_BLUE));
//        xAxis.getLabel(true).setColor(new Color(AppConstants.COLOR_BLUE));

        BarChart barChart = new BarChart(data, labels); // Second bar chart - axes reversed
        RectangularCoordinate coordinate = new RectangularCoordinate(xAxis, yAxis);
        barChart.plotOn(coordinate);
//        barChart.getItemStyle(true).setColor(new Color(AppConstants.COLOR_BLUE));
        barChart.getLabel(true).setColor(new Color(AppConstants.COLOR_WHITE));

        coordinate.getPosition(true).setLeft(Size.pixels(Math.round((float)chartWidth/(float)5)));

        soChart.add(barChart, toolbox, title);
        return  soChart;
    }

}
