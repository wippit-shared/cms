package com.wippit.cms.frontend.secure.views.cms.articles;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.content.ArticleBrief;
import com.wippit.cms.backend.data.bean.content.Section;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.services.ArticleDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.types.*;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.secure.views.MainLayout;
import jakarta.annotation.security.RolesAllowed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Slf4j
@PageTitle("Articles")
@Route(value = "cms/articles", layout = MainLayout.class)
@RolesAllowed({"AUTHOR", "MANAGER", "EDITOR", "ADMIN"})
@Uses(Icon.class)
public class ArticlesView extends OWSecureAdapterView {

    private DataProvider<ArticleBrief, Void> dataProvider;
    private Grid<ArticleBrief> grid;
    private LocalCountry selectedCountry;
    private boolean flagAuthor = false;

    @Autowired
    private ArticleDataService dataService;

    @Override
    public boolean setup() {

        log.debug("ISO: {}", AppGlobal.getInstance().getSiteInfo().getCountry_iso());

        selectedCountry = (LocalCountry) UI.getCurrent().getSession().getAttribute("country");
        if (selectedCountry == null) {
            List<LocalCountry> countries = dataService.getCountries();
            for (LocalCountry country : countries) {
                if (country.getIso().equalsIgnoreCase(AppGlobal.getInstance().getSiteInfo().getCountry_iso())) {
                    selectedCountry = country;
                    break;
                }
            }
        }

        Account account = getAccount();
        if (account != null) {
            if (account.isAuthor()) {
                dataService.setAuthor(account.getAlias());
                flagAuthor = true;
            }
        }

        dataService.setIso(selectedCountry.getIso());

        dataProvider = DataProvider.fromCallbacks(
                query -> dataService.getPage(query.getPage(), query.getLimit()).stream(),
                query -> dataService.getCount());

        return true;
    }

    public ArticlesView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);

        setSizeFull();
        addClassNames("articles-view");
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {

        ComboBox<LocalCountry> countryCombo = new ComboBox<>();
        countryCombo.setWidth("20em");
        countryCombo.setItems(dataService.getCountries());
        countryCombo.setValue(selectedCountry);
        
        countryCombo.addValueChangeListener(e->{
            selectedCountry = e.getValue();
            UI.getCurrent().getSession().setAttribute("country", selectedCountry);
            dataService.setIso(selectedCountry.getIso());
            grid.getLazyDataView().refreshAll();
        });

        ComboBox<Account> authorCombo = new ComboBox<>();
        authorCombo.setItems(dataService.getAuthors());
        authorCombo.setWidth("25em");

        ComboBox<Section> sectionCombo = new ComboBox<>();
        List<Section> sectionList = dataService.getSections();
        Section allSection = new Section();
        allSection.setName("All");
        allSection.setSecID("");
        sectionList.add(allSection);
        sectionCombo.setItems(sectionList);

        ComboBox<HomeTypes> homeCombo = new ComboBox<>();
        homeCombo.setItems(HomeTypes.toFilterList());

        ComboBox<ContentStatusTypes> statusCombo = new ComboBox<>();
        statusCombo.setItems(ContentStatusTypes.toList());

        Button addArticleButton = ViewUtils.getToolbarButton(AppIcons.ADD, "Add article");

        HorizontalLayout toolbar = ViewUtils.getToolbar(ToolbarTypes.HEADER);

        if (flagAuthor) {
            toolbar.add(
                    addArticleButton,
                    ViewUtils.getToolbarLabel("Locale"),
                    countryCombo,
                    ViewUtils.getToolbarLabel("Home"),
                    homeCombo,
                    ViewUtils.getToolbarLabel("Section"),
                    sectionCombo,
                    ViewUtils.getToolbarLabel("Status"),
                    statusCombo
            );
        }
        else {
            toolbar.add(
                    addArticleButton,
                    ViewUtils.getToolbarLabel("Locale"),
                    countryCombo,
                    ViewUtils.getToolbarLabel("Home"),
                    homeCombo,
                    ViewUtils.getToolbarLabel("Author"),
                    authorCombo,
                    ViewUtils.getToolbarLabel("Section"),
                    sectionCombo,
                    ViewUtils.getToolbarLabel("Status"),
                    statusCombo
            );
        }
        add(toolbar);
        add(ViewUtils.getLineSecondary());

        grid = new Grid<>();
        grid.setId("grid");
        grid.addColumn(item -> SectionTypes.getType(item.getSection_type()).getLabel()).setHeader("Type").setWidth("10%");
        grid.addColumn(ArticleBrief::getTitle).setHeader("Title").setWidth("30%");
        grid.addColumn(ArticleBrief::getAuthor).setHeader("Author").setWidth("15%");
        grid.addColumn(ArticleBrief::getSecID).setHeader("Section ID").setWidth("10%");
        grid.addComponentColumn(this::getHomeIcon).setHeader("Home").setWidth("5%");
        grid.addComponentColumn(this::getStatusIcon).setHeader("Published").setWidth("5%");
        grid.addColumn(item -> getStartDate(item)).setHeader("Starts").setWidth("15%");
        grid.addComponentColumn(this::getEditButton).setWidth("5%");
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setWidthFull();
        grid.setHeight((size.getHeight()  - 200) + "px");
        grid.setDataProvider(dataProvider);
        add(grid);

        sectionCombo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                dataService.setSection("");
                grid.getLazyDataView().refreshAll();
                return;
            }

            dataService.setSection(e.getValue().getSecID());
            grid.getLazyDataView().refreshAll();
        });

        authorCombo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                dataService.setAuthor("");
                grid.getLazyDataView().refreshAll();
                return;
            }

            dataService.setAuthor(e.getValue().getAlias());
            grid.getLazyDataView().refreshAll();
        });

        statusCombo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                dataService.setStatus(ContentStatusTypes.ANY);
                grid.getLazyDataView().refreshAll();
                return;
            }

            dataService.setStatus(e.getValue());
            grid.getLazyDataView().refreshAll();
        });

        homeCombo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                dataService.setHome(HomeTypes.ALL);
            }
            else {
                dataService.setHome(e.getValue());
            }

            grid.getLazyDataView().refreshAll();
        });

        addArticleButton.addClickListener(e->{
            UI.getCurrent().getSession().setAttribute("cid", "");
            UI.getCurrent().getSession().setAttribute("country", selectedCountry);
            UI.getCurrent().navigate(ArticleEditView.class);
        });

    }

    private String getStartDate(ArticleBrief item) {

        if (item.getStatus() == StatusTypes.INACTIVE.value()) {
            return "";
        }

        if (AppUtils.timestampNow().after(item.getStart_date())) {
            return "Since " + AppUtils.timestampSiteDateTimeString(item.getStart_date());
        }

        return "After " + AppUtils.timestampSiteDateTimeString(item.getStart_date());
    }

    private Button getEditButton(ArticleBrief item) {
        Button button = ViewUtils.getToolbarButton(VaadinIcon.EDIT.create(), "Edit article");
        button.addClickListener(e->{
            editArticle(item);
        });
        return button;
    }

    private Icon getStatusIcon(ArticleBrief item) {
        log.debug("Item status: {}", item.getStatus());
        StatusTypes status = StatusTypes.getType(item.getStatus());
        Icon icon;
        if (status == StatusTypes.ACTIVE) {
            icon = VaadinIcon.CHECK_CIRCLE_O.create();
        }
        else {
            icon = VaadinIcon.CIRCLE_THIN.create();
        }
        return icon;
    }

    private Icon getHomeIcon(ArticleBrief item) {
        SectionTypes sectionType = SectionTypes.getType(item.getSection_type());
        if (sectionType != SectionTypes.ARTICLES) {
            return VaadinIcon.CIRCLE_THIN.create();
        }

        Icon icon;
        if (item.getHome() > 0) {
            icon = VaadinIcon.CHECK_CIRCLE_O.create();
        }
        else {
            icon = VaadinIcon.CIRCLE_THIN.create();
        }
        return icon;
    }

    private void editArticle(ArticleBrief item) {
        UI.getCurrent().getSession().setAttribute("cid", item.getCid());
        UI.getCurrent().getSession().setAttribute("country", selectedCountry);
        UI.getCurrent().navigate(ArticleEditView.class);
    }
}
