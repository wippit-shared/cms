package com.wippit.cms.frontend.secure.views.app.about;

import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.lumo.LumoUtility.Margin;
import com.wippit.cms.AppConstants;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.secure.views.MainLayout;
import jakarta.annotation.security.PermitAll;

@PageTitle("About")
@Route(value = "about", layout = MainLayout.class)
@PermitAll
public class AboutView extends OWSecureAdapterView {

    public AboutView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {
        setSpacing(false);
        addClassNames(ViewUtils.getBackgroundAboutClass());
        Image img = new Image("/static/logo_about.png", "Wippit CMS");
        img.getElement().setAttribute("width", "400px");
        img.getElement().setAttribute("height", "auto");
        add(img);
        add(new H2("Version " + AppConstants.APP_FULL_VERSION));
        add(new H2(new Anchor(AppConstants.APP_AUTHOR_LINK, AppConstants.APP_AUTHOR)));

        H3 header = new H3("Account: " + getAccount().getFull_name() + " (" + getAccount().getAlias() + ")");
        header.addClassNames(Margin.Top.XLARGE, Margin.Bottom.MEDIUM);
        add(header);

        setSizeFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        getStyle().set("text-align", "center");
    }


}
