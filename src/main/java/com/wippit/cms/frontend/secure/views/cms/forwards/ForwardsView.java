package com.wippit.cms.frontend.secure.views.cms.forwards;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.content.ContentRedirect;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.services.ContentRedirectDataService;
import com.wippit.cms.backend.data.services.LanguageDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.types.AppIcons;
import com.wippit.cms.backend.types.StatusTypes;
import com.wippit.cms.backend.types.ToolbarTypes;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.components.DialogManager;
import com.wippit.cms.frontend.secure.views.MainLayout;
import jakarta.annotation.security.RolesAllowed;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@PageTitle("Forwards")
@Route(value = "cms/forwards", layout = MainLayout.class)
@RolesAllowed({"ADMIN", "DEVELOPER", "MANAGER", "EDITOR"})
@Uses(Icon.class)
public class ForwardsView  extends OWSecureAdapterView {

    private LocalCountry selectedCountry;

    @Autowired
    private ContentRedirectDataService dataService;

    @Autowired
    private LanguageDataService languageDataService;

    private DataProvider<ContentRedirect, Void> dataProvider;
    private Grid<ContentRedirect> grid;

    public ForwardsView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public boolean setup() {

        selectedCountry = (LocalCountry) UI.getCurrent().getSession().getAttribute("country");
        if (selectedCountry == null) {
            List<LocalCountry> countries = languageDataService.getActiveCountries();
            for (LocalCountry country : countries) {
                if (country.getIso().equalsIgnoreCase(AppGlobal.getInstance().getSiteInfo().getCountry_iso())) {
                    selectedCountry = country;
                    break;
                }
            }
        }

        dataService.setIso(selectedCountry.getIso());

        dataProvider = DataProvider.fromCallbacks(
                query -> dataService.getPage(query.getPage(), query.getLimit()).stream(),
                query -> dataService.getCount());

        return true;
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {

        HorizontalLayout toolbar = ViewUtils.getToolbar(ToolbarTypes.HEADER);

        Button addRedirectButton = ViewUtils.getToolbarButton(AppIcons.ADD, "Add redirect");
        addRedirectButton.addClickListener(e->{
            ContentRedirect item = new ContentRedirect();
            item.setStatus(StatusTypes.ACTIVE.value());
            item.setStart_date(AppUtils.timestampNow());
            item.setEnd_date(AppUtils.getNowPlusMonths(99));
            saveRedirect(item);
        });

        ComboBox<LocalCountry> countryCombo = new ComboBox<>();
        countryCombo.setWidth("20em");
        countryCombo.setItems(languageDataService.getCountries());
        countryCombo.setValue(selectedCountry);

        countryCombo.addValueChangeListener(e -> {
            dataService.setIso(e.getValue().getIso());
            grid.getLazyDataView().refreshAll();
        });

        toolbar.add(
                addRedirectButton,
                ViewUtils.getToolbarLabel("Country"),
                countryCombo
        );
        add(toolbar);
        add(ViewUtils.getLineSecondary());

        DataProvider<ContentRedirect, Void> dataProvider;
        dataProvider = DataProvider.fromCallbacks(
                query -> dataService.getPage(query.getPage(), query.getLimit()).stream(),
                query -> dataService.getCount());

        grid = new Grid<>();
        grid.setId("grid");
        grid.addComponentColumn(item->getURL(item)).setHeader("Redirect").setWidth("25%");
        grid.addColumn(ContentRedirect::getUrl).setHeader("URL").setWidth("25%");
        grid.addComponentColumn(item->getStatusIcon(item)).setHeader("Status").setWidth("5%");
        grid.addColumn(item->getTime(item.getStart_date())).setHeader("Start").setWidth("15%");
        grid.addColumn(item->getTime(item.getEnd_date())).setHeader("End").setWidth("15%");
        grid.addComponentColumn(storage -> getEditButton(storage)).setWidth("5%");
        grid.addComponentColumn(storage -> getCopyButton(storage)).setWidth("5%");
        grid.addComponentColumn(storage -> getDeleteButton(storage)).setWidth("5%");

        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setDataProvider(dataProvider);

        grid.setWidthFull();
        grid.setHeight((size.getHeight()-200)+"px");

        add(grid);
    }

    @Override
    public void resizeUI(Size size) {
        grid.setHeight((size.getHeight()-200)+"px");
    }

    private Button getDeleteButton(ContentRedirect item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.TRASH,"Delete redirect: " + item.getName());
        button.addClickListener(e->removeRedirect(item));
        return button;
    }

    private Button getEditButton(ContentRedirect item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.EDIT,"Edit redirect: " + item.getName());
        button.addClickListener(e->saveRedirect(item));
        return button;
    }

    private Button getCopyButton(ContentRedirect item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.FILE_COPY,"Copy redirect '"+ item.getName() +"' to locale");
        button.addClickListener(e->copyRedirect(item));
        return button;
    }

    private Anchor getURL(ContentRedirect redirect) {
        return new Anchor(redirect.getCompleteURL(), redirect.getName(), AnchorTarget.BLANK);
    }

    private String getTime(Timestamp timestamp) {
        return AppUtils.timestampSiteDateTimeString(timestamp);
    }

    private Icon getStatusIcon(ContentRedirect item) {
        StatusTypes status = StatusTypes.getType(item.getStatus());
        Icon icon;
        if (status == StatusTypes.ACTIVE) {
            icon = VaadinIcon.CHECK_CIRCLE_O.create();
        }
        else {
            icon = VaadinIcon.CIRCLE_THIN.create();
        }
        return icon;
    }


    private void saveRedirect(ContentRedirect item) {
        DialogManager dialog = new DialogManager("Redirect");

        TextField nameField = new TextField("Name");
        nameField.setWidth("20em");
        nameField.setValue(item.getName());
        if (!StringUtils.isBlank(item.getName())) {
            nameField.setReadOnly(true);
        }

        TextField addressField = new TextField("URL");
        addressField.setWidth("20em");
        addressField.setValue(item.getUrl());

        DateTimePicker startPicker = new DateTimePicker("Start");
        startPicker.setValue(AppUtils.timestampToSiteDateTime(item.getStart_date()));
        startPicker.setWidthFull();

        DateTimePicker endPicker = new DateTimePicker("End");
        endPicker.setValue(AppUtils.timestampToSiteDateTime(item.getEnd_date()));
        endPicker.setWidthFull();

        ComboBox<StatusTypes> statusCombo = new ComboBox<>("Status");
        statusCombo.setItems(StatusTypes.toEditList());
        if (item.getStatus() == StatusTypes.ACTIVE.value()) {
            statusCombo.setValue(StatusTypes.ACTIVE);
        }
        else {
            statusCombo.setValue(StatusTypes.INACTIVE);
        }

        dialog.addToForm(nameField);
        dialog.addToForm(addressField);
        dialog.addToForm(startPicker);
        dialog.addToForm(endPicker);
        dialog.addToForm(statusCombo);

        Button saveButton = dialog.addMainButton("Save");
        saveButton.addClickListener(e->{

            String name = nameField.getValue();
            String url = addressField.getValue();
            LocalDateTime startT = startPicker.getValue();
            LocalDateTime endT = endPicker.getValue();

            if (StringUtils.isBlank(name) || StringUtils.isBlank(url)) {
                ViewUtils.showNotificationError("Invalid data.");
                return;
            }

            if (startT.isAfter(endT)) {
                ViewUtils.showNotificationError("Invalid dates. Fix start & end dates.");
                return;
            }

            item.setIso(selectedCountry.getIso());
            item.setName(URLEncoder.encode(name, StandardCharsets.US_ASCII));
            item.setUrl(url);
            item.setStatus(statusCombo.getValue().value());
            item.setStart_date(AppUtils.siteDateToTimestamp(startT));
            item.setEnd_date(AppUtils.siteDateToTimestamp(endT));

            if (dataService.save(item)) {
                ViewUtils.showNotification("Redirect added");
                grid.getLazyDataView().refreshAll();
                dialog.close();
                return;
            }

            ViewUtils.showNotificationError("Can't add redirect");
        });

        dialog.open();
    }

    private void removeRedirect(ContentRedirect item){
        DialogManager dialog = new DialogManager("Remove forward");
        dialog.addToForm(ViewUtils.getDivLabel("Confirm to remove redirect: " + item.getName()));

        Button deleteButton = dialog.addDangerButton("Remove");
        deleteButton.addClickListener(e->{
            if (dataService.delete(item)) {
                grid.getLazyDataView().refreshAll();
                ViewUtils.showNotification("Redirect removed");
                dialog.close();
            }
            else {
                ViewUtils.showNotificationError("Can't remove redirect");
            }
        });

        Button cancelButton = dialog.addMainButton("Cancel");
        cancelButton.addClickListener(e->dialog.close());

        dialog.open();
    }

    private void copyRedirect(ContentRedirect item) {
        DialogManager dialog = new DialogManager("Copy redirect to locale");

        ComboBox<LocalCountry> countryCombo = new ComboBox<>("Locale");
        countryCombo.setWidth("20em");
        countryCombo.setItems(dataService.getCountries());

        TextField nameField = new TextField("Redirect");
        nameField.setValue(item.getName());
        nameField.setReadOnly(true);
        nameField.setWidthFull();

        dialog.addToForm(nameField);
        dialog.addToForm(countryCombo);

        Button button = dialog.addMainButton("Copy");
        button.addClickListener(e->{

            if (countryCombo.getValue() == null) {
                return;
            }

            LocalCountry country = countryCombo.getValue();
            item.setIso(country.getIso());

            if (dataService.save(item)) {
                grid.getLazyDataView().refreshAll();
                ViewUtils.showNotification("Redirect copied to locale: " + country.getIso());
                dialog.close();
            }
            else {
                ViewUtils.showNotificationError("Can't copy redirect");
            }
        });

        dialog.open();
    }
}
