package com.wippit.cms.frontend.secure.views.cms.template;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.site.SiteTemplate;
import com.wippit.cms.backend.data.services.TemplateDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.types.AppIcons;
import com.wippit.cms.backend.types.StatusTypes;
import com.wippit.cms.backend.types.ToolbarTypes;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.components.DialogManager;
import com.wippit.cms.frontend.components.TemplateUploadDialog;
import com.wippit.cms.frontend.components.TemplateUploadListener;
import com.wippit.cms.frontend.components.dialog.DialogBox;
import com.wippit.cms.frontend.components.dialog.DialogBoxAdapter;
import com.wippit.cms.frontend.secure.views.MainLayout;
import jakarta.annotation.security.RolesAllowed;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.olli.FileDownloadWrapper;

@Slf4j
@PageTitle("Template")
@Route(value = "cms/templates", layout = MainLayout.class)
@RolesAllowed({"ADMIN", "DEVELOPER"})
@Uses(Icon.class)
public class TemplatesView extends OWSecureAdapterView implements TemplateUploadListener {

    @Autowired
    private TemplateDataService dataService;
    private DataProvider<SiteTemplate, Void> dataProvider;
    private Grid<SiteTemplate> grid;
    @Autowired
    private TemplateUploadDialog templateUploadDialog;
    //private FileDownloadWrapper downloadWrapper;

    public TemplatesView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public boolean setup() {

        dataProvider = DataProvider.fromCallbacks(
                query -> dataService.getPage(query.getPage(), query.getLimit()).stream(),
                query -> dataService.getCount());

        templateUploadDialog.addUploadListener(this);
        return true;
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {

        Button addFileButton = ViewUtils.getToolbarButton(AppIcons.ADD, "New template");
        Button uploadFileButton = ViewUtils.getToolbarButton(AppIcons.CLOUD_UPLOAD, "Upload template");

        HorizontalLayout toolbar = ViewUtils.getToolbar(ToolbarTypes.HEADER);
        toolbar.add(
                addFileButton,
                uploadFileButton
                );
        add(toolbar);
        add(ViewUtils.getLineSecondary());

        grid = new Grid<>();
        grid.setId("grid");
        grid.addColumn(SiteTemplate::getName).setHeader("Title").setWidth("30%");
//        grid.addColumn(SiteTemplate::getPage_root).setHeader("Root").setWidth("15%");
//        grid.addColumn(SiteTemplate::getPage_blog).setHeader("Blog").setWidth("15%");
//        grid.addColumn(SiteTemplate::getPage_item).setHeader("Blog item").setWidth("15%");

        grid.addComponentColumn(item ->getStatusIcon(item.getName().equalsIgnoreCase(AppGlobal.getInstance().getSiteInfo().getTemplate()))).setHeader("Active").setWidth("5%");
        grid.addComponentColumn(this::getDeleteButton).setWidth("5%");
        grid.addComponentColumn(this::getEditButton).setWidth("5%");
        grid.addComponentColumn(this::getDefaultButton).setWidth("5%");
        grid.addComponentColumn(this::getExportButton).setWidth("5%");
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setWidthFull();
        grid.setHeight((size.getHeight()  - 200) + "px");
        grid.setDataProvider(dataProvider);
        add(grid);

        addFileButton.addClickListener(e->{
            SiteTemplate item = new SiteTemplate();
            item.setName("New Template");
            addTemplateDialog(item);
        });

        uploadFileButton.addClickListener(e-> uploadTemplateDialog());
    }

    @Override
    public void resizeUI(Size size) {
        grid.setHeight((size.getHeight()  - 200) + "px");
    }

    private Icon getStatusIcon(boolean selected) {
        StatusTypes status = StatusTypes.getType(selected);
        Icon icon;
        if (status == StatusTypes.ACTIVE) {
            icon = VaadinIcon.CHECK_CIRCLE_O.create();
        }
        else {
            icon = VaadinIcon.CIRCLE_THIN.create();
        }
        return icon;
    }

    private Button getEditButton(SiteTemplate item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.EDIT, "Edit template " + item.getName());
        button.addClickListener(e->{
            UI.getCurrent().getSession().setAttribute("template", item);
            UI.getCurrent().navigate(TemplateEditView.class);
        });
        return button;
    }

    private Button getDefaultButton(SiteTemplate item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.OK, "Make template default: " + item.getName());
        button.addClickListener(e->makeDefaultDialog(item));
        if (item.getName().equalsIgnoreCase(AppGlobal.getInstance().getSiteInfo().getTemplate())) {
            button.setVisible(false);
        }
        return button;
    }

    private Button getDeleteButton(SiteTemplate item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.TRASH, "Remove template " + item.getName());
        button.addClickListener(e->removeTemplateDialog(item));
        if (item.getName().equalsIgnoreCase(AppGlobal.getInstance().getSiteInfo().getTemplate())) {
            button.setVisible(false);
        }
        return button;
    }

    private FileDownloadWrapper getExportButton(SiteTemplate item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.ZIP, "Export template " + item.getName());
        String source = AppGlobal.getInstance().getTemplatePath() + "/" + item.getName();
        FileDownloadWrapper downloadWrapper = new FileDownloadWrapper(item.getName() + ".zip", new AppUtils.ZipDirAndDownloadProvider(source));
        downloadWrapper.wrapComponent(button);

        button.addClickListener(e->{
            ViewUtils.showNotification("Exporting template...");
        });
        return downloadWrapper;
    }

    private void addTemplateDialog(SiteTemplate item) {
        String title = "New Template";
        if (item.getId() != 0) {
            title = item.getName();
        }
        DialogManager dialog = new DialogManager(title);

        TextField nameField = new TextField("Name");
        nameField.setRequired(true);
        nameField.setValue(item.getName());

        dialog.addToForm(nameField);

        Button saveButton = dialog.addMainButton("Save");
        saveButton.addClickListener(e->{
            if (StringUtils.isBlank(nameField.getValue())) {
                ViewUtils.showNotificationError("Name required");
                return;
            }

            String name = AppUtils.fixPathName(nameField.getValue());
            SiteTemplate oldTemplate = dataService.getTemplateByName(name);
            if (oldTemplate != null) {
                ViewUtils.showNotificationError("Template already exists: " + name);
                return;
            }

            item.setName(name);
            if (dataService.save(item)) {
                ViewUtils.showNotification("Template saved");
                grid.getLazyDataView().refreshAll();
                dialog.close();
            }
            else {
                ViewUtils.showNotificationError("Can't save template");
            }

        });

        dialog.open();
    }

    private void uploadTemplateDialog(){
        templateUploadDialog.open();
    }

    @Override
    public void templateUploaded(String name) {

        SiteTemplate item = new SiteTemplate();
        item.setName(name);

        if (dataService.saveToImport(item)) {
            grid.getLazyDataView().refreshAll();
            ViewUtils.showNotification("Template added: " + name);
        }
        else {
            dataService.remove(item);
            ViewUtils.showNotificationError("Can't add template");
        }
    }

    @Override
    public void templateUploadError(String error) {
        ViewUtils.showNotificationError(error);
    }

    private void removeTemplateDialog(SiteTemplate item) {
        DialogBox dialog = new DialogBox();
        dialog.showDelete("Delete template", "Confirm to delete template: " + item.getName());
        dialog.addListener(new DialogBoxAdapter() {
            @Override
            public void dialogDelete() {
                log.debug("Deleting template: " + item.getName());
                if (dataService.remove(item)) {
                    ViewUtils.showNotification("Template removed: " + item.getName());
                    grid.getLazyDataView().refreshAll();
                }
                else {
                    ViewUtils.showNotificationError("Can't remove template");
                }
            }
        });
    }

    public void makeDefaultDialog(SiteTemplate item) {
        DialogBox dialog = new DialogBox();
        dialog.showConfirm("Make template default", "Confirm to make template default: " + item.getName());
        dialog.addListener(new DialogBoxAdapter(){
            @Override
            public void dialogConfirm() {
                log.debug("Making template default: {}", item.getName());
                if (dataService.makeTemplateDefault(item)) {
                    ViewUtils.showNotification("Template is default: " + item.getName());
                    grid.getLazyDataView().refreshAll();
                }
                else {
                    ViewUtils.showNotificationError("Can't make template as default");
                }
            }
        });
    }

}

