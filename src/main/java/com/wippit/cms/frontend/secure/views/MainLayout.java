package com.wippit.cms.frontend.secure.views;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Footer;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.sidenav.SideNav;
import com.vaadin.flow.component.sidenav.SideNavItem;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.server.auth.AccessAnnotationChecker;
import com.vaadin.flow.theme.lumo.LumoUtility;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.types.AppIcons;
import com.wippit.cms.frontend.secure.views.app.about.AboutView;
import com.wippit.cms.frontend.secure.views.app.logs.ActivityLogsView;
import com.wippit.cms.frontend.secure.views.app.logs.PageLogsView;
import com.wippit.cms.frontend.secure.views.app.logs.PageStatsView;
import com.wippit.cms.frontend.secure.views.app.settings.SettingsView;
import com.wippit.cms.frontend.secure.views.app.users.ProfileView;
import com.wippit.cms.frontend.secure.views.app.users.UsersView;
import com.wippit.cms.frontend.secure.views.cms.articles.ArticlesView;
import com.wippit.cms.frontend.secure.views.cms.dashboard.DashboardView;
import com.wippit.cms.frontend.secure.views.cms.forwards.ForwardsView;
import com.wippit.cms.frontend.secure.views.cms.resources.ResourcesView;
import com.wippit.cms.frontend.secure.views.cms.template.EmailTemplatesView;
import com.wippit.cms.frontend.secure.views.cms.template.TemplatesView;
import com.wippit.cms.frontend.secure.views.cms.translations.TranslationsView;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

/**
 * The main view is a top-level placeholder for other views.
 */
@Slf4j
public class MainLayout extends AppLayout {

    private H2 viewTitle;

    private final AuthenticatedUser authenticatedUser;
    private final AccessAnnotationChecker accessChecker;

    public MainLayout(AuthenticatedUser authenticatedUser, AccessAnnotationChecker accessChecker) {
        this.accessChecker = accessChecker;
        this.authenticatedUser = authenticatedUser;
        setPrimarySection(Section.DRAWER);
        addDrawerContent();
        addHeaderContent();
    }

    private void addHeaderContent() {
        DrawerToggle toggle = new DrawerToggle();
        toggle.setAriaLabel("Menu toggle");

        viewTitle = new H2();
        viewTitle.addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.NONE);

        addToNavbar(true, toggle, viewTitle);
    }

    private void addDrawerContent() {
        Div header = new Div();
        header.addClassName("menu-bg");
        Scroller scroller = new Scroller(createNavigation());
        addToDrawer(header, scroller, createFooter());
    }

    private Div createNavigation() {

        Div div = new Div();

        SideNav navManagement = new SideNav();
        navManagement.setLabel("Management");
        navManagement.setCollapsible(true);
        navManagement.setExpanded(false);

        SideNav navContent = new SideNav();
        navContent.setLabel("Content");
        navContent.setCollapsible(true);
        navContent.setExpanded(false);

        SideNav navServices = new SideNav();
        navServices.setLabel("Services");
        navServices.setCollapsible(true);
        navServices.setExpanded(false);

        SideNav navStats = new SideNav();
        navStats.setLabel("Stats");
        navStats.setCollapsible(true);
        navStats.setExpanded(false);

        //---
        //Management
        //---
        if (accessChecker.hasAccess(SettingsView.class)) {
            navManagement.addItem(new SideNavItem("Settings", SettingsView.class, AppUtils.getMenuImage(AppIcons.SETTINGS)));
        }
        if (accessChecker.hasAccess(ProfileView.class)) {
            navManagement.addItem(new SideNavItem("Profile", ProfileView.class, AppUtils.getMenuImage(AppIcons.FILE)));
        }
        if (accessChecker.hasAccess(UsersView.class)) {
            navManagement.addItem(new SideNavItem("Users", UsersView.class, AppUtils.getMenuImage(AppIcons.USERS)));
        }
        if (accessChecker.hasAccess(TemplatesView.class)) {
            navManagement.addItem(new SideNavItem("Templates", TemplatesView.class, AppUtils.getMenuImage(AppIcons.TEMPLATES)));
        }
        if (accessChecker.hasAccess(EmailTemplatesView.class)) {
            navManagement.addItem(new SideNavItem("Email Templates", EmailTemplatesView.class, AppUtils.getMenuImage(AppIcons.EMAIL_CONFIG)));
        }
        if (accessChecker.hasAccess(TranslationsView.class)) {
            navManagement.addItem(new SideNavItem("Translations", TranslationsView.class, AppUtils.getMenuImage(AppIcons.TRANSLATIONS)));
        }
        if (accessChecker.hasAccess(AboutView.class)) {
            navManagement.addItem(new SideNavItem("About", AboutView.class, AppUtils.getMenuImage(AppIcons.ABOUT)));
        }

        if (!navManagement.getItems().isEmpty()) {
            div.add(navManagement);
        }

        //---
        //Content
        //---
        if (accessChecker.hasAccess(ArticlesView.class)) {
            navContent.addItem(new SideNavItem("Articles", ArticlesView.class, AppUtils.getMenuImage(AppIcons.ARTICLES)));
        }
        if (accessChecker.hasAccess(ResourcesView.class)) {
            navContent.addItem(new SideNavItem("Resources", ResourcesView.class, AppUtils.getMenuImage(AppIcons.RESOURCES)));
        }
        if (accessChecker.hasAccess(ForwardsView.class)) {
            navContent.addItem(new SideNavItem("Forwards", ForwardsView.class, AppUtils.getMenuImage(AppIcons.FORWARDS)));
        }
        if (!navContent.getItems().isEmpty()) {
            div.add(navContent);
        }

//        //---
//        //Services
//        //---
//        if (accessChecker.hasAccess(ConnectionsView.class)) {
//            navServices.addItem(new SideNavItem("Connections", ConnectionsView.class, AppUtils.getMenuImage(AppIcons.CONNECTIONS)));
//        }

        //---
        //Stats
        //---
        if (accessChecker.hasAccess(DashboardView.class)) {
            navStats.addItem(new SideNavItem("Dashboard", DashboardView.class, AppUtils.getMenuImage(AppIcons.DASHBOARD)));
        }
        if (accessChecker.hasAccess(PageStatsView.class)) {
            navStats.addItem(new SideNavItem("Page Stats", PageStatsView.class, AppUtils.getMenuImage(AppIcons.PAGE_STATS)));
        }
        if (accessChecker.hasAccess(PageLogsView.class)) {
            navStats.addItem(new SideNavItem("Page Logs", PageLogsView.class, AppUtils.getMenuImage(AppIcons.PAGE_LOGS)));
        }
        if (accessChecker.hasAccess(ActivityLogsView.class)) {
            navStats.addItem(new SideNavItem("User Logs", ActivityLogsView.class, AppUtils.getMenuImage(AppIcons.USER_LOGS)));
        }
        if (!navStats.getItems().isEmpty()) {
            div.add(navStats);
        }

        return div;
    }

    private Footer createFooter() {
        Footer layout = new Footer();

        Account account = null;
        Optional<Account> maybeUser = authenticatedUser.get();
        if (maybeUser.isPresent()) {
            account = maybeUser.get();
        }

        if (account != null) {

            MenuBar userMenu = new MenuBar();
            userMenu.setThemeName("tertiary-inline contrast");

            MenuItem userName = userMenu.addItem("");
            Div div = new Div();
            div.add(AppUtils.getMenuImage(AppIcons.LOGOUT));
            div.add(account.getAlias());
            div.add(new Icon("lumo", "dropdown"));
            div.getElement().getStyle().set("display", "flex");
            div.getElement().getStyle().set("align-items", "center");
            div.getElement().getStyle().set("gap", "var(--lumo-space-s)");
            userName.add(div);
            userName.getSubMenu().addItem("Sign out", e -> {
                authenticatedUser.logout();
            });

            layout.add(userMenu);
        } else {
            Anchor loginLink = new Anchor("login", "Sign in");
            layout.add(loginLink);
        }

        return layout;
    }

    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        setTitle(getCurrentPageTitle());
        setDrawerOpened(false);
    }

    private String getCurrentPageTitle() {
        PageTitle title = getContent().getClass().getAnnotation(PageTitle.class);
        return title == null ? "" : title.value();
    }

    public void setTitle(String title) {
        viewTitle.setText(title);
    }

}
