package com.wippit.cms.frontend.secure.views.app.settings;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.details.DetailsVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.TabSheet;
import com.vaadin.flow.component.textfield.*;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileBuffer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.content.Section;
import com.wippit.cms.backend.data.bean.site.Country;
import com.wippit.cms.backend.data.bean.site.EmailSettings;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.dao.SystemDao;
import com.wippit.cms.backend.data.services.*;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.services.AddressService;
import com.wippit.cms.backend.services.email.EmailService;
import com.wippit.cms.backend.services.email.EmailTemplate;
import com.wippit.cms.backend.services.email.Mail;
import com.wippit.cms.backend.types.*;
import com.wippit.cms.backend.utils.CountriesUtil;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.backend.utils.timezone.TimeZoneItem;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.components.DialogManager;
import com.wippit.cms.frontend.secure.views.MainLayout;
import com.wippit.cms.frontend.secure.views.cms.template.EmailTemplatesView;
import jakarta.annotation.security.RolesAllowed;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.ThreadUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;

@Slf4j
@PageTitle("Settings")
@Route(value = "settings", layout = MainLayout.class)
@RolesAllowed({"ADMIN"})
@Uses(Icon.class)
public class SettingsView extends OWSecureAdapterView {

    @Autowired
    private SiteDataService siteDataService;

//    @Autowired
//    private SystemDao systemDao;

    @Autowired
    private UserDataService dataService;

    @Autowired
    private EmailTemplateDataService templateDataService;

    private TabSheet tabSheet;
    private Grid<LocalCountry> gridCountries;
    private Grid<Section> gridSections;
    private Button buttonSave;
    private Tab tabSettings;
    private Tab tabAdmin = null;
    private Button nextButton;
    private boolean flagReload = false;
    private final KeyDataService keyDataService;
    private FileBuffer fileBuffer;
    private TextField geoDatabaseField;

    public SettingsView(AuthenticatedUser authenticatedUser, @Autowired SystemDao systemDao) {
        super(authenticatedUser);
        keyDataService = new KeyDataService(systemDao);
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {

        tabSheet = new TabSheet();
        tabSheet.setWidthFull();
        tabSheet.setHeight((size.getHeight()-150) + "px");

        if (AppGlobal.getInstance().isSetupRequired()) {
            setTitle("Setup");
            tabAdmin = tabSheet.add("Admin", getAdminSettings());
            tabSettings = tabSheet.add("Site", getSiteSettings());
            flagReload = true;
        }
        else {
            tabSettings = tabSheet.add("Site", getSiteSettings());
            tabSheet.add("Localization", getCountriesSettings());
            tabSheet.add("Content Sections", getSectionsSettings());
            tabSheet.add("Email Service", getEmailSettings());
            tabSheet.add("Tools", getToolsSettings());
//            tabSheet.add("Social accounts", getSocialSettings());

            flagReload = false;
        }

        add(tabSheet);
    }

    @Override
    public void resizeUI(Size size) {
        tabSheet.setHeight((size.getHeight()-150) + "px");
    }

    private VerticalLayout getAdminSettings(){
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();

        FormLayout formLayout = new FormLayout();
        if (isMobile()) {
            formLayout.setWidth("90%");
        }
        else {
            formLayout.setWidth("40em");
        }

        PasswordField adminPassword1 = new PasswordField("Admin password (new)");
        adminPassword1.setRequired(true);
        adminPassword1.setMinLength(6);
        adminPassword1.setHelperText("6 characters minimum");

        PasswordField adminPassword2 = new PasswordField("Admin password (confirm)");
        adminPassword2.setRequired(true);
        adminPassword2.setMinLength(6);
        adminPassword2.setHelperText("It must match the new password.");

        formLayout.add(
                adminPassword1,
                adminPassword2
        );
        layout.add(formLayout);

        nextButton = ViewUtils.getActiveButton("Next");
        layout.add(nextButton);

        nextButton.addClickListener(e->{

            String pwd1 = adminPassword1.getValue();
            String pwd2 = adminPassword2.getValue();
            if (StringUtils.isBlank(pwd1) || StringUtils.isBlank(pwd2) || adminPassword1.isInvalid() || adminPassword2.isInvalid()) {
                ViewUtils.showNotificationError("Passwords required");
                return;
            }

            if (!pwd1.contentEquals(pwd2)) {
                ViewUtils.showNotificationError("Passwords do not match");
                return;
            }

            if (dataService.changeSecret("admin", pwd1)) {
                ViewUtils.showNotification("Admin password updated");
                buttonSave.setVisible(true);
                tabSheet.setSelectedTab(tabSettings);
            }
            else {
                ViewUtils.showNotificationError("Can't update admin password");
            }
        });

        return layout;
    }

    private VerticalLayout getSiteSettings() {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();

        FormLayout formLayout = new FormLayout();
        if (isMobile()) {
            formLayout.setWidth("90%");
        }
        else {
            formLayout.setWidth("40em");
        }

        TextField nameField = new TextField("Name");
        nameField.setValue(AppGlobal.getInstance().getSiteInfo().getName());
        nameField.setRequired(true);

        TextArea descriptionField = new TextArea("About");
        descriptionField.setValue(AppGlobal.getInstance().getSiteInfo().getDescription());

        TextField locationField = new TextField("Location");
        locationField.setValue(AppGlobal.getInstance().getSiteInfo().getLocation());
        locationField.setRequired(true);

        TextArea addressField = new TextArea("Address");
        addressField.setValue(AppGlobal.getInstance().getSiteInfo().getAddress());

        TextField phoneField = new TextField("Phone");
        phoneField.setValue(AppGlobal.getInstance().getSiteInfo().getPhone());

        EmailField emailField = new EmailField("Email");
        emailField.setValue(AppGlobal.getInstance().getSiteInfo().getEmail());
        emailField.setRequired(true);

        List<TimeZoneItem> timeZoneList = TimeZoneItem.toList();
        ComboBox<TimeZoneItem> timeZoneCombo = new ComboBox<>("Timezone");
        timeZoneCombo.setRequired(true);

        timeZoneCombo.setWidthFull();
        timeZoneCombo.setItems(timeZoneList);
        timeZoneCombo.setValue(TimeZoneItem.fromString(AppGlobal.getInstance().getSiteInfo().getTimezone()));

        formLayout.add(
                nameField,
                descriptionField,
                locationField,
                addressField,
                phoneField,
                emailField,
                timeZoneCombo
        );
        formLayout.setColspan(timeZoneCombo, 2);
        layout.add(formLayout);

        buttonSave = ViewUtils.getActiveButton("Save");
        buttonSave.addClickListener(e->{
            String name = AppUtils.validData(nameField.getValue());
            String about = AppUtils.validData(descriptionField.getValue());
            String location = AppUtils.validData(locationField.getValue());
            String address = AppUtils.validData(addressField.getValue());
            String phone = AppUtils.validData(phoneField.getValue());
            String email = AppUtils.validData(emailField.getValue());
            TimeZoneItem timezone = timeZoneCombo.getValue();

            if (StringUtils.isBlank(name) || StringUtils.isBlank(location) || timezone == null || emailField.isInvalid()) {
                ViewUtils.showNotificationError("Data is required");
                return;
            }

            AppGlobal.getInstance().getSiteInfo().setName(name);
            AppGlobal.getInstance().getSiteInfo().setDescription(about);
            AppGlobal.getInstance().getSiteInfo().setLocation(location);
            AppGlobal.getInstance().getSiteInfo().setAddress(address);
            AppGlobal.getInstance().getSiteInfo().setPhone(phone);
            AppGlobal.getInstance().getSiteInfo().setEmail(email);
            AppGlobal.getInstance().getSiteInfo().setTimezone(timezone.getName());
            AppGlobal.getInstance().getSiteInfo().setTime_offset(timezone.getOffsetInMinutes());

            ViewUtils.showNotification("Site settings updated");

            updateSetup();

            if (flagReload) {
                UI.getCurrent().navigate(SettingsView.class);
            }
        });
        layout.add(buttonSave);

        timeZoneCombo.addValueChangeListener(e->{
            log.debug("Timezone: {} \tOffset: {}", e.getValue().getName(), e.getValue().getOffsetInMinutes());

        });

        buttonSave.setVisible(!AppGlobal.getInstance().isSetupRequired());

        return layout;
    }

    private VerticalLayout getSectionsSettings() {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();

        HorizontalLayout toolbar = ViewUtils.getToolbar(ToolbarTypes.HEADER);
        Button addSectionButton = ViewUtils.getToolbarButton(AppIcons.ADD, "Add secID");
        toolbar.add(
                addSectionButton
        );
        layout.add(toolbar);
        layout.add(ViewUtils.getLineSecondary());

        gridSections = new Grid<>();
        gridSections.setId("grid");
        gridSections.addColumn(Section::getName).setHeader("Name").setWidth("30%");
        gridSections.addColumn(Section::getSecID).setHeader("ID").setWidth("10%");
        gridSections.addColumn(item-> SectionTypes.getType(item.getType()).getLabel()).setHeader("Type").setWidth("20%");
        gridSections.addColumn(this::getSectionURL).setHeader("Url").setWidth("30%");
        gridSections.addComponentColumn(this::getDeleteSectionButton).setWidth("5%");
        gridSections.setSelectionMode(Grid.SelectionMode.SINGLE);
        gridSections.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        gridSections.setWidthFull();
        gridSections.setHeight("500px");
        layout.add(gridSections);

        addSectionButton.addClickListener(e->{
            Section item = new Section();
            item.setName("New secID");
            item.setSecID("newsection");
            editSectionDialog(item);
        });

        reloadSections();
        return layout;
    }

    private String getSectionURL(Section item) {

        if (Objects.requireNonNull(SectionTypes.getType(item.getType())) == SectionTypes.ARTICLES) {
            if (item.getSecID().equalsIgnoreCase("blog")) {
                return AppUtils.createServerUrl(item.getSecID());
            } else {
                return AppUtils.createServerUrl("/content/" + item.getSecID());
            }
        }
        return "";
    }

    private VerticalLayout getCountriesSettings() {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();

        HorizontalLayout toolbar = ViewUtils.getToolbar(ToolbarTypes.HEADER);
        toolbar.setWidthFull();

        Button addCountryButton = ViewUtils.getToolbarButton(AppIcons.ADD, "Add localization");
        toolbar.add(
                addCountryButton
        );
        layout.add(toolbar);
        layout.add(ViewUtils.getLineSecondary());

        gridCountries = new Grid<>();
        gridCountries = new Grid<>();
        gridCountries.setId("grid");
        gridCountries.addColumn(LocalCountry::getIso).setHeader("ID").setWidth("10%");
        gridCountries.addColumn(LocalCountry::getName).setHeader("Name").setWidth("20%");
        gridCountries.addColumn(item->item.getIso2()+"/"+item.getIso3()).setHeader("ISO2/ISO3").setWidth("10%");
        gridCountries.addColumn(LocalCountry::getLocale).setHeader("Locale").setWidth("10%");
        gridCountries.addColumn(LocalCountry::getDate_format).setHeader("Date").setWidth("10%");
        gridCountries.addColumn(item->item.getCurrency()+"/"+item.getCurrency_prefix()).setHeader("Currency").setWidth("10%");
        gridCountries.addColumn(LocalCountry::getPhone_code).setHeader("Phone").setWidth("5%");
        gridCountries.addComponentColumn(item -> getStatusIcon(item.getIso().equalsIgnoreCase(AppGlobal.getInstance().getSiteInfo().getCountry_iso()))).setHeader("default").setWidth("5%");
        gridCountries.addComponentColumn(this::getMakeLocalizationDefaultButton).setWidth("5%");
        gridCountries.addComponentColumn(this::getDeleteLocalizationButton).setWidth("5%");
        gridCountries.setSelectionMode(Grid.SelectionMode.SINGLE);
        gridCountries.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        gridCountries.setWidthFull();
        gridCountries.setHeight("400px");
        layout.add(gridCountries);

        addCountryButton.addClickListener(e->{
            addCountryDialog();
        });

        reloadCountries();

        return layout;
    }

    private Icon getStatusIcon(boolean selected) {
        StatusTypes status = StatusTypes.getType(selected);
        Icon icon;
        if (status == StatusTypes.ACTIVE) {
            icon = VaadinIcon.CHECK_CIRCLE_O.create();
        }
        else {
            icon = VaadinIcon.CIRCLE_THIN.create();
        }
        return icon;
    }

    private Button getMakeLocalizationDefaultButton(LocalCountry item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.SELECT, "Make " + item.getName()+ " default localization");
        button.addClickListener(e->{
            AppGlobal.getInstance().getSiteInfo().setCountry_iso(item.getIso());
            keyDataService.setKey("site.country", item.getIso());
            gridCountries.getListDataView().refreshAll();
        });
        return button;
    }

    private Button getDeleteLocalizationButton(LocalCountry item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.TRASH, "Remove " + item.getName()+ " from localization");
        button.addClickListener(e->{
            DialogManager dialog = new DialogManager("Confirm to remove");

            Checkbox includeData = new Checkbox("Include localization for articles, templates & content sections");
            TextArea messageField = new TextArea("Warning");
            messageField.setReadOnly(true);
            //messageField.setHeight("300px");

            dialog.addToForm(messageField);
            dialog.addToForm(includeData);

            String sb = "Confirm to remove " +
                    item.getName() +
                    " from localization. \n\n" +
                    "This action will NOT REMOVE all translation in templates & articles data for this localization.";
            messageField.setValue(sb);

            includeData.addValueChangeListener(e3->{
                if (e3.getValue()) {
                    String sb2 = "Confirm to remove " +
                            item.getName() +
                            " from localization. \n\n" +
                            "This action will REMOVE ALL translation in templates & articles data for this localization.";
                    messageField.setValue(sb2);
                }
                else {
                    String sb3 = "Confirm to remove " +
                            item.getName() +
                            " from localization. \n\n" +
                            "All translations in templates & articles data for this localization will be retained but not available.";
                    messageField.setValue(sb3);
                }
            });

            Button removeButton = dialog.addDangerButton("Remove");
            removeButton.addClickListener(ev->{
                if (siteDataService.removeCountry(item, includeData.getValue())) {
                    ViewUtils.showNotification("Localization removed: " + item.getName());
                    reloadCountries();
                    dialog.close();
                }
                else {
                    ViewUtils.showNotificationError("Can't remove localization");
                }

            });
            Button cancelButton = dialog.addMainButton("Cancel");
            cancelButton.addClickListener(ev2->dialog.close());
            dialog.open();
        });
        return button;
    }

    private Button getDeleteSectionButton(Section item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.TRASH, "Remove " + item.getName()+ " secID");
        button.addClickListener(e->{
            deleteSectionDialog(item);
        });
        return button;
    }

    private VerticalLayout getEmailSettings() {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();

        FormLayout formLayout = new FormLayout();
        if (isMobile()) {
            formLayout.setWidth("90%");
        }
        else {
            formLayout.setWidth("40em");
        }

        EmailSettings settings = siteDataService.getEmailSettings();

        TextField hostField = new TextField("Host");
        hostField.setValue(settings.getHost());

        IntegerField portField = new IntegerField("Port");
        portField.setValue(settings.getPort());

        TextField usernameField = new TextField("Username");
        usernameField.setValue(settings.getUsername());

        TextField passwordField = new TextField("Password");
        passwordField.setValue(settings.getPassword());

        TextField fromField = new TextField("From");
        fromField.setValue(settings.getFrom());

        TextField prefixField = new TextField("Prefix");
        prefixField.setValue(settings.getPrefix());

        Checkbox authRequireheckBox = new Checkbox("SMTP Auth");
        authRequireheckBox.setValue(settings.isAuth());

        Checkbox useTLSCheckBox = new Checkbox("SMTP Start TLS");
        useTLSCheckBox.setValue(settings.isTls());

        Checkbox useSSLCheckBox = new Checkbox("SMTP Use SSL");
        useSSLCheckBox.setValue(settings.isSsl());

        Checkbox debugCheckBox = new Checkbox("Debug");
        debugCheckBox.setValue(settings.isDebug());

        formLayout.add(
                hostField,
                portField,
                usernameField,
                passwordField,
                fromField,
                prefixField,
                useTLSCheckBox,
                useSSLCheckBox,
                authRequireheckBox,
                debugCheckBox
        );

        layout.add(formLayout);

        HorizontalLayout footer = ViewUtils.getFooter();

        Button button = ViewUtils.getActiveButton("Save");
        Button buttonTest = ViewUtils.getButton("Test");
        footer.add(buttonTest, button);
        button.addClickListener(e->{
            String host = AppUtils.validData(hostField.getValue());
            Integer port = portField.getValue();
            String username  = AppUtils.validData(usernameField.getValue());
            String pwd = AppUtils.validData(passwordField.getValue());
            String from = AppUtils.validData(fromField.getValue());
            String prefix = AppUtils.validData(prefixField.getValue());
            Boolean useTLS = useTLSCheckBox.getValue();
            Boolean useSSL = useSSLCheckBox.getValue();
            Boolean auth = authRequireheckBox.getValue();
            Boolean debug = debugCheckBox.getValue();

            EmailSettings nSettings = new EmailSettings();
            nSettings.setHost(host);
            nSettings.setPort(port);
            nSettings.setUsername(username);
            nSettings.setPassword(pwd);
            nSettings.setFrom(from);
            nSettings.setPrefix(prefix);
            nSettings.setSsl(useSSL);
            nSettings.setTls(useTLS);
            nSettings.setAuth(auth);
            nSettings.setDebug(debug);

            siteDataService.save(nSettings);

            EmailService.getCurrentInstance().setup(nSettings);
            ViewUtils.showNotification("Email service updated");
        });

        buttonTest.addClickListener(e->testEmailDialog());

        layout.add(footer);

        return layout;
    }

    private VerticalLayout getToolsSettings() {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();

        FormLayout formLayout = new FormLayout();
        if (isMobile()) {
            formLayout.setWidth("90%");
        }
        else {
            formLayout.setWidth("40em");
        }

        TextField translatorField = new TextField("Translator API key");
        translatorField.setValue(AppGlobal.getInstance().getKeys().getTranslator());
        translatorField.setWidth("35em");

        TextField editorField = new TextField("Editor key");
        editorField.setWidth("35em");
        editorField.setValue(AppGlobal.getInstance().getKeys().getEditor());

        String db = keyDataService.getKey("geo.filename");
        String status = keyDataService.getKey("geo.status");
        geoDatabaseField = new TextField("GEO2 City Database");
        geoDatabaseField.setValue(db);
        geoDatabaseField.setReadOnly(true);
        geoDatabaseField.setWidth("35em");
        if (status.equalsIgnoreCase("ok")) {
            geoDatabaseField.setSuffixComponent(AppIcons.OK.icon());
        }
        else {
            geoDatabaseField.setSuffixComponent(AppIcons.ERROR.icon());
        }

        VerticalLayout translatorLayout = new VerticalLayout();
        VerticalLayout editorLayout = new VerticalLayout();
        VerticalLayout geoLayout = new VerticalLayout();

        Anchor translatorAnchor = new Anchor("https://www.deepl.com/en/pro-api", "Go to translator API (DeepL)", AnchorTarget.BLANK);
        Anchor editorAnchor = new Anchor("https://www.tiny.cloud/tinymce/", "Go to editor library (TinyMCE)", AnchorTarget.BLANK);
        Anchor geoAnchor = new Anchor("https://dev.maxmind.com/geoip/geolite2-free-geolocation-data", "Go to geo database (MaxMind)", AnchorTarget.BLANK);

        translatorLayout.add(translatorField, translatorAnchor);
        editorLayout.add(editorField, editorAnchor);

        Button uploadDatabaseButton = ViewUtils.getButton("Upload Database");
        geoLayout.add(geoDatabaseField, uploadDatabaseButton, geoAnchor);

        Details translatorDetails = new Details("Translator", translatorLayout);
        translatorDetails.setOpened(true);
        translatorDetails.addThemeVariants(DetailsVariant.FILLED);

        Details editorDetails = new Details("Editor", editorLayout);
        editorDetails.addThemeVariants(DetailsVariant.FILLED);
        editorDetails.setOpened(true);

        Details geoDetails = new Details("Geo localization", geoLayout);
        geoDetails.setOpened(true);
        geoDetails.addThemeVariants(DetailsVariant.FILLED);

        formLayout.add(
                translatorDetails,
                editorDetails,
                geoDetails
        );
        formLayout.setColspan(translatorDetails, 2);
        formLayout.setColspan(editorDetails, 2);
        formLayout.setColspan(geoDetails, 2);

        layout.add(formLayout);

        HorizontalLayout footer = ViewUtils.getFooter();

        Button button = ViewUtils.getActiveButton("Save");
        footer.add(button);

        layout.add(footer);

        button.addClickListener(e->{
            String editor = AppUtils.validData(editorField.getValue());
            String tralator = AppUtils.validData(translatorField.getValue());

            AppGlobal.getInstance().getKeys().setEditor(editor);
            AppGlobal.getInstance().getKeys().setTranslator(tralator);

            siteDataService.save(AppGlobal.getInstance().getKeys());
            ViewUtils.showNotification("Site keys updated");
        });
        uploadDatabaseButton.addClickListener(e->uploadDBDialog());
        return layout;
    }

    private VerticalLayout getSocialSettings() {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();

        FormLayout formLayout = new FormLayout();
        if (isMobile()) {
            formLayout.setWidth("90%");
        }
        else {
            formLayout.setWidth("40em");
        }

        TextField facebookField = new TextField("Facebook");
        facebookField.setValue(keyDataService.getKey("social.facebook"));

        TextField twitterField = new TextField("X");
        twitterField.setValue(keyDataService.getKey("social.x"));

        TextField youtubeField = new TextField("Youtube");
        youtubeField.setValue(keyDataService.getKey("social.youtube"));

        TextField whatsappField = new TextField("Whatsapp");
        whatsappField.setValue(keyDataService.getKey("social.whatsapp"));

        formLayout.add(
                facebookField,
                twitterField,
                youtubeField,
                whatsappField
        );

        layout.add(formLayout);

        Button button = ViewUtils.getActiveButton("Save");
        layout.add(button);
        button.addClickListener(e->{
            String facebook = AppUtils.validData(facebookField.getValue());
            String twitter = AppUtils.validData(twitterField.getValue());
            String youtube = AppUtils.validData(youtubeField.getValue());
            String whats = AppUtils.validData(whatsappField.getValue());

            keyDataService.setKey("social.facebook", facebook);
            keyDataService.setKey("social.twitter", twitter);
            keyDataService.setKey("social.youtube", youtube);
            keyDataService.setKey("social.whatsapp", whats);

            ViewUtils.showNotification("Site social updated");
        });

        return layout;
    }

    private void addCountryDialog() {
        DialogManager dialog = new DialogManager("Add localization");

        List<Country> availableCountries = CountriesUtil.getCountries();
        List<Country> countries = new ArrayList<>();
        List<LocalCountry> currentCountries = siteDataService.getLocalCountries();
        boolean flagExists;
        for (Country item : availableCountries) {
            flagExists = false;
            for (LocalCountry current : currentCountries) {
                if (item.getIso3().equalsIgnoreCase(current.getIso3())) {
                    flagExists = true;
                    break;
                }
            }

            if (!flagExists) {
                countries.add(item);
            }
        }

        ComboBox<Country> countryCombo = new ComboBox<>("Country");
        countryCombo.setItems(countries);

        TextField nameField = new TextField("Name");
        nameField.setReadOnly(true);

        TextField isoField = new TextField("ISO-2/ISO-3");
        isoField.setReadOnly(true);

        TextField currencyField = new TextField("Currency");
        currencyField.setReadOnly(true);

        TextField currencySymbolField = new TextField("Currency symbol");
        currencySymbolField.setReadOnly(true);

        TextField phoneCodeField = new TextField("Phone Code");
        phoneCodeField.setReadOnly(true);

        ComboBox<Locale> localeCombo = new ComboBox<>("Locale");
        localeCombo.setRequired(true);

        TextField dateFormatField = new TextField("Date format");
        dateFormatField.setRequired(true);

        TextField displayField = new TextField("Display as");
        displayField.setRequired(true);

        dialog.addToForm(countryCombo);
        dialog.addToForm(nameField);
        dialog.addToForm(isoField);
        dialog.addToForm(currencyField);
        dialog.addToForm(currencySymbolField);
        dialog.addToForm(phoneCodeField);
        dialog.addToForm(localeCombo);
        dialog.addToForm(dateFormatField);
        dialog.addToForm(displayField);

        Button addCountryButton = dialog.addMainButton("Add");

        Locale[] locales = Locale.getAvailableLocales();

        countryCombo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                nameField.setValue("");
                isoField.setValue("");
                currencyField.setValue("");
                currencySymbolField.setValue("");
                phoneCodeField.setValue("");
                localeCombo.setValue(null);
                dateFormatField.setValue("");
                displayField.setValue("");
                return;
            }

            Country item = e.getValue();
            nameField.setValue(item.getName());
            isoField.setValue(item.getIso2() + "/" + item.getIso3());
            currencyField.setValue(item.getCurrency_name());
            currencySymbolField.setValue(item.getCurrency_symbol());
            phoneCodeField.setValue(item.getPhone());
            localeCombo.setValue(null);
            dateFormatField.setValue("");
            displayField.setValue("");

            log.debug("Country: {}", item);

            List<Locale> localeList = new ArrayList<>();
            for (Locale curentLocale:locales) {
                log.debug("Country:{}", curentLocale.getCountry());
                if(curentLocale.getCountry().equalsIgnoreCase(item.getIso2())) {
                    localeList.add(curentLocale);
                }
            }

            localeCombo.setItems(localeList);

            displayField.setValue(item.getName());
        });

        addCountryButton.addClickListener(e->{
            dialog.close();
        });

        localeCombo.addValueChangeListener(e->{
            if (e.getValue() == null) {
                dateFormatField.setValue("");

                if (countryCombo.getValue() == null) {
                    displayField.setValue("");
                }
                else {
                    displayField.setValue(countryCombo.getValue().getName());
                }
            }
            else {
                Locale locale = e.getValue();
                DateFormat df = SimpleDateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, locale);
                if (df instanceof SimpleDateFormat) {
                    SimpleDateFormat sdf = (SimpleDateFormat) df;
                    dateFormatField.setValue(sdf.toLocalizedPattern());
                    dateFormatField.setHelperText(df.format(new Date()));
                }
                else {
                    dateFormatField.setValue("");
                    dateFormatField.setHelperText(df.format(new Date()));
                }

                Country country = countryCombo.getValue();
                String display = country.getName() +"/" + locale.getDisplayLanguage();
                displayField.setValue(display);

                log.debug("Locale: {} - {} - {} - {}", locale.getLanguage(), locale.getCountry(), locale.getISO3Country(), locale.getISO3Language());
            }
        });

        addCountryButton.addClickListener(e-> {

                    Country country = countryCombo.getValue();
                    if (country == null) {
                        ViewUtils.showNotificationError("Country required");
                        return;
                    }

                    Locale countryLocale = localeCombo.getValue();
                    if (countryLocale == null) {
                        ViewUtils.showNotificationError("Locale is required");
                        return;
                    }

                    log.debug("Locale: {}", countryLocale);

                    String iso2 = country.getIso2();
                    String iso3 = countryLocale.getISO3Country();
                    String iso = countryLocale.getCountry().toLowerCase()+"-"+countryLocale.getLanguage().toLowerCase();

                    String display = displayField.getValue();
                    String dateFormat = dateFormatField.getValue();

                    if (StringUtils.isBlank(display) || StringUtils.isBlank(dateFormat)) {
                        ViewUtils.showNotificationError("Display & date format are required");
                        return;
                    }

                    LocalCountry localCountry = new LocalCountry();
                    localCountry.setName(country.getName());
                    localCountry.setIso(iso);
                    localCountry.setIso2(iso2);
                    localCountry.setIso3(iso3);
                    localCountry.setCurrency_prefix(country.getCurrency_symbol());
                    localCountry.setCurrency(country.getCurrency_name());
                    localCountry.setLocale(countryLocale.getLanguage());
                    localCountry.setDisplay(display);
                    if (country.getPhone().startsWith("+")){
                        localCountry.setPhone_code(country.getPhone());
                    }
                    else {
                        localCountry.setPhone_code("+" + country.getPhone());
                    }
                    localCountry.setDate_format(dateFormat);
                    localCountry.setFlag(iso2.toLowerCase()+".png");

                    log.debug("Country: {}", country);
                    log.debug("Local Country: {}", localCountry);

                    if (siteDataService.addCountry(localCountry)) {
                        ViewUtils.showNotification("Localization added: " + country.getName());
                        reloadCountries();
                        dialog.close();
                    }
                    else {
                        ViewUtils.showNotificationError("Can't add localization");
                    }
        });

        dialog.open();
    }

    private void reloadCountries() {
        List<LocalCountry> countries = siteDataService.getLocalCountries();
        gridCountries.setItems(countries);
        gridCountries.getListDataView().refreshAll();
    }

    private void reloadSections(){
        List<Section> list = siteDataService.getAllDistinctSections();
        gridSections.setItems(list);
        gridSections.getListDataView().refreshAll();
    }

    private void editSectionDialog(Section item) {
        String title = "New Content Section";
        if (item.getId() != 0) {
            title = "Content secID: " + item.getName();
        }

        DialogManager dialog = new DialogManager(title);

        TextField nameField = new TextField("Name");
        nameField.setValue(item.getName());
        nameField.setRequired(true);

        TextField idField = new TextField("ID");
        idField.setValue(item.getSecID());
        idField.setRequired(true);
        idField.setReadOnly(true);

        ComboBox<SectionTypes> sectionCombo = new ComboBox<>("Type");
        sectionCombo.setItems(SectionTypes.toList());
        sectionCombo.setValue(SectionTypes.getType(item.getType()));

        dialog.addToForm(nameField);
        dialog.addToForm(idField);
        dialog.addToForm(sectionCombo);

        nameField.addValueChangeListener(e->{
            String id = e.getValue();
            if (StringUtils.isBlank(id)){
                idField.setValue("");
                return;
            }

            idField.setValue(AppUtils.fixURLFromName(id.trim()));
        });

        Button addButton = dialog.addMainButton("Save");
        addButton.addClickListener(ev->{

            String name = nameField.getValue();
            if (StringUtils.isBlank(name)) {
                ViewUtils.showNotificationError("Name required");
                return;
            }

            String url = AppUtils.fixURLFromName(name);
            if (StringUtils.isBlank(url)) {
                ViewUtils.showNotificationError("Invalid url");
                return;
            }

            item.setName(name);
            item.setSecID(url);
            item.setType(sectionCombo.getValue().getType());

            if (siteDataService.save(item)) {
                ViewUtils.showNotification("Section saved" + item.getName());
                reloadSections();
                dialog.close();
            }
            else {
                ViewUtils.showNotificationError("Can't save secID");
            }

        });
        dialog.open();
    }

    private void deleteSectionDialog(Section item){
        DialogManager dialog = new DialogManager("Confirm to remove");


        StringBuilder sb = new StringBuilder();
        sb.append("Confirm to remove secID ");
        sb.append(item.getName());
        dialog.addToForm(ViewUtils.getDivLabel(sb.toString()));

        Button removeButton = dialog.addDangerButton("Remove");
        removeButton.addClickListener(ev->{
            if (siteDataService.removeSection(item)) {
                ViewUtils.showNotification("Section removed: " + item.getName());
                reloadSections();
                dialog.close();
            }
            else {
                ViewUtils.showNotificationError("Can't remove secID");
            }
        });
        Button cancelButton = dialog.addMainButton("Cancel");
        cancelButton.addClickListener(ev2->dialog.close());
        dialog.open();
    }

    private void updateSetup() {
        siteDataService.save(AppGlobal.getInstance().getSiteInfo());
        keyDataService.setKey("setup.required", 0);
        AppGlobal.getInstance().setSetupRequired(false);
    }

    private void uploadDBDialog() {
        DialogManager dialog = new DialogManager("Upload Geo2 City Database");

        TextField fileField = new TextField("File");
        fileField.setReadOnly(true);

        fileBuffer = new FileBuffer();
        Upload upload = new Upload(fileBuffer);
        upload.setAcceptedFileTypes(".mmdb");
        upload.setAutoUpload(true);

        dialog.addToForm(upload);
        dialog.addToForm(fileField);
        fileField.setVisible(false);

        upload.addStartedListener(e->{
            log.debug("Upload started");
        });

        upload.addFinishedListener(e->{
            log.debug("Upload finished");
        });

        upload.addFileRejectedListener(e->{
            log.warn("Upload rejected");
            ViewUtils.showNotificationError("Not an acceptable file");
        });

        upload.addFailedListener(e->{
            log.warn("Upload failed");
            ViewUtils.showNotificationError("Can't upload file");
        });

        upload.addSucceededListener(evt -> {
            log.debug("Upload succeeded: {}", evt.getFileName());
            fileField.setValue(evt.getFileName());
            fileField.setVisible(true);

            Button saveButton = dialog.addMainButton("Save");
            saveButton.addClickListener(e->{
                try {
                    byte[] data = fileBuffer.getInputStream().readAllBytes();

                    Calendar now = Calendar.getInstance();
                    String dbFilename =
                            "GEO-" + now.get(Calendar.YEAR) + now.get(Calendar.DAY_OF_YEAR) + now.get(Calendar.HOUR_OF_DAY) + now.get(Calendar.MINUTE)
                            + "-" + evt.getFileName();

                    String dbFilepath = AppGlobal.getInstance().getPath() + "/geo/" + dbFilename;
                    log.debug("GEO: {}", dbFilepath);
                    FileUtils.writeByteArrayToFile(new File(dbFilepath), data);
                    keyDataService.setKey("geo.db", dbFilename);
                    keyDataService.setKey("geo.filename", evt.getFileName());

                    ThreadUtils.sleep(Duration.ofMillis(500));

                    AddressService.reload();

                    geoDatabaseField.setValue(evt.getFileName());
                    if (AddressService.status())
                        geoDatabaseField.setSuffixComponent(AppIcons.OK.icon());
                    else
                        geoDatabaseField.setSuffixComponent(AppIcons.ERROR.icon());

                    dialog.close();
                }
                catch (Exception ex) {
                    log.error("Uploading geo db: {}", ex.toString());
                    ViewUtils.showNotificationError("Can't upload database: " + ex.toString());
                }
            });

        });

        dialog.open();
    }

    private void testEmailDialog() {
        DialogManager dialog = new DialogManager("Test Email");

        List<EmailTemplate> allTemplates = templateDataService.getAll();
        List<EmailTemplate> templateList = new ArrayList<>();
        for (EmailTemplate item : allTemplates) {
            if (ApiEmailTypes.getType(item.getType()) == ApiEmailTypes.TEST) {
                templateList.add(item);
            }
        }

        ComboBox<EmailTemplate> templateCombo = new ComboBox<>("Template");
        templateCombo.setItems(templateList);
        if (!templateList.isEmpty()) {
            templateCombo.setValue(templateList.get(0));

            TextField subjectField = new TextField("Subject");
            subjectField.setRequired(true);
            subjectField.setValue("This is a test");
            subjectField.setWidth("20em");
            subjectField.setValue("Test");

            EmailField toField = new EmailField("To");
            toField.setWidth("20em");
            toField.setRequired(true);
            TextArea messageField = new TextArea("Message");
            messageField.setValue("Hello world !");
            dialog.addToForm(templateCombo);
            dialog.addToForm(toField);
            dialog.addToForm(subjectField);
            dialog.addToForm(messageField);

            Button sendButton = dialog.addMainButton("Send");
            sendButton.addClickListener(e->{

                if (templateCombo.getValue() == null) {
                    ViewUtils.showNotificationError("Template is required");
                    return;
                }

                EmailTemplate selectedTemplate = templateCombo.getValue();

                if (toField.isInvalid()) {
                    ViewUtils.showNotificationError("Email is required");
                    return;
                }

                String email = toField.getValue();
                String subject = subjectField.getValue();
                if (StringUtils.isBlank(email) || StringUtils.isBlank(subject)) {
                    ViewUtils.showNotificationError("Email & subject are required");
                    return;
                }

                String content = messageField.getValue();
                if (StringUtils.isBlank(content)) {
                    content = "This is a test from " + AppGlobal.getInstance().getSiteInfo().getName();
                }

                Mail mail = new Mail();
                mail.setMailTo(email);
                mail.setMailSubject(subject);
                mail.addToModel("CONTENT", content);
                mail.setTemplateID(selectedTemplate.getEtid());
                mail.setType(ApiEmailTypes.getType(selectedTemplate.getType()));
                mail.setIso(AppGlobal.getInstance().getSiteInfo().getCountry_iso());
                EmailService.addEmail(mail, getAccount().getId());
                ViewUtils.showNotification("Test  email sent");
                dialog.close();
            });
        }
        else {
            dialog.addToForm(new NativeLabel("A Test template is required"));
            Button gotoEmailTemplates = dialog.addMainButton("Go to Email Templates");
            gotoEmailTemplates.addClickListener(e->{
                dialog.close();
                UI.getCurrent().navigate(EmailTemplatesView.class);
            });
        }


        dialog.open();
    }
}
