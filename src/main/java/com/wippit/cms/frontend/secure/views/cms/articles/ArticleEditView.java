package com.wippit.cms.frontend.secure.views.cms.articles;

import com.deepl.api.TextResult;
import com.deepl.api.Translator;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.TabSheet;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.content.Article;
import com.wippit.cms.backend.data.bean.content.Section;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.properties.AppProperties;
import com.wippit.cms.backend.data.services.ArticleDataService;
import com.wippit.cms.backend.data.services.StorageDataService;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.services.storage.StorageService;
import com.wippit.cms.backend.types.*;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.base.OWSecureAdapterView;
import com.wippit.cms.frontend.components.DialogManager;
import com.wippit.cms.frontend.components.ResourceDialog;
import com.wippit.cms.frontend.components.ResourceListener;
import com.wippit.cms.frontend.components.ResourceUploadDialog;
import com.wippit.cms.frontend.secure.views.MainLayout;
import io.overcoded.vaadin.tinymce.TinyMceField;
import io.overcoded.vaadin.tinymce.config.TinyMceConfig;
import jakarta.annotation.security.RolesAllowed;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.olli.ClipboardHelper;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Slf4j
@PageTitle("Edit Article")
@Route(value = "cms/article/edit", layout = MainLayout.class)
@RolesAllowed({"AUTHOR", "MANAGER", "EDITOR", "ADMIN"})
@Uses(Icon.class)
public class ArticleEditView extends OWSecureAdapterView implements ResourceListener {

    @Autowired
    private AppProperties appProperties;

    @Autowired
    private ArticleDataService dataService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private StorageDataService storageDataService;

    @Autowired
    private ResourceDialog resourceDialog;

    @Autowired
    private ResourceUploadDialog resourceUploadDialog;

    private Translator translator;

    private String cid = "";
    private TinyMceField editor;
    private TinyMceField introEditor;
    private Article currentArticle;
    private SplitLayout splitLayout;

    private TextField titleField;
    private TextArea tagsField;
    private ComboBox<Account> authorCombo;
    private ComboBox<Section> sectionCombo;
    private ComboBox<Integer> positionCombo;
    private ComboBox<ContentStatusTypes> statusCombo;
    private ComboBox<HomeTypes> homeCombo;
    private DateTimePicker startDateTimePicker;
    private DateTimePicker endDateTimePicker;
    private Image introImage;
    private LocalCountry currentCountry = null;
    private TabSheet tabSheet;
    private Tab tabIntro;
    private Tab tabComplete;
    private Details homeDetails;
    private boolean flagAuthor = false;
    private SectionTypes currentSectionType;

    private final int MARGIN_H = 100;

    @Override
    public boolean setup() {

        cid = (String) UI.getCurrent().getSession().getAttribute("cid");
        if (StringUtils.isBlank(cid)) {
            cid = "";
        }

        currentCountry = (LocalCountry) UI.getCurrent().getSession().getAttribute("country");
        if (currentCountry == null) {
            List<LocalCountry> countries = dataService.getCountries();
            for (LocalCountry country : countries) {
                if (country.getIso().equalsIgnoreCase(AppGlobal.getInstance().getSiteInfo().getCountry_iso())) {
                    currentCountry = country;
                    break;
                }
            }
        }

        loadArticle();

        Account account = getAccount();
        if (account.isAdmin() || account.isManager()) {
            storageDataService.setAlias("");
        }
        else {
            storageDataService.setAlias(getAccount().getAlias());

            if (account.isAuthor()) {
                flagAuthor = true;
            }
        }

        //log.warn("User[{}], Editor={} \tAuthor={}", getAccount().getAlias(), getAccount().isManager(), getAccount().isAuthor());

        resourceUploadDialog.setAlias(getAccount().getAlias());

        try {
            if (!StringUtils.isBlank(AppGlobal.getInstance().getKeys().getTranslator())) {
                translator = new Translator(AppGlobal.getInstance().getKeys().getTranslator());
            } else {
                translator = null;
            }
        }
        catch (Exception ex) {
            translator = null;
            ViewUtils.showNotificationWarning("Can't create traslator");
            log.warn("Can't create translator : {}", ex.toString());
        }

        if (StringUtils.isBlank(AppGlobal.getInstance().getKeys().getEditor())){
            ViewUtils.showNotificationWarning("Editor key is required to edit content");
        }

        fixTitle();

        return true;
    }

    public ArticleEditView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);

        setSizeFull();
        addClassNames("articles-view");
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {

        splitLayout = new SplitLayout();
        splitLayout.setWidth((size.getWidth() - 20) + "px");
        splitLayout.setHeight((size.getHeight() - MARGIN_H) + "px");
        splitLayout.setSplitterPosition(70);

        introEditor = getEditor(size, true);
        introEditor.setValue(currentArticle.getContent_intro());

        editor = getEditor(size, false);
        editor.setValue(currentArticle.getContent_complete());

        tabSheet = new TabSheet();
        tabIntro = tabSheet.add("Intro", introEditor);
        tabComplete = tabSheet.add("Complete", editor);
        tabSheet.setWidthFull();
        tabSheet.setHeightFull();

        splitLayout.addToPrimary(tabSheet);
        splitLayout.addToSecondary(getSettingsLayout());
        add(splitLayout);

        loadArticle();

        SectionTypes sectionType = SectionTypes.getType(currentArticle.getSection_type());
        switch (sectionType) {
            case NOTES -> {
                tabSheet.setSelectedTab(tabComplete);
                tabIntro.setVisible(false);
                homeDetails.setEnabled(false);
            }
            case ARTICLES, HOME -> {
                tabIntro.setVisible(true);
                tabSheet.setSelectedTab(tabComplete);
                homeDetails.setEnabled(true);
            }
        }
    }

    @Override
    public void resizeUI(Size size) {
        splitLayout.setWidth((size.getWidth() - 20) + "px");
        splitLayout.setHeight((size.getHeight() - MARGIN_H) + "px");
        editor.setHeight((size.getHeight() - MARGIN_H - 20) + "px");
    }

    private void fixTitle() {
        currentSectionType = SectionTypes.getType(currentArticle.getSection_type());
        switch (currentSectionType) {
            case HOME, ARTICLES -> setTitle("Article[" + currentArticle.getIso()+"]: " + currentArticle.getTitle());
            case NOTES -> setTitle("Note[" + currentArticle.getIso()+"]: " + currentArticle.getTitle());
        }
    }

    private TinyMceField getEditor(Size size, boolean flagIntro) {

        TinyMceConfig config;
        if (!flagIntro) {
            config = TinyMceConfig.builder()
                    .height((size.getHeight() - MARGIN_H - 50) + "px")
                    .apiKey(AppGlobal.getInstance().getKeys().getEditor())
                    .menubar(List.of("false"))
                    .plugins(List.of("anchor", "lists", "link", "code", "fullscreen", "image", "table", "accordion", "emoticons", "charmap", "preview"))
                    .toolbar(List.of("undo", "redo", "|", "blocks", "|", "styleselect", "removeformat", "|", "bold", "italic", "underline", "bullist", "numlist", "|", "alignleft", "aligncenter", "alignright", "alignjustify", "|", "outdent", "indent", "|", "forecolor","backcolor","|", "table", "accordion", "charmap", "emoticons","link", "image", "code", "preview", "fullscreen"))
                    .build();
        }
        else {
            config = TinyMceConfig.builder()
                    .height((size.getHeight() - MARGIN_H - 50) + "px")
                    .apiKey(AppGlobal.getInstance().getKeys().getEditor())
                    .menubar(List.of("false"))
                    .plugins(List.of("anchor", "lists", "link", "code", "fullscreen"))
                    .toolbar(List.of("undo", "redo", "|", "blocks", "|", "styleselect", "removeformat", "|", "bold", "italic", "underline", "|", "bullist", "numlist", "|", "alignleft", "aligncenter", "alignright", "alignjustify", "|", "outdent", "indent", "code", "fullscreen"))
                    .build();
        }

        TinyMceField editor = new TinyMceField(config);
        editor.setWidthFull();
        editor.setHeight((size.getHeight() - MARGIN_H - 20) + "px");

        return editor;
    }

    private VerticalLayout getSettingsLayout() {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();

        Button saveButton = ViewUtils.getToolbarButton(AppIcons.FILE_SAVE, "Save article");
        Button deleteButton = ViewUtils.getToolbarButton(AppIcons.TRASH, "Delete article");
        Button resourcesButton = ViewUtils.getToolbarButton(AppIcons.RESOURCES, "View resources");
        Button uploadResourceButton = ViewUtils.getToolbarButton(AppIcons.CLOUD_UPLOAD, "Upload resource");
        Button copyToLocaleButton = ViewUtils.getToolbarButton(AppIcons.FILE_COPY, "Duplicate & translate article to locale");
        Button translateButton = ViewUtils.getToolbarButton(AppIcons.TRANSLATIONS, "Translate to " + currentCountry.getIso());

        HorizontalLayout toolbar = ViewUtils.getToolbar(ToolbarTypes.HEADER);
        toolbar.add(
                saveButton,
                resourcesButton,
                uploadResourceButton,
                copyToLocaleButton,
                translateButton,
                ViewUtils.getSpacer(80),
                deleteButton
        );
        layout.add(toolbar);
        layout.add(ViewUtils.getLineSecondary());

        saveButton.addClickListener(e -> saveArticle());
        deleteButton.addClickListener(e -> deleteArticle());
        resourcesButton.addClickListener(e -> viewResources());
        uploadResourceButton.addClickListener(e -> uploadResource());
        copyToLocaleButton.addClickListener(e -> copyToLocaleDialog());
        translateButton.addClickListener(e -> translateContentDialog());

        if (translator == null) {
            translateButton.setEnabled(false);
        }

        FormLayout formLayout = new FormLayout();

        titleField = new TextField("Title");
        titleField.setValue(currentArticle.getTitle());

        authorCombo = new ComboBox<>("Author");
        Collection<Account> authors = dataService.getAuthors();
        authorCombo.setItems(authors);
        for (Account account : authors) {
            if (account.getAlias().equalsIgnoreCase(currentArticle.getAuthor())) {
                authorCombo.setValue(account);
                break;
            }
        }
        if (flagAuthor) {
            log.warn("Author combo -> READ-ONLY");
            authorCombo.setReadOnly(true);
        }

        sectionCombo = new ComboBox<>("Section");
        Collection<Section> sections = dataService.getSections();
        sectionCombo.setItems(sections);
        Section currentSection = null;
        for (Section section : sections) {
            if (section.getSecID().equalsIgnoreCase(currentArticle.getSecID())) {
                sectionCombo.setValue(section);
                currentSection = section;
                break;
            }
        }

        homeCombo = new ComboBox<>("Home Page");
        homeCombo.setItems(HomeTypes.toList());
        homeCombo.setValue(HomeTypes.getType(currentArticle.getHome()));

        positionCombo = new ComboBox<>("Position");
        List<Integer> positions = new ArrayList<>();
        for (int pos = 1; pos <= 20; pos++) {
            positions.add(pos);
        }
        positionCombo.setItems(positions);
        if (currentArticle.getHome() > 0) {
            if (currentArticle.getHomeIndex() < 1) {
                currentArticle.setHomeIndex(1);
            }
            if (currentArticle.getHomeIndex() > 20) {
                currentArticle.setHomeIndex(20);
            }
            positionCombo.setValue(currentArticle.getHomeIndex());
            positionCombo.setVisible(true);
        } else {
            positionCombo.setValue(20);
            positionCombo.setVisible(false);
        }

        statusCombo = new ComboBox<>("Status");
        statusCombo.setItems(ContentStatusTypes.toEditList());
        statusCombo.setValue(ContentStatusTypes.getType(currentArticle.getStatus()));

        startDateTimePicker = new DateTimePicker("Starts on");
        startDateTimePicker.setValue(AppUtils.timestampToSiteDateTime(currentArticle.getStart_date()));

        endDateTimePicker = new DateTimePicker("Ends on");
        endDateTimePicker.setValue(AppUtils.timestampToSiteDateTime(currentArticle.getEnd_date()));

        Details tagsDetails = new Details("Tags");
        tagsField = new TextArea("Tags");
        tagsField.setValue(currentArticle.getTags());
        tagsField.setWidthFull();
        tagsDetails.add(tagsField);

        introImage = new Image();
        introImage.setWidthFull();
        introImage.addClassName("image_box");

        if (currentArticle.getImage_url().startsWith("http")) {
            introImage.setSrc(currentArticle.getImage_url());
        } else {
            Storage storage = dataService.getStorageImage(currentArticle.getImage_fid());
            if (storage != null) {
                introImage.setSrc(storage.getCompleteURL());
                log.debug("Image url: {}", storage.getCompleteURL());
            }
        }

        //---
        //Form
        //---
        formLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 2));

        formLayout.add(titleField);
        formLayout.setColspan(titleField, 2);
        formLayout.add(authorCombo);
        formLayout.setColspan(authorCombo, 2);
        formLayout.add(statusCombo);
        formLayout.add(sectionCombo);

        homeDetails = new Details("Home Page");
        homeDetails.add(homeCombo);
        homeDetails.add(positionCombo);
        formLayout.add(homeDetails);

        Details timeDetails = new Details("Calendar");
        timeDetails.add(startDateTimePicker);
        timeDetails.add(endDateTimePicker);
        formLayout.add(timeDetails);

        formLayout.add(tagsDetails);
        formLayout.setColspan(tagsDetails, 2);

        Details bannerDetails = new Details("Banner");
        Button selectIntroImageButton = ViewUtils.getToolbarButton(AppIcons.RESOURCES, "Select intro image");
        selectIntroImageButton.addClickListener(e->{
            resourceDialog.setTitle("Select intro image");
            resourceDialog.setAllowedTypes(StorageTypes.IMAGE_POST);
            resourceDialog.setSize(getCurrentSize().getSmallerSize(250));
            resourceDialog.addResourceAddedListener(this);
            resourceDialog.open();
        });

        Button uploadIntroImageButton = ViewUtils.getToolbarButton(AppIcons.CLOUD_UPLOAD, "Upload intro image");
        uploadIntroImageButton.addClickListener(e->{
            resourceUploadDialog.setTitle("Upload intro image");
            resourceUploadDialog.setAllowedTypes(StorageTypes.IMAGE_POST);
            resourceUploadDialog.setSize(getCurrentSize().getSmallerSize(250));
            resourceUploadDialog.addResourceAddedListener(this);
            resourceUploadDialog.open();
        });

        HorizontalLayout buttons = new HorizontalLayout();
        buttons.add(selectIntroImageButton, uploadIntroImageButton);
        bannerDetails.add(buttons);
        bannerDetails.add(introImage);

        formLayout.add(bannerDetails);
        formLayout.setColspan(bannerDetails, 2);

        layout.add(formLayout);

        sectionChanged(currentSection);

        sectionCombo.addValueChangeListener( e->{
            Section section = e.getValue();

            sectionChanged(section);

            if (section == null) {
                return;
            }

            log.debug("Section secID: {}", section.getSecID());

            currentArticle.setSecID(section.getSecID());
            currentArticle.setSection_type(section.getType());

            fixTitle();

            SectionTypes sectionType = SectionTypes.getType(section.getType());
            switch (sectionType) {
                case NOTES -> {
                    tabSheet.setSelectedTab(tabComplete);
                    tabIntro.setVisible(false);
                }
                case ARTICLES, HOME -> {
                    tabIntro.setVisible(true);
                    tabSheet.setSelectedTab(tabComplete);
                }
            }
        });
        homeCombo.addValueChangeListener(e->{
            if (e.getValue() == null || e.getValue() == HomeTypes.NOT_IN_HOME) {
                positionCombo.setVisible(false);
                currentArticle.setHomeIndex(0);
            }
            else {
                positionCombo.setVisible(true);
                if (positionCombo.getValue()>0) {
                    currentArticle.setHomeIndex(positionCombo.getValue());
                }
            }
        });
        positionCombo.addValueChangeListener(e->{
            if (e.getValue() != null) {
                currentArticle.setHomeIndex(e.getValue());
            }
        });
        authorCombo.addValueChangeListener(e->{
            log.debug("Author -> {}", e.getValue().getAlias());
        });
        return layout;
    }

    private void loadArticle() {

        currentArticle = dataService.getArticleByCID(cid);

        if (currentArticle.getAuthor().isEmpty()) {
            currentArticle.setAuthor(getAccount().getAlias());
            currentArticle.setIso(currentCountry.getIso());
        }

        if (currentArticle.getHome() > 0) {
            currentArticle.setHomeIndex(dataService.getHomeIndex(cid));
        }
    }

    private void sectionChanged(Section section) {

        if (section == null) {
            homeDetails.setEnabled(false);
            return;
        }

        currentSectionType = SectionTypes.getType(section.getType());
        if (currentSectionType != SectionTypes.ARTICLES) {
            homeDetails.setEnabled(false);
        }
        else {
            homeDetails.setEnabled(true);
        }
    }

    private void saveArticle(){

        //---
        //Validations
        //---
        String title = titleField.getValue();
        if (StringUtils.isBlank(title)) {
            ViewUtils.showNotificationError("Title is required");
            return;
        }
        if (!dataService.validateTitle(title, currentArticle.getCid())) {
            ViewUtils.showNotificationError("Title already exists. Change title is required.");
            return;
        }

        if (sectionCombo.getValue() != null) {
            currentArticle.setSecID(sectionCombo.getValue().getSecID());
            currentArticle.setSection_url(sectionCombo.getValue().getFullURL());
        }

        if (homeCombo.getValue() != null) {
            currentArticle.setHome(homeCombo.getValue().getType());
        }

        if (authorCombo.getValue() != null) {
            currentArticle.setAuthor(authorCombo.getValue().getAlias());
        }

        if (statusCombo.getValue() != null) {
            currentArticle.setStatus(statusCombo.getValue().value());
        }

        if (startDateTimePicker.getValue() != null) {
            currentArticle.setStart_date(AppUtils.siteDateToTimestamp(startDateTimePicker.getValue()));
        }

        if (endDateTimePicker.getValue()!= null) {
            currentArticle.setEnd_date(AppUtils.siteDateToTimestamp(endDateTimePicker.getValue()));
        }

        if (currentArticle.getEnd_date().before(currentArticle.getStart_date())) {
            ViewUtils.showNotificationError("Start/end dates are invalid. Change start/end date is required.");
            return;
        }

        if (StringUtils.isBlank(tagsField.getValue())) {
            currentArticle.setTags(tagsField.getValue().trim());
        }

        String content = editor.getContent();
        if (StringUtils.isBlank(content)) {
            ViewUtils.showNotificationError("Complete is required");
            return;
        }
        currentArticle.setContent_complete(content.trim());

        content = introEditor.getContent();
        if (StringUtils.isBlank(content)) {
            ViewUtils.showNotificationError("Intro is required");
            return;
        }
        currentArticle.setContent_intro(content.trim());

        if (Objects.requireNonNull(currentSectionType) == SectionTypes.NOTES) {
            currentArticle.setContent_intro(currentArticle.getContent_complete());
        }

        currentArticle.setTitle(title);
        String url = URLEncoder.encode(title, StandardCharsets.US_ASCII);
        currentArticle.setUrl(url);

        boolean saved = dataService.saveArticle(currentArticle);
        if (saved) {
            ViewUtils.showNotification("Article saved");
            UI.getCurrent().navigate(ArticlesView.class);
        }
        else {
            ViewUtils.showNotificationError("Can't save article");
        }
    }

    private void deleteArticle() {
        DialogManager dialog = new DialogManager("Remove article");
        dialog.addToForm(ViewUtils.getWarningLabel("Confirm to remove article: [" + currentArticle.getIso()+"] " + currentArticle.getTitle()));
        Button button = dialog.addDangerButton("Remove");
        Button cancelButton = dialog.addMainButton("Cancel");
        button.addClickListener(e->{
            if (dataService.removeArticle(currentArticle)) {
                ViewUtils.showNotification("Article removed: " + currentArticle.getTitle());
                UI.getCurrent().navigate(ArticlesView.class);
                dialog.close();
            }
            else {
                ViewUtils.showNotificationError("Can't remove article");
            }
        });
        cancelButton.addClickListener(e->dialog.close());
        dialog.open();
    }

    @Override
    public void resourceUploaded(Storage storage) {
        currentArticle.setImage_fid(storage.getFid());
        currentArticle.setImage_url(storage.getPartialURL());
        introImage.setSrc(storage.getCompleteURL());
        log.debug("Intro image url: {}", storage.getCompleteURL());
    }

    @Override
    public void resourceSelected(Storage storage) {
        currentArticle.setImage_fid(storage.getFid());
        currentArticle.setImage_url(storage.getPartialURL());
        introImage.setSrc(storage.getCompleteURL());
        log.debug("Selected Intro image url: {}", storage.getCompleteURL());
    }

    private void viewResources() {
        resourceDialog.setAlias(getAccount().getAlias());
        resourceDialog.setSize(getCurrentSize().getSmallerSize(250));
        resourceDialog.open();
    }

    private void uploadResource() {
        resourceUploadDialog.setTitle("Upload resource");
        resourceUploadDialog.setAllowedTypes(StorageTypes.ALL);
        resourceUploadDialog.setSize(getCurrentSize().getSmallerSize(250));
        resourceUploadDialog.removeResourceListener(this);
        resourceUploadDialog.open();
    }

    private void copyToLocaleDialog() {
        DialogManager dialog = new DialogManager("Copy Article to Locale");

        ComboBox<LocalCountry> countryCombo = new ComboBox<>("Locale");
        countryCombo.setWidth("20em");
        countryCombo.setItems(dataService.getCountries());

        boolean flagTranslationEnabled = (translator !=null);
        Checkbox titleCheck = new Checkbox("Translate title");
        Checkbox introCheck = new Checkbox("Translate intro");
        Checkbox contentCheck = new Checkbox("Translate complete");
        if (!flagTranslationEnabled) {
            titleCheck.setReadOnly(true);
            introCheck.setReadOnly(true);
            contentCheck.setReadOnly(true);
        }

        dialog.addToForm(countryCombo);
        dialog.addToForm(titleCheck);
        dialog.addToForm(introCheck);
        dialog.addToForm(contentCheck);

        Button buttonCopy = dialog.addMainButton("Copy");
        buttonCopy.addClickListener(e->{
            if (countryCombo.getValue() == null) {
                return;
            }

            if (copyToLocale(countryCombo.getValue(), titleCheck.getValue(), introCheck.getValue(), contentCheck.getValue())) {
                dialog.close();
            }
        });
        dialog.open();
    }

    private boolean copyToLocale(LocalCountry country, boolean translateTitle, boolean translateIntro, boolean translateComplete) {

        Article newArticle = new Article(currentArticle, country.getIso());
        newArticle.setOcid(currentArticle.getOcid());

        try {
            if (translateTitle) {
                TextResult result = translator.translateText(newArticle.getTitle(), null, country.getLocale());
                newArticle.setTitle(result.getText());
            }

            String title = dataService.getNewTitle(newArticle.getTitle());
            newArticle.setTitle(title);
            newArticle.setUrl(URLEncoder.encode(title, StandardCharsets.US_ASCII));

            if (translateIntro) {
                TextResult result = translator.translateText(newArticle.getContent_intro(), null, country.getLocale());
                newArticle.setContent_intro(result.getText());
            }
            if (translateComplete) {
                TextResult result = translator.translateText(newArticle.getContent_complete(), null, country.getLocale());
                newArticle.setContent_complete(result.getText());
            }
        }
        catch (Exception ex) {
            log.error("While translating: {}", ex.toString());
            ViewUtils.showNotificationWarning("Can't translate some data");
        }

        boolean saved = dataService.saveArticle(newArticle);
        if (saved) {
            ViewUtils.showNotification("Article copied to " + country.getIso() + " as: " + newArticle.getTitle());
            return true;
        }
        else {
            ViewUtils.showNotificationError("Can't save article");
            return false;
        }
    }

    private void translateContentDialog() {

        if (currentCountry == null) {
            return;
        }

        DialogManager dialog = new DialogManager("Translate to " + currentCountry.getIso());
        dialog.setResizableAndDraggable();

        TextArea sourceField = new TextArea("Source");
        sourceField.setWidthFull();
        sourceField.setHeight("10em");
        TextArea translateField = new TextArea("Translated");
        translateField.setWidthFull();
        translateField.setHeight("10em");

        dialog.addToForm(sourceField);
        dialog.addToForm(translateField);


        Button buttonCopy = ViewUtils.getToolbarButton(AppIcons.FILE_COPY, "Copy to clipboard");
        buttonCopy.addClickListener(e->dialog.close());
        ClipboardHelper clipboardHelper = new ClipboardHelper("", buttonCopy);
        dialog.addComponentToFooter(clipboardHelper);

        Button translateButton = dialog.addMainButton("Translate");
        translateButton.addClickListener(e->{
            try {
                String source = sourceField.getValue();
                if (!StringUtils.isBlank(source)) {
                    TextResult result = translator.translateText(source, null, currentCountry.getLocale());
                    translateField.setValue(result.getText());
                    clipboardHelper.setContent(result.getText());
                }
            }
            catch (Exception ex) {
                log.error("On translation: {}", ex.toString());
                ViewUtils.showNotificationError("Can't translate");
            }
        });


        dialog.open();
    }


}
