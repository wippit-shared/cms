package com.wippit.cms.frontend.components.dialog;

public interface DialogBoxListener {
    public void dialogConfirm();
    public void dialogCanceled();
    public void dialogYes();
    public void dialogNo();
    public void dialogDelete();
    public void dialogClosed();
}
