package com.wippit.cms.frontend.components.dialog;

import com.vaadin.flow.component.button.Button;
import com.wippit.cms.backend.types.DialogTypes;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.components.DialogManager;

import java.util.ArrayList;
import java.util.List;

public class DialogBox {
    private DialogManager dialog;
    private DialogTypes type = DialogTypes.MESSAGE;
    private final List<DialogBoxListener> listeners = new ArrayList<>();
    private boolean result = false;

    public DialogBox() {}

    public void showMessage(String title, String message) {
        type = DialogTypes.MESSAGE;

        dialog = new DialogManager(title);
        dialog.getContainer().add(ViewUtils.getDivLabel(message));
        Button button = dialog.addMainButton("Ok");
        button.addClickListener(e->doListeners());
        dialog.open();
    }

    public void showDelete(String title, String message) {
        type = DialogTypes.DELETE;

        dialog = new DialogManager(title);
        dialog.getContainer().add(ViewUtils.getDivLabel(message));
        Button deleteButton = dialog.addDangerButton("Remove");
        deleteButton.addClickListener(e->{
            result = true;
            doListeners();
        });
        Button cancelButton = dialog.addMainButton("Cancel");
        cancelButton.addClickListener(e->doListeners());

        dialog.open();
    }

    public void showConfirm(String title, String message) {
        type = DialogTypes.CONFIRM;

        dialog = new DialogManager(title);
        dialog.getContainer().add(ViewUtils.getDivLabel(message));

        Button cancelButton = dialog.addSecondaryButton("Cancel");
        cancelButton.addClickListener(e->doListeners());

        Button confirmButton = dialog.addMainButton("Confirm");
        confirmButton.addClickListener(e->{result = true;doListeners();});

        dialog.open();
    }

    public void showYesNo(String title, String message, String messageYes, String messageNo) {
        type = DialogTypes.YES_NO;

        dialog = new DialogManager(title);
        dialog.getContainer().add(ViewUtils.getDivLabel(message));

        Button yesButton = dialog.addMainButton(messageYes);
        yesButton.addClickListener(e->{
            result = true;
            doListeners();
        });

        Button noButton = dialog.addDangerButton(messageNo);
        noButton.addClickListener(e->{result = true;doListeners();});

        dialog.open();
    }

    public void addListener(DialogBoxListener newListener) {
        for (DialogBoxListener listener:listeners) {
            if (listener.equals(newListener)) {
                return;
            }
        }

        listeners.add(newListener);
    }

    public void removeListener(DialogBoxListener removeListener) {
        for (DialogBoxListener listener:listeners) {
            if (listener.equals(removeListener)) {
                listeners.remove(removeListener);
                return;
            }
        }
    }

    public void doListeners() {
        for (DialogBoxListener listener:listeners) {
            switch (type) {
                case MESSAGE -> dialog.close();

                case DELETE -> {
                    listener.dialogDelete();
                    dialog.close();
                }

                case YES_NO -> {
                    if (result) {
                        listener.dialogYes();
                    }
                    else {
                        listener.dialogNo();
                    }
                    dialog.close();
                }

                case CONFIRM -> {
                    if (result) {
                        listener.dialogConfirm();
                    } else {
                        listener.dialogCanceled();
                    }
                    dialog.close();
                }
            }
        }

    }
}
