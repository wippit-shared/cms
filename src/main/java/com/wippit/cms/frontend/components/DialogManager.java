package com.wippit.cms.frontend.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.wippit.cms.backend.utils.ViewUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DialogManager {
    private VerticalLayout container;
    private Dialog dialog;
    private ProgressBar progressBar = null;
    private FormLayout formLayout;
    private Button buttonClose;

    public final static int HEADER_H = 50;
    public final static int FOOTER_H = 80;
    public final static int DEFAULT_H = 300;
    public final static int DEFAULT_W = 400;
    public final static int MARGIN = 50;

    /**
     * Creates a dialog with width
     * @param width dialog width
     * @param title dialog title
     */
    public DialogManager(int width, String title) {
        this(width, title, true);
    }

    /**
     * Creates a dialog with width and closable option
     * @param width dialog width
     * @param title dialog title
     * @param closable closable option
     */
    public DialogManager(int width, String title, boolean closable) {

        dialog = new Dialog();
        if (closable) {
            dialog.setCloseOnEsc(true);
            dialog.setCloseOnOutsideClick(true);
        }
        else {
            dialog.setCloseOnEsc(false);
            dialog.setCloseOnOutsideClick(false);
        }
        dialog.getElement().setAttribute("padding","0px");
        dialog.setWidth(width + "px");

        dialog.setHeaderTitle(title);

        if (closable) {
            buttonClose = ViewUtils.getToolbarButton(VaadinIcon.CLOSE_BIG.create());
            buttonClose.getElement().getStyle().set("margin-left", "auto");
            buttonClose.addClickListener(e -> {
                dialog.close();
            });
            dialog.getHeader().add(buttonClose);
        }

        container = new VerticalLayout();
        container.setMargin(false);
        container.setPadding(false);
        container.setClassName("dialog_container");
        dialog.add(container);
    }

    /**
     * Creates a dialog with WIDTH x HEIGHT and title
     * @param width dialog width
     * @param height dialog height
     * @param title dialog title
     */
    public DialogManager(int width, int height, String title) {
        this(width, height, title, true);
    }

    /**
     * Creates a dialog with size and closable option
     * @param width dialog width
     * @param height dialog height
     * @param title dialog title
     * @param closable closable option
     */
    public DialogManager(int width, int height, String title, boolean closable) {

        dialog = new Dialog();
        if (closable) {
            dialog.setCloseOnEsc(true);
            dialog.setCloseOnOutsideClick(true);
        }
        else {
            dialog.setCloseOnEsc(false);
            dialog.setCloseOnOutsideClick(false);
        }
        dialog.getElement().setAttribute("padding","0px");
        dialog.setWidth(width + "px");
        dialog.setHeight(height + "px");

        dialog.setHeaderTitle(title);

        if (closable) {
            buttonClose = ViewUtils.getToolbarButton(VaadinIcon.CLOSE_BIG.create());
            buttonClose.getElement().getStyle().set("margin-left", "auto");
            buttonClose.addClickListener(e -> {
                dialog.close();
            });
            dialog.getHeader().add(buttonClose);
        }

        container = new VerticalLayout();
        container.setMargin(false);
        container.setPadding(false);
        container.setClassName("dialog_container");
        container.setHeight((height - HEADER_H - MARGIN) + "px");
        dialog.add(container);
    }

    /**
     * Creates a dialog with title
     * @param title title for dialog
     */
    public DialogManager(String title) {
        this(title, true);
    }

    /**
     * Creates a dialog
     * @param title title for dialog
     * @param closable Closable option
     */
    public DialogManager(String title, boolean closable) {

        dialog = new Dialog();

        if (closable) {
            dialog.setCloseOnEsc(true);
            dialog.setCloseOnOutsideClick(true);
        }
        else {
            dialog.setCloseOnEsc(false);
            dialog.setCloseOnOutsideClick(false);
        }

        dialog.getElement().setAttribute("padding","0px");
        dialog.setHeaderTitle(title);

        if (closable) {
            addCloseButton();
        }

        container = new VerticalLayout();
        container.setMargin(false);
        container.setPadding(false);
        container.setClassName("dialog_container");
        container.setSizeFull();
        dialog.add(container);
    }

    public void addCloseButton() {
        Button buttonClose = ViewUtils.getToolbarButton(VaadinIcon.CLOSE_BIG.create());
        buttonClose.getElement().getStyle().set("margin-left", "auto");
        buttonClose.addClickListener(e -> {
            dialog.close();
        });
        dialog.getHeader().add(buttonClose);
    }

    public VerticalLayout getContainer() {
        return container;
    }

    public FormLayout addToForm(Component component) {
        if (formLayout == null) {
            formLayout = new FormLayout();
            formLayout.addClassName("dialog_form");
            container.add(formLayout);
        }

        formLayout.add(component);
        return formLayout;
    }

    public FormLayout addToForm(Component component, int formHeight) {
        if (formLayout == null) {
            formLayout = new FormLayout();
            formLayout.addClassName("dialog_form");
            formLayout.getStyle().set("max-height",(formHeight - HEADER_H - FOOTER_H - MARGIN) + "px");
            container.add(formLayout);
        }

        formLayout.add(component);
        return formLayout;
    }

    public void addProgressBar(){
        progressBar = new ProgressBar();
        progressBar.setIndeterminate(true);
        getContainer().add(progressBar);
    }

    public void hideProgressBar(){
        if (progressBar != null) {
            progressBar.setVisible(false);
        }
    }

    public FormLayout getForm() {
        return formLayout;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public void open() {
        dialog.open();
    }

    public void close() {
        dialog.close();
    }

    public void setResizableAndDraggable() {
        dialog.setDraggable(true);
        dialog.setResizable(true);
    }

    public void setModal() {
        dialog.setCloseOnEsc(false);
        dialog.setCloseOnOutsideClick(false);
    }

    public Button addMainButton(String title) {
        Button button = ViewUtils.getActiveButton(title);
        button.getElement().getStyle().set("margin-left", "auto");
        dialog.getFooter().add(button);
        return button;
    }

    public Button addSecondaryButton(String title) {
        Button button = ViewUtils.getButton(title);
        dialog.getFooter().add(button);
        return button;
    }

    public Button addDangerButton(String title) {
        Button button = ViewUtils.getDangerButton(title);
        dialog.getFooter().add(button);
        return button;
    }

    public void addComponentToFooter(Component component) {
        dialog.getFooter().add(component);
    }
}
