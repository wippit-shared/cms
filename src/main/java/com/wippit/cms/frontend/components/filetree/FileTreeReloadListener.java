package com.wippit.cms.frontend.components.filetree;

public interface FileTreeReloadListener {
    void reloadRequired();
}
