package com.wippit.cms.frontend.components.filetree;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Focusable;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.editor.Editor;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.data.provider.hierarchy.TreeDataProvider;
import com.vaadin.flow.function.ValueProvider;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Slf4j
public class FileTree extends VerticalLayout {

    public interface SelectedHandler {
        void handle(File file);
    }

    public interface FileFilter {
        boolean accept(File file);
    }

    private final Tree<FileTreeFileWrapper> filesTree;

    private static class Tree<T> extends TreeGrid<T> {

        Tree(ValueProvider<T, ?> valueProvider) {
            Column<T> only = addHierarchyColumn(valueProvider);
            only.setAutoWidth(true);
        }
    }

    public FileTree(String rootDir) {
        this(rootDir, false);
    }

    public FileTree(String rootDir, boolean canRename) {
        this(rootDir, canRename, null, null);
    }

    public FileTree(String rootDir, boolean canRename, SelectedHandler selectedHandler, FileTreeReloadListener reloadListener) {
        final FileTreeFileWrapper root = new FileTreeFileWrapper(new File(rootDir));
        root.setReloadListener(reloadListener);

        this.filesTree = new Tree<>(FileTreeFileWrapper::getName);

        Binder<FileTreeFileWrapper> binder = new Binder<>();
        Editor<FileTreeFileWrapper> editor = filesTree.getEditor();
        editor.setBinder(binder);

        filesTree.setItems(Collections.singleton(root), this::getFiles);
        filesTree.setWidthFull();
        filesTree.setHeight("300px");
        if (selectedHandler == null) {
            filesTree.setSelectionMode(Grid.SelectionMode.NONE);
        }
        TextField editorTextField = new TextField();
        editorTextField.setWidthFull();
        filesTree
                .getColumns()
                .stream()
                .findFirst()
                .ifPresent(
                        fileColumn -> {
                            fileColumn.setComparator(Comparator.naturalOrder());
                            GridSortOrder<FileTreeFileWrapper> sortOrder = new GridSortOrder<>(fileColumn, SortDirection.ASCENDING);
                            filesTree.sort(Collections.singletonList(sortOrder));

                            if (selectedHandler != null) {
                                filesTree
                                        .asSingleSelect()
                                        .addValueChangeListener(
                                                event -> {
                                                    if (event.getValue() == null) {
                                                        return;
                                                    }

                                                    File file = event.getValue().getWrappedFile();
                                                    selectedHandler.handle(file);
                                                }
                                        );
                            }

                            if (canRename) {
                                fileColumn.setEditorComponent(editorTextField);
                                filesTree.addItemDoubleClickListener(e -> {

                                    if (e.getItem() == null) {
                                        return;
                                    }

                                    editor.editItem(e.getItem());
                                    Component editorComponent = e.getColumn().getEditorComponent();
                                    if (editorComponent instanceof Focusable) {
                                        ((Focusable<?>) editorComponent).focus();
                                    }
                                    editorTextField.setValue(e.getItem().getName());
                                });
                                editorTextField.getElement().addEventListener("keydown", event -> editor.cancel()).setFilter("event.key === 'Escape' || event.key === 'Esc'");

                                editorTextField.addBlurListener(e -> {

                                    if (editor.getItem() == null) {
                                        return;
                                    }

                                    editor.getItem().setName(editorTextField.getValue());
                                    editor.closeEditor();
                                });
                            }

                            this.add(filesTree);
                            setSizeFull();
                        }
                );

        filesTree.expand(root);
    }

    public void setFileNameFilter(FileFilter fileFilter){
        ((TreeDataProvider<FileTreeFileWrapper>) this.filesTree.getDataProvider()).setFilter( fileFilter == null? null : fileWrapper -> fileFilter.accept(fileWrapper.getWrappedFile()));
    }

    private List<FileTreeFileWrapper> getFiles(FileTreeFileWrapper parent) {
        if (parent.isDirectory()) {
            FileTreeFileWrapper[] list = parent.listFiles();
            if (list != null) {
                return Arrays.asList(list);
            }
        }

        return Collections.emptyList();
    }
}