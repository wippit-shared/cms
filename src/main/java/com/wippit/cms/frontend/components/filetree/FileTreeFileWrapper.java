package com.wippit.cms.frontend.components.filetree;

import com.wippit.cms.backend.utils.ViewUtils;
import lombok.Getter;
import lombok.Setter;

import java.io.File;

public class FileTreeFileWrapper  implements Comparable<FileTreeFileWrapper> {
    @Getter
    private File wrappedFile;

    @Setter
    @Getter
    private static FileTreeReloadListener reloadListener = null;

    public FileTreeFileWrapper(File toBeWrapped) {
        this.wrappedFile = toBeWrapped;
    }

    public boolean isDirectory() {
        return wrappedFile.isDirectory();
    }

    public String getName() {
        return this.wrappedFile.getName();
    }

    public void setName(String newName) {
        String parent = wrappedFile.getParent();
        File renameTo;
        if (parent != null) {
            renameTo = new File(parent + File.separatorChar + newName);
        } else {
            renameTo = new File(newName);
        }

        if (renameTo.exists()) {
            ViewUtils.showNotificationError("File already exists");
            return;
        }

        if (wrappedFile.renameTo(renameTo)) {
            wrappedFile = renameTo;
            ViewUtils.showNotification("File renamed to: " + renameTo.getName());
                if (renameTo.isDirectory()) {
                    if (reloadListener != null) {
                        reloadListener.reloadRequired();
                    }
                }
        } else {
            ViewUtils.showNotificationError("Can't rename file");
        }

    }

    public FileTreeFileWrapper[] listFiles() {
        File[] listing = wrappedFile.listFiles();
        if (listing == null) {
            return null;
        } else {
            FileTreeFileWrapper[] retVal = new FileTreeFileWrapper[listing.length];
            for (int i = 0; i < listing.length; i++) {
                retVal[i] = new FileTreeFileWrapper(listing[i]);
            }
            return retVal;
        }
    }

    @Override
    public int compareTo(FileTreeFileWrapper o) {
        return this.wrappedFile.compareTo(o.wrappedFile);
    }
}
