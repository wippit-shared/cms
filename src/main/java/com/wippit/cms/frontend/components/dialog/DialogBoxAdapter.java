package com.wippit.cms.frontend.components.dialog;

public class DialogBoxAdapter implements DialogBoxListener{

    @Override
    public void dialogConfirm() {

    }

    @Override
    public void dialogCanceled() {

    }

    @Override
    public void dialogYes() {

    }

    @Override
    public void dialogNo() {

    }

    @Override
    public void dialogDelete() {

    }

    @Override
    public void dialogClosed() {

    }
}
