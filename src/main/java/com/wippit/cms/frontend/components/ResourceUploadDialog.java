package com.wippit.cms.frontend.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.upload.Receiver;
import com.vaadin.flow.component.upload.Upload;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.services.StorageDataService;
import com.wippit.cms.backend.services.storage.StorageService;
import com.wippit.cms.backend.types.StorageTypes;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import de.f0rce.cropper.Cropper;
import de.f0rce.cropper.settings.CropperSettings;
import de.f0rce.cropper.settings.enums.ViewMode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;


@Slf4j
@Component
public class ResourceUploadDialog {

    @Getter
    @Setter
    private StorageTypes allowedTypes = StorageTypes.ALL;
    @Getter
    @Setter
    private String title;
    @Getter
    @Setter
    private String alias;
    @Getter
    @Setter
    private Size size = new Size(400, 400);

    private List<ResourceListener> listeners = new ArrayList<>();

    private DialogManager dialog;
    private StorageTypes selectedType = StorageTypes.IMAGE_OTHER;
    private final Tika tika;

    @Autowired
    private StorageService storageService;

    @Autowired
    private StorageDataService storageDataService;

    public ResourceUploadDialog() {
        this.title = "Resources";
        tika = new Tika();
    }

    public void open(){
        setupDialog();
    }

    private void setupDialog() {
        dialog = new DialogManager(size.getWidth(), size.getHeight(), title);
        dialog.setResizableAndDraggable();

        Upload upload = setupUpload();
        upload.setAcceptedFileTypes(StringUtils.join(selectedType.getMimeList(), ","));

        if (allowedTypes == StorageTypes.ALL) {
            ComboBox<StorageTypes> storageTypeCombo = new ComboBox<>("Upload type");
            storageTypeCombo.setItems(StorageTypes.toUploadList());
            storageTypeCombo.setValue(selectedType);
            storageTypeCombo.setWidthFull();
            storageTypeCombo.addValueChangeListener(e-> {

                if (e.getValue() == null) {
                    selectedType = StorageTypes.ALL;
                } else {
                    selectedType = e.getValue();
                }

                upload.setAcceptedFileTypes(StringUtils.join(selectedType.getMimeList(), ","));
            });

            dialog.getContainer().add(storageTypeCombo);
        }
        else {
            selectedType = allowedTypes;
        }

        dialog.getContainer().add(upload);
        dialog.open();
    }

    private Upload setupUpload() {

        //---
        //Upload
        //---
        ByteArrayOutputStream os = new ByteArrayOutputStream(65536);
        class FileReceiver implements Receiver {

            @Override
            public OutputStream receiveUpload(String fileName, String mimeType) {
                os.reset();
                return os;
            }
        }

        Upload upload = new Upload(new FileReceiver());
        upload.setAutoUpload(true);
        upload.addFileRejectedListener(e->{
            ViewUtils.showNotificationWarning("File not accepted");
        });
        upload.addFailedListener(e->{
            ViewUtils.showNotificationError("Can't upload file");
        });
        upload.addSucceededListener(
                evt -> {
                    log.debug("Upload succeeded: {}", evt.getFileName());

                    Size cropSize = selectedType.getCropSize();
                    boolean flagCrop = true;
                    if (evt.getFileName().endsWith(".svg") || cropSize == null) {
                        flagCrop = false;
                    }

                    log.debug("Crop file: {}", flagCrop);
                    try {
                        byte[] data = os.toByteArray();
                        if (flagCrop) {
                            addCropAndSaveToDialog(data, evt.getMIMEType(), evt.getFileName(), cropSize);
                        } else {
                            addSaveToDialog(data, evt.getMIMEType(), evt.getFileName());
                        }
                    } catch (Exception ex) {
                        log.error("Uploading file[{}]: {}", evt.getFileName(), ex.toString());
                        ViewUtils.showNotificationError("Can't upload file: " + evt.getFileName());
                    }
                });

        return upload;
    }

    private void addSaveToDialog(byte[] data, String mimeType, String filename) {
        Button saveButton = dialog.addMainButton("Save");

        saveButton.addClickListener(
                event -> {
                    try {
                        log.debug("Add to storage: {}", filename);
                        Storage storage = storageService.uploadNow(alias, data, mimeType, filename, selectedType, false, "");
                        if (storage != null) {
                            ViewUtils.showNotification("File uploaded: " + filename);
                            log.debug("Image uploaded with url: {}", storage.getCompleteURL());
                            dialog.close();
                        }
                    }
                    catch (Exception ex) {
                        ViewUtils.showNotificationError("Can't upload file: " + filename);
                    }
                });

        for (com.vaadin.flow.component.Component item : dialog.getContainer().getChildren().toList()) {
            item.setVisible(false);
        }
        dialog.getContainer().add(new NativeLabel(filename));
    }

    private void addCropAndSaveToDialog(byte[] data, String mimeType, String filename, Size cropSize) {

        //Button saveButton = dialog.addSecondaryButton("Save");
        Button cropButton = dialog.addMainButton("Crop & Save");

        float ratio = (float)cropSize.getWidth()/(float)cropSize.getHeight();
        log.debug("Ratio: {}/{} = {}", cropSize.getWidth(), cropSize.getHeight(), ratio);

        CropperSettings cropperSettings = new CropperSettings();
        cropperSettings.setAspectRatio(ratio);
        cropperSettings.setViewMode(ViewMode.ONE);
        cropperSettings.setCenter(true);
        cropperSettings.setCropBoxMovable(true);

        cropperSettings.setMinCropBoxWidth(Math.round((float)cropSize.getWidth()/(float)3));
        cropperSettings.setMinCropBoxHeight(Math.round((float)cropSize.getHeight()/(float)3));

        cropperSettings.setCroppedImageWidth(cropSize.getWidth());
        cropperSettings.setCroppedImageHeight(cropSize.getHeight());

        //log.debug("Filename to crop: {} / {} / {}", filename, mimeType, data.length);

        Cropper crop = new Cropper(cropperSettings, Base64.getEncoder().encodeToString(data), mimeType);
        crop.setEncoderQuality(1.00);

        cropButton.addClickListener(
                event -> {
                    try {
                        //log.debug("Add to storage: {}", filename);
                        Storage storage = storageService.uploadNow(alias, crop.getImageBase64(), mimeType, filename, selectedType, false, cropSize.toJSON());
                        if (storage != null) {
                            ViewUtils.showNotification("File uploaded: " + filename);
                            //log.debug("Image uploaded with url: {}", storage.getCompleteURL());
                            dialog.close();

                            for (ResourceListener listener:listeners) {
                                listener.resourceUploaded(storage);
                            }
                        }
                    }
                    catch (Exception ex) {
                        ViewUtils.showNotificationError("Can't upload file: " + filename);
                    }
                });

        for (com.vaadin.flow.component.Component item : dialog.getContainer().getChildren().toList()) {
            item.setVisible(false);
        }

        Size inner = size.getInnerSize(50);
        crop.setWidth(inner.getWidth()+"px");
        crop.setHeight(inner.getHeight()+"px");
        dialog.getContainer().add(crop);
    }

    public void addResourceAddedListener(ResourceListener listenerToAdd) {
        for (ResourceListener listener:listeners) {
            if (listener.equals(listenerToAdd)) {
                return;
            }
        }

        listeners.add(listenerToAdd);
    }

    public void removeResourceListener(ResourceListener listenerToRemove) {
        for (ResourceListener listener:listeners) {
            if (listener.equals(listenerToRemove)) {
                listeners.remove(listener);
                return;
            }
        }
    }
}
