package com.wippit.cms.frontend.components;

public interface TemplateUploadListener {
    public void templateUploaded(String name);
    public void templateUploadError(String error);
}
