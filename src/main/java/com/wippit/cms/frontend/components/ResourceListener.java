package com.wippit.cms.frontend.components;

import com.wippit.cms.backend.data.bean.storage.Storage;

public interface ResourceListener {
    public void resourceUploaded(Storage storage);
    public void resourceSelected(Storage storage);
}
