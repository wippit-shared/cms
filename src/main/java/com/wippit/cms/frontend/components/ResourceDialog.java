package com.wippit.cms.frontend.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.DataProvider;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.services.StorageDataService;
import com.wippit.cms.backend.services.storage.StorageService;
import com.wippit.cms.backend.types.AppIcons;
import com.wippit.cms.backend.types.StorageTypes;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.olli.ClipboardHelper;

import java.util.ArrayList;
import java.util.List;


@Slf4j
@Component
public class ResourceDialog {

    @Getter
    @Setter
    private StorageTypes allowedTypes;
    @Getter
    @Setter
    private String title;
    @Getter
    @Setter
    private String alias;
    @Getter
    @Setter
    private Size size = new Size(400,400);

    private List<ResourceListener> listeners = new ArrayList<>();

    private Grid<Storage> grid;
    private DialogManager dialog;

    @Autowired
    private StorageService storageService;

    @Autowired
    private StorageDataService storageDataService;

    public ResourceDialog() {
        this.title = "Resources";
        allowedTypes = StorageTypes.ALL;
    }

    public void open(){
        setupDialog();
    }

    private void setupDialog() {

        storageDataService.setType(allowedTypes);

        dialog = new DialogManager(size.getWidth(), size.getHeight(), title);
        dialog.setResizableAndDraggable();
        dialog.getContainer().add(selectLayout());
        dialog.open();
    }

    private VerticalLayout selectLayout(){
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();

        grid = new Grid<>();

        if (allowedTypes == StorageTypes.ALL) {
            ComboBox<StorageTypes> storageTypeCombo = new ComboBox<>();
            storageTypeCombo.setItems(StorageTypes.toLinkList());
            storageTypeCombo.setValue(StorageTypes.ALL);
            storageTypeCombo.setWidthFull();
            layout.add(storageTypeCombo);

            storageTypeCombo.addValueChangeListener(e -> {
                if (e.getValue() == null) {
                    storageDataService.setType(StorageTypes.ALL);
                } else {
                    storageDataService.setType(e.getValue());
                }
                grid.getLazyDataView().refreshAll();
            });
        }

        DataProvider<Storage, Void> dataProvider;
        dataProvider = DataProvider.fromCallbacks(
                query -> storageDataService.getPage(query.getPage(), query.getLimit()).stream(),
                query -> storageDataService.getCount());

        grid.setId("grid");
        grid.addColumn(Storage::getOriginal_name).setHeader("File").setWidth("50%");
        grid.addColumn(storage -> ViewUtils.getFancyFileSize(storage.getByte_size())).setHeader("Size").setWidth("15%");
        grid.addColumn(storage -> getImageProperties(storage)).setWidth("15%");
        grid.addComponentColumn(storage -> getStorageImage(storage)).setWidth("10%");
        if (listeners.isEmpty()) {
            grid.addComponentColumn(storage -> copyURLButton(storage)).setWidth("10%");
        }
        else {
            grid.addComponentColumn(storage -> selectButton(storage)).setWidth("10%");
        }
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setSizeFull();
        grid.setDataProvider(dataProvider);

        layout.add(grid);

        return layout;
    }

    private String getImageProperties(Storage storage) {
        if (StringUtils.isBlank(storage.getProperties())) {
            return "";
        }

        Size size = Size.fromJSON(storage.getProperties());
        if (size == null) {
            return  "";
        }

        return size.toDisplay();
    }

    private Image getStorageImage(Storage item) {

        Image image;
        StorageTypes type = StorageTypes.getType(item.getType());

        if (!type.isImage()) {
             image = type.getImage();
        }
        else {
            image = new Image(item.getCompleteURL(), item.getOriginal_name());
        }

        image.addClassName("image_box");
        image.getStyle().set("width","50px");
        image.getStyle().set("height","50px");
        return image;
    }

    private ClipboardHelper copyURLButton(Storage item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.FILE_COPY, "Copy resource url");
        button.addClickListener(e->{
            ViewUtils.showNotification("Resource address copied");
            dialog.close();
        });
        return new ClipboardHelper(item.getPartialURL(), button);
    }

    private Button selectButton(Storage item) {
        Button button = ViewUtils.getToolbarButton(AppIcons.IMAGE_SELECT, "Select this: " + item.getOriginal_name());
        button.addClickListener(e->{
            for (ResourceListener listener:listeners) {
                listener.resourceSelected(item);
            }
            dialog.close();
        });
        return button;
    }

    public void addResourceAddedListener(ResourceListener listenerToAdd) {
        for (ResourceListener listener:listeners) {
            if (listener.equals(listenerToAdd)) {
                return;
            }
        }

        listeners.add(listenerToAdd);
    }

    public void removeResourceListener(ResourceListener listenerToRemove) {
        for (ResourceListener listener:listeners) {
            if (listener.equals(listenerToRemove)) {
                listeners.remove(listener);
                return;
            }
        }
    }

}
