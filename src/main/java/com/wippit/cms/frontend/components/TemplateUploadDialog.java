package com.wippit.cms.frontend.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileBuffer;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.utils.ViewUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


@Slf4j
@Component
public class TemplateUploadDialog {

    private final List<TemplateUploadListener> listeners = new ArrayList<>();
    private DialogManager dialog;
    private Upload upload;
    private FileBuffer fileBuffer;
    private TextField fileField;
    private TextField sizeField;
    private TextField templateField;

    public void open(){
        setupDialog();
    }

    private void setupDialog() {
        dialog = new DialogManager("Template Upload");
        dialog.setResizableAndDraggable();

        fileField = new TextField("File");
        fileField.setReadOnly(true);

        sizeField = new TextField("Size");
        sizeField.setReadOnly(true);

        templateField = new TextField("Template name");
        templateField.setReadOnly(true);

        fileBuffer = new FileBuffer();
        upload = new Upload(fileBuffer);
        upload.setAcceptedFileTypes("application/zip");
        upload.setAutoUpload(true);

        dialog.addToForm(upload);
        dialog.addToForm(fileField);
        dialog.addToForm(sizeField);
        dialog.addToForm(templateField);

        upload.addFileRejectedListener(e->{
            log.warn("Upload rejected");
            setError("Not an acceptable file");
        });

        upload.addFailedListener(e->{
            log.warn("Upload failed");
            setError("Can't upload file");
        });

        upload.addSucceededListener(evt -> {
            log.debug("Upload succeeded: {}", evt.getFileName());

            fileField.setValue(evt.getFileName());
            sizeField.setValue(ViewUtils.getFancyFileSize(evt.getContentLength()));
            String fixedName = AppUtils.fixPathName(evt.getFileName().replace(".zip", "").toLowerCase());
            templateField.setValue(fixedName);
            templateField.setReadOnly(false);
            templateField.setHelperText("No spaces, symbols or system characters allowed");

            Button saveButton = dialog.addMainButton("Add");
            saveButton.addClickListener(e->{
                try {
                    if (StringUtils.isBlank(templateField.getValue())) {
                        return;
                    }

                    String templateName = AppUtils.fixPathName(templateField.getValue());
                    if (StringUtils.isBlank(templateName)) {
                        return;
                    }

                    byte[] data = fileBuffer.getInputStream().readAllBytes();
                    String file = moveFileToTemp(data, templateName);
                    upackFileTemplate(file, templateName);
                    setSuccess(templateName);
                    dialog.close();
                }
                catch (Exception ex) {
                    log.error("Upacking template: {}", ex.toString());
                    setError("Can't add template: " + ex.toString());
                }
            });

        });

        dialog.open();
    }

    public void addUploadListener(TemplateUploadListener listenerToAdd) {
        for (TemplateUploadListener listener:listeners) {
            if (listener.equals(listenerToAdd)) {
                return;
            }
        }

        listeners.add(listenerToAdd);
    }

    public void removeUploadListener(TemplateUploadListener listenerToRemove) {
        for (TemplateUploadListener listener:listeners) {
            if (listener.equals(listenerToRemove)) {
                listeners.remove(listener);
                return;
            }
        }
    }

    private void setSuccess(String message) {
        for (TemplateUploadListener listener:listeners){
            listener.templateUploaded(message);
        }
    }

    private void setError(String message) {
        for (TemplateUploadListener listener:listeners){
            listener.templateUploadError(message);
        }
    }

    private String moveFileToTemp(byte[] data, String templateName) {
        String filePath = "";

        try {
            String tmp = RandomStringUtils.randomAlphabetic(10) + "-" + templateName;
            Path path = Path.of(AppGlobal.getInstance().getTempPath() + "/" + tmp);
            FileUtils.writeByteArrayToFile(path.toFile(), data);
            filePath = path.toAbsolutePath().toString();
        }
        catch (Exception ex) {
            log.error("Moving file to tmpDir: {}", ex.toString());
        }
        log.info("File: {}", filePath);
        return filePath;
    }

    private void upackFileTemplate(String filePath, String templateName) throws Exception {
        log.debug("Upacking file template: {}", filePath);

        Path templatePath = Paths.get(AppGlobal.getInstance().getTemplatePath() + "/" + templateName);
        File defaultTemplateDir = templatePath.toFile();
        if (defaultTemplateDir.exists()) {
            throw new IOException("Template already exists");
        }

        //---
        //Create default template directory & index file
        //---
        try {

            Files.createDirectories(templatePath);

            byte[] buffer = new byte[1024];
            ZipInputStream zis = new ZipInputStream(new FileInputStream(filePath));
            ZipEntry zipEntry = zis.getNextEntry();
            while (zipEntry != null) {
                File newFile = newFile(templatePath.toFile(), zipEntry);
                if (zipEntry.isDirectory()) {
                    if (!newFile.isDirectory() && !newFile.mkdirs()) {
                        throw new IOException("Failed to create directory " + newFile);
                    }
                } else {
                    // fix for Windows-created archives
                    File parent = newFile.getParentFile();
                    if (!parent.isDirectory() && !parent.mkdirs()) {
                        throw new IOException("Failed to create directory " + parent);
                    }

                    // write file content
                    FileOutputStream fos = new FileOutputStream(newFile);
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        fos.write(buffer, 0, len);
                    }
                    fos.close();
                }
                zipEntry = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();
        } catch (Exception ex) {
            log.error("Unpacking template: {}", ex.toString());
            throw new IOException("Error unpacking template");
        }
    }

    private static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }
}
