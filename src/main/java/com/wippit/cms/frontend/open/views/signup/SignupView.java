package com.wippit.cms.frontend.open.views.signup;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.user.Signup;
import com.wippit.cms.backend.data.services.LanguageDataService;
import com.wippit.cms.backend.data.services.UserDataService;
import com.wippit.cms.backend.services.email.EmailService;
import com.wippit.cms.backend.services.email.Mail;
import com.wippit.cms.backend.types.ApiEmailTypes;
import com.wippit.cms.backend.types.SendCodeStatusTypes;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.base.OWOpenAdapterView;
import com.wippit.cms.frontend.components.DialogManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@AnonymousAllowed
@PageTitle("Signup")
@Route(value = "signup")
public class SignupView extends OWOpenAdapterView {

    private final LanguageDataService languageDataService;
    private final UserDataService dataService;
    private int tries = 1;
    private Signup signup;

    public SignupView(@Autowired UserDataService dataService, @Autowired LanguageDataService languageDataService) {
        super();
        this.dataService = dataService;
        this.languageDataService = languageDataService;
    }

    @Override
    public boolean setup() {
        signup = new Signup();

        return true;
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {
        addClassNames(ViewUtils.getBackgroundRandomClass());
        setSpacing(false);

        VerticalLayout container = new VerticalLayout();
        if (isMobile) {
            container.setWidth("90%");
            container.setHeight("600px");
        }
        else {
            container.setWidth("500px");
            container.setHeight("600px");
        }

        container.addClassNames("signup-view-container");
        add(container);

        HorizontalLayout hl = new HorizontalLayout();
        hl.addClassNames("signup-view-header");
        hl.setWidthFull();
        Image img = new Image("/static/logo.png", "Wippit.Systems");
        img.addClassNames("signup-view-logo");
        hl.add(img);
        container.add(hl);

        H2 header = new H2();
        header.setWidthFull();
        header.setText("Signup");
        container.add(header);

        FormLayout formLayout = new FormLayout();

        TextField userNameField = new TextField("New account");
        userNameField.setRequired(true);
        userNameField.setMinLength(6);
        userNameField.setMaxLength(64);
        userNameField.setHelperText("6 characters min, no spaces");

        TextField fullNameField = new TextField("Full name");
        fullNameField.setRequired(true);

        EmailField emailField = new EmailField("Email");
        emailField.setRequired(true);

        formLayout.add(
                userNameField,
                fullNameField,
                emailField
        );

        container.add(formLayout);

        HorizontalLayout footer = ViewUtils.getFooter();
        footer.setWidthFull();

        Anchor signinAnchor = new Anchor();
        signinAnchor.add(new NativeLabel("Sign in"));
        signinAnchor.getElement().getStyle().set("margin-top", "20px");
        signinAnchor.setHref("/app");

        Button signupButton = ViewUtils.getActiveButton("Sign up");
        signupButton.getElement().getStyle().set("margin-left", "auto");

        footer.add(signinAnchor);
        footer.add(signupButton);
        container.add(footer);

        setSizeFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        getStyle().set("text-align", "center");

        signupButton.addClickListener(e->{
            if (userNameField.isInvalid()) {
                ViewUtils.showNotificationError("Account name is invalid");
                return;
            }
            if (emailField.isInvalid()) {
                ViewUtils.showNotificationError("Email is invalid");
                return;
            }

            String requestedAlias = userNameField.getValue();
            String fullName = fullNameField.getValue();
            String email = emailField.getValue();

            if (StringUtils.isBlank(requestedAlias)) {
                ViewUtils.showNotificationError("Account name is required");
                return;
            }

            String alias = AppUtils.fixAccountName(requestedAlias);
            userNameField.setValue(alias);

            if (StringUtils.isBlank(fullName)) {
                ViewUtils.showNotificationError("Full name is required");
                return;
            }

            if (StringUtils.isBlank(email) || emailField.isInvalid()) {
                ViewUtils.showNotificationError("Invalid email");
                return;
            }

            boolean validated = dataService.validateNotExistsSignup(alias, email);
            if (!validated) {
                ViewUtils.showNotificationError("Account or email already exists: " + alias + "/" + email);
                return;
            }

            if (doSignup(alias, fullName, email)) {
                doConfirmDialog();
            }
            else {
                tries++;
                ViewUtils.showNotificationError("Can't signup, please tray again later");
            }
        });
    }

    private boolean doSignup(String alias, String fullName, String email) {

        signup.setAlias(alias);
        signup.setFull_name(fullName);
        signup.setEmail(email);
        signup.setInfo("");     //Used for extended info
        signup.setStatus(SendCodeStatusTypes.WAITING.value());

        if (!dataService.addSignup(signup)) {
            return false;
        }

        //---
        //Send email
        //---
        if (sendEmail()) {
            ViewUtils.showNotification("Email sent to " + signup.getEmail());
        }
        else {
            ViewUtils.showNotificationError("Can't send email");
        }

        return true;
    }

    private void doConfirmDialog() {
        DialogManager dialog = new DialogManager("Confirm your signup");

        dialog.addToForm(ViewUtils.getDivLabel("Please enter the code we send to " + signup.getEmail()));

        TextField codeField = new TextField("Code");
        codeField.setRequired(true);

        dialog.addToForm(codeField);

        Button sendAgainButton = dialog.addSecondaryButton("Send again");
        Button confirmButton = dialog.addMainButton("Confirm Code");
        confirmButton.addClickListener(e->{

            String code = codeField.getValue();
            if (StringUtils.isBlank(code)) {
                ViewUtils.showNotificationError("Code is required");
            }

            if (code.contentEquals(signup.getCode())) {
                if (dataService.approveSignupStatus(signup)) {
                    dialog.close();
                    UI.getCurrent().getSession().setAttribute("signup", signup);
                    UI.getCurrent().navigate(SignupConfirmView.class);
                }
                else {
                    if (dataService.isSignupExpired(signup)) {
                        dialog.close();
                        ViewUtils.showNotificationError("Code is expired");
                        UI.getCurrent().navigate(SignupView.class);
                    }
                    else {
                        ViewUtils.showNotificationError("Can't validate signup. Please try again later.");
                        log.error("Can't validate signup: {}", signup);
                    }
                }
            }
            else {
                ViewUtils.showNotificationError("Invalid code");
            }
            tries++;

            if (tries>3) {
                dialog.close();
                ViewUtils.showNotificationError("too many attempts!");
                UI.getCurrent().navigate(SignupView.class);
            }
        });

        sendAgainButton.addClickListener(e->{
            if (sendEmail()) {
                ViewUtils.showNotification("Email sent to " + signup.getEmail());
            }
            else {
                ViewUtils.showNotificationError("Can't send email");
            }
        });

        dialog.open();
    }

    private boolean sendEmail() {

        ApiEmailTypes type = ApiEmailTypes.SEND_CODE;
        String templateID = dataService.getKey(type.key());
        if (StringUtils.isBlank(templateID)) {
            log.warn("Template default for send CODE is empty");
            return false;
        }

        Mail mail = new Mail();

        String translation = languageDataService.getTranslationAndStore("email", ApiEmailTypes.SEND_CODE.getTemplate(), "us-en", "your signup code");
        String message = languageDataService.getTranslationAndStore("email", ApiEmailTypes.SEND_CODE.getTemplate(), "us-en", "This is your signup code:");
        mail.setMailSubject(signup.getFull_name() + ", " + translation);

        mail.setMailTo(signup.getEmail());
        mail.setEmailID(signup.getSgid());
        mail.setType(type);
        mail.setIso("us-en");
        mail.setTemplateID(templateID);

        mail.addToModel("EID", signup.getSgid());
        mail.addToModel("NAME", signup.getFull_name());
        mail.addToModel("CODE", signup.getCode());
        mail.addToModel("MESSAGE", message);
        mail.addToModel("HOME_URL", AppUtils.createServerURL());

        EmailService.addEmail(mail, 0);
        return true;
    }

}
