package com.wippit.cms.frontend.open.controllers;

import com.vaadin.flow.server.auth.AnonymousAllowed;
import com.wippit.cms.backend.services.StatsService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@AnonymousAllowed
public class SiteErrorController implements ErrorController {

    @RequestMapping({"/error", "/cmserror"})
    public String handleError(HttpServletRequest request) {


        Object errorStatus = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
//        Object errorURI = request.getAttribute(RequestDispatcher.ERROR_REQUEST_URI);
//        //Object errorException = request.getAttribute(RequestDispatcher.ERROR_EXCEPTION);
//        Object errorMessage = request.getAttribute(RequestDispatcher.ERROR_MESSAGE);
//        String address = request.getRemoteAddr();

        int statusCode = 500;

        if (errorStatus != null) {
            statusCode = Integer.parseInt(errorStatus.toString());
        }
//        if (errorURI != null) {
//            String uri = (String) errorURI;
//            sb.append("\nURI: ").append(uri);
//        }
//        if (errorMessage != null) {
//            String message = (String) errorMessage;
//            sb.append("\nMessage: ").append(message);
//        }
//        sb.append("\n----------------------------------");


        //StatsService.addResourceHit(resourceStorage, request);

        StatsService.addErrorHit(statusCode, request);
        return "terror";
    }

}
