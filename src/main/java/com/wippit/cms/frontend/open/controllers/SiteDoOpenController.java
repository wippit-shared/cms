package com.wippit.cms.frontend.open.controllers;

import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.api.APIOpenResponse;
import com.wippit.cms.backend.data.bean.api.ContactMessageRequest;
import com.wippit.cms.backend.data.bean.api.SessionItem;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.services.EmailDataService;
import com.wippit.cms.backend.data.services.LanguageDataService;
import com.wippit.cms.backend.exceptions.ApiException;
import com.wippit.cms.backend.services.SessionService;
import com.wippit.cms.backend.services.StatsService;
import com.wippit.cms.backend.services.email.*;
import com.wippit.cms.backend.types.ApiEmailTypes;
import com.wippit.cms.backend.types.ApiErrorTypes;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/do")
public class SiteDoOpenController {

    @Autowired
    private SessionService sessionService;

    @Autowired
    private EmailDataService emailDataService;

    @Autowired
    private LanguageDataService languageDataService;

    //------------------------------------------------------------------------------------------------------
    //Do Api methods
    //------------------------------------------------------------------------------------------------------

    //------------------------------------------------------------------------------------------------------
    @GetMapping(value = "/lang/{country}")
    public void changeLang(HttpServletRequest request, HttpServletResponse response, @PathVariable("country") String countryCode) {

        log.debug("ApiOpenPost. Change locale request: {}", countryCode);

        List<LocalCountry> countryList = languageDataService.getActiveCountries();
        for (LocalCountry country : countryList) {
            if (country.getIso().equalsIgnoreCase(countryCode)) {
                log.info("Changing to country[{}] with iso: {}", country.getName(), country.getIso());

                //Set in cookie
                Cookie cookie = new Cookie(CookieLocaleResolver.DEFAULT_COOKIE_NAME, country.getIso());
                response.addCookie(cookie);

                //Set in session
                request.getSession().setAttribute(CookieLocaleResolver.DEFAULT_COOKIE_NAME, country.getIso());

                //Go to index
                CMSWriter.getCurrent().writeIndexResponse(request, response);
                return;
            }
        }

        log.warn("Country locale not found: {}", countryCode);
        CMSWriter.getCurrent().writeIndexResponse(request, response);
    }

    //------------------------------------------------------------------------------------------------------
    @GetMapping(value = "/confirm/{linkID}")
    public void confirmLink(HttpServletRequest request, HttpServletResponse response, @PathVariable("linkID") String encodedLinkID) {

        String linkID = AppUtils.simpleDecoding(encodedLinkID);
        EmailItem emailItem = emailDataService.getEmail(linkID);
        if (emailItem == null) {
            throw new ApiException(ApiErrorTypes.NOT_FOUND, "Email not found");
        }

        ApiEmailTypes emailType = ApiEmailTypes.getType(emailItem.getType());
        String emailTemplatePage = emailType.getPage();
        if (!StringUtils.isBlank(emailItem.getEmailTemplateID())) {
            EmailTemplate emailTemplate = emailDataService.getEmailTemplate(emailItem.getEmailTemplateID());
            if (emailTemplate != null) {
                //TODO: code this
                //emailTemplatePage = emailTemplate.getPage_template();
            }
        }

        CMSWriter.getCurrent().writeEmailTemplatePage(request, response, emailItem, emailTemplatePage);
        StatsService.addHit(request);
    }

    //----------------------------------------------------------------------------------------------------//
    //NOTES:
    //Receiving POST form from javascript without CSRF header/cookies
    //CSRF disabled: http.csrf().disable() in AppConfig
    //Multiform disabled: Changed @RequestBody ContactMessageRequest contactMessageRequest --to-->  @RequestParam MultiValueMap body
    //----------------------------------------------------------------------------------------------------//
    @PostMapping(value = "/message", produces = "application/json")
    public String sendMessage(HttpServletRequest request, HttpServletResponse response, @RequestParam MultiValueMap body) {

        log.trace("ApiOpenPost. Send message request");
        APIOpenResponse apiResponse = new APIOpenResponse();

        //---
        //Validation
        //---

        if (!validateSession(request,(String) body.getFirst("sid"))) {
            apiResponse.setText("Reload required");
            return apiResponse.toJson();
        }

        String name = "";
        String email = "";
        String message = "";
        String phone = "";
        String company = "";
        String templateID = "";

        try {
            name = (String) body.getFirst("name");
            email = (String) body.getFirst("email");
            message = (String) body.getFirst("message");
            phone = (String) body.getFirst("phone");
            company = (String) body.getFirst("company");
            templateID = (String) body.getFirst("templateID");
        }
        catch (Exception ex) {
            apiResponse.setText("Invalid data");
            return apiResponse.toJson();
        }

        if (StringUtils.isBlank(name)
                || StringUtils.isBlank(email)
                || StringUtils.isBlank(message)) {
            apiResponse.setText("Invalid message");
            return apiResponse.toJson();
        }

        //---
        //Continue to send email
        //---
        ApiEmailTypes type = ApiEmailTypes.CONTACT_REQUEST;
        EmailTemplate emailTemplate = null;
        if (!StringUtils.isBlank(templateID)) {

            emailTemplate = emailDataService.getEmailTemplate(templateID);
            if (emailTemplate == null) {
                apiResponse.setText("Invalid message template");
                return apiResponse.toJson();
            }

            type = ApiEmailTypes.getType(emailTemplate.getType());
        }
        else {
            templateID = "";
        }

        ContactMessageRequest contactMessageRequest = new ContactMessageRequest();
        contactMessageRequest.setMessage(message);
        contactMessageRequest.setEmail(email);
        contactMessageRequest.setName(name);
        contactMessageRequest.setPhone(phone);
        contactMessageRequest.setCompany(company);
        contactMessageRequest.setTemplateID(templateID);
        contactMessageRequest.setType(type);

        //---
        //Save in db
        //---
        String emailID = emailDataService.addEmail(contactMessageRequest, request.getRemoteAddr());
        if (StringUtils.isBlank(emailID)) {
            apiResponse.setText("Can't create message");
            return apiResponse.toJson();
        }

        //---
        // link
        //---
        String link = AppUtils.createServerUrl("/do/confirm/" + AppUtils.simpleEncoding(emailID));
        if (StringUtils.isBlank(link)) {
            apiResponse.setText("Can't send message");
            return apiResponse.toJson();
        }

        //---
        //Send email
        //---
        Mail mail = buildEmailContactRequest(request, contactMessageRequest, link, emailID);
        EmailService.addEmail(mail, 100);

        //---
        //Response
        //---
        log.info("ApiOpenPost. Contact Message request Ok");
        apiResponse.setSuccess();
        apiResponse.setText("Message sent");
        return apiResponse.toJson();
    }

    //------------------------------------------------------------------------------------------------------
    /**
     * Validates a session using SID token
     * @param request Http request
     * @param token SID token
     * @return true if is valid, false otherwise
     */
    private boolean validateSession(HttpServletRequest request, String token) {

        if (StringUtils.isBlank(token)) {
            return false;
        }

        try {
            String sessionID = request.getSession().getId();
            SessionItem sessionItem = sessionService.getSessionItem(sessionID, token);

            if (sessionItem.isUsed()) {
                log.warn("Session already used " + request.getRemoteAddr());
                return false;
            }

            sessionItem.setUsed(true);
            sessionService.updateSession(sessionItem);
            log.trace("Session key: " + sessionItem.getKey());
        }
        catch (Exception ex) {
            log.warn("Invalid session from " + request.getRemoteAddr());
            return false;
        }

        return true;
    }

    //------------------------------------------------------------------------------------------------------
    /**
     * Builds an email contact request
     * @param contactMessageRequest ContactMessageRequest item
     * @param link confirmation link
     * @param emailID emailID
     * @return Mail item
     */
    private Mail buildEmailContactRequest(HttpServletRequest request, ContactMessageRequest contactMessageRequest, String link, String emailID) {

        Mail mail = new Mail();


        String translation = languageDataService.getTranslationAndStore("email", contactMessageRequest.getType().getTemplate(), AppUtils.getCountry(request),"confirm your contact request");
        mail.setMailSubject(contactMessageRequest.getName() + ", " + translation);

        mail.setMailTo(contactMessageRequest.getEmail());
        mail.setEmailID(emailID);
        mail.setType(contactMessageRequest.getType());
        mail.setIso(AppUtils.getCountry(request));
        mail.setTemplateID(contactMessageRequest.getTemplateID());

        EmailTemplate template = emailDataService.getEmailTemplate(contactMessageRequest.getTemplateID());
        if (template != null) {
            for (EmailAttachment attachment : emailDataService.getAttachments(template)){
                mail.addAttachment(attachment);
            }
        }

        mail.addToModel("EID", emailID);
        mail.addToModel("NAME", contactMessageRequest.getName());
        mail.addToModel("MESSAGE", contactMessageRequest.getMessage());
        mail.addToModel("LINK", link);
        mail.addToModel("HOME_URL", AppUtils.createServerUrl(""));

        return mail;
    }


}
