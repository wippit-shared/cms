package com.wippit.cms.frontend.open.views.login;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import com.wippit.cms.backend.data.bean.user.RecoverPassword;
import com.wippit.cms.backend.data.services.UserDataService;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.components.DialogManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@AnonymousAllowed
@PageTitle("Recover Password")
@Route(value = "recover/confirm")
public class RecoverPasswordConfirmView extends VerticalLayout  {

    private final UserDataService dataService;

    public RecoverPasswordConfirmView(@Autowired UserDataService dataService) {
        this.dataService = dataService;
        createView();
    }

    private void createView() {

        RecoverPassword recoverPassword = (RecoverPassword) UI.getCurrent().getSession().getAttribute("recover");
        if (recoverPassword == null) {
            ViewUtils.showNotificationError("Recover password code is required");
            UI.getCurrent().navigate(RecoverPasswordView.class);
            return;
        }

        addClassNames(ViewUtils.getBackgroundRandomClass());

        setSpacing(false);

        VerticalLayout container = new VerticalLayout();
        container.setWidth("500px");
        container.setHeight("600px");
        container.addClassNames("signup-view-container");
        add(container);

        Image img = new Image("/static/logo.png", "Wippit.Systems");
        img.addClassNames("signup-view-logo");
        container.add(img);

        H2 header = new H2();
        header.setWidthFull();
        header.setText("Recover Account");
        container.add(header);

        FormLayout formLayout = new FormLayout();

        TextField userNameField = new TextField("Your account");
        userNameField.setValue(recoverPassword.getAlias());
        userNameField.setReadOnly(true);

        PasswordField passwordField1 = new PasswordField("Password");
        passwordField1.setRequired(true);
        PasswordField passwordField2 = new PasswordField("Confirm Password");
        passwordField2.setRequired(true);

        formLayout.add(
                userNameField,
                passwordField1,
                passwordField2
        );

        container.add(formLayout);

        HorizontalLayout footer = ViewUtils.getFooter();
        footer.setWidthFull();

        Button signupButton = ViewUtils.getActiveButton("Continue");
        signupButton.getElement().getStyle().set("margin-left", "auto");

        footer.add(signupButton);
        container.add(footer);

        setSizeFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        getStyle().set("text-align", "center");

        signupButton.addClickListener(e->{
            if (passwordField1.isInvalid() || passwordField2.isInvalid()) {
                ViewUtils.showNotificationError("Password is required");
                return;
            }

            String password1 = passwordField1.getValue();
            String password2 = passwordField1.getValue();
            if (StringUtils.isBlank(password1) || StringUtils.isBlank(password2)) {
                ViewUtils.showNotificationError("Invalid passwords");
                return;
            }

            if (!password1.contentEquals(password2)) {
                ViewUtils.showNotificationError("Password not match");
                return;
            }

            if (dataService.changeSecret(recoverPassword.getAlias(), password1)) {
                doLoginDialog();
            }
            else {
                ViewUtils.showNotificationError("Can't create account");
            }

        });
    }

    private void doLoginDialog() {

        DialogManager dialog = new DialogManager("Your account is ready");
        dialog.setModal();

        dialog.addToForm(ViewUtils.getDivLabel("Will be redirected to login page."));
        dialog.addToForm(ViewUtils.getDivLabel("Use your account & password."));

        Button button = dialog.addMainButton("Go to Login");
        button.addClickListener(e->{
            UI.getCurrent().navigate(LoginView.class);
            dialog.close();
        });

        dialog.open();
    }

}
