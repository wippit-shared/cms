package com.wippit.cms.frontend.open.controllers;

import com.vladsch.flexmark.ext.admonition.AdmonitionExtension;
import com.wippit.cms.AppConstants;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.api.SessionItem;
import com.wippit.cms.backend.data.bean.content.Article;
import com.wippit.cms.backend.data.bean.content.ContentRedirect;
import com.wippit.cms.backend.data.bean.content.HomeIndex;
import com.wippit.cms.backend.data.bean.model.*;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.bean.template.TemplateSection;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.services.IntroPostDataService;
import com.wippit.cms.backend.data.services.LanguageDataService;
import com.wippit.cms.backend.exceptions.ApiException;
import com.wippit.cms.backend.exceptions.SiteTemplateExceptionHandler;
import com.wippit.cms.backend.security.OWSecureKey;
import com.wippit.cms.backend.services.StatsService;
import com.wippit.cms.backend.services.email.EmailActionResponse;
import com.wippit.cms.backend.services.email.EmailItem;
import com.wippit.cms.backend.types.ApiErrorTypes;
import com.wippit.cms.backend.types.ContentStatusTypes;
import com.wippit.cms.backend.types.StatusTypes;
import com.wippit.cms.backend.utils.ResourceFile;
import com.wippit.cms.backend.utils.ResourceSection;
import com.wippit.cms.backend.utils.ResourceStorage;
import com.wippit.cms.backend.utils.cache.FileCacheItem;
import com.wippit.cms.backend.utils.cache.FileTreeCache;
import freemarker.template.Configuration;
import freemarker.template.Template;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class CMSWriter {

    private Configuration freemarkerConfig;
    private SiteModel siteModel;

    private final ConcurrentHashMap<String, TemplateSection> sections = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, FileCacheItem> homeIndex = new ConcurrentHashMap<>();

    @Getter
    @Setter
    private String pageIndex;
    @Getter
    @Setter
    private String pageAuthor;
    @Getter
    @Setter
    private String pageSearch;
    @Getter
    @Setter
    private String pageUser;
    @Getter
    @Setter
    private String pageError400;
    @Getter
    @Setter
    private String pageError500;
    @Getter
    @Setter
    private String pageBlogPage;
    @Getter
    @Setter
    private String pageBlog;

    @Getter
    private FileTreeCache fileTreeCache = new FileTreeCache();

    @Getter
    private static CMSWriter current;

    @Autowired
    private IntroPostDataService dataService;

    @Autowired
    private LanguageDataService languageDataService;

    @PostConstruct
    public void setup() {
        current = this;
    }

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private EmailActionResponse emailActionResponse;

    private String templateName = "";

    public void reload(){
        String template = AppGlobal.getInstance().getSiteInfo().getSiteTemplate().getName();
        log.info("Site reload with template: {}", template);
        setTemplate(template);
    }

    public long itemCount() {
        return fileTreeCache.itemsCount();
    }

    public void setTemplate(String template) {

        synchronized (current) {

            this.templateName = template;
            String templateRoot = AppGlobal.getInstance().getTemplatePath() +
                    AppConstants.PATH_SEPARATOR +
                    templateName;

            fileTreeCache = new FileTreeCache(templateRoot);
            if (!StringUtils.isBlank(pageIndex)) {
                fileTreeCache.setRequestedPageIndex(pageIndex);
            }
            fileTreeCache.setFlagDisplay(true);
            fileTreeCache.reload();

            try {
                freemarkerConfig = new Configuration(Configuration.VERSION_2_3_28);
                freemarkerConfig.setDirectoryForTemplateLoading(new File(templateRoot));
                freemarkerConfig.setTemplateExceptionHandler(new SiteTemplateExceptionHandler());
                freemarkerConfig.setWhitespaceStripping(true);
                freemarkerConfig.setLocalizedLookup(false);
                freemarkerConfig.setShowErrorTips(false);
                freemarkerConfig.setLogTemplateExceptions(false);
                freemarkerConfig.setAutoFlush(true);
                //freemarkerConfig.setLazyAutoImports(true);
            }
            catch (Exception ex) {
                log.error("On freemarker config: {}", ex.toString());
            };

            List<HomeIndex> homeIndexList = dataService.getHomeIndexInTemplate(template);
            homeIndex.clear();
            for (HomeIndex item:homeIndexList) {
                FileCacheItem page = fileTreeCache.getItem(item.getPage());
                if (page != null) {
                    page.setIndex(true);
                    homeIndex.put(item.getIso(), page);
                    log.debug("Home index: {} - {}", item.getIso(), item.getPage());
                }
                else {
                    log.error("Index not found in files: {}", item.getPage());
                }
            }

            List<TemplateSection> templateSections = dataService.getSectionsInTemplate(template);
            sections.clear();
            for (TemplateSection templateSection : templateSections) {
                sections.put(templateSection.getKey(), templateSection);
                log.debug("Section: {}", templateSection);
            }

            List<LocalCountry> localCountries = dataService.getCountries();
            for (LocalCountry country : localCountries) {
                if (country.getIso().equalsIgnoreCase(AppGlobal.getInstance().getSiteInfo().getCountry_iso())) {
                    siteModel = new SiteModel(AppGlobal.getInstance().getSiteInfo(), country);
                }
            }
            AppUtils.loadCountries(localCountries);
        }
    }

    public void writeIndexResponse(HttpServletRequest request, HttpServletResponse response) {

        log.debug("Write Index");

        String iso = AppUtils.getCountry(request);

        FileCacheItem page = homeIndex.get(iso);
        if (page != null) {
            log.debug("Using root page index: {}", page);
            _writeResponse(page, request, response);
        }
        else {
            log.warn("Using default root page index");
            FileCacheItem indexItem = fileTreeCache.getIndexItem();
            if (indexItem == null) {
                log.warn("Default indexItem is null");
            }
            _writeResponse(indexItem, request, response);
        }
    }

    public void writeContentResponse(@NotNull ResourceSection resourceSection, HttpServletRequest request, HttpServletResponse response) {

        log.info("Content response: {}", resourceSection);

        //---
        //Validate secID
        //---
        if (StringUtils.isBlank(resourceSection.getSection())) {
            writeErrorResponse(404, request, response);
            return;
        }

        //---
        //Get country iso
        //---
        HashMap<String, Object> dataModel = new HashMap<>();
        String iso = AppUtils.getCountry(request);
        log.debug("Country: {}", iso);

        //---
        //Get secID template
        //---
        String sectionKey = TemplateSection.buildKey(iso, resourceSection.getSection());
        TemplateSection templateSection = sections.get(sectionKey);
        if (templateSection == null) {
            log.warn("Section not found: {}", sectionKey);
            writeErrorResponse(404, request, response);
            return;
        }

        log.debug("Template secID: {}", templateSection);
        FileCacheItem item;

        if (StringUtils.isBlank(resourceSection.getResource())) {
            //---
            //Write secID template
            //---
            item = fileTreeCache.getItem(templateSection.getSection_page());
            log.debug("Template is for secID page: {}", item);

//            ContentModel contentModel = new ContentModel(iso, templateSection.getSecID(), dataService);
//            contentModel.setPageIndex(AppUtils.getSecureParameterAsInt(request,"page",0));
//            dataModel.put("CONTENT", contentModel);

            _writeResponseTemplate(item, request, response, dataModel, templateSection.getSecID());
        }
        else {
            //---
            //Write page template
            //---
            item = fileTreeCache.getItem(templateSection.getItem_page());
            log.debug("Template is for page: {}", item);

            String encodedPreviewCID = AppUtils.getSecureParameter(request, "preview", "");
            ArticleModel articleModel = _getContentModel(iso, resourceSection.getResource(), encodedPreviewCID);
            if (articleModel == null || item == null) {
                log.warn("Page not found: {}", resourceSection.getResource());
                writeErrorResponse(404, request, response);
                return;
            }

            dataModel.put("ARTICLE", articleModel);
            _writeResponseTemplate(item, request, response, dataModel, templateSection.getSecID());
        }
    }

    public void writeErrorResponse(int error, HttpServletRequest request, HttpServletResponse response) {
        log.debug("Writing error response");

        FileCacheItem item = null;
        if (error == 404) {
            item = fileTreeCache.getItem(AppGlobal.getInstance().getSiteInfo().getSiteTemplate().getPage_404());
        }
        else if (error == 500) {
            item = fileTreeCache.getItem(AppGlobal.getInstance().getSiteInfo().getSiteTemplate().getPage_500());
        }
        else {
            item = fileTreeCache.getItem("error.html");
        }
        if (item != null) {
            _writeResponse(item, request, response);
        }
        else {
            ResourceFile resourceFile = new ResourceFile("error.html");
            String data = resourceFile.getContentAsString().replace("${ERROR}", String.valueOf(error));
            _writeResponseText(data, "text/html", request, response);
        }

        StatsService.addErrorHit(error, request);
    }

    public void writeStorage(ResourceStorage resourceStorage, @NotNull HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isBlank(resourceStorage.getUrl())) {
            writeErrorResponse(404, request, response);
            return;
        }

        Storage storage = dataService.getStorage(resourceStorage.getUrl());
        if (storage == null) {
            writeErrorResponse(404, request, response);
            return;
        }

        _writeResponseStorage(storage, request, response);
    }

    public void writeResponse(@NotNull HttpServletRequest request, HttpServletResponse response) {
        FileCacheItem item = fileTreeCache.getItem(request.getRequestURI());
        _writeResponse(item, request, response);
    }

    public void writeEmailTemplatePage(@NotNull HttpServletRequest request, HttpServletResponse response, EmailItem emailItem, String emailTemplate) {

        String template = emailTemplate;
        if (!emailTemplate.startsWith("/")) {
            template = "/" + template;
        }
        if (!template.endsWith(".ftl")) {
            template = template + ".ftl";
        }

        log.debug("Write email template: {}", template);
        FileCacheItem item = fileTreeCache.getItem(template);
        if (item == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "page not found");
        }

        HashMap<String, Object> dataModel = emailActionResponse.createResponse(request, emailItem);
        if (dataModel == null) {
            log.warn("Email template response is invalid: {}", emailItem.getEmailTemplateID());
            writeErrorResponse(500, request, response);
            return;
        }

        _writeResponseTemplate(item, request, response, dataModel, "");
    }

    public void writeCountries(HttpServletRequest request, HttpServletResponse response) {
        String countries = dataService.getLocalCountry2JSON();
        _writeResponseText(countries, "application/json", request, response);
    }

    public void writeAdmonition(boolean asJavascript, HttpServletRequest request, HttpServletResponse response) {
        String admonition ;
        if (asJavascript) {
            admonition = AdmonitionExtension.getDefaultScript();
            _writeResponseText(admonition, "text/javascript", request, response);
        }
        else  {
            admonition = AdmonitionExtension.getDefaultCSS();
            _writeResponseText(admonition, "text/css", request, response);
        }
    }

    public void writeCID(String cid, HttpServletRequest request, HttpServletResponse response) {
        Article article = dataService.getPostByCID(cid);
        if (article == null) {
            log.warn("Invalid CID request: {}", cid);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "page not found: " + request.getRequestURI());
        }
        _writeResponseText(article.getContent_complete(), "text/html", request, response);
    }

    public void writeForward(String name, HttpServletRequest request, HttpServletResponse response) {

        String iso = AppUtils.getCountry(request);
        ContentRedirect redirect = dataService.getRedirect(iso, name);
        if (redirect == null) {
            log.warn("Invalid redirect request: {}", name);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "page not found: " + request.getRequestURI());
        }

        try {
            ResourceFile resourceFile = new ResourceFile("templates/tforward.ftlh");
            String content = resourceFile.getContentAsString();
            content = content.replace("${NAME}", redirect.getName());
            content = content.replace("${URL}", redirect.getUrl());
            _writeResponseText(content, "text/html", request, response);

            StatsService.addRedirectHit(iso, redirect.getName(), redirect.getUrl(), request);
        }
        catch (Exception ex) {
            log.error("Error redirect request[{}]: {}", name, ex.toString());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "redirect error");
        }
    }

    public ContentRedirect getRedirect(String redirectPath, HttpServletRequest request, HttpServletResponse response) {

        ContentRedirect redirect = dataService.getRedirect(AppGlobal.getInstance().getSiteInfo().getCountry_iso(), redirectPath);
        if (redirect == null) {
            throw new ApiException(ApiErrorTypes.NOT_FOUND);
        }

        log.debug("Redirect: {}", redirect);
        if (redirect.getStatus() == StatusTypes.ACTIVE.value()
            && AppUtils.isTimestampValid(redirect.getStart_date(), redirect.getEnd_date())
        ) {
            return redirect;
        }

        log.warn("Redirect is invalid: {}", redirectPath);
        throw new ApiException(ApiErrorTypes.NOT_FOUND);
    }

    public void writeResource(String resourcePath, String mime, HttpServletRequest request, HttpServletResponse response) {
        ResourceFile resourceFile = new ResourceFile(resourcePath);
        String content = resourceFile.getContentAsString();
        _writeResponseText(content, mime, request, response);
    }

    public void writeStatus(HttpServletRequest request, HttpServletResponse response) {
        String status = AppConstants.APP_FULL_VERSION + ": " + LocalDateTime.now();
        _writeResponseText(status, "text/plain", request, response);
    }

    //----------------------------------------------------------------------------------------------------//
    private void _writeResponse(FileCacheItem item, HttpServletRequest request, HttpServletResponse response)  {

        if (item == null) {
            String uri = request.getRequestURI().substring(0, Math.min(request.getRequestURI().length(), 2048));
            if (uri.contains(".") || uri.startsWith("_") || uri.startsWith("?")) {
                log.warn("_writeResponse Invalid request: {}", uri);
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "page not found: " + request.getRequestURI());
            }

            String iso = AppUtils.getCountry(request);
            String filename = uri + "-" + iso + AppConstants.TEMPLATE_EXT;
            log.debug("Searching: {}", filename);
            item = fileTreeCache.getItem(filename);
            if (item == null) {
                filename = uri + AppConstants.TEMPLATE_EXT;
                log.debug("Searching: {}", filename);
                item = fileTreeCache.getItem(filename);
                if (item == null) {
                    log.warn("_writeResponse File not found: {}", uri);
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "page not found: " + request.getRequestURI());
                }
            }
        }

        if (item.isInternal() && !item.isIndex()) {
            log.warn("_writeResponse requesting internal item: {}", request.getRequestURI());
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found: " + request.getRequestURI());
        }

        log.debug("_writeResponse {} / {}", item.getName(), item.getType().getLabel());
        switch (item.getType()) {
            case HTML, RESOURCE -> {
                log.debug("_writeResponse item is HTML/RESOURCE");
                _writeResponseFileItem(item, request, response);
            }
            case FTL -> {
                log.debug("_writeResponse item is FTL");
                _writeResponseTemplate(item, request, response, null, "");
            }
            case UNKNOWN -> {
                log.warn("_writeResponse UNKNOWN item");
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No document available: " + request.getRequestURI());
            }
        }

    }

    //----------------------------------------------------------------------------------------------------//
    private void _writeResponseFileItem(@NotNull FileCacheItem item, HttpServletRequest request, HttpServletResponse response)  {

        log.trace("{} / {}", item.getName(), item.getMime());

        try {
            File file = item.getPath().toFile();
            byte[] buffer = FileUtils.readFileToByteArray(file);

            response.setContentType(item.getMime());
            response.setHeader("Content-Length", String.valueOf(buffer.length));
            response.setStatus(HttpServletResponse.SC_OK);

            OutputStream out = response.getOutputStream();
            out.write(buffer, 0, buffer.length);
            out.flush();
            out.close();
        }
        catch (Exception ex) {
            log.error("On write({}): {}", request.getRequestURI(), ex.toString());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Some errors: " + request.getRequestURI());
        }

    }

    //----------------------------------------------------------------------------------------------------//
    private void _writeResponseStorage(Storage storage, HttpServletRequest request, HttpServletResponse response)  {

        try {
            byte[] buffer = FileUtils.readFileToByteArray(new File(storage.getCompletePath()));
            response.setContentType(storage.getMime());
            response.setHeader("Content-Length", String.valueOf(buffer.length));
            response.setStatus(HttpServletResponse.SC_OK);

            OutputStream out = response.getOutputStream();
            out.write(buffer, 0, buffer.length);
            out.flush();
            out.close();
        }
        catch (Exception ex) {
            log.error("On write({}): {}", request.getRequestURI(), ex.toString());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Some errors: " + request.getRequestURI());
        }

    }

    //----------------------------------------------------------------------------------------------------//
    private void _writeResponseText(String text, String mime, HttpServletRequest request, HttpServletResponse response)  {

        try {
            byte[] buffer = text.getBytes();

            response.setContentType(mime);
            response.setHeader("Content-Length", String.valueOf(buffer.length));

//            //---
//            //Csrf token
//            //---
//            CookieCsrfTokenRepository repository = new CookieCsrfTokenRepository();
//            CsrfToken csrfToken = repository.generateToken(request);
//            response.setHeader(csrfToken.getHeaderName(), csrfToken.getToken());

            response.setStatus(HttpServletResponse.SC_OK);

            OutputStream out = response.getOutputStream();
            out.write(buffer, 0, buffer.length);
            out.flush();
            out.close();
        }
        catch (Exception ex) {
            log.error("On write({}): {}", request.getRequestURI(), ex.toString());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Some errors: " + request.getRequestURI());
        }
    }

    //----------------------------------------------------------------------------------------------------//
    private void _writeResponseTemplate(@NotNull FileCacheItem item, HttpServletRequest request, HttpServletResponse response, HashMap<String, Object> model, String secID) {

        log.debug("{} / {}", item.getName(), item.getMime());

        try {
            //---
            //Get models
            //---
            log.debug("Write template {}", item.getName());
            Template template = freemarkerConfig.getTemplate(item.getName());
            if (template == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No document found: " + request.getRequestURI());
            }

            String iso = AppUtils.getCountry(request);

            HashMap<String, Object> dataModel = new HashMap<>();
            if (model != null) {
                dataModel.putAll(model);
            }
            dataModel.put("SITE", siteModel);

            HttpSession httpSession = request.getSession(false);
            if (httpSession == null) {
                httpSession = request.getSession(true);
                log.warn("Session created for file: {}", item.getFilename());
            }
            String sessionID = httpSession.getId();

            String token = UUID.randomUUID().toString();
            SessionItem sessionItem = new SessionItem(sessionID, token);
            UtilModel utilModel = new UtilModel(applicationContext, sessionItem, languageDataService, request);
            dataModel.put("UTIL", utilModel);

            if (StringUtils.isBlank(secID)) {
                ContentModel contentModel = new ContentModel(iso, dataService);
                contentModel.setPageIndex(AppUtils.getSecureParameterAsInt(request,"page",0));
                dataModel.put("CONTENT", contentModel);
            }
            else {
                ContentModel contentModel = new ContentModel(iso, secID, dataService);
                contentModel.setPageIndex(AppUtils.getSecureParameterAsInt(request,"page",0));
                dataModel.put("CONTENT", contentModel);
            }
            //---
            //Translator
            //---
            log.debug("Creating translator");
            String country = AppUtils.getCountry(request);
            TranslationMethod translationMethod = new TranslationMethod(languageDataService, templateName, item.getName(), country);
            dataModel.put("TRANSLATE", translationMethod);

            response.setContentType(item.getMime());
            response.setStatus(HttpServletResponse.SC_OK);

            //---
            //Process template + model
            //---
            OutputStream out = response.getOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(out);
            template.process(dataModel, outputStreamWriter);
            out.close();
        }
        catch (Exception ex) {
            log.error("On write template({}): {}", request.getRequestURI(), ex.toString());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Some errors: " + request.getRequestURI());
        }
    }

    //----------------------------------------------------------------------------------------------------//
    /**
     * Get Author model
     * @param author Author alias
     * @return Author model
     */
    private @NotNull AuthorModel _getAuthorModel(String author) {
        Account account = dataService.getAccount(author);
        return new AuthorModel(account);
    }

//    //----------------------------------------------------------------------------------------------------//
//    /**
//     * Get Home model by Country ISO
//     * @param iso   Country ISO
//     * @return Home model
//     */
//    private @NotNull HomeModel _getHomeModel(String iso) {
//        return new HomeModel(iso, dataService);
//    }

//    //----------------------------------------------------------------------------------------------------//
//    /**
//     * Get secID model
//     * @param iso Country iso
//     * @param sectionUrl Section URL
//     * @param pageIndex pageIndex
//     * @return Section model
//     */
//    private @Nullable SectionModel _getSectionModel(String iso, String sectionUrl, int pageIndex) {
//        SectionModel sectionModel = new SectionModel(iso, dataService, sectionUrl);
//        sectionModel.setPageIndex(pageIndex);
//        return sectionModel;
//
//    }

    //----------------------------------------------------------------------------------------------------//
    /**
     * Get Article model by Country ISO and URL
     * @param iso   Country ISO
     * @param pageUrl   Article URL
     * @param encodedPreviewCID CID encoded (required ONLY for preview)
     * @return Article model
     */
    private @Nullable ArticleModel _getContentModel(String iso, String pageUrl, @Nullable String encodedPreviewCID) {

        log.debug("Get content model: {} / {} / {}", iso, pageUrl, encodedPreviewCID);
        Article content = dataService.getPost(iso, pageUrl);

        if (content == null) {
            log.warn("Content is null");
            return null;
        }

        boolean flagPreview = false;
        if (!StringUtils.isBlank(encodedPreviewCID)) {
            String previewCID = OWSecureKey.decode(encodedPreviewCID);
            if (previewCID.equalsIgnoreCase(content.getCid())) {
                flagPreview = true;
            }
        }
        if (!flagPreview) {
            //---
            //Validate content is active
            //---
            ContentStatusTypes status = ContentStatusTypes.getType(content.getStatus());
            if (status == ContentStatusTypes.NOT_PUBLISHED) {
                log.warn("Content not published by status: {}", content.getCid());
                return null;
            }
            Timestamp now = AppUtils.timestampNow();
            if (!(now.after(content.getStart_date()) && now.before(content.getEnd_date()))) {
                log.warn("Content not published by date: {}", content.getCid());
                return null;
            }
        }

        //String fix="<style>figure > img {max-width: 100%;}</style>";
        //String html = fix + content.getContent_complete_html();
        //content.setContent_complete_html(html);
        Account account = dataService.getAccount(content.getAuthor());
        return new ArticleModel(content, account);
    }
}
