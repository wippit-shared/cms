package com.wippit.cms.frontend.open.views.login;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.bean.user.RecoverPassword;
import com.wippit.cms.backend.data.services.LanguageDataService;
import com.wippit.cms.backend.data.services.UserDataService;
import com.wippit.cms.backend.services.email.EmailService;
import com.wippit.cms.backend.services.email.Mail;
import com.wippit.cms.backend.types.ApiEmailTypes;
import com.wippit.cms.backend.types.SendCodeStatusTypes;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.base.OWOpenAdapterView;
import com.wippit.cms.frontend.components.DialogManager;
import com.wippit.cms.frontend.open.views.signup.SignupView;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@AnonymousAllowed
@PageTitle("Recover Password")
@Route(value = "recover")
public class RecoverPasswordView extends OWOpenAdapterView {

    private final LanguageDataService languageDataService;
    private final UserDataService dataService;
    private int tries = 1;
    RecoverPassword recoverPassword;

    public RecoverPasswordView(@Autowired UserDataService dataService, @Autowired LanguageDataService languageDataService) {
        super();
        this.dataService = dataService;
        this.languageDataService = languageDataService;
    }

    @Override
    public boolean setup() {
        recoverPassword = new RecoverPassword();
        return true;
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {
        addClassNames(ViewUtils.getBackgroundRandomClass());
        setSpacing(false);

        VerticalLayout container = new VerticalLayout();

        if (isMobile) {
            container.setWidth("90%");
            container.setHeight("500px");
        }
        else {
            container.setWidth("500px");
            container.setHeight("500px");
        }

        container.addClassNames("signup-view-container");
        add(container);

        HorizontalLayout hl = new HorizontalLayout();
        hl.addClassNames("signup-view-header");
        hl.setWidthFull();
        Image img = new Image("/static/logo.png", "Wippit CMS");
        img.addClassNames("signup-view-logo");
        hl.add(img);
        container.add(hl);

        H2 header = new H2();
        header.setWidthFull();
        header.setText("Recover Lost Password");
        container.add(header);

        FormLayout formLayout = new FormLayout();
        formLayout.getElement().getStyle().set("padding","20px");

        TextField userNameField = new TextField("Account");
        userNameField.setRequired(true);
        userNameField.setMinLength(6);
        userNameField.setMaxLength(64);
        userNameField.setHelperText("6 characters min, no spaces");

        formLayout.add(
                userNameField
        );

        container.add(formLayout);

        HorizontalLayout footer = ViewUtils.getFooter();
        footer.setWidthFull();

        Anchor signinAnchor = new Anchor();
        signinAnchor.add(new NativeLabel("Sign in"));
        signinAnchor.getElement().getStyle().set("margin-top", "20px");
        signinAnchor.setHref("/app");

        Button signupButton = ViewUtils.getActiveButton("Recover");
        signupButton.getElement().getStyle().set("margin-left", "auto");

        footer.add(signinAnchor);
        footer.add(signupButton);
        container.add(footer);

        setSizeFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        getStyle().set("text-align", "center");

        signupButton.addClickListener(e->{
            if (userNameField.isInvalid()) {
                ViewUtils.showNotificationError("Account name is invalid");
                return;
            }
            String requestedAlias = userNameField.getValue();
            if (StringUtils.isBlank(requestedAlias)) {
                ViewUtils.showNotificationError("Account name is required");
                return;
            }

            String alias = AppUtils.fixAccountName(requestedAlias);
            userNameField.setValue(alias);

            Account account = dataService.getAccountByAlias(alias);
            if (account == null) {
                ViewUtils.showNotificationError("Invalid account. Please try again later.");
                log.warn("Account do not exists: {}", alias);
                return;
            }

            recoverPassword.setAlias(alias);
            recoverPassword.setEmail(account.getEmail());
            recoverPassword.setStatus(SendCodeStatusTypes.WAITING.value());
            recoverPassword.setCreated_date(AppUtils.timestampNow());
            recoverPassword.setExpire_date(AppUtils.getNowPlusMinutes(10));
            if (doRecover(account, recoverPassword)) {
                doConfirmDialog(account, recoverPassword);
            }
        });
    }

    private boolean doRecover(Account account, RecoverPassword recoverPassword) {

        //---
        //Send email
        //---
        if (sendEmail(account,recoverPassword)) {
            ViewUtils.showNotification("Email sent to account's email");
        }
        else {
            ViewUtils.showNotificationError("Can't send email");
        }

        return true;
    }

    private void doConfirmDialog(Account account, RecoverPassword recoverPassword) {
        DialogManager dialog = new DialogManager("Confirm your recover lost password");

        dialog.addToForm(ViewUtils.getDivLabel("Please enter the code sent to your email"));

        TextField codeField = new TextField("Code");
        codeField.setRequired(true);

        dialog.addToForm(codeField);

        Button sendAgainButton = dialog.addSecondaryButton("Send again");
        Button confirmButton = dialog.addMainButton("Confirm Code");
        confirmButton.addClickListener(e->{

            String code = codeField.getValue();
            if (StringUtils.isBlank(code)) {
                ViewUtils.showNotificationError("Code is required");
                return;
            }

            if (AppUtils.timestampNow().after(recoverPassword.getExpire_date())) {
                ViewUtils.showNotificationError("Code is expired");
                return;
            }

            if (code.contentEquals(recoverPassword.getCode())) {
                UI.getCurrent().getSession().setAttribute("recover", recoverPassword);
                UI.getCurrent().navigate(RecoverPasswordConfirmView.class);
                return;
            }
            else {
                ViewUtils.showNotificationError("Invalid code");
            }
            tries++;

            if (tries>3) {
                dialog.close();
                ViewUtils.showNotificationError("too many attempts!");
                UI.getCurrent().navigate(SignupView.class);
            }
        });

        sendAgainButton.addClickListener(e->{
            if (sendEmail(account, recoverPassword)) {
                ViewUtils.showNotification("Email sent to account's email");
            }
            else {
                ViewUtils.showNotificationError("Can't send email");
            }
        });

        dialog.open();
    }

    private boolean sendEmail(Account account, RecoverPassword recoverPassword) {

        Mail mail = new Mail();

        String translation = languageDataService.getTranslationAndStore("email", ApiEmailTypes.SEND_CODE.getTemplate(), "us-en", "your recover los password code");
        String message = languageDataService.getTranslationAndStore("email", ApiEmailTypes.SEND_CODE.getTemplate(), "us-en", "This is your recover lost password code:");
        mail.setMailSubject(account.getFull_name() + ", " + translation);

        //---
        //Get default template for SEND CODE
        //---
        ApiEmailTypes type = ApiEmailTypes.SEND_CODE;
        String templateID = dataService.getKey(type.key());
        if (StringUtils.isBlank(templateID)) {
            log.warn("Template default for SEND CODE is empty");
            return false;
        }

        mail.setMailTo(account.getEmail());
        mail.setEmailID(account.getAlias());
        mail.setType(type);
        mail.setIso("us-en");
        mail.setTemplateID(templateID);

        mail.addToModel("NAME", account.getFull_name());
        mail.addToModel("CODE", recoverPassword.getCode());
        mail.addToModel("MESSAGE", message);
        mail.addToModel("HOME_URL", AppUtils.createServerURL());

        EmailService.addEmail(mail, 0);
        return true;
    }

}
