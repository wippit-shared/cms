package com.wippit.cms.frontend.open.controllers;

import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.services.StatsService;
import com.wippit.cms.backend.types.CMSHitTypes;
import com.wippit.cms.backend.types.CMSItemTypes;
import com.wippit.cms.backend.utils.ResourceSection;
import com.wippit.cms.backend.utils.ResourceStorage;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@Controller
public class SiteOpenController {

    private final String NOT_START_WITH_ERROR = "(?=^(?!error))";
    private final String NOT_START_WITH_API = "(?=^(?!do))";
    private final String NOT_START_WITH_STATUS = "(?=^(?!status))";
    private final String NOT_START_WITH_REDIRECT = "(?=^(?!go))";
    private final String NOT_START_WITH_STATIC = "(?=^(?!static))";
    private final String NOT_START_WITH_PRIVATE = "(?=^(?!private))";
    private final String NOT_START_WITH_CONTENT = "(?=^(?!content))";
    private final String NOT_START_WITH_BLOG = "(?=^(?!blog))";
    private final String NOT_START_WITH_RESOURCE = "(?=^(?!resource))";
    private final String NOT_START_WITH_WRES = "(?=^(?!wres))";
    private final String NOT_START = NOT_START_WITH_BLOG + NOT_START_WITH_API + NOT_START_WITH_REDIRECT + NOT_START_WITH_WRES + NOT_START_WITH_PRIVATE + NOT_START_WITH_STATIC + NOT_START_WITH_ERROR + NOT_START_WITH_CONTENT + NOT_START_WITH_RESOURCE +  NOT_START_WITH_STATUS + ".*";
    private final String PATH_NOT_START = "/{path:" + NOT_START + "}/**";

    //----------------------------------------------------------------------------------------------------//
    @ResponseBody
    @GetMapping(value = "/")
    public void getRoot(HttpServletRequest request, HttpServletResponse response) {
        log.debug("Open. Root");
        String uri = request.getRequestURI();
        if (uri.equalsIgnoreCase("/cmserror") || uri.equalsIgnoreCase("/error")) {
            return;
        }

        log.debug("Open. Root request {}", uri);
        CMSWriter.getCurrent().writeIndexResponse(request, response);

        String agent = request.getHeader(HttpHeaders.USER_AGENT);
        String address = request.getRemoteAddr();
        CMSItemTypes type = CMSItemTypes.getTypeByURI(request.getRequestURI());
        StatsService.addHit("/", type, CMSHitTypes.OK, agent, address);
    }

    //----------------------------------------------------------------------------------------------------//
    @ResponseBody
    @GetMapping(value = {PATH_NOT_START})
    public void getPathNoStart(HttpServletRequest request, HttpServletResponse response) {
        log.debug("Open. Not start request {}", request.getRequestURI());
        CMSWriter.getCurrent().writeResponse(request, response);
        StatsService.addHit(request);
    }

    //----------------------------------------------------------------------------------------------------//
    @ResponseBody
    @GetMapping(value = {"/content/**"})
    public void getContent(HttpServletRequest request, HttpServletResponse response) {
        log.debug("Open. Content request {}", request.getRequestURI());

        ResourceSection resourceSection = AppUtils.getResourceSection(request.getRequestURI());
        resourceSection.setQuery(request.getQueryString());

        CMSWriter.getCurrent().writeContentResponse(resourceSection, request, response);

        StatsService.addSectionHit(resourceSection, request);
    }

    //----------------------------------------------------------------------------------------------------//
    @ResponseBody
    @GetMapping(value = {"/blog/**"})
    public void getBlogContent(HttpServletRequest request, HttpServletResponse response) {
        log.info("Open. Blog request {}", request.getRequestURI());

        String uri = request.getRequestURI();
        String resource = uri.replace("/blog", "");

        ResourceSection resourceSection = AppUtils.getBlogResource(resource);
        log.info("Resource secID: {}", resourceSection);

        CMSWriter.getCurrent().writeContentResponse(resourceSection, request, response);
        StatsService.addSectionHit(resourceSection, request);
    }

    //------------------------------------------------------------------------------------------------------
    @RequestMapping(value = "/go/{name}")
    public void doForward(HttpServletRequest request, HttpServletResponse response, @PathVariable("name") String name) {
        log.debug("ApiOpenPost. Go to: {}", name);
        CMSWriter.getCurrent().writeForward(name, request, response);
    }

    //----------------------------------------------------------------------------------------------------//
    @ResponseBody
    @GetMapping(value = "/resource/**")
    public void getResource(HttpServletRequest request, HttpServletResponse response) {

        log.debug("Open. Resource request {}", request.getRequestURI());
        ResourceStorage resourceStorage = AppUtils.getResourceStorage(request.getRequestURI());
        resourceStorage.setQuery(request.getQueryString());

        CMSWriter.getCurrent().writeStorage(resourceStorage, request, response);

        StatsService.addResourceHit(resourceStorage, request);
    }

    //----------------------------------------------------------------------------------------------------//
    @ResponseBody
    @GetMapping(value = "/wres/**")
    public void getWippResource(HttpServletRequest request, HttpServletResponse response){
        log.debug("Open. internal resource: {}", request.getRequestURI());
        String path = request.getRequestURI().replace("/wres", "");
        String agent = request.getHeader(HttpHeaders.USER_AGENT);
        String address = request.getRemoteAddr();

        boolean isOk = false;
        if (path.startsWith("/admonition/")) {
            if (path.endsWith(".css")) {
                CMSWriter.getCurrent().writeAdmonition(false, request, response);
                isOk = true;
            }
            else if (path.endsWith(".js")) {
                CMSWriter.getCurrent().writeAdmonition(true, request, response);
                isOk = true;
            }
        }
        else if (path.startsWith("/data/")) {
            String resource = path.replace("/data/", "");
            if (resource.equalsIgnoreCase("countries.json")) {
                CMSWriter.getCurrent().writeCountries(request, response);
                isOk = true;
            }
        }
        else if (path.startsWith("/cid/")) {
            String resource = path.replace("/cid/", "");
            CMSWriter.getCurrent().writeCID(resource, request, response);
            isOk = true;
        }
        else if (path.startsWith("/js/")) {
            String resource = path.replace("/js/", "");
            CMSWriter.getCurrent().writeResource("wippit/scripts/" + resource, "text/javascript", request, response);
            isOk = true;
        }

        if (isOk) {
            StatsService.addHit("/wres" + path, CMSItemTypes.RESOURCE_INTERNAL, CMSHitTypes.OK, agent, address);
        }
        else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "file not found: " + request.getRequestURI());
        }

//                if(path.startsWith("/ace-builds/")) {
//                    resource = path.replace("/ace-builds/", "");
//                    ResourceFile rf = new ResourceFile("wippit/" + resource);
//                    data = rf.getContentAsBytes();
//                    mime = "text/javascript";
//                }
//                else if (path.startsWith("/images/")) {
//                    resource = path.replace("/images/", "");
//                    ResourceFile rf = new ResourceFile("wippit/images/" + resource);
//                    data = rf.getContentAsBytes();
//                }
//                else if (path.startsWith("/icons/")) {
//                    resource = path.replace("/icons/", "");
//                    ResourceFile rf = new ResourceFile("wippit/icons/" + resource);
//                    data = rf.getContentAsBytes();
//                }
//                else if (path.startsWith("/css/")) {
//                    resource = path.replace("/css/", "");
//                    ResourceFile rf = new ResourceFile("wippit/css/" + resource);
//                    data = rf.getContentAsBytes();
//                    mime = "text/css";
//                }
//                else if (path.startsWith("/json/")) {
//                    resource = path.replace("/json/", "");
//                    ResourceFile rf = new ResourceFile("wippit/json/" + resource);
//                    data = rf.getContentAsBytes();
//                    mime = "application/json";
//                }
//
    }

    //----------------------------------------------------------------------------------------------------//
    @ResponseBody
    @GetMapping(value = "/status")
    public void getStatus(HttpServletRequest request, HttpServletResponse response){
        log.debug("Open. Status");
        CMSWriter.getCurrent().writeStatus(request, response);
    }

}
