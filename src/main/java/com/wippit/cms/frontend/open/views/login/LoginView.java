package com.wippit.cms.frontend.open.views.login;

import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.internal.RouteUtil;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.frontend.base.OWOpenAdapterView;

@AnonymousAllowed
@PageTitle("Login")
@Route(value = "login")
public class LoginView extends OWOpenAdapterView implements BeforeEnterObserver {

    private final AuthenticatedUser authenticatedUser;
    private LoginOverlay loginOverlay;

    public LoginView(AuthenticatedUser authenticatedUser) {
        super();
        this.authenticatedUser = authenticatedUser;
        loginOverlay = new LoginOverlay();
        add(loginOverlay);
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {
        addClassNames("login-view");
        addClassNames(ViewUtils.getBackgroundRandomClass());

        loginOverlay.setAction(RouteUtil.getRoutePath(VaadinService.getCurrent().getContext(), getClass()));

        LoginI18n i18n = LoginI18n.createDefault();
        i18n.setHeader(new LoginI18n.Header());
        i18n.setAdditionalInformation(null);
        loginOverlay.setI18n(i18n);

        loginOverlay.setForgotPasswordButtonVisible(false);
        loginOverlay.setOpened(true);
    }


    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if (authenticatedUser.get().isPresent()) {
            loginOverlay.setOpened(false);
            event.forwardTo("");
        }

        loginOverlay.setError(event.getLocation().getQueryParameters().getParameters().containsKey("error"));
        super.beforeEnter(event);
    }
}
