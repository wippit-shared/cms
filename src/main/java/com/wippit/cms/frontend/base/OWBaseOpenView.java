package com.wippit.cms.frontend.base;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Optional;

public class OWBaseOpenView extends VerticalLayout implements BeforeEnterObserver, AfterNavigationObserver  {

    @Setter
    @Getter
    private String messageError = "";
    @Setter
    @Getter
    private String messageWarning = "";
    @Getter
    private boolean isMobile;
    @Getter
    private final Size currentSize = new Size(1,1);

    private final HashMap<String, String> params = new HashMap<>();

    public OWBaseOpenView(){
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        messageError = "";
        messageWarning = "";

        for (String paramName:event.getRouteParameters().getParameterNames()) {
            Optional<String> paramValue = event.getRouteParameters().get(paramName);
            paramValue.ifPresent(s -> params.put(paramName, s));
        }
    }

    public String getParam(String name) {
        if (params.containsKey(name)) {
            return params.get(name);
        }
        return "";
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {

        removeAll();

        if (this instanceof OWBaseObserver observer) {

            if (observer.setup()) {

                if (!messageWarning.isEmpty()) {
                    add(ViewUtils.getWarningLabel(messageWarning));
                }

                if (!messageError.isEmpty()) {
                    add(ViewUtils.getErrorLabel(messageError));
                }

                Page page = UI.getCurrent().getPage();
                page.retrieveExtendedClientDetails(extendedClientDetails -> {
                    Size size = new Size(extendedClientDetails.getWindowInnerWidth(), extendedClientDetails.getWindowInnerHeight());

                    if (extendedClientDetails.isTouchDevice() && extendedClientDetails.getBodyClientHeight()>extendedClientDetails.getBodyClientWidth()) {
                        isMobile = true;
                    }

                    currentSize.setHeight(size.getHeight());
                    currentSize.setWidth(size.getWidth());

                    observer.buildUI(currentSize, isMobile);

                });

                page.addBrowserWindowResizeListener(resizeEvent -> {
                    currentSize.setHeight(resizeEvent.getHeight());
                    currentSize.setWidth(resizeEvent.getWidth());
                    observer.resizeUI(currentSize);
                });

            }
            else {
                if (!messageWarning.isEmpty()) {
                    add(ViewUtils.getWarningLabel(messageWarning));
                }

                if (!messageError.isEmpty()) {
                    add(ViewUtils.getErrorLabel(messageError));
                }

                observer.buildErrorUI();
            }
        }
    }

}
