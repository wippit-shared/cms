package com.wippit.cms.frontend.base;


import com.wippit.cms.backend.utils.Size;

public interface OWBaseObserver {
    boolean setup();
    void buildUI(Size size, boolean isMobile);
    void buildErrorUI();
    void resizeUI(Size size);
}