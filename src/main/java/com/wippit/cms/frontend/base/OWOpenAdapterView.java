package com.wippit.cms.frontend.base;

import com.wippit.cms.backend.utils.Size;

public class OWOpenAdapterView extends OWBaseOpenView implements OWBaseObserver {

    public OWOpenAdapterView() {
        super();
    }

    @Override
    public boolean setup() {
        return true;
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {

    }

    @Override
    public void buildErrorUI() {

    }

    @Override
    public void resizeUI(Size size) {

    }
}
