package com.wippit.cms.frontend.base;

import com.wippit.cms.backend.security.AuthenticatedUser;
import com.wippit.cms.backend.utils.Size;

public class OWSecureAdapterView extends OWBaseSecureView implements OWBaseObserver {

    public OWSecureAdapterView(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }

    @Override
    public boolean setup() {
        return true;
    }

    @Override
    public void buildUI(Size size, boolean isMobile) {

    }

    @Override
    public void buildErrorUI() {

    }

    @Override
    public void resizeUI(Size size) {

    }
}
