/***********************************************************************
 Copyright 2024 Wippit.Systems

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ************************************************************************/

package com.wippit.cms;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.TimeZone;

@Slf4j
public class AppConstants {

    public static final String APP_NAME = "Wippit CMS";
    public static final String APP_VERSION = "1";
    public static final String APP_MINOR = "0";
    public static final String APP_BUILD = "132";
    public static final String APP_BUILD_DATE = "2024-06-24";
    public static final String APP_AUTHOR = "Wippit.Systems";
    public static final String APP_AUTHOR_LINK = "https://wippit.systems/tools/cms";
    public static final String APP_COPYRIGHT = "2024";
    public static final String APP_FULL_VERSION = APP_VERSION + "." + APP_MINOR + "." + APP_BUILD;
    public static final String APP_ID = "cmsApp";

    public static final String PATH_SEPARATOR = "/";

    public static final int SERVICE_INTERVAL_STATS = 2000;
    public static final int SERVICE_INTERVAL_EMAIL = 5000;
    public static final int SERVICE_INTERVAL_HEARTBEAT = 60000;


    public static final int SERVICE_INITIAL_DELAY_STATS = 15000;
    public static final int SERVICE_INITIAL_DELAY_EMAIL = 20000;
    public static final int SERVICE_INITIAL_DELAY_HEARTBEAT = 60000;

    public static final int QUERY_DEFAULT_LIMIT = 20;

    public static final int WEBSOCKET_SERVER_PORT = 19423;

    public static final int SESSION_EXPIRE_DAYS = 2;

    public static final String TEMPLATE_DEFAULT = "default";
    public static final String TEMPLATE_EXT = ".ftl";
    public static final String TEMPLATE_DOC1 = ".html";
    public static final String TEMPLATE_DOC2 = ".htm";
    public static final String TEMPLATE_ROOT = "index";

    public static final String API_VERSION = "1.0";

    public static final String SYSTEM_USER = "System";

    public static final int CACHE_TIMEOUT_MIN = 15;
    public static final String CACHE_SESSIONS = "sessions";
    public static final String CACHE_IP_LOCATION = "ip-location";

    public static final String COLOR_RED = "#DD2C10";
    public static final String COLOR_BLUE = "#619AFA";
    public static final String COLOR_WHITE = "#D7E2F5";

    public static final BigDecimal CURRENCY_FACTOR = new BigDecimal(1000000);

    public static void about(){
        StringBuilder sb = new StringBuilder("\n----------------------------------------------------");
        sb.append("\n" + APP_NAME + " " + APP_FULL_VERSION + " - " + APP_BUILD_DATE);
        sb.append("\n" +APP_AUTHOR + " (C) " + APP_COPYRIGHT );
        sb.append("\n" +"----------------------------------------------------");
        sb.append("\n" +"Charset: " + Charset.defaultCharset().displayName() );
        sb.append("\n" +"Timezone: " + TimeZone.getDefault().getDisplayName());
        sb.append("\n" +"----------------------------------------------------");
        sb.append("\n");
        log.info(sb.toString());
    }

}
