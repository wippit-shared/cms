package com.wippit.cms.backend.utils.cache;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class FileTree {

    @Getter
    private final Path root;
    @Getter
    private List<Path> tree;

    public FileTree(String root) {
        this.root = Paths.get(root);
        reload();
    }

    public void reload() {
        try {
            reloadTree();
        }
        catch (Exception ex) {
            log.error("On fillTree: {}", ex.toString());
        }
    }

    private void reloadTree() throws IOException {
        List<Path> result;
        try (Stream<Path> walk = Files.walk(root)) {
            result = walk.collect(Collectors.toList());
        }
        tree = new ArrayList<>();
        tree.addAll(result);
    }

    public List<Path> getFiles() throws IOException {

        List<Path> result;
        try (Stream<Path> walk = Files.walk(root)) {
            result = walk.filter(Files::isRegularFile).collect(Collectors.toList());
        }
        return result;
    }

    public List<Path> getDirectories() throws IOException {
        List<Path> result;
        try (Stream<Path> walk = Files.walk(root)) {
            result = walk.filter(Files::isDirectory).collect(Collectors.toList());
        }
        return result;
    }

    public boolean isRoot(Path item) {
        return item.toAbsolutePath().toString().equalsIgnoreCase(root.toAbsolutePath().toString());
    }
}
