package com.wippit.cms.backend.utils.color;

public class RGBColor {
    protected double r;
    protected double g;
    protected double b;

    public RGBColor(double r, double g, double b) {
        set(r, g, b);
    }

    public void set(double r, double g, double b) {
        this.r = clamp(r);
        this.g = clamp(g);
        this.b = clamp(b);
    }

    public String toString() {
        return String.format("rgb(%d, %d, %d)", Math.round(r), Math.round(g), Math.round(b));
    }

    public void hueRotate(double angle) {
        angle = angle / 180 * Math.PI;
        double sin = Math.sin(angle);
        double cos = Math.cos(angle);

        multiply(new double[]{
                0.213 + cos * 0.787 - sin * 0.213, 0.715 - cos * 0.715 - sin * 0.715, 0.072 - cos * 0.072 + sin * 0.928,
                0.213 - cos * 0.213 + sin * 0.143, 0.715 + cos * 0.285 + sin * 0.140, 0.072 - cos * 0.072 - sin * 0.283,
                0.213 - cos * 0.213 - sin * 0.787, 0.715 - cos * 0.715 + sin * 0.715, 0.072 + cos * 0.928 + sin * 0.072
        });
    }

    public void grayscale(double value) {
        multiply(new double[]{
                0.2126 + 0.7874 * (1 - value), 0.7152 - 0.7152 * (1 - value), 0.0722 - 0.0722 * (1 - value),
                0.2126 - 0.2126 * (1 - value), 0.7152 + 0.2848 * (1 - value), 0.0722 - 0.0722 * (1 - value),
                0.2126 - 0.2126 * (1 - value), 0.7152 - 0.7152 * (1 - value), 0.0722 + 0.9278 * (1 - value)
        });
    }

    public void sepia(double value) {
        multiply(new double[]{
                0.393 + 0.607 * (1 - value), 0.769 - 0.769 * (1 - value), 0.189 - 0.189 * (1 - value),
                0.349 - 0.349 * (1 - value), 0.686 + 0.314 * (1 - value), 0.168 - 0.168 * (1 - value),
                0.272 - 0.272 * (1 - value), 0.534 - 0.534 * (1 - value), 0.131 + 0.869 * (1 - value)
        });
    }

    public void saturate(double value) {
        multiply(new double[]{
                0.213 + 0.787 * value, 0.715 - 0.715 * value, 0.072 - 0.072 * value,
                0.213 - 0.213 * value, 0.715 + 0.285 * value, 0.072 - 0.072 * value,
                0.213 - 0.213 * value, 0.715 - 0.715 * value, 0.072 + 0.928 * value
        });
    }

    private void multiply(double[] matrix) {
        double newR = clamp(r * matrix[0] + g * matrix[1] + b * matrix[2]);
        double newG = clamp(r * matrix[3] + g * matrix[4] + b * matrix[5]);
        double newB = clamp(r * matrix[6] + g * matrix[7] + b * matrix[8]);
        r = newR; g = newG; b = newB;
    }

    public void brightness(double value) {
        linear(value,0);
    }

    public void contrast(double value) {
        linear(value, -(0.5 * value) + 0.5);
    }

    private void linear(double slope, double intercept) {
        r = clamp(r * slope + intercept * 255);
        g = clamp(g * slope + intercept * 255);
        b = clamp(b * slope + intercept * 255);
    }

    public void invert(double value) {
        r = clamp((value + (r / 255) * (1 - 2 * value)) * 255);
        g = clamp((value + (g / 255) * (1 - 2 * value)) * 255);
        b = clamp((value + (b / 255) * (1 - 2 * value)) * 255);
    }

    public double[] hsl() {
        double r = this.r / 255.0;
        double g = this.g / 255.0;
        double b = this.b / 255.0;
        double max = Math.max(r, Math.max(g, b));
        double min = Math.min(r, Math.min(g, b));
        double h, s, l = (max + min) / 2;

        if (max == min) {
            h = s = 0;
        } else {
            double d = max - min;
            s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
            if (max == r) {
                h = (g - b) / d + (g < b ? 6 : 0);
            } else if (max == g) {
                h = (b - r) / d + 2;
            } else {
                h = (r - g) / d + 4;
            }
            h /= 6;
        }

        return new double[]{h * 100, s * 100, l * 100};
    }

    private int clamp(double value) {
        if (value > 255) {
            return 255;
        } else if (value < 0) {
            return 0;
        } else {
            return (int) value;
        }
    }
}

