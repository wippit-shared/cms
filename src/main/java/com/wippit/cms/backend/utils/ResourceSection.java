package com.wippit.cms.backend.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResourceSection {
    private String section = "";
    private String resource = "";
    private String query = "";
}
