package com.wippit.cms.backend.utils.timezone;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Slf4j
public class TimeZoneItem  {
    @Getter
    private final ZoneId zoneId;
    @Getter
    private final String display;

    private enum OffsetBase {
        GMT, UTC
    }

    public TimeZoneItem(ZoneId id, String display) {
        this.zoneId = id;
        this.display = display;
    }

    public TimeZoneItem() {
        this.zoneId = ZoneId.systemDefault();
        LocalDateTime now = LocalDateTime.now();
        this.display = String.format("(%s%s) %s", OffsetBase.GMT, getOffset(now, zoneId), zoneId.getId());
    }

    public String toString() {
        return display;
    }

    public static TimeZoneItem fromString(String zoneName) {

        if (StringUtils.isBlank(zoneName)) {
            return new TimeZoneItem();
        }

        for (TimeZoneItem item : toList()) {
            if (item.zoneId.getId().equalsIgnoreCase(zoneName)) {
                return item;
            }
        }

        return new TimeZoneItem();
    }

    public String getName() {
        return zoneId.getId();
    }

    public int getOffsetInMinutes() {
        LocalDateTime now = LocalDateTime.now();

        ZoneOffset offset = now
                .atZone(zoneId)
                .getOffset();

        int seconds = offset.getTotalSeconds();
        int minutes = Math.floorDiv(seconds, 60);
        return minutes;
    }

    public static List<TimeZoneItem> toList() {

        List<ZoneId> zoneList = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();
        for (String zoneName : ZoneId.getAvailableZoneIds()) {
            ZoneId zoneItem = ZoneId.of(zoneName);
            zoneList.add(zoneItem);
        }

        zoneList.sort(new ZoneComparator());
        List<TimeZoneItem> list = new ArrayList<>();
        for (ZoneId zoneItem:zoneList) {
            list.add(new TimeZoneItem(zoneItem, String.format("(%s%s) %s", OffsetBase.GMT, getOffset(now, zoneItem), zoneItem.getId())));
        }

        return list;
    }

    private static String getOffset(LocalDateTime dateTime, ZoneId id) {
        return dateTime
                .atZone(id)
                .getOffset()
                .getId()
                .replace("Z", "+00:00");
    }

    private static class ZoneComparator implements Comparator<ZoneId> {

        @Override
        public int compare(ZoneId zoneId1, ZoneId zoneId2) {
            LocalDateTime now = LocalDateTime.now();

            ZoneOffset offset1 = now
                    .atZone(zoneId1)
                    .getOffset();

            ZoneOffset offset2 = now
                    .atZone(zoneId2)
                    .getOffset();

            return offset1.compareTo(offset2);
        }
    }
}
