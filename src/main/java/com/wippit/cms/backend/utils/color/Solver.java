package com.wippit.cms.backend.utils.color;

import lombok.Data;

import java.util.Random;

public class Solver {
    private final RGBColor target;
    private final double[] targetHSL;
    private final RGBColor reusedRGBColor;

    public Solver(RGBColor target) {
        this.target = target;
        this.targetHSL = target.hsl();
        this.reusedRGBColor = new RGBColor(0, 0, 0); // Object pool
    }

    public Result solve() {
        Result result = solveNarrow(solveWide());
        return new Result(result.values, result.loss, css(result.values));
    }

    private Result solveWide() {
        final int A = 5;
        final int c = 15;
        final double[] a = {60

                , 180, 18000, 600, 1.2, 1.2};

        Result best = new Result(null, Double.POSITIVE_INFINITY, "");
        for (int i = 0; best.loss > 25 && i < 3; i++) {
            double[] initial = {50, 20, 3750, 50, 100, 100};
            Result result = spsa(A, a, c, initial, 1000);
            if (result.loss < best.loss) {
                best = result;
            }
        }
        return best;
    }

    private Result solveNarrow(Result wide) {
        final int A = (int) wide.loss;
        final int c = 2;
        final double A1 = A + 1;
        final double[] a = {0.25 * A1, 0.25 * A1, A1, 0.25 * A1, 0.2 * A1, 0.2 * A1};
        return spsa(A, a, c, wide.values, 500);
    }

    private Result spsa(int A, double[] a, int c, double[] values, int iters) {
        final double alpha = 1;
        final double gamma = 0.16666666666666666;

        double[] best = null;
        double bestLoss = Double.POSITIVE_INFINITY;
        double[] deltas = new double[6];
        double[] highArgs = new double[6];
        double[] lowArgs = new double[6];

        Random rand = new Random();
        for (int k = 0; k < iters; k++) {
            double ck = c / Math.pow(k + 1, gamma);
            for (int i = 0; i < 6; i++) {
                deltas[i] = rand.nextDouble() > 0.5 ? 1 : -1;
                highArgs[i] = values[i] + ck * deltas[i];
                lowArgs[i] = values[i] - ck * deltas[i];
            }

            double lossDiff = loss(highArgs) - loss(lowArgs);
            for (int i = 0; i < 6; i++) {
                double g = lossDiff / (2 * ck) * deltas[i];
                double ak = a[i] / Math.pow(A + k + 1, alpha);
                values[i] = fix(values[i] - ak * g, i);
            }

            double loss = loss(values);
            if (loss < bestLoss) {
                best = values.clone();
                bestLoss = loss;
            }
        }
        return new Result(best, bestLoss, "");
    }

    private double fix(double value, int idx) {
        double max = 100;
        if (idx == 2) { max = 7500; }
        else if (idx == 4 || idx == 5) { max = 200; }

        if (idx == 3) {
            if (value > max) { value = value % max; }
            else if (value < 0) { value = max + value % max; }
        } else if (value < 0) { value = 0; }
        else if (value > max) { value = max; }
        return value;
    }

    private double loss(double[] filters) {
        RGBColor RGBColor = reusedRGBColor;
        RGBColor.set(0, 0, 0);

        RGBColor.invert(filters[0] / 100);
        RGBColor.sepia(filters[1] / 100);
        RGBColor.saturate(filters[2] / 100);
        RGBColor.hueRotate(filters[3] * 3.6);
        RGBColor.brightness(filters[4] / 100);
        RGBColor.contrast(filters[5] / 100);

        double[] colorHSL = RGBColor.hsl();
        return Math.abs(RGBColor.r - target.r)
                + Math.abs(RGBColor.g - target.g)
                + Math.abs(RGBColor.b - target.b)
                + Math.abs(colorHSL[0] - targetHSL[0])
                + Math.abs(colorHSL[1] - targetHSL[1])
                + Math.abs(colorHSL[2] - targetHSL[2]);
    }

    private String css(double[] filters) {
        return String.format("invert(%d%%) sepia(%d%%) saturate(%d%%) hue-rotate(%.1fdeg) brightness(%d%%) contrast(%d%%)",
                Math.round(filters[0]),
                Math.round(filters[1]),
                Math.round(filters[2]),
                filters[3] * 3.6,
                Math.round(filters[4]),
                Math.round(filters[5]));
    }

    @Data
    public static class Result {
        double[] values;
        double loss;
        String filter;

        Result(double[] values, double loss, String filter) {
            this.values = values;
            this.loss = loss;
            this.filter = filter;
        }
    }
}
