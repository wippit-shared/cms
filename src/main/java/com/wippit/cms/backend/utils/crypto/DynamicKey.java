package com.wippit.cms.backend.utils.crypto;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class DynamicKey {

    @Getter
    private int size;

    private final int width;
    private final List<byte[]> keys;

    public DynamicKey(int size, int width) {
        this.size = size;
        this.width = width;
        keys = new ArrayList<>();
        createKeys();
    }

    public DynamicKey(int width, byte[] data) {
        this.width = width;
        keys = new ArrayList<>();
        loadKeys(data);
    }

    public byte[] getKey(int index) {
        if (index<0 || index >= size) {
            log.warn("Invalid key index: {}", index);
            return new byte[]{};
        }
        return keys.get(index);
    }

    public byte[] getData() {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            int index;
            for (index = 0; index < keys.size(); index++) {
                byte[] data = keys.get(index);
                bos.write(data);
            }
            return bos.toByteArray();
        }
        catch (Exception ex) {
            log.error("Getting data from dynamic key");
        }
        return new byte[]{};
    }

    public byte[] getKey() {
        return RandomStringUtils.randomAlphanumeric(width).getBytes(StandardCharsets.UTF_8);
    }

    private void createKeys(){
        int index;
        for (index=0; index < size; index++) {
            byte[] row = RandomStringUtils.randomAlphanumeric(width).getBytes(StandardCharsets.UTF_8);
            keys.add(row);
        }
    }

    private void loadKeys(byte[] data) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int index;
        for (index=0; index < data.length; index += width) {
            bos.reset();
            bos.write(data, index, width);
            keys.add(bos.toByteArray());
        }
        size = keys.size();
        log.debug("Keys({}, {}) loaded", size,width);
    }
}
