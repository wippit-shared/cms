package com.wippit.cms.backend.utils.i18n;

import lombok.Data;

@Data
public class LanguageItem {
    private long id;
    private String  template;
    private String  page;
    private String  iso;
    private String  key;
    private String  content;
}
