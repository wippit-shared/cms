package com.wippit.cms.backend.utils.crypto;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class DynamicKeyStorage {
    private final ConcurrentHashMap<String, DynamicKey> keys;
    private final DynamicKey serverKey;

    @Getter
    public static DynamicKeyStorage instance = new DynamicKeyStorage();

    private DynamicKeyStorage(){
        keys = new ConcurrentHashMap<>();
        serverKey = new DynamicKey(99, 44);
    }

    public void addKeys(String deviceID, byte[] data) {
        DynamicKey dynamicKey = new DynamicKey(44, data);
        keys.put(deviceID, dynamicKey);
    }

    public DynamicKey getKeys(String deviceID) {
        return keys.get(deviceID);
    }

    public DynamicKey getServerKey() {
        return serverKey;
    }

}
