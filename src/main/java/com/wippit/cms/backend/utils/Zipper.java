package com.wippit.cms.backend.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

@Slf4j
public class Zipper {


    /**
     * Compress a directory into a zip file
     * @param sourceDir Directory path
     * @param outputFile Zip file path
     * @throws Exception
     * */
    public void zipDirectory(String sourceDir, String outputFile) throws Exception {
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(outputFile))) {
            compressDirectoryToZipFile((new File(sourceDir)).toURI(), new File(sourceDir), zipOutputStream);
        }
    }

    private void compressDirectoryToZipFile(URI basePath, File dir, ZipOutputStream out) throws IOException {
        List<File> fileList = Files.list(Paths.get(dir.getAbsolutePath()))
                .map(Path::toFile)
                .collect(Collectors.toList());
        for (File file : fileList) {
            if (file.isDirectory()) {
                compressDirectoryToZipFile(basePath, file, out);
            } else {
                out.putNextEntry(new ZipEntry(basePath.relativize(file.toURI()).getPath()));
                try (FileInputStream in = new FileInputStream(file)) {
                    IOUtils.copy(in, out);
                }
            }
        }
    }

    /**
     * Unzip file to directory
     * @param zipFilePath zip file path
     * @param destDir directory
     * @return true if uncompressed, false if not
     */
    public boolean unzipToDirectory(String zipFilePath, String destDir) {

        File dir = new File(destDir);
        // create output directory if it doesn't exist
        if(!dir.exists()) {
            dir.mkdirs();
        }

        File destDirFile = new File(destDir);
        FileInputStream fis;
        boolean ignoreFile = false;

        //buffer for read and write data to file
        byte[] buffer = new byte[1024];
        try {
            fis = new FileInputStream(zipFilePath);
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry zipEntry = zis.getNextEntry();
            while(zipEntry != null){

                File newFile = newFile(destDirFile, zipEntry);
                if (zipEntry.isDirectory()) {
                    if (!newFile.isDirectory() && !newFile.mkdirs()) {
                        throw new IOException("Failed to create directory " + newFile);
                    }
                }
                else {
                    //---
                    // fix for Windows-created archives
                    //---
                    File parent = newFile.getParentFile();
                    if (!parent.isDirectory() && !parent.mkdirs()) {
                        throw new IOException("Failed to create directory " + parent);
                    }


                    //---
                    //Filter file
                    //---
                    ignoreFile = false;
                    if (newFile.getName().startsWith(".")) {
                        ignoreFile = true;
                    }

                    //---
                    //Write file
                    //---
                    if (ignoreFile) {
                        log.warn("Ignoring file: {}", newFile.getName());
                    }
                    else {
                        FileOutputStream fos = new FileOutputStream(newFile);
                        int len;
                        while ((len = zis.read(buffer)) > 0) {
                            fos.write(buffer, 0, len);
                        }
                        fos.close();
                    }
                }
                zipEntry = zis.getNextEntry();
            }
            //---
            //close last ZipEntry
            //---
            zis.closeEntry();
            zis.close();
            fis.close();

            return true;
        } catch (IOException e) {
            log.error("On unzip file: {}", e.toString());
        }

        return false;
    }

    private File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {

        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }

    public String getRootPath(String path) {
        String rootPath = "";

        int dirCount = 0;
        int fileCount = 0;
        int other = 0;
        String firstDir = "";

        log.debug("Analyzing root path for: {}", path);

        File dirPath = new File(path);
        for (File item : dirPath.listFiles()) {
            if (item.isDirectory()) {
                if (item.getName().equalsIgnoreCase("__MACOSX")) {
                    log.debug("Ignoring directory __MACOSX");
                    continue;
                }

                dirCount++;
                if (dirCount == 1) {
                    firstDir = item.getAbsolutePath();
                }

                log.debug("Found   dir: {}", item.getAbsolutePath());
            }
            else if (item.isFile()) {
                fileCount++;
                log.debug("Found  file: {}", item.getAbsolutePath());
            }
            else {
                other++;
                log.debug("Found other: {}", item.getAbsolutePath());
            }
        }

        log.debug("Counts dir: {} \t file: {} \t other: {}", dirCount, fileCount, other);
        if (dirCount > 0 && fileCount == 0) {
            rootPath = firstDir;
        }
        else if (dirCount == 0 && fileCount == 0) {
            log.warn("Zip is empty");
            rootPath = "";
        }
        else if (fileCount>0) {
            rootPath = path;
        }

        log.debug("Root path: {}", rootPath);

        return rootPath;
    }

}