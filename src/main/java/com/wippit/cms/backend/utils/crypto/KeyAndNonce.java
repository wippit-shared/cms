package com.wippit.cms.backend.utils.crypto;

import com.wippit.cms.backend.utils.ResourceFile;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;

import java.nio.charset.StandardCharsets;

@Data
@Slf4j
public class KeyAndNonce {
    private byte[] key;
    private byte[] nonce;

    public static final String VALID_CHARACTERS_AND_SYMBOLS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-_=+~";
    private static String keySource = "";
    private static int keyLines = 0;

    static final int LEN_KEY = 32;
    static final int LEN_NONCE = 12;

    /**
     * Creates Key&Nonce with random data
     */
    public KeyAndNonce() {
        configureKeyAndNonce(RandomStringUtils.randomAlphanumeric(44));
    }

    /**
     * Creates Key&Nonce for deviceID using index
     * @param deviceID DeviceID
     * @param index Index to use
     */
    public KeyAndNonce(String deviceID, int index) {
        configureKeyAndNonce(new String(DynamicKeyStorage.getInstance().getKeys(deviceID).getKey(index)));
    }

    /**
     * Creates Key&Nonce for server using index
     * @param index Index to use
     */
    public KeyAndNonce(int index) {
//        DynamicKeyStorage.getInstance().getServerKey().getKey(index);
//        ResourceFile resourceFile = new ResourceFile(KeyAndNonce.keySource);
//        String content = resourceFile.getContentAsString();
//        String[] lines = StringUtils.split(content, '\n');
//        if (index < 0 || index >= lines.length) {
//            log.error("Invalid K&N index: {}", index);
//            return;
//        }
//
//        String kn = lines[index];
        configureKeyAndNonce(new String(DynamicKeyStorage.getInstance().getServerKey().getKey(index)));
    }

    /**
     * Creates Ken&Nonce with string
     * @param value Value to use as key & nonce
     */
    public KeyAndNonce(String value) {
        configureKeyAndNonce(value);
    }

    /**
     * Creates Key&Nonce with key & nonce bytes
     * @param key byte[] for key
     * @param nonce byte[] for nonce
     */
    public KeyAndNonce(byte[] key, byte[] nonce) {
        this.key = new byte[key.length];
        this.nonce = new byte[nonce.length];

        int index;
        for (index=0; index< key.length && index < LEN_KEY; index++) {
            this.key[index] = key[index];
        }
        for (index=0; index< nonce.length && index < LEN_NONCE; index++) {
            this.nonce[index] = nonce[index];
        }
    }

    /**
     * Get Key&Nonce from string value
     * @param value Key&Nonce
     */
    private void configureKeyAndNonce(String value) {
        String completeKey;
        if (value.length() < 44) {
            completeKey = value + VALID_CHARACTERS_AND_SYMBOLS;
        }
        else {
            completeKey = value;
        }
        String kn = completeKey.substring(0, 44);
        this.key = kn.substring(0, LEN_KEY).getBytes(StandardCharsets.UTF_8);
        this.nonce = kn.substring(LEN_KEY).getBytes(StandardCharsets.UTF_8);
    }

    /**
     * Get random index for server
     * @return random index
     */
    public static int getRandomIndex() {
        return RandomUtils.nextInt(0, DynamicKeyStorage.getInstance().getServerKey().getSize());
    }

    /**
     * Get random index for deviceID
     * @param deviceID DeviceID
     * @return random index
     */
    public static int getRandomIndex(String deviceID) {
        return RandomUtils.nextInt(0, DynamicKeyStorage.getInstance().getKeys(deviceID).getSize());
    }

    public static void setKeySource(String value) {
        KeyAndNonce.keySource = value;

        ResourceFile resourceFile = new ResourceFile(KeyAndNonce.keySource);
        String content = resourceFile.getContentAsString();
        String[] lines = StringUtils.split(content, '\n');
        KeyAndNonce.keyLines = lines.length;
    }

}
