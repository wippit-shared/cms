package com.wippit.cms.backend.utils.crypto;

import lombok.extern.slf4j.Slf4j;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.ChaCha20ParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

@Slf4j
public class ChaChaLib {

    private final String ALG = "ChaCha20";
    private ChaCha20ParameterSpec paramSpec;
    private SecretKey key;

    public ChaChaLib(KeyAndNonce keyAndNonce) throws Exception{
        this(keyAndNonce.getKey(), keyAndNonce.getNonce());
    }

    public ChaChaLib(byte[] requestedKey, byte[] requestedNonce) {
        try {
            int LENKEY = 32;
            byte[] codeKey = Arrays.copyOfRange(requestedKey, 0, LENKEY);
            int LENIV = 12;
            byte[] codeNonce = Arrays.copyOfRange(requestedNonce, 0, LENIV);

//            log.debug("KEY:   {}", HexUtils.toHexString(codeKey));
//            log.debug("NONCE: {}", HexUtils.toHexString(codeNonce));
            key = new SecretKeySpec(codeKey,  ALG);
            int counter = 8;
            paramSpec = new ChaCha20ParameterSpec(codeNonce, counter);
        }
        catch (Exception ex) {
            log.error("Creating cipher: {}", ex.toString());
        }
    }

    public byte[] encode(String message) throws Exception {
        byte[] msg = message.getBytes(StandardCharsets.UTF_8);
        Cipher cipher = Cipher.getInstance(ALG);
        cipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
        return cipher.doFinal(msg);
    }

    public byte[] encode(byte[] msg) throws Exception {
        Cipher cipher = Cipher.getInstance(ALG);
        cipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
        return cipher.doFinal(msg);
    }

    public byte[] decode(byte[] encodedMessage) throws Exception {
        return encode(encodedMessage);
    }

}
