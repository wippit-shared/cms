package com.wippit.cms.backend.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@Slf4j
public class ResourceFile {
    private InputStream is = null;
    private String filename = "";

    /**
     * Reads a file from resource
     * @param file resource path and filename. Ex: "images/logo_wippit_cms.png"
     */
    public ResourceFile(String file) {
        this.filename = file;
        is = readStream();
    }

    private InputStream readStream() {

        try {
            ClassLoader classLoader = getClass().getClassLoader();
            InputStream inputStream = classLoader.getResourceAsStream(filename);
            if (inputStream == null) {
                throw new IllegalArgumentException("file not found! " + filename);
            } else {
                return inputStream;
            }
        }
        catch (Exception ex) {
            log.error("Loading resource({}) stream: {}", filename, ex.toString());
            return null;
        }
    }

    /**
     * Get content as byte array
     * @return byte array with content
     */
    public byte[] getContentAsBytes() {
        try {
            return is.readAllBytes();
        }
        catch (Exception ex) {
            log.error("Cant get resource({}) as bytes: {}", filename, ex.toString());
        }
        return null;
    }

    /**
     * Get content as stream
     * @return InputStream
     */
    public InputStream getContentAsStream() {
        return is;
    }

    /**
     * Get content as String
     * @return String with content
     */
    public String getContentAsString() {

        try {
            return new String(getContentAsBytes(), StandardCharsets.UTF_8);
        }
        catch (Exception ex) {
            log.error("Cant get resource({}) as String: {}", filename, ex.toString());
            return "";
        }
    }
}
