package com.wippit.cms.backend.utils;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.server.StreamResource;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.types.AppIcons;
import com.wippit.cms.backend.types.StorageTypes;
import com.wippit.cms.backend.types.ToolbarTypes;
import com.wippit.cms.backend.utils.color.Color;
import com.wippit.cms.backend.utils.color.RGBColor;
import com.wippit.cms.backend.utils.color.Solver;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Slf4j
public class ViewUtils {

    public static final int NOTIFICATION_DURATION = 5000;

    private final static DecimalFormat decimalFormat = new DecimalFormat("####.#");

    public static String getBackgroundRandomClass() {
        return "background-view-"+RandomUtils.nextInt(1,10);
    }

    public static String getBackgroundClearClass() {
        return "background-view-clear";
    }

    public static String getBackgroundAboutClass() {
        return "background-view-about";
    }

    public static Image getImageFromContent(byte[] content, String name) {
        try {
            StreamResource streamResource = new StreamResource(name, ()-> new ByteArrayInputStream(content));
            return new Image(streamResource, name);
        }
        catch (Exception ex) {
            log.error("Error getting image (" + name + "): " + ex.toString());
        }

        return null;
    }

    /**
     * Get a MIME image for a storage
     * @param storage Storage
     * @return PNG Image 64x64 px
     */
    public static Image getMimeImage(Storage storage, int size) {

        Image image;
        String resname = "file-other.png";;
        StorageTypes type = StorageTypes.getType(storage.getType());

        switch (type) {
            case IMAGE_AVATAR:
            case IMAGE_BANNER:
            case IMAGE_POST:
            case IMAGE_OTHER:
            case IMAGE_PRODUCT:
            case IMAGE_THUMBNAIL:
                image = new Image(storage.getCompleteURL(), storage.getOriginal_name());
                image.getStyle().set("width","auto");
                image.getStyle().set("height","auto");
                image.getStyle().set("max-width", size + "px");
                image.getStyle().set("max-height",size + "100px");
                image.getStyle().set("display","block");
                return image;

            case TEXT:
                resname = "file-text.png";
                break;

            case ZIP:
                resname = "file-zip.png";
                break;

            case DOCUMENT:
                resname = "file-document.png";
                break;

            case AUDIO:
                resname = "file-audio.png";
                break;

            case VIDEO:
                resname = "file-video.png";
                break;

            case OTHER:
                resname = "file-other.png";
                break;

            default:
                break;
        }

        ResourceFile resourceFile = new ResourceFile("/static/images/icons/" + resname);
        image = ViewUtils.getImageFromContent(resourceFile.getContentAsBytes(), resname);
        if (image == null) {
            image = new Image();
        }
        return image;
    }

    public static Image getImageFromBufferedImage(BufferedImage bufferedImage, String name) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "jpg", baos);
            byte[] imageInByte = baos.toByteArray();
            return getImageFromContent(imageInByte, name);
        }
        catch (Exception ex) {
            log.error("Cant create image: " + ex.toString());
            return new Image();
        }
    }

    public static void showNotificationError(String message) {
        NativeLabel label = new NativeLabel(message);
        Notification notification = new Notification();
        notification.setPosition(Notification.Position.BOTTOM_CENTER);
        notification.setDuration(NOTIFICATION_DURATION);
        notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
        notification.add(label);
        notification.open();
    }

    public static void showNotificationWarning(String message) {
        NativeLabel label = new NativeLabel(message);
        Notification notification = new Notification();
        notification.setPosition(Notification.Position.BOTTOM_CENTER);
        notification.setDuration(NOTIFICATION_DURATION);
        notification.addThemeVariants(NotificationVariant.LUMO_WARNING);
        notification.add(label);
        notification.open();
    }

    public static void showNotification(String message) {
        Div div = new Div();
        div.setText(message);
        div.setSizeFull();
        Notification notification = new Notification(div);
        notification.setPosition(Notification.Position.TOP_CENTER);
        notification.setDuration(NOTIFICATION_DURATION);
        notification.addThemeVariants(NotificationVariant.LUMO_PRIMARY);
        notification.open();
    }

    public static Hr getLine(){
        return new Hr();
    }

    public static Hr getLinePrimary(){
        Hr hr = new Hr();
        hr.addClassName("color_primary");
        return hr;
    }

    public static Hr getLineSecondary(){
        Hr hr = new Hr();
        hr.addClassName("color_secondary");
        return hr;
    }

    public static Div getToolbarLabel(String title){
        Div label = new Div();
        label.getElement().setProperty("innerHTML", title);
        label.setClassName("toolbar_label");
        return label;
    }

    public static Div getToolbarTitle(String title){
        Div label = new Div();
        label.getElement().setProperty("innerHTML", title);
        label.setClassName("toolbar_title");
        return label;
    }

    public static HorizontalLayout getToolbar(ToolbarTypes type){
        HorizontalLayout toolbar = new HorizontalLayout();
        toolbar.setClassName(type.getStyle());
        return toolbar;
    }

    public static Div getDivLabel(String title){
        Div label = new Div();
        label.getElement().setProperty("innerHTML", title);
        return label;
    }

    public static Div getDivLabel(String title, String style){
        Div label = new Div();
        label.getElement().setAttribute("style", style);
        label.getElement().setProperty("innerHTML", title);
        return label;
    }

    public static Div getDivTitleLabel(String title){
        Div label = new Div();
        label.getElement().setAttribute("style", "width:100%; height:50px;text-align:center;");
        label.getElement().setProperty("innerHTML", title);
        return label;
    }

    public static Div getSeparatorDivLabel(String title){
        Div label = new Div();
        label.getElement().setAttribute("style", "font-size:1em;font-weight:bold;width:100%; height:30px;text-align:left; border-bottom: 1px solid darkgrey;");
        label.getElement().setProperty("innerHTML", title);
        return label;
    }

    public static HorizontalLayout getFormFooter() {
        HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
        buttonLayout.addClassName("dialog_footer");
        return buttonLayout;
    }

    public static HorizontalLayout getFooter(){
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addClassName("footer_primary");
        horizontalLayout.setWidth("100%");
        return horizontalLayout;
    }

    public static String getSpan(String text, String style){
        StringBuilder sb = new StringBuilder();
        sb.append("<span ");
        sb.append("style=\"");
        sb.append(style);
        sb.append("\">");
        sb.append(text);
        sb.append("</span>");
        return sb.toString();
    }

    public static Div getWarningLabel(String title){
        Div label = new Div();
        label.getElement().setProperty("innerHTML", title);
        label.setClassName("view_warning_label");
        return label;
    }

    public static Div getErrorLabel(String title){
        Div label = new Div();
        label.getElement().setProperty("innerHTML", title);
        label.setClassName("view_error_label");
        return label;
    }

    public static Div getSpacer(){
        return getSpacer(38);
    }

    public static Div getSpacer(int width){
        Div div = new Div();
        div.setWidth(width + "px");
        div.setHeight("40px");
        return div;
    }

    public static Div getSpacer(int width, int height){
        Div div = new Div();
        div.setWidth(width + "px");
        div.setHeight(height + "px");
        return div;
    }

    public static Button getActiveButton(String title) {
        Button button = new Button(title);
        button.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_LARGE);
        button.addClassName("wippit_active_button");
        return button;
    }

    public static Button getDangerButton(String title) {
        Button button = new Button(title);
        button.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
        button.addClassName("wippit_active_button");
        return button;
    }

    public static Button getButton(String title) {
        Button button = new Button(title);
        button.addThemeVariants(ButtonVariant.LUMO_LARGE);
        button.addClassName("wippit_default_button");
        return button;
    }

    public static Checkbox getCheckbox(String title, boolean value) {
        Checkbox checkbox = new Checkbox(title);
        checkbox.setValue(value);
        return checkbox;
    }

    public static Button getToolbarButton(Icon icon) {
        Button button = new Button(icon);
        button.addThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_SMALL);
        button.addClassName("toolbar_button");
        return button;
    }

    public static Button getToolbarButton(AppIcons svgIcon) {
        Button button = new Button(AppUtils.getToolbarImage(svgIcon));
        button.addThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_SMALL);
        button.addClassName("toolbar_button");
        return button;
    }

    public static Button getToolbarButton(AppIcons svgIcon, String tooltip) {
        Button button = getToolbarButton(svgIcon);
        button.setTooltipText(tooltip);
        return button;
    }

//    public static Button getToolbarButton(AppIcons svgIcon, String tooltip, String color) {
//        Button button = new Button(AppUtils.getToolbarImage(svgIcon));
//        button.addThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_SMALL);
//        button.addClassName("toolbar_button");
//        button.setTooltipText(tooltip);
//        return button;
//    }

    public static Button getToolbarButton(Icon icon, String tooltip) {
        Button button = getToolbarButton(icon);
        button.setTooltipText(tooltip);
        return button;
    }

    public static Button getToolbarButton(Icon icon, String tooltip, String color) {
        icon.setColor(color);
        Button button = getToolbarButton(icon);
        button.setTooltipText(tooltip);
        return button;
    }

    public static Button getToolbarButtonDisabled(Icon icon) {
        Button button = new Button(icon);
        button.addClassName("builder_toolbar_button_disabled");
        button.setEnabled(false);
        return button;
    }

    public static String getFancyFileSize(long size) {
        final double KB = 1014;
        final double MB = KB*KB;

        if (size < KB ) {
            return size + " b";
        }
        else if (size < MB) {
            double sizeKB = (double)(size)/KB;
            return decimalFormat.format(sizeKB) + " Kb";
        }
        else {
            double sizeMB = (double)(size)/MB;
            return decimalFormat.format(sizeMB) + " Mb";
        }
    }

    public static  String getFancyDateAndTime(LocalDateTime dateTime) {

        if (dateTime == null) return "";

        StringBuilder sb = new StringBuilder();
        LocalDateTime today = LocalDateTime.now();
        LocalDateTime yesterday = LocalDateTime.now().plusDays(-1);
        LocalDateTime tomorrow = LocalDateTime.now().plusDays(1);

        if (today.getDayOfYear() == dateTime.getDayOfYear()
                && today.getYear() == dateTime.getYear()
        ) {
            sb.append("Today ");
        }
        else if (yesterday.getDayOfYear() == dateTime.getDayOfYear()
                && yesterday.getYear() == dateTime.getYear()
        ) {
            sb.append("Yesterday ");
        }
        else if (tomorrow.getDayOfYear() == dateTime.getDayOfYear()
                && tomorrow.getYear() == dateTime.getYear()
        ) {
            sb.append("Tomorrow ");
        }
        else {
            sb.append(dateTime.getMonth()).append("/");
            sb.append(dateTime.getDayOfMonth()).append("/");
            sb.append(dateTime.getYear()).append(" ");
        }

        sb.append(" ");
        sb.append(dateTime.getHour());
        sb.append(":");
        if (dateTime.getMinute() < 10) {
            sb.append("0").append(dateTime.getMinute());
        }
        else {
            sb.append(dateTime.getMinute());
        }


        return sb.toString();
    }

    public static  String getShortDateAndTime(LocalDateTime dateTime) {

        if (dateTime == null) return "";

        StringBuilder sb = new StringBuilder();
        sb.append(dateTime.getYear()).append("/");
        sb.append(dateTime.getMonthValue()).append("/");
        sb.append(dateTime.getDayOfMonth());
        sb.append(" ");
        sb.append(dateTime.getHour());
        sb.append(":");
        if (dateTime.getMinute() < 10) {
            sb.append("0").append(dateTime.getMinute());
        }
        else {
            sb.append(dateTime.getMinute());
        }

        return sb.toString();
    }

    public static  String getShortDate(LocalDateTime dateTime) {

        if (dateTime == null) return "";

        StringBuilder sb = new StringBuilder();

        sb.append(dateTime.getYear()).append("/");
        sb.append(dateTime.getMonthValue()).append("/");
        sb.append(dateTime.getDayOfMonth());

        return sb.toString();
    }

    public static  String getPracticalDate(LocalDateTime dateTime) {

        if (dateTime == null) return "";

        StringBuilder sb = new StringBuilder();

        sb.append(dateTime.getYear() + "-");
        if (dateTime.getMonthValue() < 10) {
            sb.append("0" + dateTime.getMonthValue() + "-");
        }
        else {
            sb.append(dateTime.getMonthValue() + "-");
        }
        if (dateTime.getDayOfMonth() < 10) {
            sb.append("0" + dateTime.getDayOfMonth());
        }
        else {
            sb.append(dateTime.getDayOfMonth());
        }

        return sb.toString();
    }

    public static  String getMediumDate(LocalDate dateTime) {

        if (dateTime == null) return "";

        StringBuilder sb = new StringBuilder();

        sb.append(dateTime.getYear() + "-");
        switch (dateTime.getMonthValue()) {
            case 1:sb.append("Ene");break;
            case 2:sb.append("Feb");break;
            case 3:sb.append("Mar");break;
            case 4:sb.append("Abr");break;
            case 5:sb.append("May");break;
            case 6:sb.append("Jun");break;
            case 7:sb.append("Jul");break;
            case 8:sb.append("Ago");break;
            case 9:sb.append("Sep");break;
            case 10:sb.append("Oct");break;
            case 11:sb.append("Nov");break;
            case 12:sb.append("Dic");break;
        }
        sb.append("-");
        sb.append(dateTime.getDayOfMonth());

        return sb.toString();
    }

    public static H1 getHeaderLabel(String title){
        H1 label = new H1();
        label.setSizeFull();
        label.setText(title);
        return label;
    }

    public static H2 getTitleLabel(String title){
        H2 label = new H2();
        label.setSizeFull();
        label.setText(title);
        label.setClassName("title_label");
        return label;
    }

    public static H3 getSubtitleLabel(String title){
        H3 label = new H3();
        label.setSizeFull();
        label.setText(title);
        label.setClassName("title_label");
        return label;
    }

    public static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8);
        }
        catch (Exception ex) {
            log.error("Encoding url[{}]: {}", value, ex.toString());
            return "";
        }
    }

    public static DatePicker getDatePickerMexico(String label) {
        DatePicker datePicker = new DatePicker(label);
        DatePicker.DatePickerI18n spanishI18n = new DatePicker.DatePickerI18n();
        spanishI18n.setMonthNames(List.of("Enero", "Febrero", "Marzo", "Abril",
                "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
                "Noviembre", "Diciembre"));
        spanishI18n.setWeekdays(List.of("Domingo", "Lunes", "Martes", "Miércoles",
                "Jueves", "Viernes", "Sábado"));
        spanishI18n.setWeekdaysShort(
                List.of("Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"));
        //spanishI18n.setWeek("Semana");
        spanishI18n.setToday("Hoy");
        spanishI18n.setCancel("Cancelar");

        datePicker.setI18n(spanishI18n);
        datePicker.setLocale(new Locale("es", "MX"));

        return datePicker;
    }

    public static Optional<String> getCookie(String key, HttpServletRequest request) {
        if (request == null) {
            return Optional.empty();
        }
        if (request.getCookies() == null) {
            return Optional.empty();
        }

        return Arrays.stream(request.getCookies())
                .filter(c -> key.equals(c.getName()))
                .map(Cookie::getValue)
                .findAny();
    }

    public static String toImageProperties(byte[] data) {
        try {
            InputStream in = new ByteArrayInputStream(data);
            BufferedImage buf = ImageIO.read(in);
            //ColorModel model = buf.getColorModel();

            int h = buf.getHeight();
            int w = buf.getWidth();
            Size size = new Size(w,h);
            return size.toJSON();
        }
        catch (Exception ex) {
            log.error("Cant get image properties: {}", ex.toString());
        }

        return "";
    }

    public static Size getSizeFromProperties(String properties) {

        Size size = Size.fromJSON(properties);
        if (size != null) {
            return size;
        }

        return new Size();
    }

    public static String getColorFilter(String hexColor) {
        Color color = Color.hex(hexColor);
        Solver solver = new Solver(new RGBColor(color.getRed(),color.getGreen(),color.getBlue()));
        Solver.Result result = solver.solve();
        return result.getFilter();
    }

}
