

package com.wippit.cms.backend.utils;

import com.wippit.cms.backend.data.bean.site.Country;
import com.wippit.cms.backend.data.bean.site.CountryCode;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Slf4j
public class CountriesUtil {

    public static Country getCountryByCodes(String iso2, String iso3, String phone) {

        log.debug("getCountryByCodes. iso2: {} \tiso3: {} \t Phone: {}", iso2, iso3, phone);

        CountryCode selected = CountryCode.getByAlpha2Code(iso2);
        if (selected == null) {
            selected = CountryCode.getByAlpha3Code(iso3);
            if (selected == null) {
                selected = CountryCode.getByPhone(phone);
                if (selected == null) {
                    log.error("Country not found ({}, {}, {})", iso2, iso3, phone);
                    Country country = new Country();
                    country.setStatus(1);
                    country.setName(CountryCode.US.getName());
                    country.setPhone(CountryCode.US.getPhone());
                    country.setCurrency_name(CountryCode.US.getCurrency().getDisplayName());
                    country.setCurrency_symbol(CountryCode.US.getCurrency().getSymbol());
                    country.setIso2(CountryCode.US.getAlpha2());
                    country.setIso3(CountryCode.US.getAlpha3());
                }
            }
        }

        log.debug("getCountryByCodes: {}", selected.getName());
        return CountryCode2Country(selected);
    }

    public static Country getCountryByISO2(String code) {

        CountryCode selected = CountryCode.getByAlpha2Code(code);
        if (selected != null) {
            return CountryCode2Country(selected);
        }
        return null;
    }

    public static Country getCountryByISO3(String code) {

        CountryCode selected = CountryCode.getByAlpha3Code(code);
        if (selected != null) {
            return CountryCode2Country(selected);
        }

        return null;
    }

    public static Country getCountryByPhone(String code) {
        CountryCode selected = CountryCode.getByPhone(code);
        if (selected != null) {
            return CountryCode2Country(selected);
        }
        return null;
    }

    private static Country CountryCode2Country(CountryCode selected) {
        if (selected == null) {
            return null;
        }

        if (selected.getAlpha2() == null
                || selected.getAlpha3() == null
                || selected.getName() == null
                || selected.getCurrency() == null)
        {
          return null;
        }

        Country country = new Country();
        country.setId(selected.getId());
        country.setIso2(selected.getAlpha2());
        country.setIso3(selected.getAlpha3());
        country.setName(selected.getName());
        country.setStatus(1);
        country.setCurrency_symbol(selected.getCurrency().getCurrencyCode());
        country.setCurrency_name(selected.getCurrency().getDisplayName());
        country.setPhone(selected.getPhone());
        return country;
    }

    public static List<Country> getCountries() {

        List<Country> countries = new ArrayList<>();
        int index = 1;
        for (CountryCode countryCode : CountryCode.values()) {
            Country country = CountryCode2Country(countryCode);
            if (country != null) {
                country.setId(index);
                countries.add(CountryCode2Country(countryCode));
                index++;
            }
            else {
                log.warn("Country is invalid: {}", countryCode.getName());
            }
        }
        countries.sort(Comparator.comparing(Country::getName));
        return countries;
    }
}
