package com.wippit.cms.backend.utils.cache;

import com.wippit.cms.AppConstants;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.Tika;

import java.nio.file.Path;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class FileTreeCache {

    private final ConcurrentHashMap<String, FileCacheItem> fileMap;
    private final ConcurrentHashMap<String, FileCacheItem> internalMap;
    private final String root;
    private final boolean hideDotItems;
    @Getter
    private long sizeTotal = 0;
    @Getter
    private long filesTotal = 0;
    @Getter
    private FileCacheItem indexItem = null;

    @Getter
    @Setter
    private boolean flagDisplay = false;

    @Setter
    @Getter
    private String requestedPageIndex;

    public FileTreeCache(){
        this.root = "";
        hideDotItems = true;
        fileMap = new ConcurrentHashMap<>();
        internalMap = new ConcurrentHashMap<>();
    }

    public FileTreeCache(String root){
        this(root, true);
    }

    public FileTreeCache(String root, boolean hideDotItems){
        this.root = root;
        this.hideDotItems = hideDotItems;
        this.requestedPageIndex = requestedPageIndex;
        fileMap = new ConcurrentHashMap<>();
        internalMap = new ConcurrentHashMap<>();
        //reload();
    }

    public void reload(){
        fileMap.clear();
        internalMap.clear();

        Tika tika = new Tika();
        byte[] data;
        FileTree fileTree = new FileTree(root);
        int left = root.length();

        try {
            List<Path> files = fileTree.getFiles();

            for (Path item : files) {

                data = FileUtils.readFileToByteArray(item.toFile());
                String mime = tika.detect(data, item.getFileName().toString());
                long size = data.length;

                FileCacheItem fileItem = new FileCacheItem(
                        item,
                        size,
                        mime,
                        item.toAbsolutePath().toString().substring(left),
                        false
                );
                if (hideDotItems) {
                    if (fileItem.getFilename().startsWith(".")) {
                        log.debug("Ignored: {}", fileItem.getPath().toAbsolutePath());
                        continue;
                    }
                }

                if (isIndex(fileItem.getName())) {
                    indexItem = fileItem;
                    indexItem.setIndex(true);
                }

                sizeTotal += size;
                filesTotal++;

                if (fileItem.isInternal()) {
                    internalMap.put(fileItem.getName(), fileItem);
                }
                fileMap.put(fileItem.getName(), fileItem);

                if (flagDisplay) {
                    log.debug("Added[{}]:  {} / {} ", fileItem.getName(), fileItem.getMime(), fileItem.getSize());
                }
            }
        }
        catch (Exception ex) {
            log.error("On reload: {}", ex.toString());
        }

        if (indexItem != null) {
            log.debug("Index[{}]:  {} / {} ", indexItem.getName(), indexItem.getMime(), indexItem.getSize());
        }
        else {
            log.warn("Index: NULL");
        }
    }

    public long itemsCount() {
        return fileMap.size();
    }

    private boolean isIndex(String name) {

        if (!StringUtils.isBlank(requestedPageIndex) && name.equalsIgnoreCase(requestedPageIndex)) {
            return true;
        }

        if (name.equalsIgnoreCase("/" + AppConstants.TEMPLATE_ROOT + AppConstants.TEMPLATE_EXT)) {
            return true;
        }
        if (name.equalsIgnoreCase("/_" + AppConstants.TEMPLATE_ROOT + AppConstants.TEMPLATE_EXT)) {
            return true;
        }
        else if (name.equalsIgnoreCase("/" + AppConstants.TEMPLATE_ROOT + AppConstants.TEMPLATE_DOC1)) {
            return true;
        }
        else return name.equalsIgnoreCase("/" + AppConstants.TEMPLATE_ROOT + AppConstants.TEMPLATE_DOC2);
    }

    public FileCacheItem getItem(String name) {
        return fileMap.get(name);
    }

    public FileCacheItem getInternalItem(String name) {
        return internalMap.get(name);
    }

    public boolean setIndex(String name) {
        FileCacheItem index = getItem(name);
        if (index != null) {
            indexItem = index;
            return true;
        }

        return false;
    }

    public boolean isIndex(FileCacheItem item) {
        if (indexItem == null) {
            return false;
        }

        return item.getName().equalsIgnoreCase(indexItem.getName());
    }

    public FileCacheItem similarItem(String name, String ext) {
        String searchItem = name.toLowerCase();
        Enumeration<String> keys = fileMap.keys();
        while(keys.hasMoreElements()) {
            String itemName = keys.nextElement();

            if (itemName.toLowerCase().contains(searchItem) && itemName.toLowerCase().contains(ext)) {
                return fileMap.get(itemName);
            }
        }

        return null;
    }
}
