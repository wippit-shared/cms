package com.wippit.cms.backend.utils.cache;

import com.wippit.cms.backend.types.CMSItemTypes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.nio.file.Path;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileCacheItem {
    private Path path = null;
    private long size = 0;
    private String mime = "";
    private String name = "";
    private boolean index = false;

    public String getFilename() {
        return path.getFileName().toString();
    }

    public CMSItemTypes getType() {
        return CMSItemTypes.getType(getFilename());
    }

    public CMSItemTypes getTypeInSection(String section) {
        return CMSItemTypes.getType(getFilename(), section);
    }

    public boolean isInternal() {
        return getFilename().startsWith("_");
    }

}
