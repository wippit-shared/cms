package com.wippit.cms.backend.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class Size {
    private int width = 250;
    private int height = 250;

    public Size(){
    }

    public Size(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setSize(Size size) {
        this.width = size.getWidth();
        this.height = size.getHeight();
    }

    public Size getInnerSize(int margin) {
        if (margin > 1 && margin*2 < width && margin*2 < height) {
            return new Size(width - 2 * margin, height - 2 * margin);
        }

        return this;
    }

    public Size getSmallerSize(int margin) {
        if (margin > 1 && margin < width && margin < height) {
            return new Size(width - margin, height - margin);
        }

        return this;
    }

    public Size getBiggerSize(int margin) {
        return new Size(width + margin, height + margin);
    }

    @JsonIgnore
    public String toJSON() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        }
        catch (Exception ex) {
            log.error("Cant serialize card: {}", ex.toString());
        }
        return "";
    }

    @JsonIgnore
    public static Size fromJSON(String json) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(json, Size.class);
        }
        catch (Exception ex) {
            return  null;
        }
    }


    @JsonIgnore
    public String toDisplay() {
        return width +
                "x" +
                height +
                " px";
    }

}
