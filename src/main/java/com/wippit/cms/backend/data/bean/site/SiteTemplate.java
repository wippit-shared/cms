package com.wippit.cms.backend.data.bean.site;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class SiteTemplate   {
    private long            id = 0;
    private String          name = "default";
    private String          page_root = "";
    private String          page_404 = "";
    private String          page_500 = "";
    private String          page_author = "";
    private String          page_search = "";
    private String          page_user = "";
    private String          page_blog = "";
    private String          page_item = "";
}
