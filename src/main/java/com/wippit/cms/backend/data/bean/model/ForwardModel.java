package com.wippit.cms.backend.data.bean.model;

import lombok.Data;

@Data
public class ForwardModel {
    private String name = "";
    private String url = "";

    public ForwardModel() {
    }

    public ForwardModel(String name, String url) {
        this.name = name;
        this.url = url;
    }

}
