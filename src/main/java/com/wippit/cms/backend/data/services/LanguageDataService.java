package com.wippit.cms.backend.data.services;

import com.wippit.cms.AppConstants;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.dao.LangDao;
import com.wippit.cms.backend.types.StatusTypes;
import com.wippit.cms.backend.utils.i18n.LanguageItem;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class LanguageDataService {
    private final LangDao dao;
    @Getter
    @Setter
    private String template = AppGlobal.getInstance().getSiteInfo().getTemplate();
    @Getter
    @Setter
    private String iso = AppGlobal.getInstance().getSiteInfo().getCountry_iso();
    @Getter
    @Setter
    private String page = "";

    public LanguageDataService(@Autowired LangDao dao) {
        this.dao = dao;
    }

    public LanguageItem getItem(String template, String page, String iso, String key) {
        return dao.getItem(template, page, iso, key);
    }

    public String getTranslationAndStore(String template, String page, String iso, String key) {
        LanguageItem item = dao.getItem(template, page, iso, key);
        if (item != null) {
            return item.getContent();
        }

        if (addItem(template, page, iso, key, key)) {
            return key;
        }

        return "";
    }

    public String getTranslation(String template, String page, String iso, String key) {
        LanguageItem item = dao.getItem(template, page, iso, key);
        if (item == null) {
            return key;
        }

        return item.getContent();
    }

    public boolean addItem(String template, String page, String iso, String key, String content) {
        int added = dao.addItem(template, page, iso, key, content);
        return (added>0);
    }

    public List<LocalCountry> getCountries() {
        List<LocalCountry> countries = dao.getCountries();
        if (countries == null) {
            return Collections.emptyList();
        }

        return countries;
    }

    public List<LocalCountry> getActiveCountries() {
        List<LocalCountry> countries = dao.getCountriesByStatus(StatusTypes.ACTIVE.value());
        if (countries == null) {
            return Collections.emptyList();
        }

        return countries;
    }

    public List<String> getPages() {
        return dao.getPagesForCountryAndTemplate(iso, template);
    }

    public Page<LanguageItem> getPage(int pageNumber, int limit) {
        long offset = (long) pageNumber * AppConstants.QUERY_DEFAULT_LIMIT;
        Page<LanguageItem> page = new PageImpl<LanguageItem>(fetch(offset, limit));
        return page;
    }

    public int getCount() {
        if (StringUtils.isBlank(page)) {
            return dao.getItemsForCountryAndTemplateCount(iso, template);
        }
        else {
            return dao.getItemsForCountryAndTemplateAndPageCount(iso, template, page);
        }
    }

    private List<LanguageItem> fetch(long offset, int limit) {
        if (StringUtils.isBlank(page)) {
            return dao.getItemsForCountryAndTemplate(iso, template, offset, limit);
        }
        else {
            return dao.getItemsForCountryAndTemplateAndPage(iso, template, page, offset, limit);
        }
    }


    public boolean resetLocalization(String template, String iso) {
        int removed = dao.resetLocalization(template, iso);
        return (removed>0);
    }

    public boolean resetLocalizationByPage(String template, String iso, String page) {
        int removed = dao.resetLocalizationByPage(template, iso, page);
        return (removed>0);
    }
}
