/*
 * Copyright (c) 2019-2020. Onewip Corp. All rights reserved.
 * onewip.com/terms
 * contact@onewip.com
 */

package com.wippit.cms.backend.data.dao;

import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.utils.i18n.LanguageItem;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface LangDao {

    //****************************************************************************************************************
    //Internationalization
    //****************************************************************************************************************

    //----------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_languages where template = #{template} and page = #{page} and iso = #{iso} and key = #{key}")
    LanguageItem getItem(
            @Param("template") String template,
            @Param("page") String page,
            @Param("iso") String iso,
            @Param("key") String key
    );

    //----------------------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_languages (template, page, iso, key, content) " +
            "values (#{template}, #{page}, #{iso}, #{key}, #{content}) " +
            "on conflict (template, page, iso, key) do update " +
            "set content = #{content}")
    int addItem(
            @Param("template") String template,
            @Param("page") String page,
            @Param("iso") String locale,
            @Param("key") String key,
            @Param("content") String content
    );

    //----------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_country order by name asc")
    List<LocalCountry> getCountries();

    //----------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_country " +
            "where status = #{status}" +
            "order by name asc")
    List<LocalCountry> getCountriesByStatus(
            @Param("status") int status
    );

    //----------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_languages " +
            "where template = #{template} " +
            "and iso = #{iso} " +
            "and page = #{page} " +
            "order by key asc " +
            "offset #{offset} limit #{limit} "
    )
    List<LanguageItem> getItemsForCountryAndTemplateAndPage(
            @Param("iso") String locale,
            @Param("template") String template,
            @Param("page") String page,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //----------------------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_languages " +
            "where template = #{template} " +
            "and iso = #{iso} " +
            "and page = #{page} "
    )
    int getItemsForCountryAndTemplateAndPageCount(
            @Param("iso") String locale,
            @Param("template") String template,
            @Param("page") String page
    );

    //----------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_languages " +
            "where template = #{template} " +
            "and iso = #{iso} " +
            "order by page asc, key asc "+
            "offset #{offset} limit #{limit} "
    )
    List<LanguageItem> getItemsForCountryAndTemplate(
            @Param("iso") String locale,
            @Param("template") String template,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //----------------------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_languages " +
            "where template = #{template} " +
            "and iso = #{iso} "
    )
    int getItemsForCountryAndTemplateCount(
            @Param("iso") String locale,
            @Param("template") String template
    );

    //----------------------------------------------------------------------------------------------------------------
    @Select("select distinct(page) from cms.ow_languages " +
            "where template = #{template} " +
            "and iso = #{iso}")
    List<String> getPagesForCountryAndTemplate(
            @Param("iso") String locale,
            @Param("template") String template
    );

    //----------------------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_languages " +
            "where template = #{template} " +
            "and iso = #{iso}")
    int resetLocalization(
            @Param("template") String template,
            @Param("iso") String iso
    );

    //----------------------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_languages " +
            "where template = #{template} " +
            "and iso = #{iso} " +
            "and page = #{page}")
    int resetLocalizationByPage(
            @Param("template") String template,
            @Param("iso") String iso,
            @Param("page") String page
    );


}

