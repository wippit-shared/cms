package com.wippit.cms.backend.data.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class PostQueryFilter {
    private String iso;
    private String section;
    private String tags;
    private String title;

    @JsonIgnore
    public static PostQueryFilter fromJSON(String json) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(json, PostQueryFilter.class);
        }
        catch (Exception ex) {
            log.error("Cant get PostQueryFilter Json: {}", ex.toString());
            return  null;
        }
    }
}
