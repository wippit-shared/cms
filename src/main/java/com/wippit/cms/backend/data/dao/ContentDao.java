/*
 * Copyright (c) 2019-2020. Onewip Corp. All rights reserved.
 * onewip.com/terms
 * contact@onewip.com
 */

package com.wippit.cms.backend.data.dao;

import com.wippit.cms.backend.data.bean.content.*;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.bean.template.TemplateSection;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.types.PostSortTypes;
import org.apache.ibatis.annotations.*;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface ContentDao {

    //****************************************************************************************************************
    //HomeIndex
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_template_index " +
            "where template = #{template}")
    List<HomeIndex> getHomeIndexByTemplate(
            @Param("template") String template
    );

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_template_index (iso, template, page) " +
            "values (#{iso}, #{template}, #{page}) " +
            "on conflict (template, iso) do update set " +
            "page = #{page}, " +
            "created_date = now()")
    int addHomeIndex(
            @Param("template") String template,
            @Param("iso") String iso,
            @Param("page") String page
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_template_index where iso = #{iso}")
    int removeHomeIndexByISO(
            @Param("iso") String iso
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_template_index where template = #{template}")
    int removeHomeIndexByTemplate(
            @Param("template") String template
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_template_index where " +
            "iso = #{iso} and " +
            "template = #{template}")
    int removeHomeIndexByTemplateAndISO(
            @Param("template") String template,
            @Param("iso") String iso
    );

    //****************************************************************************************************************
    //Redirect
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_redirect " +
            "where iso = #{iso} " +
            "and name = #{name} " +
            "and status = 15001 " +
            "and start_date < now() " +
            "and end_date > now() ")
    ContentRedirect getRedirect(
            @Param("iso") String iso,
            @Param("name") String name
    );


    //****************************************************************************************************************
    //Content
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_storage where url = #{url}")
    Storage getStorageByURL(
            @Param("url") String url
    );


    //****************************************************************************************************************
    //Section
    //****************************************************************************************************************

    //------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_section where iso = #{iso} and secID = #{secID}")
    Section getSectionsByISOAndURL(
            @Param("iso") String iso,
            @Param("secID") String name
    );

    //****************************************************************************************************************
    //Account
    //****************************************************************************************************************


    //------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_account where alias = #{alias}")
    Account selectAccountByName(
            @Param("alias") String alias
    );


    //****************************************************************************************************************
    //TemplateSection
    //****************************************************************************************************************


    //------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_template_section where name = #{template}")
    List<TemplateSection> getTemplateSections(
            @Param("template") String template
    );

    //------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_template_section " +
            "where name = #{template} " +
            "and iso = #{iso} " +
            "and secID = #{secID}")
    TemplateSection getTemplateSection(
            @Param("template") String template,
            @Param("iso") String iso,
            @Param("secID") String section
    );

    //****************************************************************************************************************
    //Content
    //****************************************************************************************************************

    //------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_content where iso = #{iso} and url = #{url}")
    Article getContentByISOAndURL(
            @Param("iso") String iso,
            @Param("url") String url
    );

    //------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_content where cid = #{cid}")
    Article getContentByCID(
            @Param("cid") String cid
    );

    //------------------------------------------------------------------------------------------------------------
    @Select("select A.* " +
            "from cms.ow_content_home H, " +
            "cms.ow_content A " +
            "where H.iso = #{iso} " +
            "and H.index = 1 " +
            "and A.cid = H.cid")
    IntroPost getMainArticle(
            @Param("iso") String iso
    );

    //------------------------------------------------------------------------------------------------------------
    @SelectProvider(type=ContentQueryBuilder.class, method = "getArticles")
    List<IntroPost> getArticles(
            @Param("iso") String iso,
            @Param("secID") String secID,
            @Param("home") int home,
            @Param("sort") PostSortTypes sortType,
            @Param("offset") int offset,
            @Param("limit") int limit
    );

    //------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_content where ocid = ${ocid}")
    List<IntroPost> getArticlesFromOriginal(
            @Param("ocid") String ocid
    );

    //------------------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_content where ocid = ${ocid}")
    int getArticlesFromOriginalCount(
            @Param("ocid") String ocid
    );

    //------------------------------------------------------------------------------------------------------------
    @SelectProvider(type=ContentQueryBuilder.class, method = "getArticlesCount")
    int getArticlesCount(
            @Param("iso") String iso,
            @Param("secID") String secID,
            @Param("home") int home
    );


    //------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_redirect " +
            "where iso = #{iso} " +
            "offset #{offset} limit #{limit}")
    List<ContentRedirect> getContentRedirect(
            @Param("iso") String iso,
            @Param("offset") int offset,
            @Param("limit") int limit
    );

    //------------------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) " +
            "from cms.ow_redirect " +
            "where iso = #{iso}")
    int getContentRedirectCount(
            @Param("iso") String iso
    );

    //------------------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_redirect (iso, name, url, status, start_date, end_date) " +
            "values (#{iso}, #{name}, #{url}, #{status}, #{start_date}, #{end_date}) " +
            "on conflict (iso, name) do update " +
            "set url = #{url}, " +
            "status = #{status}, " +
            "start_date = #{start_date}, " +
            "end_date = #{end_date} ")
    int addRedirect(
            @Param("iso") String iso,
            @Param("name") String name,
            @Param("url") String url,
            @Param("status") int status,
            @Param("start_date") Timestamp start,
            @Param("end_date") Timestamp end
    );

    //------------------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_redirect where iso = #{iso} and name = #{name}")
    int deleteRedirect(
            @Param("iso") String iso,
            @Param("name") String name
    );

    //****************************************************************************************************************
    //Countries
    //****************************************************************************************************************

    //----------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_country order by name asc")
    List<LocalCountry> getCountries();


}

