package com.wippit.cms.backend.data.bean.content;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wippit.cms.backend.types.ContentStatusTypes;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;

@Data
@Slf4j
public class ApiFullPost {
    private long id;
    private String cid = "";
    private String title = "";
    private String url = "";
    private String iso = "";
    private String section_url = "";
    private String tags = "";
    private String image_url = "";
    private String content_intro = "";
    private String content_complete = "";
    private String author = "";
    private int status = ContentStatusTypes.NOT_PUBLISHED.value();
    private int hit_count = 0;

    @JsonIgnore
    private Timestamp start_date;
    @JsonIgnore
    private Timestamp end_date;
    @JsonIgnore
    private Timestamp created_date;
    @JsonIgnore
    private Timestamp event_date;
    private long startDate;
    private long endDate;
    private long eventDate;
    private long createdDate;

    @JsonIgnore
    public String toJSON() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        }
        catch (Exception ex) {
            log.error("Cant serialize ApiFullPost: {}", ex.toString());
        }

        return "";
    }
}
