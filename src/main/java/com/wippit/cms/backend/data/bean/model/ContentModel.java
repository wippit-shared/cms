package com.wippit.cms.backend.data.bean.model;

import com.wippit.cms.backend.data.bean.content.IntroPost;
import com.wippit.cms.backend.data.bean.content.Section;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.services.IntroPostDataService;
import com.wippit.cms.backend.types.SectionTypes;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ContentModel {
    private final String iso;
    private final IntroPostDataService dataService;
    @Setter
    private int pageIndex = 0;

    private String secID = "";

    private boolean flagStartZero = true;

    public void setArrayZero() {
        flagStartZero = true;
    }

    public void setArrayOne(){
        flagStartZero = false;
    }

    public ContentModel(String iso, IntroPostDataService dataService) {
        this.iso = iso;
        this.dataService = dataService;
    }

    public ContentModel(String iso, String secID, IntroPostDataService dataService) {
        this.iso = iso;
        this.secID = secID;
        this.dataService = dataService;
    }

    public ContentModelItem getHome() {
        log.debug("Content HOME");
        ContentModelItem  item = new  ContentModelItem("home", iso, dataService, SectionTypes.HOME);
        item.setPageIndex(pageIndex);
        item.setFlagArrayZero(flagStartZero);
        return item;
    }

    public ContentModelItem getBlog() {
        log.debug("Content BLOG");
        this.secID = "blog";
        return getSection();
    }

    public ContentModelItem getSection() {
        log.debug("Content ARTICLES: {}", secID);
        ContentModelItem  item = new  ContentModelItem(secID, iso, dataService, SectionTypes.ARTICLES);
        item.setPageIndex(pageIndex);
        item.setFlagArrayZero(flagStartZero);
        return item;
    }

    public ContentModelItem getNotes(String name) {
        log.debug("Content NOTES: {}", name);
        ContentModelItem  item = new  ContentModelItem(name, iso, dataService, SectionTypes.NOTES);
        item.setPageIndex(0);
        item.setFlagArrayZero(flagStartZero);
        return item;
    }

    public ArticleIntroModel getMain() {
        IntroPost introPost = dataService.getMainArticle(iso);
        Account author = dataService.getAccount(introPost.getAuthor());
        Section section = dataService.getSection(iso, introPost.getSecID());
        if (section == null) {
            section = new Section();
        }

        return new ArticleIntroModel(introPost, author, section.getName(), section.getFullURL(), section.getSecID());
    }

}
