package com.wippit.cms.backend.data.services;

import com.wippit.cms.AppConstants;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.content.Article;
import com.wippit.cms.backend.data.bean.content.ArticleBrief;
import com.wippit.cms.backend.data.bean.content.Section;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.dao.ArticlesDao;
import com.wippit.cms.backend.data.dao.LangDao;
import com.wippit.cms.backend.types.ContentStatusTypes;
import com.wippit.cms.backend.types.HomeTypes;
import com.wippit.cms.backend.types.RoleTypes;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Component
public class ArticleDataService {
    private final ArticlesDao articlesDao;
    private final LangDao langDao;

    @Getter
    @Setter
    private String iso = "";
    @Getter
    @Setter
    private String section = "";
    @Getter
    @Setter
    private ContentStatusTypes status = ContentStatusTypes.ANY;
    @Setter
    @Getter
    private String author = "";
    @Setter
    @Getter
    private HomeTypes home = HomeTypes.ALL;


    public ArticleDataService(@Autowired ArticlesDao articlesDao, @Autowired LangDao langDao) {
        this.articlesDao = articlesDao;
        this.langDao = langDao;
        this.iso = AppGlobal.getInstance().getSiteInfo().getCountry_iso();
    }

    public Page<ArticleBrief> getPage(int pageNumber, int limit) {
        int offset = pageNumber * AppConstants.QUERY_DEFAULT_LIMIT;
        return new PageImpl<>(fetch(offset, limit));
    }

    public List<ArticleBrief> fetch(int offset, int limit) {
        List<ArticleBrief> items = null;
        items = articlesDao.getArticles(iso, section,  author, status, home, offset, limit);
        if (items == null) {
            items = Collections.emptyList();
        }

        return items;
    }

    public int getCount() {
        return articlesDao.getArticlesCount(iso, section, author, status, home);
    }

    public List<Section> getSections() {
        List<Section> items = articlesDao.getSectionsByISO(iso, 0, 999);
        if (items == null) {
            items = Collections.emptyList();
        }

        return items;
    }

    public List<Account> getAuthors() {
        List<Account> items = articlesDao.getAllAccountsByRole(RoleTypes.AUTHOR.getCode());
        if (items == null) {
            items = Collections.emptyList();
        }

        return items;
    }

    public Article getArticleByCID(String cid) {
        Article item = articlesDao.getContentByCID(cid);

        if (item == null) {
            item = new Article();
            item.setCid(AppUtils.createContentID());
            item.setIso(AppGlobal.getInstance().getSiteInfo().getCountry_iso());
            item.setSecID("blog");
            item.setSection_url("blog");

            item.setContent_intro("Hello world intro");
            item.setContent_complete("Hello world");
            item.setStart_date(AppUtils.getNowPlusMinutes(1));
            item.setEnd_date(AppUtils.getNowPlusMonths(12*999));

            String title = getNewTitle("New Article");
            item.setTitle(title);
        }

        return item;
    }

    public Account getAuthor(String username) {
        return articlesDao.selectAccountByName(username);
    }

    public boolean saveArticle(Article article) {
        int added = articlesDao.addContent(
                article.getCid(),
                article.getOcid(),
                article.getIso(),
                article.getUrl(),
                article.getSecID(),
                article.getSection_type(),
                article.getTitle(),
                article.getTags(),
                article.getImage_fid(),
                article.getImage_url(),
                article.getContent_intro(),
                article.getContent_complete(),
                article.getAuthor(),
                article.getStatus(),
                article.getStart_date(),
                article.getEnd_date(),
                article.getHome());

        if (article.getHome()>0) {

            //---
            //Add article to home
            //---

            boolean flagAdded = false;
            List<String> currentCIDS = getHomeCIDS(article.getIso());
            List<String> cleanedCIDS  = new ArrayList<>();
            List<String> newCIDS  = new ArrayList<>();

            //Clean from CID
            for (String item:currentCIDS) {
                if (item.equalsIgnoreCase(article.getCid())) {
                    continue;
                }

                cleanedCIDS.add(item);
            }

            //Add CID in index
            boolean flagItemAdded = false;
            int index=0;
            for (String item:cleanedCIDS) {
                if (index == (article.getHomeIndex()-1)) {
                    newCIDS.add(article.getCid());
                    flagItemAdded = true;
                }
                newCIDS.add(item);
                index++;
            }
            if (!flagAdded) {
                newCIDS.add(article.getCid());
            }

            //Update db
            articlesDao.removeHomeIndexForISO(article.getIso());
            for (index=1;index<=newCIDS.size();index++) {
                articlesDao.addHomeIndex(article.getIso(), newCIDS.get(index-1), index);
            }
        }

        return (added > 0);
    }

    public boolean removeArticle(Article item) {
        int removed = articlesDao.removeArticleByCID(item.getCid());
        return (removed>0);
    }

    public boolean validateTitle(String title, String cid) {
        int titleCount = articlesDao.validateExistsByTitleAndNotCID(title, cid);
        return (titleCount==0);
    }

    public String getNewTitle(String oldTitle) {
        log.debug("Get Article new title");
        int index = 1;
        String title = oldTitle + " (" + index + ")";
        String url = URLEncoder.encode(title, StandardCharsets.US_ASCII);
        while (articlesDao.validateExistsByURL(url)!= 0) {
            title = oldTitle + " (" + index++ + ")";
            url = URLEncoder.encode(title, StandardCharsets.US_ASCII);
        }

        return title;
    }

    public Storage getStorageImage(String fid) {
        return articlesDao.getStorageByFID(fid);
    }

    public List<LocalCountry> getCountries() {
        List<LocalCountry> countries = langDao.getCountries();
        if (countries == null) {
            return Collections.emptyList();
        }

        return countries;
    }

    public int getHomeIndex(String cid) {
        Integer index = articlesDao.getHomeIndex(cid);
        if (index == null) {
            return 0;
        }

        return index;
    }

    public List<String> getHomeCIDS(String iso){
        List<String> list = articlesDao.getHomeCIDSSByISO(iso);
        if (list == null) {
            return Collections.emptyList();
        }
        return list;
    }
}
