package com.wippit.cms.backend.data.bean;

import lombok.Data;

@Data
public class UserProfile {
    private String alias;
    private String fullName;
    private String email;
    private String about;
    private String color;
    private String avatarUrl;
    private String bannerUrl;
}
