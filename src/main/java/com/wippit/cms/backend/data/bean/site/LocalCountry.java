package com.wippit.cms.backend.data.bean.site;

import com.wippit.cms.AppGlobal;
import com.wippit.cms.backend.types.StatusTypes;
import lombok.Data;
import org.apache.commons.text.StringEscapeUtils;

@Data
public class LocalCountry {
    private String iso = "";
    private String iso2 = "";
    private String iso3 = "";
    private String name = "";
    private String flag = "";
    private String locale = "";
    private String currency = "";
    private String currency_prefix = "";
    private String date_format = "";
    private String phone_code = "";
    private String display = "";
    private int status = StatusTypes.INACTIVE.value();

    public String toString() {
        return name + " (" + iso + ")";
    }

    public boolean getSelected() {
        return AppGlobal.getInstance().getSiteInfo().getCountry_iso().equalsIgnoreCase(iso);
    }

    public void setDisplay(String value) {
        display = StringEscapeUtils.escapeHtml4(value);
    }

}
