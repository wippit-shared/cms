package com.wippit.cms.backend.data.bean.model;

import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.content.IntroPost;
import com.wippit.cms.backend.data.bean.user.Account;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.Locale;

@Data
public class ArticleIntroModel {
    private String cid = "";
    private String title = "";
    private String tags = "";
    private String image_url = "";
    private String content = "";
    private AuthorModel author;
    private String date;
    private String url = "";
    private int row = 0;
    private int col = 0;
    private int index = 0;
    private String year = "";
    private String month = "";
    private String day = "";
    private String hour = "";
    private String minute = "";
    private String iso = AppGlobal.getInstance().getSiteInfo().getCountry_iso();

    private String secID = "";
    private String section = "";
    private String sectionUrl = "";

    public ArticleIntroModel(){
    }

    public ArticleIntroModel(IntroPost article, Account account, String sectionName, String sectionFullURL, String secID) {
        cid = article.getCid();
        this.secID = secID;
        this.section = sectionName;
        this.sectionUrl = sectionFullURL;

        url = sectionFullURL+ "/" + article.getUrl();

        title = article.getTitle();
        tags = article.getTags();
        image_url = article.getImageURL();
        content = article.getContent_intro();
        date = article.getDate();
        author = new AuthorModel(account);
        iso = article.getIso();

        LocalDateTime ldt = AppUtils.timestampToLocalDateTime(article.getEvent_date(), AppGlobal.getInstance().getSiteInfo().getTime_offset());
        Locale locale = Locale.forLanguageTag(iso);

        year = String.valueOf(ldt.getYear());
        month = ldt.getMonth().getDisplayName(TextStyle.FULL, locale);
        day = String.valueOf(ldt.getDayOfMonth());
        hour = String.valueOf(ldt.getHour());
        minute = String.valueOf(ldt.getMinute());
    }

    public boolean isValid() {
        return !StringUtils.isBlank(cid);
    }

    public String createLabel(String prefix) {
        if (!StringUtils.isBlank(prefix)) {
            return prefix + secID + index;
        }

        return createLabel();
    }

    public String createLabel() {
        return secID + index;
    }

}
