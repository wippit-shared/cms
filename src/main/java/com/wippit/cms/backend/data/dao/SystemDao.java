/*
 * Copyright (c) 2019-2020. Onewip Corp. All rights reserved.
 * onewip.com/terms
 * contact@onewip.com
 */

package com.wippit.cms.backend.data.dao;

import com.wippit.cms.backend.data.bean.content.*;
import com.wippit.cms.backend.data.bean.log.ActivityItem;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.bean.site.SiteTemplate;
import com.wippit.cms.backend.data.bean.stats.HitItem;
import com.wippit.cms.backend.data.bean.stats.HitStatItem;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.bean.subscriber.MailSubscriber;
import com.wippit.cms.backend.data.bean.subscriber.MailTopic;
import com.wippit.cms.backend.data.bean.template.TemplateSection;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.bean.user.AccountExtended;
import com.wippit.cms.backend.data.bean.user.Team;
import org.apache.ibatis.annotations.*;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface SystemDao {

    //---
    //Languages
    //---

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_languages where iso = #{iso}")
    int removeLanguageByISO(
            @Param("iso") String iso
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_content where iso = #{iso}")
    int removeArticlesByISO(
            @Param("iso") String iso
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_template_section where iso = #{iso}")
    int removeTemplateSectionByISO(
            @Param("iso") String iso
    );

    //---
    //Settings
    //---

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_site_setting (key, value) " +
            "values (#{key}, #{value}) " +
            "on conflict (key) do update set " +
            "value = #{value}, " +
            "created_date = now() ")
    int saveSiteSetting(
            @Param("key") String key,
            @Param("value") String value
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(value, '') from cms.ow_site_setting where key = #{key}")
    String getSiteSetting(
        @Param("key") String key
    );


    //---
    //Teacher
    //---

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into edu.ow_teacher (inid, teid, alias, title) " +
            "values (#{inid}, #{teid}, #{alias}, #{title}) " +
            "on conflict (teid) do update " +
            "set title = #{title} ")
    int addTeacher(
        @Param("inid") String inid,
        @Param("teid") String teid,
        @Param("alias") String alias,
        @Param("title") String title
    );


    //****************************************************************************************************************
    //Team & accounts
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_team limit 1")
    Team getTeam();

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_team (oid, owner, name, commercial_name, type, about, color, site_url, avatar_url, banner_url, email, phone, contact, address, work_hours) " +
            "values (#{oid}, #{owner}, #{name}, #{commercial_name}, #{type}, #{about}, #{color}, #{site_url}, #{avatar_url}, #{banner_url}, #{email}, #{phone}, #{contact}, #{address}, #{work_hours}) " +
            "on conflict (oid) do update " +
            "set name = #{name}," +
            "commercial_name = #{commercial_name}, " +
            "type = #{type}, " +
            "about = #{about}, " +
            "color = #{color}, " +
            "site_url = #{site_url}, " +
            "avatar_url = #{avatar_url}, " +
            "banner_url = #{banner_url}, " +
            "email = #{email}, " +
            "phone = #{phone}, " +
            "contact = #{contact}, " +
            "address = #{address}, " +
            "work_hours = #{work_hours} ")
    int addTeam(
            @Param("oid") String oid,
            @Param("owner") String owner,
            @Param("name") String name,
            @Param("commercial_name") String commercial_name,
            @Param("type") int type,
            @Param("about") String about,
            @Param("color") String color,
            @Param("site_url") String site_url,
            @Param("avatar_url") String avatar_url,
            @Param("banner_url") String banner_url,
            @Param("email") String email,
            @Param("phone") String phone,
            @Param("contact") String contact,
            @Param("address") String address,
            @Param("work_hours") String work_hours
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_account where alias = #{alias}")
    Account selectAccountByName(
            @Param("alias") String alias
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select A.*, E.* " +
            "from cms.ow_account A, cms.ow_account_extended E " +
            "where A.alias = #{alias} " +
            "and E.alias = A.alias ")
    AccountExtended selectAccountExtendedByName(
            @Param("alias") String alias
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_account order by alias asc offset #{offset} limit #{limit} ")
    List<Account> getAccounts(
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select A.* " +
            "from cms.ow_account A, cms.ow_account_role R " +
            "where R.role = #{role} " +
            "and A.alias = R.alias " +
            "order by alias asc " +
            "offset #{offset} limit #{limit} ")
    List<Account> getAccountsByRole(
            @Param("role") String role,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_account")
    int getAccountsCount();

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(A.*),0) " +
            "from cms.ow_account A, cms.ow_account_role R " +
            "where R.role = #{role} " +
            "and A.alias = R.alias ")
    int getAccountsByRoleCount(
            @Param("role") String role
    );

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_account (alias, email, status, full_name, avatar_url, banner_url, about, color, type) " +
            "values (#{alias}, #{email}, #{status}, #{full_name}, #{avatar_url}, #{banner_url}, #{about}, #{color}, #{type}) " +
            "on conflict (alias) do update set " +
            "status = #{status}, " +
            "email = #{email}, " +
            "full_name = #{full_name}, " +
            "avatar_url = #{avatar_url}, " +
            "banner_url = #{banner_url}, " +
            "about = #{about}, " +
            "color = #{color}, " +
            "type = #{type} "
    )
    int saveAccount(
            @Param("alias") String name,
            @Param("email") String email,
            @Param("status") int status,
            @Param("full_name") String full_name,
            @Param("avatar_url") String avatar_url,
            @Param("banner_url") String banner_url,
            @Param("about") String about,
            @Param("color") String color,
            @Param("type") int type
    );

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_account_extended (alias, setup, inid, teid) " +
            "values (#{alias}, #{setup}, #{inid}, #{teid}) " +
            "on conflict (alias) do nothing ")
    int addAccountExtended(
            @Param("alias") String name,
            @Param("setup") int setup,
            @Param("inid") String inid,
            @Param("teid") String teid
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_account set secret = #{secret} where alias = #{alias}")
    int updateAccountSecret(
            @Param("alias") String alias,
            @Param("secret") String secret
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_account_extended set setup = #{setup} where alias = #{alias}")
    int updateAccountExtendedSetup(
            @Param("alias") String alias,
            @Param("setup") int setup
    );

    //--------------------------------------------------------------------------------------------------
    @Update("insert into cms.ow_account_role (alias, role) values (#{alias}, #{role}) " +
            "on conflict (alias, role) do nothing ")
    int updateAccountRole(
            @Param("alias") String alias,
            @Param("role") String role
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_account_role " +
            "where alias = #{alias} ")
    int removeAccountAllRoles(
            @Param("alias") String alias
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_account_role " +
            "where alias = #{alias} " +
            "and role = #{role}" )
    int removeAccountRole(
            @Param("alias") String alias,
            @Param("role") String role
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select role from cms.ow_account_role where alias = #{alias}")
    List<String> getAccountRoles(
            @Param("alias") String alias
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_account set status = #{status} where alias = #{alias}")
    int updateAccountStatus(
            @Param("alias") String alias,
            @Param("status") int status
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_account where alias = #{alias}")
    int removeAccount(
            @Param("alias") String alias
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_account_extended where alias = #{alias}")
    int removeAccountExtended(
            @Param("alias") String alias
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from edu.ow_teacher where teid = #{id}")
    int removeTeacher(
            @Param("id") String teid
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_account where alias = #{alias}")
    int validateAccountExists(
            @Param("alias") String alias
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select alias from cms.ow_account order by alias asc")
    List<String> getAllAccountsAlias();


    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_account set " +
            "full_name = #{full_name}, " +
            "about = #{about}, " +
            "email = #{email}, " +
            "color = #{color}, " +
            "avatar_url = #{avatar}, " +
            "banner_url = #{banner} " +
            "where alias = #{alias}")
    int updateAccount(
            @Param("alias") String alias,
            @Param("full_name") String full_name,
            @Param("about") String about,
            @Param("email") String email,
            @Param("color") String color,
            @Param("avatar") String avatar,
            @Param("banner") String banner
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_account set " +
            "avatar_url = #{avatar} " +
            "where alias = #{alias}")
    int updateAccountAvatar(
            @Param("alias") String alias,
            @Param("avatar") String avatar
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_account set " +
            "banner_url = #{banner} " +
            "where alias = #{alias}")
    int updateAccountBanner(
            @Param("alias") String alias,
            @Param("banner") String banner
    );

    //****************************************************************************************************************
    //Subscriber
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_subscriber (suid, email, name, status) " +
            "values (#{suid}, #{email}, #{name}, #{status}) " +
            "on conflict (suid) do update set " +
            "email = #{email}, " +
            "name = #{name} ")
    int addSubscriber(
            @Param("suid") String mlid,
            @Param("email") String email,
            @Param("name") String name,
            @Param("status") int status
    );

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_subscriber_list (stid, suid) values (#{stid}, #{suid}) " +
            "on conflict (stid, suid) do update set " +
            "status = 15001")
    int addSubscriberToList(
            @Param("stid") String mtid,
            @Param("suid") String suid
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_subscriber where suid = #{suid}")
    int removeSubscriber(
            @Param("suid") String suid
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_subscriber_list where suid = #{suid}")
    int removeSubscriberFromTopics(
            @Param("suid") String suid
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_subscriber where email = #{email}")
    MailSubscriber getSubscriberByEmail(
            @Param("email") String email
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_subscriber order by name asc")
    List<MailSubscriber> getAllSubscribers();

    @Select("select coalesce(count(*),0) from cms.ow_subscriber")
    long getAllSubscribersCount();

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_subscriber order by name asc " +
            "offset #{offset} limit #{limit}")
    List<MailSubscriber> getSubscribersAll(
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select SU.* from " +
            "cms.ow_subscriber_list SU, cms.ow_subscriber_list SL where " +
            "SL.stid = #{stid} " +
            "and SU.suid = SL.suid " +
            "order by email asc")
    List<MailSubscriber> getSubscribersForTopic(
            @Param("stid") String topic
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_subscriber_topics " +
            "order by name asc")
    List<MailTopic> getSubscriberTopicAll();

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_subscriber_topics where " +
            "status = #{status} " +
            "order by name asc")
    List<MailTopic> getSubscriberTopicByStatus(int status);

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_subscriber_topics where name = #{name}")
    MailTopic getSubscriberTopicByName(
            @Param("name") String name
    );

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_subscriber_topics (name, description, status) " +
            "values(#{name}, #{description}, #{status}) ")
    int addSubscriberTopic(
            @Param("name") String name,
            @Param("description") String description,
            @Param("status") int status
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_subscriber_topics where stid = #{stid}")
    int removeSubscriberTopic(
            @Param("stid") String mtid
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_subscriber_list where stid = #{stid}")
    int removeSubscriberTopicFromList(
            @Param("stid") String stid
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update from cms.ow_subscriber_topic set " +
            "status = #{status} " +
            "where stid = #{stid}")
    int updateSubscriberTopicStatus(
            @Param("stid") String stid,
            @Param("status") int status
    );

    //****************************************************************************************************************
    //Hits & Stats
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_hit " +
            "order by id desc " +
            "offset #{offset} limit #{limit}")
    List<HitItem> getHitLogs(
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_hit " +
            "where type = #{type} " +
            "order by id desc " +
            "offset #{offset} limit #{limit}")
    List<HitItem> getHitLogsByType(
            @Param("type") int type,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_hit " +
            "where hit <> 200 " +
            "offset order by id desc #{offset} limit #{limit}")
    List<HitItem> getHitLogsErrors(
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_hit")
    long getHitLogsCount();

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_hit " +
            "where type = #{type}")
    long getHitLogsByTypeCount(
            @Param("type") int type
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_hit " +
            "where hit <> 200 ")
    long getHitLogsErrorsCount();

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_hit")
    int deleteLog();

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_hit_log")
    int deleteHitLog();

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_hit_log (url, type, hit, agent, address, country, city) " +
            "values (#{url}, #{type}, #{hit}, #{agent}, #{address}, #{country}, #{city})")
    int addHitLog(
            @Param("url") String url,
            @Param("type") int type,
            @Param("hit") int hit,
            @Param("agent") String agent,
            @Param("address") String address,
            @Param("country") String country,
            @Param("city") String city
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_hit set " +
            "count = count + 1 " +
            "where url = #{url}")
    int updateURLHitCount(
            @Param("url") String url
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_content set " +
            "hit_count = hit_count + 1 " +
            "where cid = #{cid}")
    int updateCIDHitCount(
            @Param("cid") String cid
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_section set " +
            "hit_count = hit_count + 1 " +
            "where name = #{secID} " +
            "and iso = #{country}")
    int updateSectionHitCount(
            @Param("country") String country,
            @Param("secID") String section
    );

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_hit (url, type, count) " +
            "values (#{url}, #{type}, 1) " +
            "on conflict (url) do update set count = cms.ow_hit.count + 1")
    int addUrlToHitCount(
            @Param("url") String url,
            @Param("type") int type
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_hit " +
            "order by count desc " +
            "offset #{offset} " +
            "limit #{limit} ")
    List<HitStatItem> getAllHitStats(
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_hit ")
    long getAllHitsStatsCount();

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_hit " +
            "where type = #{type} " +
            "order by id desc " +
            "offset #{offset} " +
            "limit #{limit} ")
    List<HitStatItem> getHitStatsByType(
            @Param("type") int type,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_hit " +
            "where type = #{type} ")
    long getHitsStatsByTypeCount(
            @Param("type") int type
    );

    //****************************************************************************************************************
    //Server
    //****************************************************************************************************************

//    //--------------------------------------------------------------------------------------------------
//    @Select("select * from cms.ow_server_keys limit 1")
//    ServerKeys getServerKeys();
//
//    //--------------------------------------------------------------------------------------------------
//    @Insert("insert into cms.ow_server_keys (publicKey, privateKey) " +
//            "values (#{publicKey}, #{privateKey})")
//    int addServerKeys(
//            @Param("privateKey") String privateKey,
//            @Param("publicKey") String publicKey
//    );

//    //--------------------------------------------------------------------------------------------------
//    @Update("update cms.ow_server_keys set clientPublicKey = #{key}")
//    int updateClientPublicKey(
//            @Param("key") String key
//    );


    //****************************************************************************************************************
    // Country
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_country (iso, iso2, iso3, name, flag, locale, currency, currency_prefix, date_format, phone_code, display) " +
            "values (#{iso}, #{iso2}, #{iso3}, #{name}, #{flag}, #{locale}, #{currency}, #{currency_prefix}, #{date_format}, #{phone_code}, #{display}) " +
            "on conflict (iso) do update set " +
            "iso2 = #{iso2}, " +
            "iso3 = #{iso3}, " +
            "name = #{name}, " +
            "flag = #{flag}, " +
            "locale = #{locale}, " +
            "currency = #{currency}, " +
            "currency_prefix = #{currency_prefix}, " +
            "date_format = #{date_format}, " +
            "phone_code = #{phone_code}, " +
            "display = #{display}")
    int addCountry(
            @Param("iso") String iso,
            @Param("iso2") String iso2,
            @Param("iso3") String iso3,
            @Param("name") String name,
            @Param("flag") String flag,
            @Param("locale") String locale,
            @Param("currency") String currency,
            @Param("currency_prefix") String currency_prefix,
            @Param("date_format") String date_format,
            @Param("phone_code") String phone_code,
            @Param("display") String display
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_country where iso = #{iso}")
    LocalCountry getLocalCountry(
            @Param("iso") String iso
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_country")
    List<LocalCountry> getLocalCountries();

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_country where iso = #{iso}")
    int removeCountry(
            @Param("iso") String iso
    );

    //****************************************************************************************************************
    // Sections
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_section (iso, secID, name, type) values (#{iso}, #{secID}, #{name}, #{type}) " +
            "on conflict (iso, secID) do update set " +
            "name = #{name}, " +
            "type = #{type}")
    int addSection(
            @Param("iso") String iso,
            @Param("secID") String secID,
            @Param("name") String name,
            @Param("type") int type
    );

//    //--------------------------------------------------------------------------------------------------
//    @Update("Update cms.ow_section set name = #{name}, type = #{type} " +
//            "where id = #{id}")
//    int updateSection(
//            @Param("id") long id,
//            @Param("name") String name,
//            @Param("type") int type
//    );
//
//    //--------------------------------------------------------------------------------------------------
//    @Update("Update cms.ow_content set secID = #{name} " +
//            "where section_url = #{url} " +
//            "and iso = #{iso}")
//    int updateSectionInContent(
//            @Param("name") String name,
//            @Param("url") String url,
//            @Param("iso") String iso
//    );

//    //--------------------------------------------------------------------------------------------------
//    @Update("Update cms.ow_content set secID = #{name}, section_url = #{url} " +
//            "where section_url = #{old_url} " +
//            "and iso = #{iso}")
//    int relocateSectionInContent(
//            @Param("name") String name,
//            @Param("url") String url,
//            @Param("old_url") String old_url,
//            @Param("iso") String iso
//    );

//    //--------------------------------------------------------------------------------------------------
//    @Select("select coalesce(count(*),0) from cms.ow_content " +
//            "where section_url = #{url} " +
//            "and iso = #{iso}")
//    int getSectionContentCount(
//            @Param("url") String url,
//            @Param("iso") String iso
//    );

//    //--------------------------------------------------------------------------------------------------
//    @Delete("delete from cms.ow_section where iso = #{iso} and url = #{url}")
//    int removeSection(
//            @Param("iso") String iso,
//            @Param("url") String url
//    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_section where secID = #{secID}")
    int removeSectionByURL(
            @Param("secID") String url
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_template_section where secID = #{secID}")
    int removeTemplateSectionByURL(
            @Param("secID") String url
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_section where iso = #{iso}")
    int removeSectionByISO(
            @Param("iso") String iso
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_section offset #{offset} limit #{limit}")
    List<SectionExtended> getAllSections(
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select max(secID) as secID, max(name) as name, max(type) as type " +
            "from cms.ow_section " +
            "group by name")
    List<Section> getAllDistinctSections();

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_section where iso = #{iso} offset #{offset} limit #{limit}")
    List<Section> getSectionsByISO(
            @Param("iso") String iso,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

//    //--------------------------------------------------------------------------------------------------
//    @Select("select * from cms.ow_section where iso = #{iso} and secID = #{secID}")
//    Section getSectionsByISOAndURL(
//            @Param("iso") String iso,
//            @Param("secID") String secID
//    );


    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) " +
            "from cms.ow_section " +
            "where iso = #{iso} ")
    int getSectionCount(
            @Param("iso") String iso
    );

//    //--------------------------------------------------------------------------------------------------
//    @Select("select coalesce(count(*),0) from cms.ow_section where iso = #{iso} and url = #{url}")
//    int validateSectionURL(
//            @Param("iso") String iso,
//            @Param("url") String url
//    );

    //****************************************************************************************************************
    //Content
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
//    @Insert("insert into cms.ow_content (cid, iso, url, secID, section_url, title, tags, image_fid, image_url, content_intro, content_intro_html, content_complete, content_complete_html, author, status, home, start_date, end_date) " +
//            "values (#{cid}, #{iso}, #{url}, #{secID}, #{section_url}, #{title}, #{tags}, #{image_fid}, #{image_url}, #{content_intro}, #{content_intro_html}, #{content_complete}, #{content_complete_html}, #{author}, #{status}, #{home}, #{start_date}, #{end_date}) " +
//            "on conflict (cid) do update set " +
//            "url = #{url}, " +
//            "secID = #{secID}, " +
//            "section_url = #{section_url}, " +
//            "title = #{title}, " +
//            "tags = #{tags}, " +
//            "image_fid = #{image_fid}, " +
//            "image_url = #{image_url}, " +
//            "content_intro = #{content_intro}, " +
//            "content_intro_html = #{content_intro_html}, " +
//            "content_complete = #{content_complete}, " +
//            "content_complete_html = #{content_complete_html}, " +
//            "author = #{author}, " +
//            "status = #{status}, " +
//            "home = #{home}, " +
//            "start_date = #{start_date}, " +
//            "end_date = #{end_date} " )
//    int addContent(
//            @Param("cid") String cid,
//            @Param("iso") String iso,
//            @Param("url") String url,
//            @Param("secID") String secID,
//            @Param("section_url") String section_url,
//            @Param("title") String title,
//            @Param("tags") String tags,
//            @Param("image_fid") String fid,
//            @Param("image_url") String image_url,
//            @Param("content_intro") String content_intro,
//            @Param("content_complete") String content_complete,
//            @Param("content_intro_html") String content_intro_html,
//            @Param("content_complete_html") String content_complete_html,
//            @Param("author") String author,
//            @Param("status") int status,
//            @Param("home") int home,
//            @Param("start_date") Timestamp start_date,
//            @Param("end_date") Timestamp end_date
//    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_content where cid = #{cid}")
    Article getContentByCID(
        @Param("cid") String cid
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_content where iso = #{iso} and url = #{url}")
    Article getContentByISOAndURL(
            @Param("iso") String iso,
            @Param("url") String url
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_content")
    List<Article> getAllContent();

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_content " +
            "where iso = #{iso} " +
            "and section_url = #{secID} " +
            "and status = 15001 " +
            "and home in (16000, 16001) " +
            "and start_date < now() " +
            "and end_date >= now() " +
            "order by start_date desc " +
            "offset #{offset} limit #{limit}")
    List<IntroPost> getSectionPosts(
            @Param("iso") String iso,
            @Param("secID") String section,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_content " +
            "where iso = #{iso} " +
            "and section_url = #{secID} " +
            "and status = 15001 " +
            "and home in (16000, 16001) " +
            "and start_date < now() " +
            "and end_date >= now() ")
    int getSectionPostsCount(
            @Param("iso") String iso,
            @Param("secID") String section
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_content " +
            "where iso = #{iso} " +
            "and status = 15001 " +
            "and home in (16000, 16002) " +
            "and start_date < now() " +
            "and end_date >= now() " +
            "order by start_date desc " +
            "offset #{offset} limit #{limit} ")
    List<IntroPost> getHomePosts(
            @Param("iso") String iso,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_content " +
            "where iso = #{iso} " +
            "and status = 15001 " +
            "and home in (16000, 16002) " +
            "and start_date < now() " +
            "and end_date >= now()")
    int getHomePostsCount(
            @Param("iso") String iso
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_content " +
            "where iso = #{iso} " +
            "and section_url = #{section_url} " +
            "and home in (16000, 16001) " +
            "order by id desc " +
            "offset #{offset} limit #{limit} ")
    List<ArticleBrief> getArticleInSectionBrief(
            @Param("iso") String iso,
            @Param("section_url") String section_url,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*)) from cms.ow_content " +
            "where iso = #{iso} " +
            "and home in (16000, 16001) " +
            "and section_url = #{section_url} ")
    int getArticleInSectionCount(
            @Param("iso") String iso,
            @Param("section_url") String section_url
    );


    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_content " +
            "where iso = #{iso} " +
            "and home in (16000, 16002) " +
            "order by id desc " +
            "offset #{offset} limit #{limit} ")
    List<ArticleBrief> getArticleInHomeBrief(
            @Param("iso") String iso,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*)) from cms.ow_content " +
            "where iso = #{iso} " +
            "and home in (16000, 16002) ")
    int getArticleInHomeCount(
            @Param("iso") String iso
    );



    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_content " +
            "where iso = #{iso} " +
            "order by id desc " +
            "offset #{offset} limit #{limit} ")
    List<ArticleBrief> getAllArticleBrief(
            @Param("iso") String iso,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*)) from cms.ow_content " +
            "where iso = #{iso} ")
    int getAllArticleCount(
            @Param("iso") String iso
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_content " +
            "where iso = #{iso} " +
            "and status = 15000 " +
            "and section_url = #{section_url} " +
            "and start_date < now() " +
            "and end_date >= now() " +
            "order by start_date desc " +
            "offset #{offset} limit #{limit} ")
    List<IntroPost> getSectionContentIntroByISO(
            @Param("iso") String iso,
            @Param("section_url") String section_url,
            @Param("offset") long offset,
            @Param("limit") int limit
    );


    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_content " +
            "where iso = #{iso} and secID = #{secID} ")
    int getContentByISOAndSectionCount(
            @Param("iso") String iso,
            @Param("secID") String section
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select cid from cms.ow_content where iso = #{iso} and url = #{url} limit 1")
    String validateContentURL(
            @Param("iso") String iso,
            @Param("url") String url
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_content set " +
            "status = #{status}, " +
            "home = #{home}, " +
            "start_date = #{start_date}, " +
            "end_date = #{start_date} " +
            "where cid = #{cid}")
    int updateContentSettings(
            @Param("cid") String cid,
            @Param("status") int status,
            @Param("home") int home,
            @Param("start_date") Timestamp start_date,
            @Param("end_date") Timestamp end_date
    );

    //****************************************************************************************************************
    //Templates
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_template (name, page_root) values (#{template}, #{root}) " +
            "on conflict (name) do update set " +
            "page_root = #{root}, " +
            "created_date = now() ")
    int updateTemplateRoot(
            @Param("template") String template,
            @Param("root") String rootPage
    );

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_template (name, page_root, page_404, page_500, page_author, page_search, page_user, page_blog, page_item) " +
            "values (#{template}, #{root}, #{p404}, #{p500}, #{pAuthor}, #{pSearch}, #{pUser}, #{pBlog}, #{pItem}) " +
            "on conflict (name) do update set " +
            "page_root = #{root}, " +
            "page_404 = #{p404}, " +
            "page_500 = #{p500}, " +
            "page_author = #{pAuthor}, " +
            "page_search = #{pSearch}, " +
            "page_user = #{pUser}, " +
            "page_blog = #{pBlog}, " +
            "page_item = #{pItem}, " +
            "created_date = now() ")
    int addTemplate(
            @Param("template") String template,
            @Param("root") String rootPage,
            @Param("p404") String p404,
            @Param("p500") String p500,
            @Param("pAuthor") String pAuthor,
            @Param("pSearch") String pSearch,
            @Param("pUser") String pUser,
            @Param("pBlog") String pBlog,
            @Param("pItem") String pItem
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_template where name = #{template}")
    SiteTemplate getTemplate(
            @Param("template") String template
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_template order by name")
    List<SiteTemplate> getAllTemplates();

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_template")
    int getAllTemplatesCount();

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_template_section (name, iso, secID, section_page, item_page) " +
            "values (#{template}, #{iso}, #{secID}, #{section_page}, #{item_page}) " +
            "on conflict (name, iso, secID) do update set " +
            "section_page = #{section_page}, " +
            "item_page = #{item_page}, " +
            "created_date = now() ")
    int addTemplateSection(
            @Param("template") String template,
            @Param("iso") String iso,
            @Param("secID") String section,
            @Param("section_page") String section_page,
            @Param("item_page") String item_page
    );

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_template_section (name, iso, secID, section_page, item_page) " +
            "values (#{template}, #{iso}, #{secID}, #{section_page}, #{item_page}) " +
            "on conflict (name, iso, secID) do update set " +
            "section_page = #{section_page}, " +
            "item_page = #{item_page}, " +
            "created_date = now() ")
    int addTemplateSectionWithSection(
            @Param("template") String template,
            @Param("iso") String iso,
            @Param("secID") String section,
            @Param("section_page") String section_page,
            @Param("item_page") String item_page
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_template_section where name = #{template}")
    List<TemplateSection> getTemplateSections(
            @Param("template") String template
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_template_section where name = #{template} and secID = #{secID}")
    List<TemplateSection> getTemplateSectionsBySectionID(
            @Param("template") String template,
            @Param("secID") String section
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_template_section order by name, secID, iso")
    List<TemplateSection> getAllTemplateSections();

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_template_section " +
            "where iso = #{iso} " +
            "order by name, secID")
    List<TemplateSection> getTemplateSectionsByCountry(
            @Param("iso") String iso
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_template where name = #{template}")
    int deleteTemplate(
            @Param("template") String template
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_template_section where name = #{template}")
    int deleteTemplateSections(
            @Param("template") String template
    );

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_template_index (template, iso, page) values (#{template}, #{iso}, #{page})")
    int addTemplateHome(
            @Param("template") String template,
            @Param("iso") String iso,
            @Param("page") String page
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_template_index where iso = #{iso}")
    int removeTemplateHomeByISO(
            @Param("iso") String iso
    );

    //****************************************************************************************************************
    //Storage
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Select("select nextval('cms.ow_storage_id_seq')")
    long getStorageNextID();

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_storage (id, url, fid, alias, type, path, is_private, original_name, byte_size, mime, properties) " +
            "values (#{id}, #{url}, #{fid}, #{alias}, #{type}, #{path}, #{is_private}, #{original_name}, #{byte_size}, #{mime}, #{properties})")
    int addStorage(
            @Param("id") long id,
            @Param("url") String url,
            @Param("fid") String fid,
            @Param("alias") String alias,
            @Param("type") int type,
            @Param("path") String path,
            @Param("is_private") boolean isPrivate,
            @Param("original_name") String original_filename,
            @Param("byte_size") long byte_size,
            @Param("mime") String mime,
            @Param("properties") String properties
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_storage where fid = #{fid}")
    Storage getStorageByFID(
            @Param("fid") String fid
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_storage where url = #{url}")
    Storage getStorageByURL(
            @Param("url") String url
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_storage where fid = #{fid}")
    int removeStorage(
            @Param("fid") String fid
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_storage where url = #{url} or fid = #{fid}")
    int validateStorageByFIDandURL(
            @Param("fid") String fid,
            @Param("url") String url
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_storage " +
            "where alias = #{alias} " +
            "order by type asc, original_name asc " +
            "offset #{offset} limit #{limit}")
    List<Storage> getStorageByAlias(
            @Param("alias") String alias,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_storage " +
            "where alias = #{alias} " )
    long getStorageByAliasCount(
            @Param("alias") String alias
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_storage " +
            "where alias = #{alias} and type = #{type} " +
            "order by type asc, original_name asc " +
            "offset #{offset} limit #{limit}")
    List<Storage> getStorageByAliasAndType(
            @Param("alias") String alias,
            @Param("type") int type,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_storage " +
            "where alias = #{alias} and type = #{type}" )
    long getStorageByAliasAndTypeCount(
            @Param("alias") String alias,
            @Param("type") int type
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_storage " +
            "where type = #{type} " +
            "order by original_name asc " +
            "offset #{offset} limit #{limit} ")
    List<Storage> getStorageByType(
            @Param("type") int type,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_storage " +
            "where type = #{type} ")
    long getStorageByTypeCount(
            @Param("type") int type
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_storage")
    List<Storage> getAllStoragesRecords();

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_storage " +
            "order by original_name asc " +
            "offset #{offset} limit #{limit} ")
    List<Storage> getAllStorage(
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_storage ")
    long getAllStorageCount();

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_storage where " +
            "alias = #{alias} " +
            "and type = #{type} " +
            "and original_name like #{filter} " +
            "order by original_name asc")
    List<Storage> searchStorageByAliasAndType(
            @Param("alias") String alias,
            @Param("type") int type,
            @Param("filter") String filter
    );

    //****************************************************************************************************************
    //Activity log
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_log (alias, type, detail) " +
            "values ( #{alias}, #{type}, #{detail} )")
    int addLog(
            @Param("alias") String alias,
            @Param("type") int type,
            @Param("detail") String detail
    );


    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_log " +
            "order by id desc " +
            "offset #{offset} limit #{limit} ")
    List<ActivityItem> getAllActivity(
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*)) from cms.ow_log")
    long getAllActivityCount();

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_log " +
            "where alias = #{alias} " +
            "order by id desc " +
            "offset #{offset} limit #{limit} ")
    List<ActivityItem> getAllActivityByAlias(
            @Param("alias") String alias,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*)) from cms.ow_log " +
            "where alias = #{alias}")
    long getAllActivityByAliasCount(
            @Param("alias") String alias
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_log " +
            "where type = #{type} " +
            "order by id desc "+
            "offset #{offset} limit #{limit} ")
    List<ActivityItem> getAllActivityByType(
            @Param("type") int type,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*)) from cms.ow_log" +
            "where type = #{type} ")
    long getAllActivityByTypeCount(
            @Param("type") int type
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_log " +
            "where type = #{type} " +
            "and alias = #{alias} " +
            "order by id desc "+
            "offset #{offset} limit #{limit} ")
    List<ActivityItem> getAllActivityByTypeAndAlias(
            @Param("type") int type,
            @Param("alias") String alias,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*)) from cms.ow_log" +
            "where type = #{type} " +
            "and alias = #{alias} ")
    long getAllActivityByTypeAndAliasCount(
            @Param("type") int type,
            @Param("alias") String alias
    );

    //****************************************************************************************************************
    //Redirect
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_redirect where iso = #{iso} order by name asc")
    List<ContentRedirect> getRedirects(
            @Param("iso") String iso
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_redirect " +
            "where iso = #{iso} " +
            "and name = #{name} " +
            "and status = 15001 " +
            "and start_date < now() " +
            "and end_date > now() ")
    ContentRedirect getRedirect(
            @Param("iso") String iso,
            @Param("name") String name
    );

    @Update("update cms.ow_redirect set hit_count = hit_count + 1 where iso = #{iso} and name = #{name} ")
    int addRedirectHit(
            @Param("iso") String iso,
            @Param("name") String redirect
    );

    //****************************************************************************************************************
    //Signup
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) " +
            "from cms.ow_account_signup " +
            "where (alias = #{alias} or email = #{email}) " +
            "and status = 15001 ")
    int validateSignupExists(
            @Param("alias") String alias,
            @Param("email") String email
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) " +
            "from cms.ow_account " +
            "where (alias = #{alias} or email = #{email}) ")
    int validateAccountAneEmailExists(
            @Param("alias") String alias,
            @Param("email") String email
    );

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_account_signup (sgid, alias, full_name, email, code, info, status, created_date, expire_date, status_date) " +
            "values (#{sgid}, #{alias}, #{full_name}, #{email}, #{code}, #{info}, #{status}, now(), now()+ (10 * interval '1 minute'), now()) " +
            "on conflict (sgid) do update set " +
            "alias = #{alias}, " +
            "full_name = #{full_name}, " +
            "email = #{email}, " +
            "code = #{code}, " +
            "status = #{status}, " +
            "created_date = now(), " +
            "expire_date = now() + (10 * interval '1 minute')," +
            "status_date = now() ")
    int addSignup(
            @Param("sgid") String sgid,
            @Param("alias") String alias,
            @Param("full_name") String fullName,
            @Param("email") String email,
            @Param("code") String code,
            @Param("info") String info,
            @Param("status") int status
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(code, '') " +
            "from cms.ow_account_signup " +
            "where sgid = #{sgid} " +
            "and status = #{status} " +
            "and expire_date < now() ")
    String getSignupCodeByID(
            @Param("sgid") String sgid,
            @Param("status") int status
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_account_signup set " +
            "status = #{status}, " +
            "status_date = now() " +
            "where sgid = #{sgid} " +
            "and expire_date > now() ")
    int updateSignupStatus(
            @Param("sgid") String sgid,
            @Param("status") int status
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) " +
            "from cms.ow_account_signup " +
            "where sgid = #{sgid} " +
            "and expire_date < now()")
    int validateIfSignupIsExpired(
            @Param("sgid") String sgid
    );

}

