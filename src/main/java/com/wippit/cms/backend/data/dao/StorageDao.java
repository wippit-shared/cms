/*
 * Copyright (c) 2019-2020. Onewip Corp. All rights reserved.
 * onewip.com/terms
 * contact@onewip.com
 */

package com.wippit.cms.backend.data.dao;

import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.types.StorageTypes;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface StorageDao {


    //------------------------------------------------------------------------------------------------------------
    @Select("select nextval('cms.ow_storage_id_seq')")
    long getNextID();

    //------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_storage where id = #{id}")
    Storage getStorageByID(
            @Param("id") long id
    );

    //------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_storage where fid = #{fid}")
    Storage getStorageByFID(
            @Param("fid") String fid
    );

    //------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_storage where url = #{url}")
    Storage getStorageByURL(
            @Param("url") String url
    );

    @Insert("insert into cms.ow_storage (id, url, fid, alias, type, path, is_private, original_name, byte_size, mime, properties) " +
            "values (#{id}, #{url}, #{fid}, #{alias}, #{type}, #{path}, #{private}, #{original}, #{size}, #{mime}, #{properties}) " +
            "on conflict (url) do nothing ")
    int addStorage(
            @Param("id") long id,
            @Param("url") String url,
            @Param("fid") String fid,
            @Param("alias") String alias,
            @Param("type") int type,
            @Param("path") String path,
            @Param("private") boolean isPrivate,
            @Param("original") String originalName,
            @Param("size") int byteSize,
            @Param("mime") String mime,
            @Param("properties") String properties
    );

    //------------------------------------------------------------------------------------------------------------
    @SelectProvider(type=StorageQueryBuilder.class, method = "get")
    List<Storage> getStorages(
            @Param("flagPrivate") boolean flagPrivate,
            @Param("alias") String alias,
            @Param("type") StorageTypes type,
            @Param("offset") int offset,
            @Param("limit") int limit
    );

    //------------------------------------------------------------------------------------------------------------
    @SelectProvider(type=StorageQueryBuilder.class, method = "count")
    int getStoragesCount(
            @Param("flagPrivate") boolean flagPrivate,
            @Param("alias") String alias,
            @Param("type") StorageTypes type
    );

}

