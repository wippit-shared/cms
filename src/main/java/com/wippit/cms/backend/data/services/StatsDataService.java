package com.wippit.cms.backend.data.services;

import com.wippit.cms.backend.data.bean.stats.VisitorsByCity;
import com.wippit.cms.backend.data.bean.stats.VisitorsByCountry;
import com.wippit.cms.backend.data.dao.StatsDao;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class StatsDataService {
    private StatsDao dao;

    public StatsDataService(@Autowired StatsDao statsDao) {
        this.dao = statsDao;
    }

    public List<VisitorsByCity> getVisitorsByCity() {
        List<VisitorsByCity> items = dao.getVisitorsByCity(15);
        if (items == null) {
            return Collections.emptyList();
        }

        return items;
    }

    public List<VisitorsByCity> getVisitorsByCityByDays(int days) {
        List<VisitorsByCity> items = dao.getVisitorsByCityByDays(days, 15);
        if (items == null) {
            return Collections.emptyList();
        }

        return items;
    }

    public List<VisitorsByCity> getVisitorsByCityDemo() {
        List<VisitorsByCity> items = new ArrayList<>();
        items.add(new VisitorsByCity("US","Dallas", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCity("US","Austin", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCity("US","Chicago", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCity("US","San Francisco", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCity("MX","Monterrey", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCity("NL","Amsterdam", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCity("BE","Bruselas", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCity("SG","Singapore", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCity("KR","Seoul", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCity("GB","London", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCity("GB","Oxford", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCity("FR","Paris", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCity("FI","Helsinki", RandomUtils.nextInt(0, 100)));
        return items;
    }

    public List<VisitorsByCountry> getVisitorsByCountry() {
        List<VisitorsByCountry> items = dao.getVisitorsByCountry(15);
        if (items == null) {
            return Collections.emptyList();
        }

        return items;
    }

    public List<VisitorsByCountry> getVisitorsByCountryByDays(int days) {
        List<VisitorsByCountry> items = dao.getVisitorsByCountryByDays(days,15);
        if (items == null) {
            return Collections.emptyList();
        }

        return items;
    }

    public List<VisitorsByCountry> getVisitorsByCountryDemo() {
        List<VisitorsByCountry> items = new ArrayList<>();
        items.add(new VisitorsByCountry("US", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("MX", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("FI", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("DE", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("GB", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("SK", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("JP", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("NA", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("FR", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("CA", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("CH", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("IE", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("BE", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("MD", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("AE", RandomUtils.nextInt(0, 100)));
        items.add(new VisitorsByCountry("GR", RandomUtils.nextInt(0, 100)));

        return items;
    }


    public List<String> getAddressForEmptyHitLog(){
        return dao.getEmptyAddress();
    }

    public void updateHitLogForAddress(String address, String country, String city) {
        dao.updateEmptyHit(address, country, city);
    }

}
