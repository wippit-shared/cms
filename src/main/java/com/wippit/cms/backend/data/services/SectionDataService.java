package com.wippit.cms.backend.data.services;

import com.wippit.cms.backend.data.bean.content.Section;
import com.wippit.cms.backend.data.dao.SystemDao;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.List;

@Slf4j
public class SectionDataService {
    private SystemDao systemDao;
    @Getter
    @Setter
    private String iso = "";

    public SectionDataService(SystemDao systemDao, String iso) {
        this.systemDao = systemDao;
        this.iso = iso;
    }

//    public Page<Section> getPage(Page currentPage) {
//        long offset = currentPage.getNumber()* AppConstants.QUERY_DEFAULT_LIMIT;
//        Page<Section> page = new PageImpl<Section>(getSections(offset, AppConstants.QUERY_DEFAULT_LIMIT));
//        return page;
//    }

    public List<Section> getSections(long offset, int limit) {
        List<Section> items = null;
        items = systemDao.getSectionsByISO(iso, offset, limit);
        if (items == null) {
            return Collections.emptyList();
        }

        return items;
    }

    public long getCount() {
        return systemDao.getSectionCount(iso);
    }
}
