package com.wippit.cms.backend.data.services;

import com.wippit.cms.AppConstants;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.backend.data.bean.QueryFilter;
import com.wippit.cms.backend.data.bean.RedirectQueryResult;
import com.wippit.cms.backend.data.bean.content.ContentRedirect;
import com.wippit.cms.backend.data.dao.ApiRedirectDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@Slf4j
@Service
public class RedirectDataService {
    private final ApiRedirectDao dao;

    public RedirectDataService(@Autowired ApiRedirectDao dao) {
        this.dao = dao;
    }

    public boolean addRedirect(String iso, String name, String url, int status, long startDate, long endDate) {

        Timestamp startTimestamp = Timestamp.from(Instant.ofEpochMilli(startDate));
        Timestamp endTimestamp = Timestamp.from(Instant.ofEpochMilli(endDate));

        int updated = dao.addRedirect(iso, name, url, status, startTimestamp, endTimestamp);
        if (updated < 1) {
            log.warn("Redirect not updated: " + name);
            return false;
        }

        return true;
    }

    public RedirectQueryResult getRedirects(QueryFilter queryFilter) {

        RedirectQueryResult queryResult = new RedirectQueryResult();

        int offset = AppConstants.QUERY_DEFAULT_LIMIT*queryFilter.getOffset();
        int total = queryFilter.getTotal();

        String iso = AppGlobal.getInstance().getSiteInfo().getCountry_iso();
        if(total == 0) {
            total = dao.getRedirectsCount(iso);
        }

        List<ContentRedirect> items = dao.getRedirects(iso, offset, AppConstants.QUERY_DEFAULT_LIMIT);
        if (items != null) {
//            for (ContentRedirect item: items) {
//                item.setStartDate(item.getStart_date().toInstant().toEpochMilli());
//                item.setEndDate(item.getEnd_date().toInstant().toEpochMilli());
//            }
            queryResult.setItems(items);
            queryResult.getResult().setFilter(queryFilter.getFilter());
            queryResult.getResult().setOffset(queryFilter.getOffset());
            queryResult.getResult().setSort(queryFilter.getSort());
            queryResult.getResult().setSize(items.size());
            queryResult.getResult().setTotal(total);
        }

        return queryResult;
    }
}
