package com.wippit.cms.backend.data.bean;

import com.wippit.cms.backend.data.bean.user.Account;
import lombok.Data;

@Data
public class DeviceAccount {
    private String username = "";
    private String full_name = "";
    private String avatar_url = "";
    private String banner_url = "";
    private String about = "";
    private String email = "";
    private String color  = "";

    public DeviceAccount() {
    }

    public DeviceAccount(Account account) {
        this.username = account.getAlias();
        this.full_name = account.getFull_name();
        this.avatar_url = account.getAvatar_url();
        this.banner_url = account.getBanner_url();
        this.about = account.getAbout();
        this.color = account.getColor();
        this.email = account.getEmail();
    }

}
