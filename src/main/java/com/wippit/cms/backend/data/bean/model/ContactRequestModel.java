package com.wippit.cms.backend.data.bean.model;

import com.wippit.cms.backend.data.bean.api.ContactMessageRequest;
import lombok.Data;

@Data
public class ContactRequestModel {
    private String name = "";
    private String email = "";
    private String message = "";
    private String company = "";
    private String phone = "";
    private boolean confirmed = false;

    public ContactRequestModel() {
    }

    public ContactRequestModel(ContactMessageRequest data) {
        name = data.getName();
        email = data.getEmail();
        message = data.getMessage();
        company = data.getCompany();
        phone = data.getPhone();
    }
}
