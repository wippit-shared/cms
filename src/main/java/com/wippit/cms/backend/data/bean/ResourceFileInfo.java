package com.wippit.cms.backend.data.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wippit.cms.backend.types.StorageTypes;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class ResourceFileInfo {
    private int resID = 0;
    private String filename = "";
    private String mime = "";
    private boolean restricted = false;
    private int size = 0;
    private int type = StorageTypes.OTHER.value();

    @JsonIgnore
    public static ResourceFileInfo fromJSON(String json) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(json, ResourceFileInfo.class);
        }
        catch (Exception ex) {
            log.error("Cant get ResourceFileInfo Json: {}", ex.toString());
            return  null;
        }
    }
}
