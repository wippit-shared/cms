/*
 * Copyright (c) 2019-2020. Onewip Corp. All rights reserved.
 * onewip.com/terms
 * contact@onewip.com
 */

package com.wippit.cms.backend.data.dao;

import com.wippit.cms.backend.data.bean.stats.VisitorsByCity;
import com.wippit.cms.backend.data.bean.stats.VisitorsByCountry;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface StatsDao {

    //--------------------------------------------------------------------------------------------------
    @Select("select max(country) as country, city, count(*) " +
            "from cms.ow_hit_log " +
            "where type in (0,200) " +
            "and hit = 200 " +
            "group by city " +
            "order by count desc " +
            "limit #{limit}")
    List<VisitorsByCity> getVisitorsByCity(
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select max(country) as country, city, count(*) " +
            "from cms.ow_hit_log " +
            "where type in (0,200) " +
            "and hit = 200 " +
            "and created_date > (now()  - INTERVAL '${days} DAY') " +
            "group by city " +
            "order by count desc " +
            "limit #{limit}")
    List<VisitorsByCity> getVisitorsByCityByDays(
            @Param("days") int days,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select country, count(*) " +
            "from cms.ow_hit_log " +
            "where type in (0,200) " +
            "and hit = 200 " +
            "group by country " +
            "order by count desc " +
            "limit #{limit}")
    List<VisitorsByCountry> getVisitorsByCountry(
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select country, count(*) " +
            "from cms.ow_hit_log " +
            "where type in (0,200) " +
            "and hit = 200 " +
            "and created_date > (now()  - INTERVAL '${days} DAY') " +
            "group by country " +
            "order by count desc " +
            "limit #{limit}")
    List<VisitorsByCountry> getVisitorsByCountryByDays(
            @Param("days") int days,
            @Param("limit") int limit
    );


    //--------------------------------------------------------------------------------------------------
    @Select("select distinct(address) from cms.ow_hit_log where country = '' or city = ''")
    List<String> getEmptyAddress();

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_hit_log set country = #{country}, city = #{city} where address = #{address}")
    int updateEmptyHit(
            @Param("address") String address,
            @Param("country") String country,
            @Param("city") String city
    );
}

