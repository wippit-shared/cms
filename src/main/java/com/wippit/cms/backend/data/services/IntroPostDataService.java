package com.wippit.cms.backend.data.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wippit.cms.backend.data.bean.content.*;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.bean.template.TemplateSection;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.dao.ContentDao;
import com.wippit.cms.backend.types.PostSortTypes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class IntroPostDataService {

    private final ContentDao dao;

    public IntroPostDataService(@Autowired ContentDao dao) {
        this.dao = dao;
    }

    /**
     * Get sections list for template
     * @param template Template name
     * @return Sections list
     */
    public List<HomeIndex> getHomeIndexInTemplate(String template) {
        List<HomeIndex> homeIndexList = dao.getHomeIndexByTemplate(template);
        return Objects.requireNonNullElse(homeIndexList, Collections.emptyList());
    }

    /**
     * Get sections list for template
     * @param template Template name
     * @return Sections list
     */
    public List<TemplateSection> getSectionsInTemplate(String template) {
        List<TemplateSection> templateSections = dao.getTemplateSections(template);
        return Objects.requireNonNullElse(templateSections, Collections.emptyList());
    }

    public List<IntroPost> getPostsIntro(String section, String iso, int home, int offset, int limit, PostSortTypes sortType) {
        return  dao.getArticles(iso, section, home, sortType, offset, limit);
    }

    public IntroPost getMainArticle(String iso) {
        IntroPost item =  dao.getMainArticle(iso);
        if (item == null) {
            return new IntroPost();
        }
        return item;
    }

    public long getCount(String section, String iso, int home) {
        return  dao.getArticlesCount(iso, section, home);
    }

    /**
     * Get article by Country ISO and URL
     * @param iso Country ISO
     * @param url URL
     * @return article
     */
    public Article getPost(String iso, String url) {
        return dao.getContentByISOAndURL(iso, url);
    }

    /**
     * Get Article by CIO
     * @param cid Article CID
     * @return Article
     */
    public Article getPostByCID(String cid) {
        return dao.getContentByCID(cid);
    }

    /**
     * Get Account by alias
     * @param alias Alias account
     * @return Account
     */
    public Account getAccount(String alias) {
        return dao.selectAccountByName(alias);
    }

    /**
     * Get Section by URL
     * @param iso Country ISO
     * @param secID SectionID
     * @return Section
     */
    public TemplateSection getTemplateSection(String template, String iso, String secID) {
        return dao.getTemplateSection(template, iso, secID);
    }

    /**
     * Get Section by URL
     * @param iso Country ISO
     * @param secID SectionID
     * @return Section
     */
    public Section getSection(String iso, String secID) {
        return dao.getSectionsByISOAndURL(iso, secID);
    }

    /**
     * Get Storage by URL
     * @param url Storage URL
     * @return Storage
     */
    public Storage getStorage(String url) {
        return dao.getStorageByURL(url);
    }

    /**
     * Get Countries list
     * @return countries list
     */
    public List<LocalCountry> getCountries() {
        List<LocalCountry> localCountries = dao.getCountries();
        if (localCountries == null) {
            return Collections.emptyList();
        }
        return localCountries;
    }

    /**
     * Get Country list in JSON format
     * @return JSON string with country list
     */
    public String getLocalCountry2JSON() {
        try {
            List<LocalCountry> localCountries = getCountries();
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = mapper.writeValueAsString(localCountries);
            log.debug("JSON: {}", jsonString);
            return jsonString;
        }
        catch (Exception ex) {
            log.error("On localCountries2JSON: {}", ex.toString());
        }
        return "";
    }

    /**
     * Get Redirect by URL request
     * @param iso Country ISO
     * @param name URL requested
     * @return ContentRedirect if exists
     */
    public ContentRedirect getRedirect(String iso, String name) {
        return dao.getRedirect(iso, name);
    }

}
