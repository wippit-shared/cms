package com.wippit.cms.backend.data.bean.content;

import lombok.Data;

@Data
public class HomeIndex {
    private String iso = "";
    private String template = "";
    private String page = "";
}
