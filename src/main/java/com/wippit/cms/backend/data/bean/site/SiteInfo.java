package com.wippit.cms.backend.data.bean.site;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class SiteInfo {
    private String          name = "";
    private String          description = "";
    private String          location = "";
    private String          address = "";
    private String          phone = "";
    private String          email = "";

    private String          country_iso = "";
    private int             time_offset = 0;
    private String          timezone = "";
    private String          template;

    @JsonIgnore
    private SiteTemplate    siteTemplate;

    @JsonIgnore
    public void replace(SiteInfo siteInfo) {
        name = siteInfo.getName();
        description = siteInfo.getDescription();
        location = siteInfo.getLocation();
        address = siteInfo.getAddress();
        phone = siteInfo.getPhone();
        email = siteInfo.getEmail();
        country_iso = siteInfo.getCountry_iso();
        time_offset = siteInfo.getTime_offset();
        timezone = siteInfo.getTimezone();
    }
}
