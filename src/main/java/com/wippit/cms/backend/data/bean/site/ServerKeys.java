package com.wippit.cms.backend.data.bean.site;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class ServerKeys {
    private String publicKey = "";
    private String privateKey = "";
    private String clientPublicKey = "";

    public boolean isEmpty() {
        if (StringUtils.isBlank(publicKey)
        || StringUtils.isBlank(privateKey)
        || StringUtils.isBlank(clientPublicKey)) {
            return true;
        }

        return false;
    }
}
