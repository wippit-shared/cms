package com.wippit.cms.backend.data.bean.site;

import com.wippit.cms.backend.types.ApiEmailTypes;
import lombok.Data;

@Data
public class EmailTemplate {
    private String name = "";
    private ApiEmailTypes type = ApiEmailTypes.UNKNOWN;
}
