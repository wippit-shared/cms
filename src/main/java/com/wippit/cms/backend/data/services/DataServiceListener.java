package com.wippit.cms.backend.data.services;

public interface DataServiceListener {
    void removeAccount(String alias);
}
