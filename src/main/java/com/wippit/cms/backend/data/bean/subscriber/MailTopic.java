package com.wippit.cms.backend.data.bean.subscriber;

import lombok.Data;

@Data
public class MailTopic {
    private long id = 0;
    private String mtid = "";
    private String name = "";
    private String description = "";
    private int status = 0;
}
