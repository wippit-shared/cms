package com.wippit.cms.backend.data.bean.content;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.backend.types.StatusTypes;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class ContentRedirect {
    private int     id = 0;
    private String  name = "";
    private String  url = "";
    private String  iso = "";
    private int     hit_count = 0;
    private int     status = StatusTypes.ACTIVE.value();
    @JsonIgnore
    private Timestamp start_date;
    @JsonIgnore
    private Timestamp end_date;
    @JsonIgnore
    private Timestamp created_date;


    public String getCompleteURL() {
        return AppGlobal.getInstance().getUrl()+"/go/"+name;
    }

}
