package com.wippit.cms.backend.data.bean.model;

import com.wippit.cms.AppConstants;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.api.SessionItem;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.services.LanguageDataService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class UtilModel {

    private final SessionItem sessionItem;
    private final ApplicationContext applicationContext;
    private final LanguageDataService languageDataService;

    @Getter
    private List<CountryModel> countries = new ArrayList<>();
    @Getter
    private CountryModel country;
    @Getter
    private String iso = "";

    public UtilModel(ApplicationContext applicationContext, SessionItem sessionItem, LanguageDataService languageDataService, HttpServletRequest request) {
        this.applicationContext = applicationContext;
        this.sessionItem = sessionItem;
        this.languageDataService = languageDataService;

        iso = AppUtils.getCountry(request);

        List<LocalCountry> localCountries = languageDataService.getCountries();
        String selectedCountry = AppUtils.getCountry(request);
        for (LocalCountry country: localCountries) {
            countries.add(new CountryModel(country, selectedCountry));
            if (selectedCountry.equalsIgnoreCase(country.getIso())) {
                this.country = new CountryModel(country, selectedCountry);
            }
        }
    }

    public String getToken() {
        log.trace("Getting session item");
        try {
            CacheManager cacheManager = applicationContext.getBean(CacheManager.class);
            Cache cache = cacheManager.getCache(AppConstants.CACHE_SESSIONS);
            cache.put(sessionItem.getKey(), sessionItem);
            log.trace("Session item in cache: " + sessionItem.getKey());
        }
        catch (Exception ex) {
            log.error("Cant set session in cache: " + ex.toString());
        }

        return sessionItem.getToken();
    }

    public boolean isLocale(String locale) {
        return iso.equalsIgnoreCase(locale);
    }
}
