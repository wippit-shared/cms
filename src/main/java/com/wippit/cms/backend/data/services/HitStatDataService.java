package com.wippit.cms.backend.data.services;

import com.wippit.cms.AppConstants;
import com.wippit.cms.backend.data.bean.stats.HitStatItem;
import com.wippit.cms.backend.data.dao.SystemDao;
import com.wippit.cms.backend.types.CMSItemTypes;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class HitStatDataService {
    private SystemDao systemDao;

    @Getter
    @Setter
    private CMSItemTypes type = CMSItemTypes.UNKNOWN;

    public HitStatDataService(@Autowired SystemDao systemDao) {
        this.systemDao = systemDao;
    }

    public Page<HitStatItem> getPage(int pageNumber, int limit) {
        int offset = pageNumber * AppConstants.QUERY_DEFAULT_LIMIT;
        return new PageImpl<>(fetch(offset, limit));
    }

    private List<HitStatItem> fetch(long offset, int limit) {
        List<HitStatItem> items = null;
        switch (type) {
            case ALL -> {items = systemDao.getAllHitStats(offset, limit);}
            default -> {items = systemDao.getHitStatsByType(type.value(), offset, limit);}
        }
        if (items == null) {
            return Collections.emptyList();
        }

        return items;
    }

    public long getCount() {
        long count = 0;
        switch (type) {
            case ALL -> count = systemDao.getAllHitsStatsCount();
            default -> { count = systemDao.getHitsStatsByTypeCount(type.value());}
        }
        return count;
    }

    public void deleteAll() {
        systemDao.deleteLog();
    }
}
