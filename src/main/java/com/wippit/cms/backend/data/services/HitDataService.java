package com.wippit.cms.backend.data.services;

import com.wippit.cms.AppConstants;
import com.wippit.cms.backend.data.bean.stats.HitItem;
import com.wippit.cms.backend.data.dao.SystemDao;
import com.wippit.cms.backend.types.CMSItemTypes;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class HitDataService {
    private SystemDao systemDao;

    @Getter
    @Setter
    private CMSItemTypes type = CMSItemTypes.ALL;

    public HitDataService(@Autowired SystemDao systemDao) {
        this.systemDao = systemDao;
    }

    public Page<HitItem> getPage(int currentPage, int limit) {
        long offset = currentPage* AppConstants.QUERY_DEFAULT_LIMIT;
        Page<HitItem> page = new PageImpl<HitItem>(getLogs(offset, limit));
        return page;
    }

    private List<HitItem> getLogs(long offset, int limit) {
        List<HitItem> items = null;
        switch (type) {
            case ALL -> {items = systemDao.getHitLogs(offset, limit);}
            default -> {items = systemDao.getHitLogsByType(type.value(), offset, limit);}
        }
        if (items == null) {
            return Collections.emptyList();
        }

        return items;
    }

    public long getCount() {
        long count = 0;
        switch (type) {
            case ALL -> count = systemDao.getHitLogsCount();
            default -> { count = systemDao.getHitLogsByTypeCount(type.value());}
        }
        return count;
    }

    public void deleteAll() {
        systemDao.deleteHitLog();
    }
}
