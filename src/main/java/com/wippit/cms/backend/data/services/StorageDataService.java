package com.wippit.cms.backend.data.services;

import com.wippit.cms.AppConstants;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.dao.StorageDao;
import com.wippit.cms.backend.types.StorageTypes;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class StorageDataService {
    private final StorageDao dao;

    @Getter
    @Setter
    private StorageTypes type = StorageTypes.ALL;
    @Getter
    @Setter
    private String alias = "";
    @Getter
    @Setter
    private boolean flagPrivate = false;

    public StorageDataService(@Autowired StorageDao dao) {
        this.dao = dao;
    }

    public Page<Storage> getPage(int pageNumber, int limit) {
        int offset = pageNumber * AppConstants.QUERY_DEFAULT_LIMIT;
        return new PageImpl<>(fetch(offset, limit));
    }

    private List<Storage> fetch(int offset, int limit) {
        List<Storage> items = dao.getStorages(flagPrivate, alias, type, offset, limit);
        if (items == null) {
            return Collections.emptyList();
        }

        return items;
    }

    public int getCount() {
        return dao.getStoragesCount(flagPrivate, alias, type);
    }

}
