package com.wippit.cms.backend.data.bean.template;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wippit.cms.AppUtils;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class TemplateSection {
    private String name = "";
    private String secID = "";
    private String iso = "";
    private String section_page = "";
    private String item_page = "";

    @JsonIgnore
    public String getFullURL() {
        if (secID.equalsIgnoreCase("blog")) {
            return AppUtils.createServerUrl("blog");
        }

        return AppUtils.createServerUrl("content/" + secID);
    }

    @JsonIgnore
    public boolean isComplete() {
        if (StringUtils.isBlank(section_page) || StringUtils.isBlank(item_page)) {
            return false;
        }

        return true;
    }

    @JsonIgnore
    public String getKey() {
        return buildKey(iso, secID);
    }

    @JsonIgnore
    public static String buildKey(String iso, String section) {
        return iso + "-"  + section;
    }
}
