package com.wippit.cms.backend.data.services;

import com.wippit.cms.AppConstants;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.backend.data.bean.content.ContentRedirect;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.dao.ContentDao;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Slf4j
@Component
public class ContentRedirectDataService {
    private final ContentDao dao;
    @Getter
    @Setter
    private String iso = "";

    public ContentRedirectDataService(@Autowired ContentDao dao) {
        this.dao = dao;
        this.iso = AppGlobal.getInstance().getSiteInfo().getCountry_iso();
    }

    public Page<ContentRedirect> getPage(int pageNumber, int limit) {
        int offset = pageNumber * AppConstants.QUERY_DEFAULT_LIMIT;
        return new PageImpl<>(fetch(offset, limit));
    }

    public List<ContentRedirect> fetch(int offset, int limit) {
        List<ContentRedirect> items = null;
        items = dao.getContentRedirect(iso, offset, limit);
        if (items == null) {
            items = Collections.emptyList();
        }

        return items;
    }

    public int getCount() {
        return dao.getContentRedirectCount(iso);
    }

    public boolean save(ContentRedirect item) {
        int added = dao.addRedirect(item.getIso(), item.getName(), item.getUrl(), item.getStatus(), item.getStart_date(), item.getEnd_date());
        return (added>0);
    }

    public boolean delete(ContentRedirect item) {
        int deleted = dao.deleteRedirect(item.getIso(), item.getName());
        return (deleted>0);
    }

    public List<LocalCountry> getCountries() {
        List<LocalCountry> countries = dao.getCountries();
        if (countries == null) {
            return Collections.emptyList();
        }

        return countries;
    }
}
