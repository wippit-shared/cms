package com.wippit.cms.backend.data.bean;

import com.wippit.cms.backend.types.StorageTypes;
import lombok.Data;

@Data
public class ResourceItem {
    private String filename = "";
    private String mime = "";
    private boolean restricted = false;
    private int size = 0;
    private int type = StorageTypes.OTHER.value();
    private String content = "";
}
