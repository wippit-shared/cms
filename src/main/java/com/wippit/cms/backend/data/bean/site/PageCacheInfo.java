package com.wippit.cms.backend.data.bean.site;

import lombok.Data;

import java.util.concurrent.ConcurrentHashMap;

@Data
public class PageCacheInfo {
    private ConcurrentHashMap<String, PageInfo> cache = new ConcurrentHashMap<>();

    public PageInfo getCache(String page) {
        return cache.get(page);
    }

    public PageInfo setCache(String page, String mime, long size) {
        PageInfo pageInfo = new PageInfo();
        pageInfo.setMime(mime);
        pageInfo.setSize(size);

        if (!cache.contains(page)) {
            cache.putIfAbsent(page, pageInfo);
        }

        return pageInfo;
    }

    public void clear() {
        cache.clear();
    }
}
