package com.wippit.cms.backend.data.bean.stats;

import lombok.Data;

@Data
public class HitStatItem {
    private long id;
    private String url = "";
    private int type = 0;
    private int count = 0;
}
