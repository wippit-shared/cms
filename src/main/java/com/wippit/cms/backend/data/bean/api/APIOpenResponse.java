package com.wippit.cms.backend.data.bean.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class APIOpenResponse {
    private String type = "error";
    private String text = "";
    private String data = "";

    public String toJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        }
        catch (Exception ex) {
            log.error("Serializing: " + ex.toString());
        }

        return "";
    }

    public void setSuccess() {
        type = "Success";
    }

    public void setError() {
        type = "Error";
    }
}
