package com.wippit.cms.backend.data.bean;

import lombok.Data;

@Data
public class QueryResult {
    private int total = 0;
    private int offset = 0;
    private int size = 0;
    private String filter = "";
    private String sort = "";
}
