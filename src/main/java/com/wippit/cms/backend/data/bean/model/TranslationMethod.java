package com.wippit.cms.backend.data.bean.model;

import com.wippit.cms.backend.data.services.LanguageDataService;
import freemarker.template.TemplateMethodModelEx;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class TranslationMethod implements TemplateMethodModelEx {

    private final LanguageDataService dataService;
    private final String iso;
    private final String page;
    private final String template;

    public TranslationMethod(LanguageDataService dataService, String template, String page, String iso) {
        //this.locale = locale;
        this.iso = iso;
        this.dataService = dataService;
        this.page = page;
        this.template = template;
    }

    @Override
    public Object exec(List args) {

        try {
            if (args == null || args.isEmpty()) {
                log.warn("No key to translate");
                return "";
            }

            String pPage = page;
            String key = args.get(0).toString();
            if (args.size()>1) {
                pPage = args.get(1).toString();
            }

            return dataService.getTranslationAndStore(template, pPage, iso, key);

//            LanguageItem item = dataService.getItem(template, pPage, iso, key);
//            if (item != null) {
//                return item.getContent();
//            }
//
//            //log.warn("Language item not found: {}/{}/{} ", pPage, key, iso);
//            dataService.addItem(template, pPage, iso, key, key);
//            return key;
        }
        catch (Exception ex) {
            log.error("Translating error: {}", ex.toString());
        }

        return "";
    }
}
