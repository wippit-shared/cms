package com.wippit.cms.backend.data.bean.template;

import lombok.Data;

@Data
public class TemplateAttachmentItem {
    private String filename;
    private long size;
}
