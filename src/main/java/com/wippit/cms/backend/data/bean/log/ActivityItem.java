package com.wippit.cms.backend.data.bean.log;

import com.wippit.cms.backend.types.LogTypes;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class ActivityItem {
    private long id;
    private String alias = "";
    private int type = LogTypes.INFORMATION.value();
    private String detail = "";
    private Timestamp created_date;
}
