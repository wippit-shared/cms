package com.wippit.cms.backend.data.bean.geo;

import lombok.Data;

@Data
public class CountryLocation {
    private String address = "";
    private String country = "";
    private String city = "";

    public CountryLocation(){
    }

}
