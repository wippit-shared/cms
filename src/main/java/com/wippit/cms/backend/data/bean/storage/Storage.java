package com.wippit.cms.backend.data.bean.storage;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.backend.types.StorageTypes;
import lombok.Data;

import java.sql.Timestamp;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Storage {
    private long        id = 0;
    private String      alias = "";
    private String      url = "";
    private String      fid = "";
    private int         type = StorageTypes.OTHER.value();
    private String      path = "";
    private boolean     is_private = false;
    private String      original_name = "";
    private String      properties = "";
    private long        byte_size = 0L;
    private String      mime = "";
    private Timestamp   created_date;

    @JsonIgnore
    public String getCompleteURL() {
        return AppGlobal.getInstance().getUrl() + getPartialURL();
    }

    @JsonIgnore
    public String getPartialURL() {
        return "/resource" + url;
    }

    @JsonIgnore
    public String getCompletePath() {
        return AppGlobal.getInstance().getStoragePath() + path;
    }
}
