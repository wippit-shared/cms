package com.wippit.cms.backend.data.bean.model;

import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import lombok.Data;

@Data
public class CountryModel {
    private String name = "";
    private String url = "";
    private String selected = "";
    private String flag = "";
    private String display  = "";
    private String locale = "";

    public CountryModel(LocalCountry country, String selectedCountry) {
        this.name = country.getName();
        this.locale = country.getLocale();
        this.url = AppUtils.createServerUrl("/do/lang/" + country.getIso());
        this.flag = "/static/flags/" + country.getFlag();
        this.display = country.getDisplay();

        if (selectedCountry.equalsIgnoreCase(country.getIso())) {
            selected = "selected";
        }
    }

}
