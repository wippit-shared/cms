package com.wippit.cms.backend.data.bean.api;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Data
@Getter
public class SessionItem {

    private final String ID;
    private final String token;
    @Setter
    private boolean used;

    public SessionItem(String key){

        String[] tokens = StringUtils.split(key, "/");
        if (tokens.length>0) {
            ID = tokens[0];
        }
        else {
            ID = "x";
        }
        if (tokens.length>1) {
            token = tokens[1];
        }
        else {
            token = "y";
        }

        used = false;
    }

    public SessionItem(){
        ID = "x";
        token = "x";
        used = false;
    }

    public SessionItem(String ID, String token) {
        this.ID = ID;
        this.token = token;
        used = false;
    }

    public String getKey() {
        return SessionItem.buildKey(ID, token);
    }

    public static String buildKey(String sessionID, String token) {
        return sessionID + "/" + token;
    }

}
