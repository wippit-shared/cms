package com.wippit.cms.backend.data.bean;

import com.wippit.cms.backend.data.bean.user.Account;
import lombok.Data;

@Data
public class UserInfo {
    private String username = "";
    private String full_name = "";
    private String color = "";
    private String about = "";

    public UserInfo(){
    }

    public UserInfo(Account user) {
        this.username = user.getAlias();
        this.full_name = user.getFull_name();
        this.color = user.getColor();
        this.about = user.getAbout();
    }

}
