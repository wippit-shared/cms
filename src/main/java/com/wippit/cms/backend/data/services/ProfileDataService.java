package com.wippit.cms.backend.data.services;

import com.wippit.cms.backend.data.dao.ApiDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ProfileDataService {
    private final ApiDao apiDao;

    public ProfileDataService(@Autowired ApiDao dao) {
        this.apiDao = dao;
    }

    public boolean updateProfile(String alias, String fullName, String email, String about, String color, String avatarURL, String bannerURL) {
        int result = apiDao.updateProfile(fullName, email, about, color, avatarURL, bannerURL, alias);
        return (result > 0);
    }

    public boolean updateProfileWithoutURL(String alias, String fullName, String email, String about, String color) {
        int result = apiDao.updateProfileWithoutURLs(fullName, email, about, color, alias);
        return (result > 0);
    }

}
