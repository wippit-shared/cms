package com.wippit.cms.backend.data.services;

import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.dao.StorageDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ResourceDataService {

    @Autowired
    private StorageDao dao;

    public long nextID(){
        return dao.getNextID();
    }


    public Storage add(Storage storage) {

        int added = dao.addStorage(
                storage.getId(),
                storage.getUrl(),
                storage.getFid(),
                storage.getAlias(),
                storage.getType(),
                storage.getPath(),
                storage.is_private(),
                storage.getOriginal_name(),
                (int)storage.getByte_size(),
                storage.getMime(),
                storage.getProperties()
        );

        if (added > 0) {
            return storage;
        }

        return null;
    }
}
