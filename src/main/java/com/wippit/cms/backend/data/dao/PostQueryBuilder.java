package com.wippit.cms.backend.data.dao;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class PostQueryBuilder {

    public static String getPosts(@Param("iso") String iso,
                                  @Param("secID") String section,
                                  @Param("author") String author,
                                  @Param("title") String title,
                                  @Param("tags") String tags,
                                  @Param("offset") int offset,
                                  @Param("limit") int limit) {

        return new SQL() {
            {
                SELECT("id, cid, url, iso, section_url, title, tags, image_url, content_intro, author, status, home, start_date, end_date, event_date");
                FROM("cms.ow_content ");

                boolean addAND = false;
                if (!StringUtils.isBlank(author)) {
                    WHERE("author = #{author}");
                    addAND = true;
                }
                if (!StringUtils.isBlank(iso)) {
                    if (addAND) {
                        AND();
                    }
                    WHERE("iso = #{iso}");
                    addAND = true;
                }
                if (!StringUtils.isBlank(section)) {
                    if (addAND) {
                        AND();
                    }
                    WHERE("secID = #{secID}");
                    addAND = true;
                }
                if (!StringUtils.isBlank(title)) {
                    if (addAND) {
                        AND();
                    }
                    WHERE("title like '%#{iso}%'");
                    addAND = true;
                }
                if (!StringUtils.isBlank(tags)) {
                    if (addAND) {
                        AND();
                    }
                    WHERE("tags like '%#{tags}%'");
                    addAND = true;
                }
                ORDER_BY("id desc");
                OFFSET("#{offset}");
                LIMIT("#{limit}");

            }}.toString();
    }

    public static String getPostsCount(@Param("iso") String iso,
                                  @Param("secID") String section,
                                  @Param("author") String author,
                                  @Param("title") String title,
                                  @Param("tags") String tags) {

        return new SQL() {
            {
                SELECT("coalesce(count(*), 0)");
                FROM("cms.ow_content");

                boolean addAND = false;
                if (!StringUtils.isBlank(author)) {
                    WHERE("author = #{author}");
                    addAND = true;
                }
                if (!StringUtils.isBlank(iso)) {
                    if (addAND) {
                        AND();
                    }
                    WHERE("iso = #{iso}");
                    addAND = true;
                }
                if (!StringUtils.isBlank(section)) {
                    if (addAND) {
                        AND();
                    }
                    WHERE("secID = #{secID}");
                    addAND = true;
                }
                if (!StringUtils.isBlank(title)) {
                    if (addAND) {
                        AND();
                    }
                    WHERE("title like '%#{iso}%'");
                    addAND = true;
                }
                if (!StringUtils.isBlank(tags)) {
                    if (addAND) {
                        AND();
                    }
                    WHERE("tags like '%#{tags}%'");
                    addAND = true;
                }

            }}.toString();
    }
}
