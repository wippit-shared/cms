package com.wippit.cms.backend.data.bean;

import lombok.Data;

@Data
public class SiteSection {
    private String name = "";
    private String url = "";
}
