package com.wippit.cms.backend.data.bean.user;

import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.types.SendCodeStatusTypes;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class Signup {
    private long id = 0;
    private String sgid = "";
    private String alias = "";
    private String full_name = "";
    private String email = "";
    private String code = "";
    private int status = SendCodeStatusTypes.UNKNOWN.value();
    private String info = "";
    private Timestamp created_date;
    private Timestamp expire_date;
    private Timestamp status_date;

    public Signup() {
        sgid = AppUtils.createID("SG");
        code = AppUtils.createShortConfirmationCode();
    }
}
