package com.wippit.cms.backend.data.bean.site;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wippit.cms.backend.data.bean.content.Article;
import com.wippit.cms.backend.data.bean.content.Section;
import com.wippit.cms.backend.data.bean.storage.Storage;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SiteContent {
    private SiteInfo site = new SiteInfo();
    private List<Article> articles = new ArrayList<>();
    private List<Storage> storage = new ArrayList<>();
    private List<Section> sections = new ArrayList<>();

    @JsonIgnore
    public String toJSON() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
            return json;
        }
        catch (Exception ex) {
            log.error("Cant serialize site: {}", ex.toString());
        }

        return "";    }

    @JsonIgnore
    public static SiteContent fromJSON(String json) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            SiteContent item = mapper.readValue(json, SiteContent.class);
            return item;
        }
        catch (Exception ex) {
            log.error("Cant get site content: {}", ex.toString());
            return  null;
        }
    }
}
