package com.wippit.cms.backend.data.bean.stats;

import com.wippit.cms.backend.data.bean.site.CountryCode;
import lombok.Data;

@Data
public class VisitorsByCity {
    private String country = "Unknown";
    private String city = "Unknown";
    private long count = 0;

    public VisitorsByCity(){
    }

    public VisitorsByCity(String country, String city, int count){
        this.country = country;
        this.city = city;
        this.count = count;
    }

    public String getCountryName() {
        CountryCode countryCode = CountryCode.getByAlpha2Code(country);
        if (countryCode == null) {
            return "Unknown";
        }

        return countryCode.getName();
    }
}
