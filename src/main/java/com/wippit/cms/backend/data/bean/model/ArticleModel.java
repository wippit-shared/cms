package com.wippit.cms.backend.data.bean.model;

import com.wippit.cms.AppConstants;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.content.Article;
import com.wippit.cms.backend.data.bean.user.Account;
import lombok.Data;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.Locale;

@Data
public class ArticleModel {
    private String cid = "";
    private String section = "";
    private String section_url = "";
    private String title = "";
    private String tags = "";
    private String image_url = "";
    private String introduction = "";
    private String content = "";
    private AuthorModel author;
    private String date;
    private String url = "";
    private Timestamp event_date;

    private String year = "";
    private String month = "";
    private String day = "";
    private String hour = "";
    private String minute = "";

    public ArticleModel(Article article, Account account) {
        cid = article.getCid();
        section = article.getSecID();
        section_url = article.getSectionFullURL();
        title = article.getTitle();
        tags = article.getTags();
        image_url = article.getImageURL();
        introduction = article.getContent_intro();
        content = article.getContent_complete();
        date = article.getDate();
        url = article.getFullURL();

        author = new AuthorModel(account);

        LocalDateTime ldt = AppUtils.timestampToLocalDateTime(article.getEvent_date(), AppGlobal.getInstance().getSiteInfo().getTime_offset());
        Locale locale = Locale.forLanguageTag(AppGlobal.getInstance().getSiteInfo().getCountry_iso());

        year = String.valueOf(ldt.getYear());
        month = ldt.getMonth().getDisplayName(TextStyle.FULL, locale);
        day = String.valueOf(ldt.getDayOfMonth());
        hour = String.valueOf(ldt.getHour());
        minute = String.valueOf(ldt.getMinute());
    }

    public String getImageURL() {
        if (image_url.startsWith("http")) {
            return image_url;
        }

        return AppGlobal.getInstance().getUrl() + AppConstants.PATH_SEPARATOR + "resource" + image_url;
    }

}
