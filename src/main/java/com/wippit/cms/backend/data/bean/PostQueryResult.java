package com.wippit.cms.backend.data.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wippit.cms.backend.data.bean.content.ApiIntroPost;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Data
@Slf4j
public class PostQueryResult {
    private List<ApiIntroPost> items = new ArrayList<>();
    private QueryResult result = new QueryResult();

    @JsonIgnore
    public String toJSON() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        }
        catch (Exception ex) {
            log.error("Cant serialize postQueryResult: {}", ex.toString());
        }

        return "";
    }
}
