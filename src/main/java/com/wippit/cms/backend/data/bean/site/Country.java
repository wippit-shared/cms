/*
 * Copyright (c) 2019-2020. Onewip Corp. All rights reserved.
 * onewip.com/terms
 * contact@onewip.com
 */

package com.wippit.cms.backend.data.bean.site;

import com.wippit.cms.backend.types.StatusTypes;
import lombok.Data;

@Data
public class Country {
    private int id = 0;
    private String name = "";
    private int status = 0;
    private String currency_name = "";
    private String currency_symbol = "";
    private boolean metric = true;
    private String iso2 = "";
    private String iso3 = "";
    private String phone = "";

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append(" (");
        sb.append(iso3);
        sb.append(")");
        return sb.toString();
    }
}
