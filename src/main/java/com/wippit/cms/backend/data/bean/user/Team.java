package com.wippit.cms.backend.data.bean.user;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class Team {
    private String oid = "";
    private String owner = "";
    private String name = "";
    private String commercial_name = "";
    private int type = 0;
    private String about = "";
    private String color = "";
    private String site_url = "";
    private String avatar_url = "";
    private String banner_url = "";
    private String email = "";
    private String phone = "";
    private String contact = "";
    private String address = "";
    private String work_hours = "";
    private OffsetDateTime created_date;
}
