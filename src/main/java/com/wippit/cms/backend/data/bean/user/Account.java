package com.wippit.cms.backend.data.bean.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wippit.cms.backend.types.AccountTypes;
import com.wippit.cms.backend.types.RoleTypes;
import com.wippit.cms.backend.types.StatusTypes;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Data
public class Account {
    private long id;
    private String alias = "";
    private int status = StatusTypes.ACTIVE.value();
    private String full_name = "";
    private String avatar_url = "";
    private String banner_url = "";
    private String about = "";
    private String color = "";
    private String email = "";
    private String secret = "";
    private int type = AccountTypes.SYSTEM.getType();
    private List<RoleTypes> roles = new ArrayList<>();

    public Account() {
    }

    public Account(Account original) {
        id = original.id;
        alias = original.alias;
        status = original.status;
        full_name = original.full_name;
        avatar_url = original.avatar_url;
        banner_url = original.banner_url;
        about = original.about;
        color = original.color;
        email = original.email;
        secret = original.secret;
        type = original.type;
        roles.clear();
        roles.addAll(original.roles);
    }

    private AccountExtended extended = new AccountExtended();

    public AccountTypes getAccountType() {
        return AccountTypes.getType(type);
    }

    public boolean hasRole(RoleTypes role) {
        for (RoleTypes current:roles) {
            if (role == current) {
                return true;
            }
        }

        return false;
    }

    public void setRole(RoleTypes role, boolean value) {
        if (value) {
            addRole(role);
        }
        else {
            removeRole(role);
        }
    }

    public void addRole(RoleTypes role) {
        if (hasRole(role)) return;

        roles.add(role);
    }

    public void removeRole(RoleTypes role) {
        if (!hasRole(role)) return;

        for (RoleTypes current:roles) {
            if (current == role) {
                roles.remove(current);
                return;
            }
        }
    }

    @JsonIgnore
    public boolean isAdmin() {
        return hasRole(RoleTypes.ADMIN);
    }

    @JsonIgnore
    public boolean isManager() {
        return hasRole(RoleTypes.EDITOR);
    }

    @JsonIgnore
    public boolean isAuthor() {
        return hasRole(RoleTypes.AUTHOR);
    }

    @JsonIgnore
    public boolean isDeveloper() {
        return hasRole(RoleTypes.DEVELOPER);
    }

    @JsonIgnore
    public String toString(){
        if (StringUtils.isBlank(alias)) {
            return full_name;
        }

        return full_name + " (" + alias + ")";
    }
}
