package com.wippit.cms.backend.data.bean.content;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.types.ContentStatusTypes;
import com.wippit.cms.backend.types.HomeTypes;
import com.wippit.cms.backend.types.SectionTypes;
import lombok.Data;

import java.sql.Timestamp;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Article {
    private String cid = "";
    private String ocid = "";
    private String iso = "";
    private String url = "";
    private String secID = "";
    private String section_url = "";
    private int section_type = SectionTypes.ARTICLES.getType();
    private String title = "";
    private String tags = "";
    private String image_fid = "";
    private String image_url = "";
    private String content_intro = "";
    private String content_complete = "";
    private String author = "";
    private int status = ContentStatusTypes.NOT_PUBLISHED.value();
    private int home = HomeTypes.NOT_IN_HOME.getType();
    private int homeIndex = 0;
    private Timestamp start_date;
    private Timestamp end_date;
    private Timestamp event_date;
    private Timestamp created_date;

    public Article(){
    }

    public Article(Article original, String iso){
        this.cid = AppUtils.createContentID();
        this.ocid = original.ocid;
        this.iso = iso;
        this.url = original.url;
        this.secID = original.secID;
        this.section_url = original.section_url;
        this.title = original.title;
        this.tags = original.tags;
        this.home = original.home;
        this.image_fid = original.image_fid;
        this.image_url = original.image_url;
        this.content_intro = original.content_intro;
        this.content_complete = original.content_complete;
        this.author = original.author;
        this.status = original.status;
        this.start_date = original.start_date;
        this.end_date = original.end_date;
        this.event_date = original.event_date;
    }

    @JsonIgnore
    public String getFullURL() {
        return AppUtils.createServerUrl("/content/" + secID + "/" + url);
    }

    @JsonIgnore
    public String getSectionFullURL() {
        return AppUtils.createServerUrl("/content/" +secID);
    }

    @JsonIgnore
    public String getImageURL() {
        if (image_url.startsWith("http")) {
            return image_url;
        }

        return AppUtils.createServerUrl(image_url);
    }

    @JsonIgnore
    public String getDate() {
        return AppUtils.timestampToSiteDateString(start_date);
    }
}
