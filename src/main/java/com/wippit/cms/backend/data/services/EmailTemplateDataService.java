package com.wippit.cms.backend.data.services;

import com.wippit.cms.AppConstants;
import com.wippit.cms.backend.data.dao.EmailDao;
import com.wippit.cms.backend.services.email.EmailAttachment;
import com.wippit.cms.backend.services.email.EmailTemplate;
import com.wippit.cms.backend.types.ApiEmailTypes;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Slf4j
@Component
public class EmailTemplateDataService {
    private final EmailDao dao;
    @Getter
    @Setter
    private ApiEmailTypes type = ApiEmailTypes.ALL;

    public EmailTemplateDataService(@Autowired EmailDao dao) {
        this.dao = dao;
    }

    public Page<EmailTemplate> getPage(int pageNumber, int limit) {
        int offset = pageNumber * AppConstants.QUERY_DEFAULT_LIMIT;
        List<EmailTemplate> items;
        if (type == ApiEmailTypes.ALL) {
            items = dao.getEmailTemplatesPage(offset, limit);
        }
        else {
            items = dao.getEmailTemplatesByTypePage(type.getType(), offset, limit);
        }
        if (items == null) {
            items = Collections.emptyList();
        }
        return new PageImpl<>(items);
    }

    public int getCount() {
        if (type == ApiEmailTypes.ALL) {
            return dao.getEmailTemplatesCount();
        }
        else {
            return dao.getEmailTemplatesByTypeCount(type.getType());
        }
    }

    public List<EmailTemplate> getAll() {
        List<EmailTemplate> items;
        items = dao.getEmailTemplatesPage(0, 999);
        if (items == null) {
            items = Collections.emptyList();
        }
        return items;
    }

    public boolean save(EmailTemplate template) {
        int added = dao.addEmailTemplate(
            template.getEtid(),
            template.getName(),
            template.getType(),
            template.getPage(),
            template.getHtml(),
            template.getPlain(),
            template.getData(),
            template.getComments()
        );

        return (added>0);
    }

    public EmailTemplate getByID(String eteid) {
        return dao.getEmailTemplateByID(eteid);
    }

    public boolean remove(EmailTemplate item) {
        int removed = dao.removeTemplateByID(item.getEtid());
        dao.removeTemplateAttachmentByID(item.getEtid());

        return (removed>0);
    }

    public List<EmailAttachment> getAttachments(EmailTemplate item) {
        List<EmailAttachment> items = dao.getTemplateAttachments(item.getEtid());
        if (items == null) {
            return Collections.emptyList();
        }

        return items;
    }

    public boolean addAttachment(String etid, EmailAttachment item) {
        int added = dao.addEmailAttachment(etid, item.getCid(), item.getFid());
        return (added>0);
    }

}
