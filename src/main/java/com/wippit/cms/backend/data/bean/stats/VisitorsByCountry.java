package com.wippit.cms.backend.data.bean.stats;

import com.wippit.cms.backend.data.bean.site.CountryCode;
import lombok.Data;

@Data
public class VisitorsByCountry {
    private String country = "Unknown";
    private long count = 0;

    public VisitorsByCountry(){
    }

    public VisitorsByCountry(String country, int count) {
        this.country = country;
        this.count = count;
    }

    public String getCountryName() {
        CountryCode countryCode = CountryCode.getByAlpha2Code(country);
        if (countryCode == null) {
            return "Unknown";
        }

        return countryCode.getName();
    }
}
