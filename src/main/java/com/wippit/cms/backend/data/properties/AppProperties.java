package com.wippit.cms.backend.data.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("app")
public class AppProperties {

    private String path = "";
    private String url = "";
    private String template = "";

    private DebugProperties debug = new DebugProperties();
    private StorageProperties storage = new StorageProperties();

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public DebugProperties getDebug() {
        return debug;
    }

    public void setDebug(DebugProperties debug) {
        this.debug = debug;
    }

    public StorageProperties getStorage() {
        return storage;
    }

    public void setStorage(StorageProperties storage) {
        this.storage = storage;
    }

}