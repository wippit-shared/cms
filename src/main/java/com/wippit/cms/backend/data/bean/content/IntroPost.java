package com.wippit.cms.backend.data.bean.content;

import com.wippit.cms.AppUtils;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.sql.Timestamp;

@Data
public class IntroPost {
    private long id;
    private String cid = "";
    private String ocid = "";
    private String url = "";
    private String title = "";
    private String tags = "";
    private String image_fid = "";
    private String image_url = "";
    private String content_intro = "";
    private String author = "";
    private String iso  = "";
    private String secID  = "";
    private Timestamp event_date;
    private Timestamp start_date;
    private Timestamp end_date;

    public String getDate() {
        return AppUtils.timestampToSiteDateString(start_date);
    }

    public String getImageURL() {

        if (image_url.startsWith("http")) {
            return image_url;
        }

        return AppUtils.createServerUrl(image_url);
    }

    public boolean isValid() {
        return !StringUtils.isBlank(cid);
    }
}
