package com.wippit.cms.backend.data.bean.stats;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class HitItem {
    private long id;
    private String url = "";
    private int type = 0;
    private int hit = 0;
    private String address = "";
    private String agent = "";
    private String section = "";
    private String iso = "";
    private String cid = "";
    private String city = "";
    private String country = "";
    private Timestamp created_date;
}
