package com.wippit.cms.backend.data.dao;

import com.wippit.cms.backend.types.PostSortTypes;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ContentQueryBuilder {

    public static String getArticles(
            @Param("iso") String iso,
            @Param("secID") String secID,
            @Param("home") int home,
            @Param("sort") PostSortTypes sortType,
            @Param("offset") int offset,
            @Param("limit") int limit
    ) {
        return new SQL() {
            {

                if (home > 0) {
                    SELECT("A.*, H.index ");
                    FROM("cms.ow_content A");
                    FROM("cms.ow_content_home H");
                    WHERE("H.iso = #{iso}");
                    AND();
                    WHERE("H.index > 1");
                    AND();
                    WHERE("A.cid = H.cid");
                    AND();
                    WHERE("A.status = 15001");
                    AND();
                    WHERE("A.start_date < now()");
                    AND();
                    WHERE("A.end_date >= now()");
                    ORDER_BY("index asc");
                }
                else {
                    SELECT("A.* ");
                    FROM("cms.ow_content A");
                    WHERE("A.iso = #{iso}");
                    if (secID.equalsIgnoreCase("blog")) {
                        AND();
                        WHERE("A.section_type = 200");
                    }
                    else {
                        AND();
                        WHERE("A.secID = #{secID}");
                    }
                    AND();
                    WHERE("A.status = 15001");
                    AND();
                    WHERE("A.start_date < now()");
                    AND();
                    WHERE("A.end_date >= now()");

                    switch (sortType) {
                        case DEFAULT -> ORDER_BY("id desc");
                        case TITLE -> ORDER_BY("title asc");
                        case TITLE_DESC -> ORDER_BY("title desc");
                        case START_DATE -> ORDER_BY("start_date desc");
                        case START_DATE_ASC -> ORDER_BY("start_date asc");
                        case EVENT_DATE -> ORDER_BY("event_date desc");
                        case EVENT_DATE_ASC -> ORDER_BY("event_date asc");
                    }
                }

                OFFSET("#{offset}");
                LIMIT("#{limit}");
            }
        }.toString();
    }

    public static String getArticlesCount(
            @Param("iso") String iso,
            @Param("secID") String secID,
            @Param("home") int home
    ) {

        return new SQL() {
            {
                SELECT("coalesce(count(A.*),0) ");
                FROM("cms.ow_content A");
                WHERE("A.iso = #{iso}");
                if (home==0) {
                    if (secID.equalsIgnoreCase("blog")) {
                        AND();
                        WHERE("A.section_type = 200");
                    }
                    else {
                        AND();
                        WHERE("A.secID = #{secID}");
                    }
                }
                else {
                    AND();
                    WHERE("A.home = #{home}");
                }
                AND();
                WHERE("A.status = 15001");
                AND();
                WHERE("A.start_date < now()");
                AND();
                WHERE("A.end_date >= now()");
            }
        }.toString();
    }
}
