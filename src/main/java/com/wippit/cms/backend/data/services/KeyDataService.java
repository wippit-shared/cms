package com.wippit.cms.backend.data.services;

import com.wippit.cms.backend.data.dao.SystemDao;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class KeyDataService {
    private SystemDao systemDao;

    public KeyDataService() {
        systemDao = null;
    }

    public KeyDataService(SystemDao dao) {
        setSystemDao(dao);
    }

    public void setSystemDao(SystemDao dao) {
        this.systemDao = dao;
    }

    public String getKey(String key) {
        if (systemDao == null){
            log.warn("Dao setup required");
            return "";
        }

        String value = systemDao.getSiteSetting(key);
        if (StringUtils.isBlank(value)) {
            return "";
        }

        return value;
    }

    public String getKey(String key, String defaultValue) {
        if (systemDao == null){
            log.warn("Dao setup required");
            return "";
        }

        String value = systemDao.getSiteSetting(key);
        if (StringUtils.isBlank(value)) {
            return defaultValue;
        }

        return value;
    }

    public Integer getKeyAsInt(String key) {
        String value = getKey(key);
        if (StringUtils.isBlank(value)) {
            return 0;
        }

        try {
            return Integer.parseInt(value);
        }
        catch (Exception ex) {
            return 0;
        }
    }

    public Integer getKeyAsInt(String key, Integer defaultValue) {
        Integer value = getKeyAsInt(key);
        if (value == 0) {
            return defaultValue;
        }
        return value;
    }

    public boolean setKey(String key, String value) {
        if (systemDao == null){
            log.warn("Dao setup required");
            return false;
        }

        int updated = systemDao.saveSiteSetting(key, value);
        return (updated>0);
    }

    public boolean setKey(String key, Integer value) {
        if (systemDao == null){
            log.warn("Dao setup required");
            return false;
        }

        int updated = systemDao.saveSiteSetting(key, value.toString());
        return (updated>0);
    }
}
