package com.wippit.cms.backend.data.bean.model;

import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.data.bean.site.SiteInfo;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class SiteModel {

    private String          name = "";
    private String          description = "";
    private String          location = "";
    private String          address = "";
    private String          phone = "";
    private String          email = "";
    private LocalCountry    country;
    private String          whatsapp = "";
    private int             time_offset = 0;

    public SiteModel(SiteInfo siteInfo, LocalCountry localCountry) {
        name = siteInfo.getName();
        description = siteInfo.getDescription();
        location = siteInfo.getLocation();
        address = siteInfo.getAddress();
        phone = localCountry.getPhone_code() + " " +  siteInfo.getPhone();
        if (localCountry.getPhone_code().startsWith("+")) {
            whatsapp = localCountry.getPhone_code().replace("+", "") + siteInfo.getPhone();
        }
        else {
            whatsapp = localCountry.getPhone_code() + siteInfo.getPhone();
        }
        email = siteInfo.getEmail();
        country = localCountry;
        time_offset = siteInfo.getTime_offset();
    }
}
