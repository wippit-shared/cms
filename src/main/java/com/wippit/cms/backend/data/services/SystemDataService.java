package com.wippit.cms.backend.data.services;

import com.wippit.cms.backend.data.bean.SiteSection;
import com.wippit.cms.backend.data.bean.api.Device;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.dao.ApiDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class SystemDataService {
    private final ApiDao apiDao;

    public SystemDataService(@Autowired ApiDao apiDao) {
        this.apiDao = apiDao;
    }

    public void addDevice(Device device, String secret) {
        apiDao.addDevice(device.getDeviceID(), secret, device.getUsername());
    }

    public Device getDeviceByID(String deviceID) {
        return apiDao.getDeviceByID(deviceID);
    }

    public Device getDeviceByJWT(String jwt) {
        return apiDao.getDeviceByJWT(jwt);
    }

    public void updateDeviceJWT(Device device) {
        apiDao.updateDeviceJWT(device.getDeviceID(), device.getJwt(), device.getJwt_start_date(), device.getJwt_end_date());
    }

    public void updateDeviceDetails(Device device) {
        apiDao.updateDeviceDetails(device.getDeviceID(), device.getApp(), device.getOs(), device.getModel(), device.getPub_key());
    }

    public void removeDevice(String deviceID) {
        apiDao.removeDevice(deviceID);
    }

    public Account getAccount(String alias) {
        return apiDao.selectAccountByName(alias);
    }

    public List<SiteSection> getSections(String iso, String template) {
        return apiDao.getSections(iso, template);
    }

}
