package com.wippit.cms.backend.data.bean.api;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class Device {
    private long id;
    private String deviceID = "";
    private String app = "";
    private String os = "";
    private String model = "";
    private String pub_key = "";
    private String username = "";
    private String secret = "";
    private String jwt = "";
    private Timestamp jwt_start_date;
    private Timestamp jwt_end_date;
    private Timestamp created_date;
}
