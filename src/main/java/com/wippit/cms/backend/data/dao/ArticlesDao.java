/*
 * Copyright (c) 2019-2020. Onewip Corp. All rights reserved.
 * onewip.com/terms
 * contact@onewip.com
 */

package com.wippit.cms.backend.data.dao;

import com.wippit.cms.backend.data.bean.content.Article;
import com.wippit.cms.backend.data.bean.content.ArticleBrief;
import com.wippit.cms.backend.data.bean.content.Section;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.types.ContentStatusTypes;
import com.wippit.cms.backend.types.HomeTypes;
import org.apache.ibatis.annotations.*;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface ArticlesDao {


    //------------------------------------------------------------------------------------------------------------
    @SelectProvider(type=ArticlesQueryBuilder.class, method = "getArticles")
    List<ArticleBrief> getArticles(
            @Param("iso") String iso,
            @Param("secID") String section,
            @Param("author") String author,
            @Param("status") ContentStatusTypes status,
            @Param("home") HomeTypes home,
            @Param("offset") int offset,
            @Param("limit") int limit
    );

    //------------------------------------------------------------------------------------------------------------
    @SelectProvider(type=ArticlesQueryBuilder.class, method = "getArticlesCount")
    int getArticlesCount(
            @Param("iso") String iso,
            @Param("secID") String section,
            @Param("author") String author,
            @Param("status") ContentStatusTypes status,
            @Param("home") HomeTypes home
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_section where iso = #{iso} offset #{offset} limit #{limit}")
    List<Section> getSectionsByISO(
            @Param("iso") String iso,
            @Param("offset") long offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select A.* " +
            "from cms.ow_account A, cms.ow_account_role R " +
            "where R.role = #{role} " +
            "and A.alias = R.alias " +
            "and A.status = 15001 " +
            "order by alias asc")
    List<Account> getAllAccountsByRole(
            @Param("role") String role
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_content where cid = #{cid}")
    Article getContentByCID(
            @Param("cid") String cid
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_account where alias = #{alias}")
    Account selectAccountByName(
            @Param("alias") String alias
    );

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_content (cid, ocid, iso, url, secID, section_type, title, tags, image_fid, image_url, content_intro, content_complete, author, status, start_date, end_date, home) " +
            "values (#{cid}, #{ocid}, #{iso}, #{url}, #{secID}, #{section_type}, #{title}, #{tags}, #{image_fid}, #{image_url}, #{content_intro}, #{content_complete}, #{author}, #{status}, #{start_date}, #{end_date}, #{home}) " +
            "on conflict (cid) do update set " +
            "url = #{url}, " +
            "secID = #{secID}, " +
            "section_type = #{section_type}, " +
            "title = #{title}, " +
            "tags = #{tags}, " +
            "image_fid = #{image_fid}, " +
            "image_url = #{image_url}, " +
            "content_intro = #{content_intro}, " +
            "content_complete = #{content_complete}, " +
            "author = #{author}, " +
            "status = #{status}, " +
            "start_date = #{start_date}, " +
            "end_date = #{end_date}," +
            "home = #{home} " )
    int addContent(
            @Param("cid") String cid,
            @Param("ocid") String ocid,
            @Param("iso") String iso,
            @Param("url") String url,
            @Param("secID") String section,
            @Param("section_type") int section_type,
            @Param("title") String title,
            @Param("tags") String tags,
            @Param("image_fid") String fid,
            @Param("image_url") String image_url,
            @Param("content_intro") String content_intro,
            @Param("content_complete") String content_complete,
            @Param("author") String author,
            @Param("status") int status,
            @Param("start_date") Timestamp start_date,
            @Param("end_date") Timestamp end_date,
            @Param("home") int home
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) " +
            "from cms.ow_content " +
            "where title = #{title} ")
    int validateExistsByTitle(
            @Param("title") String title
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) " +
            "from cms.ow_content " +
            "where title = #{title} " +
            "and cid <> #{cid}")
    int validateExistsByTitleAndNotCID(
            @Param("title") String title,
            @Param("cid") String cid
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) " +
            "from cms.ow_content " +
            "where cid = #{cid} ")
    int validateExistsByCID(
            @Param("cid") String title
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) " +
            "from cms.ow_content " +
            "where url = #{url} ")
    int validateExistsByURL(
            @Param("url") String url
    );

    //------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_storage where fid = #{fid}")
    Storage getStorageByFID(
            @Param("fid") String fid
    );


    //------------------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_content where cid = #{cid}")
    int removeArticleByCID(
            @Param("cid") String cid
    );

    //--------------------------------------------------------------------------------------------------
    // HOME
    //--------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(index,0) " +
            "from cms.ow_content_home " +
            "where cid = #{cid}")
    Integer getHomeIndex(
            @Param("cid") String cid
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select cid " +
            "from cms.ow_content_home " +
            "where iso = #{iso} " +
            "order by index asc")
    List<String> getHomeCIDSSByISO(
            @Param("iso") String iso
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_content_home where iso = #{iso}")
    int removeHomeIndexForISO(
            @Param("iso") String iso
    );

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_content_home (iso, cid, index) " +
            "values (#{iso}, #{cid}, #{index}) " +
            "on conflict (iso, cid) do update " +
            "set index = #{index}")
    int addHomeIndex(
        @Param("iso") String iso,
        @Param("cid") String cid,
        @Param("index") int index
    );

}

