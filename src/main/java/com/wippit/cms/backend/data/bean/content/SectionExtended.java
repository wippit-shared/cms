package com.wippit.cms.backend.data.bean.content;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.types.SectionTypes;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SectionExtended {
    private long id = 0;
    private String iso = "";
    private String secID = "";
    private String name = "";
    private int hit_count = 0;
    private int type = SectionTypes.HOME.getType();
    private LocalCountry country = new LocalCountry();

    @JsonIgnore
    public String toString() {
        return name;
    }
}
