/*
 * Copyright (c) 2019-2020. Onewip Corp. All rights reserved.
 * onewip.com/terms
 * contact@onewip.com
 */

package com.wippit.cms.backend.data.dao;

import com.wippit.cms.backend.data.bean.SiteSection;
import com.wippit.cms.backend.data.bean.api.Device;
import com.wippit.cms.backend.data.bean.user.Account;
import org.apache.ibatis.annotations.*;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface ApiDao {

    //****************************************************************************************************************
    //Device
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_device where deviceID = #{deviceID}")
    Device getDeviceByID(
            @Param("deviceID") String deviceID
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_device where jwt = #{jwt} limit 1")
    Device getDeviceByJWT(
            @Param("jwt") String deviceID
    );

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_device (deviceID, secret, username) values (#{deviceID}, #{secret}, #{username})")
    int addDevice(
            @Param("deviceID") String deviceID,
            @Param("secret") String secret,
            @Param("username") String username
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_device set secret = #{secret} where deviceID = #{deviceID}")
    int updateDeviceSecret(
            @Param("deviceID") String deviceID,
            @Param("secret") String secret
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_device set app = #{app}, os = #{os}, model = #{model}, pub_key = #{pub_key} where deviceID = #{deviceID}")
    int updateDeviceDetails(
            @Param("deviceID") String deviceID,
            @Param("app") String app,
            @Param("os") String os,
            @Param("model") String model,
            @Param("pub_key") String pubKey
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_device set jwt = #{jwt}, jwt_start_date = #{start_date}, jwt_end_date = #{end_date} where deviceID = #{deviceID}")
    int updateDeviceJWT(
            @Param("deviceID") String deviceID,
            @Param("jwt") String jwt,
            @Param("start_date") Timestamp start_date,
            @Param("end_date") Timestamp end_date
    );

    //--------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_device where deviceID = #{deviceID}")
    int removeDevice(
            @Param("deviceID") String deviceID
    );

    //****************************************************************************************************************
    //Account
    //****************************************************************************************************************
    @Select("select * from cms.ow_account where alias = #{alias}")
    Account selectAccountByName(
            @Param("alias") String alias
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_account set " +
            "full_name = #{full_name}, " +
            "about = #{about}, " +
            "color = #{color}, " +
            "email = #{email}, " +
            "avatar_url = #{avatar_url}, " +
            "banner_url = #{banner_url} " +
            "where alias = #{alias}")
    int updateProfile(
            @Param("full_name") String full_name,
            @Param("email") String email,
            @Param("about") String about,
            @Param("color") String color,
            @Param("avatar_url") String avatar_url,
            @Param("banner_url") String banner_url,
            @Param("alias") String alias
    );

    //--------------------------------------------------------------------------------------------------
    @Update("update cms.ow_account set " +
            "full_name = #{full_name}, " +
            "about = #{about}, " +
            "color = #{color}, " +
            "email = #{email} " +
            "where alias = #{alias}")
    int updateProfileWithoutURLs(
            @Param("full_name") String full_name,
            @Param("email") String email,
            @Param("about") String about,
            @Param("color") String color,
            @Param("alias") String alias
    );

    //****************************************************************************************************************
    //Sections
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Select("select S.* from cms.ow_template_section TS, cms.ow_section S " +
            "where TS.name = #{template} " +
            "and TS.iso = #{iso} " +
            "and S.url = TS.secID " +
            "and S.iso = TS.iso" )
    List<SiteSection> getSections(
            @Param("iso") String iso,
            @Param("template") String template
    );

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_section (iso, name, url) " +
            "values (#{iso}, #{name}, #{url}) " +
            "on conflict (iso, url) " +
            "do update name = #{name}")
    int addSection(
            @Param("iso") String iso,
            @Param("name") String name,
            @Param("url") String url
    );
}

