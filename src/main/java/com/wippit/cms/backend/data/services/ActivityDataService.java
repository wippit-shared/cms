package com.wippit.cms.backend.data.services;

import com.wippit.cms.AppConstants;
import com.wippit.cms.backend.data.bean.log.ActivityItem;
import com.wippit.cms.backend.data.dao.SystemDao;
import com.wippit.cms.backend.types.LogTypes;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class ActivityDataService {
    private SystemDao systemDao;
    @Getter
    @Setter
    private String alias = "";
    @Getter
    @Setter
    private LogTypes type = LogTypes.ALL;

    public ActivityDataService(@Autowired SystemDao systemDao) {
        this.systemDao = systemDao;
    }

    public Page<ActivityItem> getPage(int currentPage, int limit) {
        long offset = currentPage* AppConstants.QUERY_DEFAULT_LIMIT;
        Page<ActivityItem> page = new PageImpl<ActivityItem>(getLogs(offset, limit));
        return page;
    }

    private List<ActivityItem> getLogs(long offset, int limit) {
        List<ActivityItem> items = null;
        if (alias.equalsIgnoreCase("ALL")) {
            switch (type) {
                case ALL -> {items = systemDao.getAllActivity(offset, limit);}
                default -> {items = systemDao.getAllActivityByType(type.value(), offset, limit);}
            }

        }
        else {
            switch (type) {
                case ALL -> {items = systemDao.getAllActivityByAlias(alias, offset, limit);}
                default -> {items = systemDao.getAllActivityByTypeAndAlias(type.value(), alias, offset, limit);}
            }

        }

        if (items == null) {
            return Collections.emptyList();
        }

        return items;
    }

    public long getCount() {
        long count = 0;
        if (alias.equalsIgnoreCase("ALL")) {
            switch (type) {
                case ALL -> count = systemDao.getAllActivityCount();
                default -> { count = systemDao.getAllActivityByTypeCount(type.value());}
            }
        }
        else {
            switch (type) {
                case ALL -> count = systemDao.getAllActivityByAliasCount(alias);
                default -> { count = systemDao.getAllActivityByTypeAndAliasCount(type.value(), alias);}
            }

        }
        return count;
    }

}
