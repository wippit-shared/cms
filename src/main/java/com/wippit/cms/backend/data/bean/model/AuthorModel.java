package com.wippit.cms.backend.data.bean.model;

import com.wippit.cms.backend.data.bean.user.Account;
import lombok.Data;

@Data
public class AuthorModel {
    private String alias = "";
    private String full_name = "";
    private String about = "";
    private String avatar_url = "";
    private String banner_url = "";
    private String color = "";
    private String email = "";

    public AuthorModel(){
        alias = "anonymous";
        about = "Hi";
    }

    public AuthorModel(Account account) {
        if (account == null) {
            alias = "anonymous";
            about = "Hi";
            return;
        }

        alias = account.getAlias();
        full_name = account.getFull_name();
        about = account.getAbout();
        avatar_url = account.getAvatar_url();
        banner_url = account.getBanner_url();
        color = account.getColor();
        email = account.getEmail();
    }
}
