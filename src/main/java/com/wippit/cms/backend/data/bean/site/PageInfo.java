package com.wippit.cms.backend.data.bean.site;

import lombok.Data;

@Data
public class PageInfo {
    private long size = 0;
    private String mime = "";
}
