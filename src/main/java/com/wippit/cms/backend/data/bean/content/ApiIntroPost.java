package com.wippit.cms.backend.data.bean.content;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wippit.cms.backend.types.ContentStatusTypes;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class ApiIntroPost {
    private long id;
    private String cid = "";
    private String ocid = "";
    private String title = "";
    private String url = "";
    private String iso = "";
    private String section_url = "";
    private String tags = "";
    private String image_url = "";
    private String content_intro = "";
    private String author = "";
    private int status = ContentStatusTypes.NOT_PUBLISHED.value();
    private int hit_count = 0;
    @JsonIgnore
    private Timestamp start_date;
    @JsonIgnore
    private Timestamp end_date;
    @JsonIgnore
    private Timestamp event_date;
    private long startDate;
    private long endDate;
    private long eventDate;
}
