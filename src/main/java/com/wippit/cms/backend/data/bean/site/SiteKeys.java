package com.wippit.cms.backend.data.bean.site;

import lombok.Data;

@Data
public class SiteKeys {
    private String editor = "";
    private String translator = "";
}
