package com.wippit.cms.backend.data.bean.subscriber;

import lombok.Data;

@Data
public class MailSubscriber {
    private long id = 0;
    private String suid = "";
    private String email = "";
    private String name = "";
    private int status = 0;
}
