package com.wippit.cms.backend.data.services;

import com.wippit.cms.backend.data.bean.subscriber.MailSubscriber;
import com.wippit.cms.backend.data.dao.SystemDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class SubscriberDataService {
    private SystemDao systemDao;

    public SubscriberDataService(@Autowired SystemDao systemDao) {
        this.systemDao = systemDao;
    }

//    public Page<MailSubscriber> getPage(int currentPage) {
//        long offset = currentPage* AppConstants.QUERY_DEFAULT_LIMIT;
//        Page<MailSubscriber> page = new PageImpl<MailSubscriber>(getSubscribers(offset, AppConstants.QUERY_DEFAULT_LIMIT));
//        return page;
//    }

    public List<MailSubscriber> getSubscribers(long offset, int limit) {
        List<MailSubscriber> items = null;
        items = systemDao.getSubscribersAll(offset, limit);
        if (items == null) {
            return Collections.emptyList();
        }

        return items;
    }

    public List<MailSubscriber> getAll() {
        return systemDao.getAllSubscribers();
    }

    public long getCount() {
        return systemDao.getAllSubscribersCount();
    }
}
