package com.wippit.cms.backend.data.services;

import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.content.HomeIndex;
import com.wippit.cms.backend.data.bean.content.Section;
import com.wippit.cms.backend.data.bean.site.*;
import com.wippit.cms.backend.data.bean.template.TemplateSection;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.dao.ContentDao;
import com.wippit.cms.backend.data.dao.SystemDao;
import com.wippit.cms.backend.types.AccountTypes;
import com.wippit.cms.backend.types.RoleTypes;
import com.wippit.cms.backend.types.SectionTypes;
import com.wippit.cms.backend.utils.timezone.TimeZoneItem;
import com.wippit.cms.frontend.open.controllers.CMSWriter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class SiteDataService {
    private final SystemDao systemDao;
    private final ContentDao contentDao;
    private final KeyDataService keyDataService;

    public SiteDataService(@Autowired SystemDao systemDao, @Autowired ContentDao contentDao) {
        this.systemDao = systemDao;
        this.contentDao = contentDao;
        keyDataService = new KeyDataService(systemDao);
    }

    public SiteInfo getSiteInfo() {

        SiteInfo siteInfo = new SiteInfo();

        siteInfo.setName(keyDataService.getKey("site.name"));
        siteInfo.setDescription(keyDataService.getKey("site.description"));
        siteInfo.setLocation(keyDataService.getKey("site.location"));
        siteInfo.setAddress(keyDataService.getKey("site.address"));
        siteInfo.setPhone(keyDataService.getKey("site.phone"));
        siteInfo.setEmail(keyDataService.getKey("site.email"));
        siteInfo.setTemplate(keyDataService.getKey("site.template"));
        siteInfo.setCountry_iso(keyDataService.getKey("site.country"));
        siteInfo.setTimezone(keyDataService.getKey("site.timezone"));
        TimeZoneItem timeZoneItem = TimeZoneItem.fromString(siteInfo.getTimezone());
        siteInfo.setTime_offset(timeZoneItem.getOffsetInMinutes());
        return siteInfo;
    }

    public void save(SiteInfo siteInfo) {
        keyDataService.setKey("site.name", siteInfo.getName());
        keyDataService.setKey("site.description", siteInfo.getDescription());
        keyDataService.setKey("site.location", siteInfo.getLocation());
        keyDataService.setKey("site.address", siteInfo.getAddress());
        keyDataService.setKey("site.phone", siteInfo.getPhone());
        keyDataService.setKey("site.email", siteInfo.getEmail());
        keyDataService.setKey("site.template", siteInfo.getTemplate());
        keyDataService.setKey("site.country", siteInfo.getCountry_iso());
        keyDataService.setKey("site.timezone", siteInfo.getTimezone());
    }

    public EmailSettings getEmailSettings() {
        EmailSettings settings = new EmailSettings();

        settings.setHost(keyDataService.getKey("email.host"));
        settings.setPort(keyDataService.getKeyAsInt("email.port"));
        settings.setUsername(keyDataService.getKey("email.username"));
        settings.setPassword(keyDataService.getKey("email.password"));
        settings.setFrom(keyDataService.getKey("email.from"));
        settings.setPrefix(keyDataService.getKey("email.prefix"));
        settings.setAuth(keyDataService.getKey("email.auth").equalsIgnoreCase("1"));
        settings.setTls(keyDataService.getKey("email.tls").equalsIgnoreCase("1"));
        settings.setSsl(keyDataService.getKey("email.ssl").equalsIgnoreCase("1"));
        settings.setDebug(keyDataService.getKey("email.debug").equalsIgnoreCase("1"));

        return settings;
    }

    public void save(EmailSettings settings) {
        keyDataService.setKey("email.host", settings.getHost());
        keyDataService.setKey("email.port", settings.getPort());
        keyDataService.setKey("email.username", settings.getUsername());
        keyDataService.setKey("email.password", settings.getPassword());
        keyDataService.setKey("email.from", settings.getFrom());
        keyDataService.setKey("email.prefix", settings.getPrefix());
        keyDataService.setKey("email.auth", settings.isAuth()?1:0);
        keyDataService.setKey("email.tls", settings.isTls()?1:0);
        keyDataService.setKey("email.ssl", settings.isSsl()?1:0);
        keyDataService.setKey("email.debug", settings.isDebug()?1:0);
    }

    public SiteKeys getSiteKeys() {
        SiteKeys siteKeys = new SiteKeys();
        siteKeys.setTranslator(keyDataService.getKey("key.translator"));
        siteKeys.setEditor(keyDataService.getKey("key.editor"));
        return siteKeys;
    }

    public void save(SiteKeys keys) {
        keyDataService.setKey("key.translator", keys.getTranslator());
        keyDataService.setKey("key.editor", keys.getEditor());
    }

    public SiteTemplate getSiteTemplate(String template) {
        return systemDao.getTemplate(template);
    }

    public void save(Account account) {
        systemDao.saveAccount(account.getAlias(),
                account.getEmail(),
                account.getStatus(),
                account.getFull_name(),
                account.getAvatar_url(),
                account.getBanner_url(),
                account.getAbout(),
                account.getColor(),
                AccountTypes.SYSTEM.getType()
        );

        for (RoleTypes role : account.getRoles()){
            systemDao.updateAccountRole(account.getAlias(), role.getCode());
        }

        systemDao.updateAccountSecret(account.getAlias(), account.getSecret());
    }

    public boolean save(SiteTemplate siteTemplate) {
        int saved = systemDao.addTemplate(
                siteTemplate.getName(),
                siteTemplate.getPage_root(),
                siteTemplate.getPage_404(),
                siteTemplate.getPage_500(),
                siteTemplate.getPage_author(),
                siteTemplate.getPage_search(),
                siteTemplate.getPage_user(),
                siteTemplate.getPage_blog(),
                siteTemplate.getPage_item());

        return (saved>0);
    }

    public void saveTemplateSection(String template, String section, SectionTypes type, String iso, String page, String pageItem) {
        systemDao.addSection(iso, section, section, type.getType());
        systemDao.addTemplateSectionWithSection(template, iso, section, page, pageItem);
    }

    public void save(ServerKeys serverKeys) {
        keyDataService.setKey("key.private", serverKeys.getPrivateKey());
        keyDataService.setKey("key.public", serverKeys.getPublicKey());
        keyDataService.setKey("key.client", serverKeys.getClientPublicKey());
        //systemDao.addServerKeys(serverKeys.getPrivateKey(), serverKeys.getPublicKey());
    }

    public ServerKeys getServerKeys() {
        ServerKeys serverKeys = new ServerKeys();
        serverKeys.setPublicKey(keyDataService.getKey("key.public"));
        serverKeys.setPrivateKey(keyDataService.getKey("key.private"));
        serverKeys.setClientPublicKey(keyDataService.getKey("key.client"));

        return serverKeys;
    }

    public Account getAccount(String alias) {
        return systemDao.selectAccountByName(alias);
    }

    public List<LocalCountry> getLocalCountries() {
        List<LocalCountry> list =  systemDao.getLocalCountries();
        if (list == null){
            list = Collections.emptyList();
        }
        return list;
    }

    public LocalCountry getCountry(String iso) {
        return systemDao.getLocalCountry(iso);
    }

//    public List<TemplateSection> getTemplateSectionsByTemplate(String template) {
//        List<TemplateSection> list = systemDao.getTemplateSections(template);
//        if (list == null){
//            list = Collections.emptyList();
//        }
//        return list;
//    }

//    public List<TemplateSection> getTemplateSectionsByCountry(LocalCountry country) {
//        List<TemplateSection> list = systemDao.getTemplateSectionsByCountry(country.getIso());
//        if (list == null){
//            list = Collections.emptyList();
//        }
//        return list;
//    }

//    public List<Section> getSectionsByCountry(LocalCountry country){
//        List<Section> list = systemDao.getSectionsByISO(country.getIso(), 0,999);
//        if (list == null){
//            list = Collections.emptyList();
//        }
//        return list;
//    }

//    public List<SectionExtended> getAllSections(){
//        List<SectionExtended> list = systemDao.getAllSections(0,999);
//        if (list == null){
//            list = Collections.emptyList();
//        }
//
//        for (SectionExtended item : list) {
//            LocalCountry country = systemDao.getLocalCountry(item.getIso());
//            if (country != null) {
//                item.setCountry(country);
//            }
//        }
//
//        return list;
//    }

    public boolean setupRequired(){
        int value = keyDataService.getKeyAsInt("setup.required",0);
        return (value>0);
    }

    public boolean save(Section item) {

        int added = 0;
        int secAdded = 0;
        List<LocalCountry> countries = getLocalCountries();
        List<SiteTemplate> templates = systemDao.getAllTemplates();
        if (templates == null) {
            templates = Collections.emptyList();
        }
        for (LocalCountry country:countries) {
            secAdded = systemDao.addSection(country.getIso(), item.getSecID(), item.getName(), item.getType());
            if (secAdded>0) {
                for (SiteTemplate template:templates) {
                    systemDao.addTemplateSection(template.getName(), country.getIso(), item.getSecID(), template.getPage_blog(), template.getPage_item());
                }
                added += secAdded;
            }
        }

        return (added>0);
    }

    public List<Section> getAllDistinctSections(){
        List<Section> list = systemDao.getAllDistinctSections();
        if (list == null){
            list = Collections.emptyList();
        }
        return list;
    }

    public List<HomeIndex> getHomeTemplate(String template){

        List<LocalCountry> localization = systemDao.getLocalCountries();
        List<HomeIndex> homeList = new ArrayList<>();
        List<HomeIndex> currentHomeList = contentDao.getHomeIndexByTemplate(template);
        boolean flagAdd;
        for (LocalCountry country : localization) {
            flagAdd = true;

            for (HomeIndex homeIndex : currentHomeList) {
                if (homeIndex.getIso().equalsIgnoreCase(country.getIso())) {
                    flagAdd = false;
                    homeList.add(homeIndex);
                    break;
                }
            }

            if (flagAdd) {
                HomeIndex home = new HomeIndex();
                home.setPage(AppGlobal.getInstance().getSiteInfo().getSiteTemplate().getPage_root());
                home.setTemplate(template);
                home.setIso(country.getIso());
                homeList.add(home);
            }
        }

        return homeList;
    }

    public boolean save(HomeIndex item) {
        int added = contentDao.addHomeIndex(item.getTemplate(), item.getIso(), item.getPage());
        return (added>0);
    }

    public List<TemplateSection> getTemplateSectionsForTemplate(String template, Section section){

        List<TemplateSection> list = new ArrayList<>();
        List<LocalCountry> localization = systemDao.getLocalCountries();

        List<TemplateSection> currentList = systemDao.getTemplateSectionsBySectionID(template, section.getSecID());
        if (currentList == null){
            list = new ArrayList<>();

            for (LocalCountry country : localization) {
                TemplateSection templateSection = new TemplateSection();
                templateSection.setIso(country.getIso());
                templateSection.setSecID(section.getSecID());
                templateSection.setName(template);
                list.add(templateSection);
            }

            return list;
        }

        boolean flagFound;

        for (LocalCountry country : localization) {

            flagFound = false;
            for (TemplateSection templateSection : currentList) {

                if (templateSection.getIso().equalsIgnoreCase(country.getIso())
                    && templateSection.getSecID().equalsIgnoreCase(section.getSecID())) {
                    list.add(templateSection);
                    flagFound = true;
                    break;
                }
            }


            if (flagFound) {
                continue;
            }

            //---
            //Add section
            //---
            TemplateSection ts = new TemplateSection();
            ts.setIso(country.getIso());
            ts.setSecID(section.getSecID());
            ts.setName(template);
            ts.setSection_page(AppGlobal.getInstance().getSiteInfo().getSiteTemplate().getPage_blog());

            list.add(ts);
        }

        return list;
    }

    public boolean removeCountry(LocalCountry item, boolean includeData) {

        int removed = systemDao.removeCountry(item.getIso());

        if (removed>0) {
            if (includeData) {
                systemDao.removeSectionByISO(item.getIso());
                systemDao.removeTemplateSectionByISO(item.getIso());
                systemDao.removeArticlesByISO(item.getIso());
                systemDao.removeLanguageByISO(item.getIso());
                systemDao.removeTemplateHomeByISO(item.getIso());
            }
        }

        return (removed>0);
    }

    public boolean addCountry(LocalCountry item) {

        int added = systemDao.addCountry(
                item.getIso(),
                item.getIso2(),
                item.getIso3(),
                item.getName(),
                item.getFlag(),
                item.getLocale(),
                item.getCurrency(),
                item.getCurrency_prefix(),
                item.getDate_format(),
                item.getPhone_code(),
                item.getDisplay()
        );


        List<Section> sections = systemDao.getAllDistinctSections();
        if (sections == null) {
            sections = Collections.emptyList();
        }
        List<SiteTemplate> templates = systemDao.getAllTemplates();
        if (templates == null) {
            templates = Collections.emptyList();
        }

        //---
        //Add sections
        //---
        for (Section section: sections) {
            systemDao.addSection(item.getIso(), section.getSecID(), section.getName(), SectionTypes.HOME.getType());

            //---
            //Add template sections
            //---
            for (SiteTemplate template:templates) {
                    systemDao.addTemplateSection(
                            template.getName(),
                            item.getIso(),
                            section.getSecID(),
                            template.getPage_blog(),
                            template.getPage_item());
            }
        }

        //---
        //Add template home
        //---
        for (SiteTemplate template:templates) {
            systemDao.addTemplateHome(template.getName(), item.getIso(), template.getPage_root());
        }

        return (added>0);
    }

    public boolean removeSection(Section item) {

        int removed = systemDao.removeSectionByURL(item.getSecID());
        removed += systemDao.removeTemplateSectionByURL(item.getSecID());

        return (removed>0);
    }

    public boolean makeTemplateDefault(SiteTemplate template) {
        boolean updated = keyDataService.setKey("site.template", template.getName());
        if (updated) {
            AppGlobal.getInstance().getSiteInfo().setTemplate(template.getName());
            CMSWriter.getCurrent().setTemplate(template.getName());
        }
        return updated;
    }

    public boolean reloadDefaultTemplate() {
        String template = keyDataService.getKey("site.template");
        if (StringUtils.isBlank(template)) {
            return false;
        }

        AppGlobal.getInstance().getSiteInfo().setTemplate(template);
        CMSWriter.getCurrent().setTemplate(template);
        return true;
    }
}
