package com.wippit.cms.backend.data.services;

import com.wippit.cms.AppConstants;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.backend.data.bean.site.SiteTemplate;
import com.wippit.cms.backend.data.dao.SystemDao;
import com.wippit.cms.frontend.open.controllers.CMSWriter;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Collections;
import java.util.List;

@Slf4j
@Component
public class TemplateDataService {
    @Autowired
    private SystemDao systemDao;
    @Getter
    @Setter
    private String template = "";

    public Page<SiteTemplate> getPage(int pageNumber, int limit) {
        int offset = pageNumber * AppConstants.QUERY_DEFAULT_LIMIT;
        return new PageImpl<>(fetch(offset, limit));
    }

    private List<SiteTemplate> fetch(long offset, int limit) {
        List<SiteTemplate> list = systemDao.getAllTemplates();
        if (list == null) {
            list = Collections.emptyList();
        }
        return list;
    }

    public int getCount() {
        return systemDao.getAllTemplatesCount();
    }

    public SiteTemplate getTemplateByName(String name) {
        return systemDao.getTemplate(name);
    }

    private boolean save(SiteTemplate item, boolean createDir) {

        int added = systemDao.addTemplate(
                item.getName(),
                item.getPage_root(),
                item.getPage_404(),
                item.getPage_500(),
                item.getPage_author(),
                item.getPage_search(),
                item.getPage_user(),
                item.getPage_blog(),
                item.getPage_item()
        );

        if (createDir) {
            String dirPath = AppGlobal.getInstance().getTemplatePath() + AppConstants.PATH_SEPARATOR + item.getName();
            File directory = new File(dirPath);
            boolean created = directory.mkdirs();

            log.debug("Template saved: {} \tunzipped: {}", (added > 0), created);

            return (added > 0) && created;
        }

        return (added>0);
    }

    public boolean saveToImport(SiteTemplate item) {
        return save(item, false);
    }

    public boolean save(SiteTemplate item) {
        return save(item, true);
    }



    public boolean remove(SiteTemplate item) {
        int deleted = systemDao.deleteTemplate(item.getName());
        systemDao.deleteTemplateSections(item.getName());
        if (deleted>0) {
            try {
                String path = AppGlobal.getInstance().getTemplatePath() + "/" + item.getName();
                FileUtils.deleteDirectory(new File(path));
                return true;
            }
            catch (Exception ex) {
                log.error("Deleting template[{}] directory: {}", item.getName(), ex.toString());
                return false;
            }
        }

        return false;
    }

    public boolean makeTemplateDefault(SiteTemplate template) {
        boolean updated = setKey("site.template", template.getName());
        if (updated) {
            AppGlobal.getInstance().getSiteInfo().setTemplate(template.getName());
            CMSWriter.getCurrent().setTemplate(template.getName());
        }
        return updated;
    }

    private boolean setKey(String key, String value) {
        int updated = systemDao.saveSiteSetting(key, value);
        return (updated>0);
    }
}
