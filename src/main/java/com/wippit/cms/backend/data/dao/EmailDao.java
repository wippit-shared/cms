/*
 * Copyright (c) 2019-2020. Onewip Corp. All rights reserved.
 * onewip.com/terms
 * contact@onewip.com
 */

package com.wippit.cms.backend.data.dao;

import com.wippit.cms.backend.services.email.EmailAttachment;
import com.wippit.cms.backend.services.email.EmailItem;
import com.wippit.cms.backend.services.email.EmailTemplate;
import org.apache.ibatis.annotations.*;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface EmailDao {

    //****************************************************************************************************************
    //Email
    //****************************************************************************************************************

    //----------------------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_email_pool (emailID, emailTemplateID, type, status, email, info, from_address, expire_date) " +
            "values (#{emailID}, #{emailTemplateID}, #{type}, #{status}, #{email}, #{info}, #{fromAddress}, #{expireDate})")
    int addEmailItem(
        @Param("emailID") String confirmID,
        @Param("emailTemplateID") String emailTemplateID,
        @Param("type") int type,
        @Param("status") int status,
        @Param("email") String email,
        @Param("info") String info,
        @Param("fromAddress") String fromAddress,
        @Param("expireDate") Timestamp expireDate
    );

    //----------------------------------------------------------------------------------------------------------------
    @Update("update cms.ow_email_pool set status = #{status}, status_date = now() " +
            "where type in (10, 20, 30) and expire_date < now() and status in (0, 10, 20)")
    int expireEmailRequests(
            @Param("status") int status
    );

    //----------------------------------------------------------------------------------------------------------------
    @Update("update cms.ow_email_pool set status = #{newStatus}, status_date = now(), confirm_address = #{confirmAddress} " +
            "where emailID = #{emailID}")
    int confirmEmail(
            @Param("emailID") String emailID,
            @Param("newStatus") int newStatus,
            @Param("confirmAddress") String confirmAddress
    );

    //----------------------------------------------------------------------------------------------------------------
    @Update("update cms.ow_email_pool set status = #{newStatus}, status_date = now() " +
            "where emailID = #{emailID}")
    int updateEmailAsSent(
            @Param("emailID") String emailID,
            @Param("newStatus") int newStatus
    );

    //----------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_email_pool where emailID = #{emailID}")
    EmailItem getEmailItem(
            @Param("emailID") String confirmID
    );

    //----------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_email_template where etid = #{etid}")
    EmailTemplate getEmailTemplateByID(
            @Param("etid") String templateID
    );

    //----------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_email_template order by type asc, name asc")
    List<EmailTemplate> getEmailTemplates();

    //----------------------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_email_template")
    int getEmailTemplatesCount();

    //----------------------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) " +
            "from cms.ow_email_template " +
            "where type = #{type}")
    int getEmailTemplatesByTypeCount(
            @Param("type") int type
    );

    //----------------------------------------------------------------------------------------------------------------
    @Select("select * " +
            "from cms.ow_email_template " +
            "order by type asc, name asc " +
            "offset #{offset} limit #{limit}"
    )
    List<EmailTemplate> getEmailTemplatesPage(
            @Param("offset") int offset,
            @Param("limit") int limit
    );

    //----------------------------------------------------------------------------------------------------------------
    @Select("select * " +
            "from cms.ow_email_template " +
            "where type = #{type} " +
            "order by type asc, name asc " +
            "offset #{offset} limit #{limit}"
    )
    List<EmailTemplate> getEmailTemplatesByTypePage(
            @Param("type") int type,
            @Param("offset") int offset,
            @Param("limit") int limit
    );

    //----------------------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_email_template (etid, name, type, page, html, plain, data, comments) " +
            "values (#{etid}, #{name}, #{type}, #{page}, #{html}, #{plain}, #{data}, #{comments}) " +
            "on conflict (etid) do update " +
            "set type = #{type}," +
            "name = #{name}, " +
            "page = #{page}," +
            "html = #{html}," +
            "plain = #{plain}," +
            "data = #{data}," +
            "comments = #{comments}," +
            "created_date = now() ")
    int addEmailTemplate(
            @Param("etid") String templateID,
            @Param("name") String name,
            @Param("type") int type,
            @Param("page") String page,
            @Param("html") String html,
            @Param("plain") String plain,
            @Param("data") String data,
            @Param("comments") String comments
    );

    //----------------------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_email_template where etid = #{id}")
    int removeTemplateByID(
            @Param("id") String id
    );

    //----------------------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_email_template_attachment where etid = #{id}")
    int removeTemplateAttachmentByID(
            @Param("id") String id
    );

    //----------------------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_email_template_attachment where etid = #{id}")
    List<EmailAttachment> getTemplateAttachments(
            @Param("id") String id
    );

    //----------------------------------------------------------------------------------------------------------------
    @Delete("delete from cms.ow_email_template_attachment " +
            "where etid = #{etid} " +
            "and cid = #{cid}")
    int removeAttachment(
            @Param("etid") String etid,
            @Param("cid") String cid
    );

    //----------------------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_email_template_attachment (etid, cid, fid) " +
            "values (#{etid}, #{cid}, #{fid}) " +
            "on conflict (etid, cid) do update set " +
            "fid = #{fid}")
    int addEmailAttachment(
            @Param("etid") String etid,
            @Param("cid") String cid,
            @Param("fid") String fid
    );

    //----------------------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_email_contact_request (emailID, name, email, phone, company, message, status, from_address, confirm_address) " +
            "values (#{emailID}, #{name}, #{email}, #{phone}, #{company}, #{message}, #{status}, #{from_address}, #{confirm_address})")
    int addEmailContactRequest(
            @Param("emailID") String emailID,
            @Param("name") String name,
            @Param("email") String email,
            @Param("phone") String phone,
            @Param("company") String company,
            @Param("message") String message,
            @Param("status") int status,
            @Param("from_address") String from_address,
            @Param("confirm_address") String confirm_address
    );

}

