package com.wippit.cms.backend.data.bean.content;

import com.wippit.cms.backend.types.ContentStatusTypes;
import com.wippit.cms.backend.types.SectionTypes;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class ArticleBrief {
    private long id;
    private String cid = "";
    private String ocid = "";
    private String iso = "";
    private String url = "";
    private String secID = "";
    private int section_type = SectionTypes.ARTICLES.getType();
    private String title = "";
    private String author = "";
    private int status = ContentStatusTypes.NOT_PUBLISHED.value();
    private int home = 0;
    private int hit_count = 0;
    private Timestamp start_date;
    private Timestamp end_date;
    private Timestamp created_date;
}
