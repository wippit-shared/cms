package com.wippit.cms.backend.data.services;

import com.vladsch.flexmark.ext.admonition.AdmonitionExtension;
import com.vladsch.flexmark.ext.gfm.strikethrough.StrikethroughExtension;
import com.vladsch.flexmark.ext.tables.TablesExtension;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.data.MutableDataSet;
import com.wippit.cms.AppConstants;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.PostQueryFilter;
import com.wippit.cms.backend.data.bean.PostQueryResult;
import com.wippit.cms.backend.data.bean.QueryFilter;
import com.wippit.cms.backend.data.bean.content.ApiFullPost;
import com.wippit.cms.backend.data.bean.content.ApiIntroPost;
import com.wippit.cms.backend.data.bean.content.Section;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.dao.ApiPostDao;
import com.wippit.cms.backend.exceptions.ApiException;
import com.wippit.cms.backend.types.ApiErrorTypes;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class PostDataService {
    private final ApiPostDao dao;

    public PostDataService(@Autowired ApiPostDao dao) {
        this.dao = dao;
    }

    public boolean addPost(String cid, String iso, String title, String sectionUrl, String tags, String image_url, String contentIntro, String contentComplete, String author, int status, int home, long startDate, long endDate, long eventDate) {

        String url = AppUtils.encodeAsUrl(title);
        if (StringUtils.isBlank(url)) {
            throw new ApiException(ApiErrorTypes.INTERNAL_ERROR, "Cant encode title");
        }

        Section section = dao.getSectionsByISOAndURL(iso, sectionUrl);
        if (section == null) {
            throw new ApiException(ApiErrorTypes.INTERNAL_ERROR, "Cant find secID on iso: " + iso);
        }

        Storage storage = dao.getStorageByURL(image_url);
        if (storage == null) {
            throw new ApiException(ApiErrorTypes.INTERNAL_ERROR, "Cant find storage: " + image_url);
        }

        String introHtml = convertToHTML(contentIntro);
        String completeHtml = convertToHTML(contentComplete);

        Timestamp startTimestamp = Timestamp.from(Instant.ofEpochMilli(startDate));
        Timestamp endTimestamp = Timestamp.from(Instant.ofEpochMilli(endDate));
        Timestamp eventTimestamp = Timestamp.from(Instant.ofEpochMilli(eventDate));

        int updated = dao.addContent(cid, iso, url, section.getName(), section.getSecID(), title, tags, storage.getFid(), storage.getUrl(), contentIntro, contentComplete, introHtml, completeHtml, author, status, home, startTimestamp, endTimestamp, eventTimestamp);
        if (updated < 1) {
            log.warn("Post not updated: " + cid);
            return false;
        }

        return true;
    }

    private String convertToHTML(String markdown) {
        //---
        //Extensions
        //---
        MutableDataSet options = new MutableDataSet();
        options.set(Parser.EXTENSIONS, Arrays.asList(
                TablesExtension.create(),
                AdmonitionExtension.create(),
                StrikethroughExtension.create()));
        options.set(HtmlRenderer.SOFT_BREAK, "<br />\n");

        //---
        //Conversion
        //---
        Parser parser = Parser.builder(options).build();
        HtmlRenderer renderer = HtmlRenderer.builder(options).build();
        Node document = parser.parse(markdown);
        return renderer.render(document);
    }

    public PostQueryResult getPost(QueryFilter queryFilter, String author) {

        log.debug("PostDataService.getPost: {}", queryFilter);

        PostQueryResult postQueryResult = new PostQueryResult();

        int offset = AppConstants.QUERY_DEFAULT_LIMIT*queryFilter.getOffset();
        int total = queryFilter.getTotal();

        String iso = AppGlobal.getInstance().getSiteInfo().getCountry_iso();
        String section = "blog";
        String tags = "";
        String title = "";
        if (!StringUtils.isBlank(queryFilter.getFilter())) {
            PostQueryFilter postQueryFilter = PostQueryFilter.fromJSON(queryFilter.getFilter());
            if (postQueryFilter != null) {
                iso = postQueryFilter.getIso();
                section = postQueryFilter.getSection();
                tags = postQueryFilter.getTags();
                title = postQueryFilter.getTitle();
            }
        }
        if (!StringUtils.isBlank(queryFilter.getSort())) {
            log.warn("TO-DO: code PostDateService.sort");
        }

        if (total == 0) {
            log.debug("PostDataService.getPost: total={}", queryFilter.getTotal());
            total = dao.getPostsCount(iso, section, author, title, tags);
        }

        List<ApiIntroPost> items = dao.getPosts(iso, section, author, title, tags, offset, AppConstants.QUERY_DEFAULT_LIMIT);
        if (items != null) {
            for (ApiIntroPost item: items) {
                item.setStartDate(item.getStart_date().toInstant().toEpochMilli());
                item.setEndDate(item.getEnd_date().toInstant().toEpochMilli());
                item.setEventDate(item.getEvent_date().toInstant().toEpochMilli());
            }
            postQueryResult.setItems(items);
            postQueryResult.getResult().setFilter(queryFilter.getFilter());
            postQueryResult.getResult().setOffset(queryFilter.getOffset());
            postQueryResult.getResult().setSort(queryFilter.getSort());
            postQueryResult.getResult().setSize(items.size());
            postQueryResult.getResult().setTotal(total);
        }

        log.debug("PostDataService.getPost: Result={}",postQueryResult);
        return postQueryResult;
    }

    public ApiFullPost getFullPost(String cid) {
        ApiFullPost post = dao.getContentByCID(cid);
        post.setStartDate(post.getStart_date().toInstant().toEpochMilli());
        post.setEndDate(post.getEnd_date().toInstant().toEpochMilli());
        post.setCreatedDate(post.getCreated_date().toInstant().toEpochMilli());
        post.setEventDate(post.getEvent_date().toInstant().toEpochMilli());
        return post;
    }
}
