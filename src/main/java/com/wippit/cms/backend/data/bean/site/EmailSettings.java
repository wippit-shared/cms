package com.wippit.cms.backend.data.bean.site;

import lombok.Data;

@Data
public class EmailSettings {
    private String host = "";
    private int port = 0;
    private String username = "";
    private String password = "";
    private boolean tls = true;
    private boolean ssl = false;
    private boolean auth = true;
    private boolean debug = false;
    private String from = "";
    private String prefix = "";

    public EmailSettings(){}

    public EmailSettings(EmailSettings original) {
        host = original.getHost();
        port = original.getPort();
        username = original.getUsername();
        password = original.getPassword();
        tls = original.isTls();
        ssl = original.isSsl();
        auth = original.isAuth();
        debug = original.isDebug();
        from = original.getFrom();
        prefix = original.getPrefix();
    }
}