package com.wippit.cms.backend.data.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class QueryFilter {
    private String filter;
    private String sort;
    private int offset;
    private int total;

    @JsonIgnore
    public static QueryFilter fromJSON(String json) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(json, QueryFilter.class);
        }
        catch (Exception ex) {
            log.error("Cant get QueryFilter Json: {}", ex.toString());
            return  null;
        }
    }
}
