package com.wippit.cms.backend.data.services;

import com.wippit.cms.AppConstants;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.bean.user.AccountExtended;
import com.wippit.cms.backend.data.bean.user.Signup;
import com.wippit.cms.backend.data.dao.SystemDao;
import com.wippit.cms.backend.types.*;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class UserDataService {

    private final PasswordEncoder passwordEncoder;
    private final SystemDao systemDao;
    @Getter
    @Setter
    private RoleTypes selectedRole = RoleTypes.ALL;

    private KeyDataService keyDataService;

    public UserDataService(@Autowired SystemDao systemDao, @Autowired PasswordEncoder passwordEncoder) {
        this.systemDao = systemDao;
        this.passwordEncoder = passwordEncoder;
        this.keyDataService = new KeyDataService(systemDao);
    }

    public Page<Account> getPage(int pageNumber, int limit) {
        int offset = pageNumber * AppConstants.QUERY_DEFAULT_LIMIT;
        return new PageImpl<>(fetch(offset, limit));
    }

    public List<Account> fetch(long offset, int limit) {
        List<Account> items;
        if (selectedRole == RoleTypes.ALL) {
            items = systemDao.getAccounts(offset, limit);
        }
        else {
            items = systemDao.getAccountsByRole(selectedRole.getCode(), offset, limit);
        }

        if (items == null) {
            return Collections.emptyList();
        }
        else {
            for (Account item : items) {
                List<RoleTypes> roles = getUserRoles(item.getAlias());
                item.setRoles(roles);

                if (item.getAccountType() != AccountTypes.SYSTEM) {
                    item.setExtended(getAccountExtendedByAlias(item.getAlias()));
                }
            }
        }

        return items;
    }

    public int getCount() {
        if (selectedRole == RoleTypes.ALL) {
            return systemDao.getAccountsCount();
        }
        else {
            return systemDao.getAccountsByRoleCount(selectedRole.getCode());
        }
    }

    public Account getAccountByAlias(String username) {
        Account account = systemDao.selectAccountByName(username);
        if (account!= null) {
            account.setRoles(getUserRoles(username));

            if (account.getAccountType() != AccountTypes.SYSTEM) {
                account.setExtended(getAccountExtendedByAlias(account.getAlias()));
            }
        }

        return account;
    }

    public AccountExtended getAccountExtendedByAlias(String username) {
        AccountExtended ext = systemDao.selectAccountExtendedByName(username);
        if (ext == null) {
            return new AccountExtended();
        }
        return ext;
    }

    public boolean save(Account account) {


        boolean flagNew = (account.getId() == 0);

        log.debug("Save account: {} -> new: {}", account, flagNew);

        int added = systemDao.saveAccount(
                account.getAlias(),
                account.getEmail(),
                account.getStatus(),
                account.getFull_name(),
                account.getAvatar_url(),
                account.getBanner_url(),
                account.getAbout(),
                account.getColor(),
                account.getType());

        if (flagNew && account.getAccountType() != AccountTypes.SYSTEM) {
            systemDao.addAccountExtended(
                    account.getAlias(),
                    account.getExtended().getSetup(),
                    account.getExtended().getParam1(),
                    account.getExtended().getParam2());
        }

        if (added>0) {
            updateUserRoles(account.getAlias(), account.getRoles());
        }

        return (added>0);
    }

    public boolean remove(Account account) {

        //---
        //WARNING
        //Removing extended account may affect transactions
        //---
        if (account.getAccountType() != AccountTypes.SYSTEM) {
            systemDao.removeAccountExtended(account.getAlias());
        }

        int removed = systemDao.removeAccount(account.getAlias());
        systemDao.removeAccountAllRoles(account.getAlias());

        return (removed>0);
    }

    public boolean changeSecret(String alias, String secret) {
        String encoded = passwordEncoder.encode(secret);
        int added = systemDao.updateAccountSecret(alias, encoded);
        return (added>0);
    }

    public boolean changeSetup(String alias, AccountSetupTypes setup) {
        int added = systemDao.updateAccountExtendedSetup(alias, setup.getType());
        return (added>0);
    }

    public List<RoleTypes> getUserRoles(String alias) {
        List<RoleTypes> roles = new ArrayList<>();

        List<String> userRoles = systemDao.getAccountRoles(alias);
        if (userRoles == null || userRoles.isEmpty()) {
            return roles;
        }

        for (String role : userRoles) {
            RoleTypes userRole = RoleTypes.getType(role);
            if (userRole == RoleTypes.GUEST) {
                continue;
            }

            roles.add(userRole);
        }

        return roles;
    }

    public void updateUserRoles(String alias, List<RoleTypes> newRoles) {
        systemDao.removeAccountAllRoles(alias);
        for (RoleTypes role:newRoles) {
            systemDao.updateAccountRole(alias, role.getCode());
        }
    }

    public boolean addSignup(Signup signup) {

        int added = systemDao.addSignup(
                signup.getSgid(),
                signup.getAlias(),
                signup.getFull_name(),
                signup.getEmail(),
                signup.getCode(),
                signup.getInfo(),
                SendCodeStatusTypes.WAITING.value()
        );
        return (added>0);
    }

    public boolean validateNotExistsSignup(String alias, String email) {
        int val1 = systemDao.validateAccountAneEmailExists(alias, email);
        int val2 = systemDao.validateSignupExists(alias, email);

        return val1 == 0 && val2 == 0;
    }

    public String getSignupCodeByID(String sgid) {
        return systemDao.getSignupCodeByID(sgid, SendCodeStatusTypes.WAITING.value());
    }

    public boolean approveSignupStatus(Signup signup) {
        int updated = systemDao.updateSignupStatus(signup.getSgid(), SendCodeStatusTypes.CONFIRMED.value());
        return (updated>0);
    }

    public boolean isSignupExpired(Signup signup) {
        int expired = systemDao.validateIfSignupIsExpired(signup.getSgid());
        return (expired>0);
    }

    public boolean createAccount(Signup signup, String newPassword, RoleTypes role) {

        String encodedPassword = passwordEncoder.encode(newPassword);

        Account account = new Account();
        account.setAlias(signup.getAlias());
        account.setStatus(StatusTypes.ACTIVE.value());
        account.setFull_name(signup.getFull_name());
        account.setAvatar_url("");
        account.setBanner_url("");
        account.setEmail(signup.getEmail());
        account.setAbout("");
        account.setColor("#123456");
        account.getRoles().add(role);
        account.setSecret(encodedPassword);

        AccountTypes accountType;

        switch (role) {
            case ADMIN:
            case EDITOR:
            case DEVELOPER:
            case AUTHOR:
                accountType = AccountTypes.SYSTEM;
                break;

            default:
                accountType = AccountTypes.EXTERNAL;
                break;
        };

        account.setType(accountType.getType());

        if (save(account)) {
            int pwdUpdated = systemDao.updateAccountSecret(account.getAlias(), account.getSecret());
            return (pwdUpdated>0);
        }

        return false;
    }

    public boolean setKey(String key, String value) {
        return keyDataService.setKey(key, value);
    }

    public String getKey(String key) {
        return keyDataService.getKey(key);
    }

}
