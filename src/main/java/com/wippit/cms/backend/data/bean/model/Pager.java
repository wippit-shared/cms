package com.wippit.cms.backend.data.bean.model;

import lombok.Getter;

public class Pager {

    @Getter
    private int previousIndex = -1;
    @Getter
    private int currentIndex = -1;
    @Getter
    private int currentSize = 0;
    @Getter
    private int nextIndex = -1;
    @Getter
    private int firstIndex = -1;
    @Getter
    private int lastIndex = -1;
    @Getter
    private int totalPages = 0;
    @Getter
    private int pageSize = 0;
    @Getter
    private int totalItems = 0;

    private String url;

    public Pager(){
    }

    /**
     * Creates a Section Pager
     * @param url Base url
     * @param pageIndex current page index
     * @param pageSize page size
     * @param totalItems total items
     */
    public Pager(String url, int pageIndex, int pageSize, int totalItems) {

        this.url = url;
        this.totalItems = totalItems;
        this.pageSize = pageSize;
        this.currentIndex = pageIndex;
        this.firstIndex = 0;

        if (pageIndex < 0) {
            currentIndex = -1;
            currentSize = 0;
            return;
        }

        totalPages = Math.floorDiv(totalItems, pageSize);
        int remainder = Math.floorDiv(totalItems, pageSize);
        int currentMax = (pageIndex+1)*pageSize;

        if (currentMax >= totalItems) {
            //---
            //This is the last page
            //---
            currentSize = totalItems - pageIndex*pageSize;
            lastIndex = pageIndex;
            if (pageIndex > 0) {
                previousIndex = pageIndex - 1;
            }
        }
        else {
            //---
            //This isn't the last page
            //---
            if (remainder == 0) {
                lastIndex = totalPages;
            }
            else {
                lastIndex = totalPages - 1;
            }
            currentSize = pageSize;
            previousIndex = pageIndex - 1;
            nextIndex = pageIndex + 1;
        }

    }

    /**
     * Validates if pager index is valid
     * @param index page index to validate
     * @return true if index is valid, false otherwise
     */
    public boolean isValid(int index) {
        if (index < 0) {
            return false;
        }
        if (index >= totalPages) {
            return false;
        }

        return true;
    }

    /**
     * Creates a url based on page index
     * @param index page index
     * @return url if page index is valid, empty string otherwise
     */
    public String getUrl(int index) {
        if (!isValid(index)) {
            return "";
        }

        return url + "?page=" + index;
    }

    protected void setPager(Pager original) {
        this.url = original.url;
        this.totalItems = original.totalItems;
        this.pageSize = original.pageSize;
        this.totalPages = original.totalPages;
        this.firstIndex = original.firstIndex;
        this.lastIndex = original.lastIndex;
        this.currentSize = original.currentSize;
        this.currentIndex = original.currentIndex;
        this.previousIndex = original.previousIndex;
        this.nextIndex = original.nextIndex;
    }

    protected void clear() {
        this.url = "";
        this.totalItems = 0;
        this.pageSize = 0;
        this.totalPages = 0;
        this.firstIndex = 0;
        this.lastIndex = 0;
        this.currentSize = 0;
        this.currentIndex = 0;
        this.previousIndex = 0;
        this.nextIndex = 0;
    }
}
