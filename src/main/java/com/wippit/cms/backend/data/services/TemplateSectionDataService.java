package com.wippit.cms.backend.data.services;

import com.wippit.cms.backend.data.bean.content.SectionExtended;
import com.wippit.cms.backend.data.bean.template.TemplateSection;
import com.wippit.cms.backend.data.dao.SystemDao;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public class TemplateSectionDataService {
    private SystemDao systemDao;
    private int _count;
    @Getter
    @Setter
    private String template = "";

    public TemplateSectionDataService(SystemDao systemDao) {
        this.systemDao = systemDao;
        _count = 0;
    }

    public TemplateSectionDataService(SystemDao systemDao, String iso) {
        this.systemDao = systemDao;
//        this.iso = iso;
        _count = 0;
    }

//    public Page<TemplateSection> getPage(int pageNumber) {
//        long offset = pageNumber* AppConstants.QUERY_DEFAULT_LIMIT;
//        Page<TemplateSection> page = new PageImpl<TemplateSection>(fetch(offset, AppConstants.QUERY_DEFAULT_LIMIT));
//        return page;
//    }

    public List<TemplateSection> fetch(long offset, int limit) {

        List<SectionExtended> sections = systemDao.getAllSections( 0, 999);
        if (sections == null) {
            return Collections.emptyList();
        }

        List<TemplateSection> foundItems = null;
        List<TemplateSection> templateSectionList = new ArrayList<>();

        foundItems = systemDao.getTemplateSections(template);
        if (foundItems == null) {
            foundItems = new ArrayList<>();
        }

        boolean addSection;
        for (SectionExtended section : sections) {
            addSection = true;
            for (TemplateSection templateSection:foundItems) {
                if (templateSection.getSecID().equalsIgnoreCase(section.getSecID())
                    && templateSection.getIso().equalsIgnoreCase(section.getIso())) {
                    addSection = false;
                    templateSectionList.add(templateSection);
                    break;
                }
            }
            if (addSection) {
                TemplateSection templateSection = new TemplateSection();
                templateSection.setName(template);
                templateSection.setSecID(section.getSecID());
                templateSection.setIso(section.getIso());
                templateSectionList.add(templateSection);
            }
        }

        return templateSectionList;
    }

}
