package com.wippit.cms.backend.data.bean;

import com.wippit.cms.backend.types.QueryCommandTypes;
import lombok.Data;

@Data
public class QueryRequestMessage {
    private QueryCommandTypes command = QueryCommandTypes.UNKNOWN;
    private String params;
}
