package com.wippit.cms.backend.data.bean.user;

import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.types.SendCodeStatusTypes;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class RecoverPassword {
    private long id = 0;
    private String reid = "";
    private String alias = "";
    private String email = "";
    private String code = "";
    private int status = SendCodeStatusTypes.UNKNOWN.value();
    private Timestamp created_date;
    private Timestamp expire_date;
    private Timestamp status_date;

    public RecoverPassword() {
        reid = AppUtils.createID("RE");
        code = AppUtils.createShortConfirmationCode();
    }
}
