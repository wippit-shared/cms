package com.wippit.cms.backend.data.services;

import com.wippit.cms.AppConstants;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.api.ContactMessageRequest;
import com.wippit.cms.backend.data.dao.EmailDao;
import com.wippit.cms.backend.services.email.EmailAttachment;
import com.wippit.cms.backend.services.email.EmailItem;
import com.wippit.cms.backend.services.email.EmailTemplate;
import com.wippit.cms.backend.types.ApiEmailContactStatus;
import com.wippit.cms.backend.types.ApiEmailRequestStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class EmailDataService {
    private final EmailDao dao;

    public EmailDataService(@Autowired EmailDao emailDao) {
        this.dao = emailDao;
    }

    public String createEmailID() {
        return AppUtils.createID("EC");
    }

    public String addEmail(ContactMessageRequest contactMessageRequest, String fromAddress) {

        String emailID = createEmailID();

        int added = dao.addEmailItem(
                emailID,
                contactMessageRequest.getTemplateID(),
                contactMessageRequest.getType().value(),
                ApiEmailRequestStatus.CREATED.value(),
                contactMessageRequest.getEmail(),
                contactMessageRequest.toJson(),
                fromAddress,
                AppUtils.getNowPlusMinutes(AppConstants.CACHE_TIMEOUT_MIN));

        if (added > 0) {
            return emailID;
        }

        return "";
    }

    public EmailItem getEmail(String emailID) {
        return dao.getEmailItem(emailID);
    }

    public void expireEmailRequests() {
        int expired = dao.expireEmailRequests(ApiEmailRequestStatus.EXPIRED.value());
        if (expired > 0) {
            log.info("Expired {} emails", expired);
        }
    }

    public boolean confirmEmailRequest(String confirmID, String confirmAddress) {
        int confirmed = dao.confirmEmail(confirmID, ApiEmailRequestStatus.CONFIRMED.value(), confirmAddress);
        return confirmed > 0;
    }

    public boolean updateEmailAsSent(String emailID) {
        int updated = dao.updateEmailAsSent(emailID, ApiEmailRequestStatus.SENT.value());
        return (updated>0);
    }

    public EmailTemplate getEmailTemplate(String templateID) {
        return dao.getEmailTemplateByID(templateID);
    }

//    public boolean addEmailTemplate(ApiEmailTypes type, String emailTemplate, String pageTemplate, String name, String comments) {
//        String emailTemplateID = AppUtils.createID("ET");
//        int added = dao.addEmailTemplate(emailTemplateID, type.getType(), emailTemplate, pageTemplate, name, comments);
//        return added>0;
//    }
//
//    public boolean updateEmailTemplate(String emailTemplateID, ApiEmailTypes type, String emailTemplate, String pageTemplate, String name, String comments) {
//        int updated = dao.addEmailTemplate(emailTemplateID, type.getType(), emailTemplate, pageTemplate, name, comments);
//        return updated>0;
//    }

    public List<EmailAttachment> getAttachments(EmailTemplate item) {
        List<EmailAttachment> items = dao.getTemplateAttachments(item.getEtid());
        if (items == null) {
            return Collections.emptyList();
        }

        return items;
    }

    public List<EmailTemplate> getEmailTemplates() {
        List<EmailTemplate> items = dao.getEmailTemplates();
        if (items == null) {
            return Collections.emptyList();
        }

        return items;
    }

    public boolean addContactRequest(EmailItem emailItem, ContactMessageRequest messageRequest) {

        messageRequest.makeClean();
        int added = dao.addEmailContactRequest(
                emailItem.getEmailID(),
                messageRequest.getName(),
                messageRequest.getEmail(),
                messageRequest.getPhone(),
                messageRequest.getCompany(),
                messageRequest.getMessage(),
                ApiEmailContactStatus.CREATED.value(),
                emailItem.getFrom_address(),
                emailItem.getConfirm_address());

        return (added>0);
    }
}
