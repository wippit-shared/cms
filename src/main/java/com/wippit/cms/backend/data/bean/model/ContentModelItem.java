package com.wippit.cms.backend.data.bean.model;


import com.wippit.cms.AppConstants;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.content.IntroPost;
import com.wippit.cms.backend.data.bean.content.Section;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.services.IntroPostDataService;
import com.wippit.cms.backend.types.PostSortTypes;
import com.wippit.cms.backend.types.SectionTypes;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ContentModelItem {
    @Getter
    private String name = "";
    @Getter
    public String iso = "";
    @Getter
    public final Pager pager = new Pager();
    @Getter
    private int rows = 0;
    @Getter
    @Setter
    private int pageIndex = 0;
    @Getter
    private String sectionID = "";
    @Getter
    private String sectionName = "";
    @Getter
    private String url = "";
    @Getter
    private String message = "";
    @Getter
    private int maxRow = AppConstants.QUERY_DEFAULT_LIMIT;
    @Getter
    private int maxCol = 1;

    @Setter
    protected boolean flagArrayZero = true;

    private boolean flagInvalid = false;
    private final SectionTypes type;
    private int articleIndex = -1;
    private final PostSortTypes sortType = PostSortTypes.DEFAULT;
    private final IntroPostDataService dataService;
    public final List<ArticleIntroModel> articles = new ArrayList<>();

    public ContentModelItem(String sectionID, String iso, IntroPostDataService dataService, SectionTypes type) {

        log.debug("ContentModelItem sectionID: {}", sectionID);

        this.sectionID = sectionID;
        this.iso = iso;
        this.dataService = dataService;
        this.type = type;

        if (StringUtils.isBlank(sectionID)) {
            flagInvalid = true;
            name = "Invalid";
            log.warn("Invalid sectionID: {}", sectionID);
            return;
        }

        if (sectionID.equalsIgnoreCase("home") && type == SectionTypes.HOME) {
            name = "Home";
            url = AppUtils.createServerUrl("");
        }
        else {
            Section section = dataService.getSection(iso, sectionID);
            if (section == null) {
                flagInvalid = true;
                name = "Invalid";
                log.warn("Invalid sectionID: {}", sectionID);
                return;
            }

            name = section.getName();
            url = section.getFullURL();
            sectionName = section.getName();
        }

        log.info("ContentItem with sectionID: {}/{}", sectionID,url);
    }

    public void loadGrid(int rows, int columns) {
        if (flagInvalid) {
            clear();
            return;
        }
        this.maxCol = columns;
        this.maxRow = rows;
        log.warn("MAX({}, {}) ", maxRow, maxCol);
        load();
    }

    public void load() {

        clear();
        if (flagInvalid){
            log.warn("Content model item is invalid: {}", sectionID);
            return;
        }

        try {
            switch (type) {
                case HOME -> loadHome();
                case ARTICLES -> loadSection();
                case NOTES -> loadNotes();
            }
        }
        catch (Exception ex) {
            log.error("On loading sectionID={}, error: {}", sectionID, ex.toString());
            clearWithError(ex.toString());
        }
    }

    public ArticleIntroModel next() {
        articleIndex++;
        if (articleIndex < articles.size()) {
            return   articles.get(articleIndex);
        }

        return new ArticleIntroModel();
    }

    public ArticleIntroModel first() {
        if (!articles.isEmpty()) {
            return articles.get(0);
        }

        return new ArticleIntroModel();
    }

    public ArticleIntroModel get(int row, int col) {

        log.warn("Request {}({}, {})", sectionID.toUpperCase(), row, col);

        if (articles.isEmpty()) {
            return new ArticleIntroModel();
        }

        int pos;
        if (flagArrayZero) {
            pos = row*maxCol + col;
        }
        else {
            pos = (row-1)*maxCol + (col-1);
        }

        if (pos >= articles.size() || pos < 0) {
            return new ArticleIntroModel();
        }

        ArticleIntroModel item = articles.get(pos);
        log.warn("{}[{}] = ITEM({}, {}) = {}", sectionID.toUpperCase(), pos, item.getRow(), item.getCol(), item.getTitle());
        return item;
    }

//    public int getIndex() {
//        return articleIndex;
//    }

    private void loadHome(){

        pageIndex = 0;
        log.debug("Loading posts in HOME");
        List<IntroPost> posts = new ArrayList<>();
        posts = dataService.getPostsIntro(sectionID, iso, 1, 0, AppConstants.QUERY_DEFAULT_LIMIT, sortType);
        if (posts == null) {
            pager.setPager(new Pager(AppGlobal.getInstance().getUrl(), 0, AppConstants.QUERY_DEFAULT_LIMIT, 0));
            return;
        }

        int total = (int) dataService.getCount(sectionID, iso, 1);
        pager.setPager(new Pager(AppGlobal.getInstance().getUrl(), 0, AppConstants.QUERY_DEFAULT_LIMIT, total));

        loadItems(posts);
    }

    private void loadSection() {

        List<IntroPost> posts = new ArrayList<>();
        log.debug("Loading posts in secID: {}", sectionID);

        int offset = pageIndex*AppConstants.QUERY_DEFAULT_LIMIT;
        posts = dataService.getPostsIntro(sectionID, iso, 0, offset, AppConstants.QUERY_DEFAULT_LIMIT, sortType);
        if (posts == null) {
            return;
        }

        int total = (int) dataService.getCount(sectionID, iso, 0);
        pager.setPager(new Pager(AppGlobal.getInstance().getUrl(), pageIndex, AppConstants.QUERY_DEFAULT_LIMIT, total));

        loadItems(posts);
    }

    private void loadNotes() {
        List<IntroPost> posts = new ArrayList<>();
        log.debug("Loading posts in notes: {}", sectionID);

        int offset = pageIndex*AppConstants.QUERY_DEFAULT_LIMIT;
        posts = dataService.getPostsIntro(sectionID, iso, 0, offset, AppConstants.QUERY_DEFAULT_LIMIT, sortType);
        if (posts == null) {
            return;
        }

        int total = (int) dataService.getCount(sectionID, iso, 0);
        pager.setPager(new Pager(AppGlobal.getInstance().getUrl(), pageIndex, AppConstants.QUERY_DEFAULT_LIMIT, total));

        loadItems(posts);
    }

    private void loadItems(List<IntroPost> posts) {

        int row = 0;
        int col = 0;
        int index = 0;
        Section currentSection = null;
        String currentSectionURL = url;
        String currentSectionName = sectionName;
        String currentSectionID = sectionID;


        for (IntroPost item : posts) {
            Account account = dataService.getAccount(item.getAuthor());

            if (currentSection == null || !currentSection.getSecID().equalsIgnoreCase(item.getSecID())) {
                currentSection = dataService.getSection(iso, item.getSecID());
            }
            if (currentSection != null) {
                currentSectionURL = currentSection.getFullURL();
                currentSectionName = currentSection.getName();
                currentSectionID = currentSection.getSecID();
            }

            ArticleIntroModel introModel = new ArticleIntroModel(item, account, currentSectionName, currentSectionURL, currentSectionID);
            introModel.setIndex(index);
            introModel.setRow(row);
            introModel.setCol(col);

            col++;
            if (col>= maxCol) {
                col = 0;
                row++;
            }

            articles.add(introModel);
            index++;
        }

        rows = row;
    }

    private void clear(){
        articles.clear();
        pager.clear();
        rows = 0;
    }

    private void clearWithError(String error){
        message = error;
        articles.clear();
        pager.clear();
    }
}
