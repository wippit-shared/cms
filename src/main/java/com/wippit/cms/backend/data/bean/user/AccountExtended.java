package com.wippit.cms.backend.data.bean.user;

import com.wippit.cms.backend.types.AccountSetupTypes;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountExtended  {
    private int setup = AccountSetupTypes.SETUP_REQUIRED.getType();
    private String param1 = "";
    private String param2 = "";
}
