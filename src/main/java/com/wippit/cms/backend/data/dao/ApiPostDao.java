/*
 * Copyright (c) 2019-2020. Onewip Corp. All rights reserved.
 * onewip.com/terms
 * contact@onewip.com
 */

package com.wippit.cms.backend.data.dao;

import com.wippit.cms.backend.data.bean.content.ApiFullPost;
import com.wippit.cms.backend.data.bean.content.ApiIntroPost;
import com.wippit.cms.backend.data.bean.content.Section;
import com.wippit.cms.backend.data.bean.storage.Storage;
import org.apache.ibatis.annotations.*;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface ApiPostDao {

    //****************************************************************************************************************
    //Storage
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_storage where url = #{url}")
    Storage getStorageByURL(
            @Param("url") String url
    );

    //****************************************************************************************************************
    //Section
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_section where iso = #{iso} and url = #{url}")
    Section getSectionsByISOAndURL(
            @Param("iso") String iso,
            @Param("url") String name
    );

    //****************************************************************************************************************
    //Content
    //****************************************************************************************************************

    //--------------------------------------------------------------------------------------------------
    @Insert("insert into cms.ow_content (cid, iso, url, secID, section_url, title, tags, image_fid, image_url, content_intro, content_intro_html, content_complete, content_complete_html, author, status, home, start_date, end_date, event_date) " +
            "values (#{cid}, #{iso}, #{url}, #{secID}, #{section_url}, #{title}, #{tags}, #{image_fid}, #{image_url}, #{content_intro}, #{content_intro_html}, #{content_complete}, #{content_complete_html}, #{author}, #{status}, #{home}, #{start_date}, #{end_date}, #{event_date}) " +
            "on conflict (cid) do update set " +
            "url = #{url}, " +
            "secID = #{secID}, " +
            "section_url = #{section_url}, " +
            "title = #{title}, " +
            "tags = #{tags}, " +
            "image_fid = #{image_fid}, " +
            "image_url = #{image_url}, " +
            "content_intro = #{content_intro}, " +
            "content_intro_html = #{content_intro_html}, " +
            "content_complete = #{content_complete}, " +
            "content_complete_html = #{content_complete_html}, " +
            "author = #{author}, " +
            "status = #{status}, " +
            "home = #{home}, " +
            "start_date = #{start_date}, " +
            "end_date = #{end_date}, " +
            "event_date = #{event_date} ")
    int addContent(
            @Param("cid") String cid,
            @Param("iso") String iso,
            @Param("url") String url,
            @Param("secID") String section,
            @Param("section_url") String section_url,
            @Param("title") String title,
            @Param("tags") String tags,
            @Param("image_fid") String fid,
            @Param("image_url") String image_url,
            @Param("content_intro") String content_intro,
            @Param("content_complete") String content_complete,
            @Param("content_intro_html") String content_intro_html,
            @Param("content_complete_html") String content_complete_html,
            @Param("author") String author,
            @Param("status") int status,
            @Param("home") int home,
            @Param("start_date") Timestamp start_date,
            @Param("end_date") Timestamp end_date,
            @Param("event_date") Timestamp event_date
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_content where cid = #{cid}")
    ApiFullPost getContentByCID(
            @Param("cid") String cid
    );

    //------------------------------------------------------------------------------------------------------------
    @SelectProvider(type=PostQueryBuilder.class, method = "getPosts")
    List<ApiIntroPost> getPosts(@Param("iso") String iso,
                                @Param("secID") String section,
                                @Param("author") String author,
                                @Param("title") String title,
                                @Param("tags") String tags,
                                @Param("offset") int offset,
                                @Param("limit") int limit);

    //------------------------------------------------------------------------------------------------------------
    @SelectProvider(type=PostQueryBuilder.class, method = "getPostsCount")
    int getPostsCount(@Param("iso") String iso,
                             @Param("secID") String section,
                             @Param("author") String author,
                             @Param("title") String title,
                             @Param("tags") String tags);
}

