package com.wippit.cms.backend.data.bean;

public interface ImportExport<T> {
    public String toJSON();
    public T fromJSON(String json);
}
