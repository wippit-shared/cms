/*
 * Copyright (c) 2019-2020. Onewip Corp. All rights reserved.
 * onewip.com/terms
 * contact@onewip.com
 */

package com.wippit.cms.backend.data.dao;

import com.wippit.cms.backend.data.bean.content.ContentRedirect;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface ApiRedirectDao {
    //--------------------------------------------------------------------------------------------------
    @Select("select * from cms.ow_redirect where iso = #{iso} order by name asc offset #{offset} limit #{limit}")
    List<ContentRedirect> getRedirects(
            @Param("iso") String iso,
            @Param("offset") int offset,
            @Param("limit") int limit
    );

    //--------------------------------------------------------------------------------------------------
    @Select("select coalesce(count(*),0) from cms.ow_redirect where iso = #{iso}")
    int getRedirectsCount(
            @Param("iso") String iso
    );

    @Insert("insert into cms.ow_redirect (iso, name, url, status, start_date, end_date) " +
            "values (#{iso}, #{name}, #{url}, #{status}, #{start_date}, #{end_date}) " +
            "on conflict (iso, name) do update set " +
            "status = #{status}, " +
            "start_date = #{start_date}," +
            "end_date = #{end_date}")
    int addRedirect(
            @Param("iso") String iso,
            @Param("name") String name,
            @Param("url") String  url,
            @Param("status") int status,
            @Param("start_date") Timestamp start_date,
            @Param("end_date") Timestamp end_date
    );
}

