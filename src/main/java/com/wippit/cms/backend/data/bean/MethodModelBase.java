package com.wippit.cms.backend.data.bean;

import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;

@Slf4j
public class MethodModelBase implements TemplateMethodModelEx {

    private DateTimeFormatter dateFormat;

    public MethodModelBase(){
        //dateFormat = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withLocale(localeType.getLocale());
        dateFormat = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
    }

    @Override
    public Object exec(List list) throws TemplateModelException {
        if (list == null || list.size() < 1) {
            return new SimpleScalar("");
        }

        if (list.size() == 3) {
            return doMethod(list.get(0).toString(), list.get(1).toString(), list.get(2).toString());
        }
        else if (list.size() == 2) {
            return doMethod(list.get(0).toString(), list.get(1).toString());
        }
        else {
            return doMethod(list.get(0).toString());
        }
    }

    public Object doMethod(String method, String param1, String param2) {
        return new SimpleScalar("");
    }

    public Object doMethod(String method, String param) {
        return new SimpleScalar("");
    }

    public Object doMethod(String method) {
        return new SimpleScalar("");
    }

    public String dateToString(Timestamp timestamp) {
        return timestamp.toLocalDateTime().format(dateFormat);
    }

    public String dateToString(OffsetDateTime timestamp) {
        timestamp.toZonedDateTime().format(dateFormat);
        return timestamp.toZonedDateTime().format(dateFormat);
    }

}
