package com.wippit.cms.backend.data.bean.content;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.types.SectionTypes;
import lombok.Data;

import java.util.Objects;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Section {
    private long id = 0;
    private String iso = "";
    private String secID = "";
    private String name = "";
    private int hit_count = 0;
    private int type = SectionTypes.HOME.getType();

    @JsonIgnore
    public String toString() {
        SectionTypes type = SectionTypes.getType(this.type);
        if (Objects.requireNonNull(type) == SectionTypes.HOME) {
            return name;
        }
        return name + " (" + type.getLabel() + ")";
    }

    public String getFullURL() {
        if (name.equalsIgnoreCase("blog")) {
            return AppUtils.createServerUrl("blog");
        }

        return AppUtils.createServerUrl("content/" + secID);
    }
}
