package com.wippit.cms.backend.data.bean.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wippit.cms.backend.types.ApiEmailTypes;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@Data
public class ContactMessageRequest {
    private String name = "";
    private String message = "";
    private String email = "";
    private String phone = "";
    private String company = "";
    private String templateID = "";
    private ApiEmailTypes type = ApiEmailTypes.CONTACT_REQUEST;

    public String toJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        }
        catch (Exception ex) {
            log.error("Serializing: " + ex.toString());
        }

        return "";
    }

    public static ContactMessageRequest fromJSON(String json) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(json, ContactMessageRequest.class);
        }
        catch (Exception ex) {
            log.error("Cant get object: {}", ex.toString());
            return  null;
        }
    }

    public void makeClean(){
        if (StringUtils.isBlank(message)) {
            message = "";
        }
        if (StringUtils.isBlank(phone)) {
            phone = "";
        }
        if (StringUtils.isBlank(company)) {
            company = "";
        }
        if (StringUtils.isBlank(templateID)) {
            templateID = "";
        }
    }

}
