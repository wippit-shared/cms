package com.wippit.cms.backend.data.handler;


import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import org.postgresql.util.HStoreConverter;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

@MappedTypes(LinkedHashMap.class)
@MappedJdbcTypes(JdbcType.OTHER)
public class HStoreHandler implements TypeHandler<Map<String, String>> {

    public HStoreHandler() {}

    @Override
    public void setParameter(PreparedStatement ps,
                             int i,
                             Map<String, String> map,
                             JdbcType jdbcType) throws SQLException {
//        ps.setObject(i, HStoreConverter.toString(map));
        ps.setString(i, HStoreConverter.toString(map));
    }

    public Map<String, String> getResult(ResultSet rs, String columnName) throws SQLException {
        return readMap(rs.getString(columnName));
    }

    public Map<String, String> getResult(ResultSet rs, int columnIndex) throws SQLException {
        return readMap(rs.getString(columnIndex));
    }

    public Map<String, String> getResult(CallableStatement cs, int columnIndex) throws SQLException {
        return readMap(cs.getString(columnIndex));
    }

    private Map<String, String> readMap(String hstring) throws SQLException {
        if (hstring != null) {
            Map<String, String> map = new LinkedHashMap<String, String>();
            Map<String, String> rawMap = HStoreConverter.fromString(hstring);
            for (Map.Entry<String, String> entry : rawMap.entrySet()) {

                //Here we can convert to another type
                //convert from <String, String> to <String,Integer>
                //convert from <String, String> to <String,Float>
                //convert from <String, String> to <String,Date>
                //convert from <String, String> to <String,etc>

                map.put(entry.getKey(), entry.getValue());
            }

            return map;
        }
        return null;
    }

}
