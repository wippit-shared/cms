package com.wippit.cms.backend.data.dao;

import com.wippit.cms.backend.types.ContentStatusTypes;
import com.wippit.cms.backend.types.HomeTypes;
import com.wippit.cms.backend.types.SectionTypes;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class ArticlesQueryBuilder {

    public static String getArticles(
            @Param("iso") String iso,
            @Param("secID") String section,
            @Param("author") String author,
            @Param("status") ContentStatusTypes status,
            @Param("home") HomeTypes home,
            @Param("offset") int offset,
            @Param("limit") int limit
    ) {
        return new SQL() {
            {
                SELECT("* ");
                FROM("cms.ow_content ");
                WHERE("iso = #{iso}");
                switch (home) {
                    case ALL: break;
                    case IN_HOME:
                        AND();
                        WHERE("home = 1");
                        AND();
                        WHERE("section_type = " + SectionTypes.ARTICLES.getType());
                        break;

                    case NOT_IN_HOME:
                        AND();
                        WHERE("home = 0");
                }
                if (status != ContentStatusTypes.ANY) {
                    AND();
                    WHERE("status = " + status.value());
                }
                if (!StringUtils.isBlank(author)) {

                    WHERE("author = #{author}");
                }
                if (!StringUtils.isBlank(section)) {
                    AND();
                    WHERE("secID = #{secID}");
                }
                ORDER_BY("id desc");
                OFFSET("#{offset}");
                LIMIT("#{limit}");
            }
        }.toString();
    }

    public static String getArticlesCount(
            @Param("iso") String iso,
            @Param("secID") String section,
            @Param("author") String author,
            @Param("status")ContentStatusTypes status,
            @Param("home") HomeTypes home
    ) {

        return new SQL() {
            {
                SELECT("coalesce(count(*), 0) ");
                FROM("cms.ow_content ");
                WHERE("iso = #{iso}");
                switch (home) {
                    case ALL: break;
                    case IN_HOME:
                        AND();
                        WHERE("home = 1");
                        AND();
                        WHERE("section_type = " + SectionTypes.ARTICLES.getType());
                        break;

                    case NOT_IN_HOME:
                        AND();
                        WHERE("home = 0");
                }
                if (status != ContentStatusTypes.ANY) {
                    AND();
                    WHERE("status = " + status.value());
                }
                if (!StringUtils.isBlank(author)) {
                    AND();
                    WHERE("author = #{author}");
                }
                if (!StringUtils.isBlank(section)) {
                    AND();
                    WHERE("secID = #{secID}");
                }
            }
        }.toString();
    }
}
