package com.wippit.cms.backend.data.dao;

import com.wippit.cms.backend.types.StorageTypes;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class StorageQueryBuilder {

    public static String get(
            @Param("flagPrivate") boolean flagPrivate,
            @Param("alias") String alias,
            @Param("type") StorageTypes type,
            @Param("offset") int offset,
            @Param("limit") int limit
    ) {
        return new SQL() {
            {
                SELECT("* ");
                FROM("cms.ow_storage ");
                WHERE("is_private = #{flagPrivate}");
                if (!StringUtils.isBlank(alias)) {
                    AND();
                    WHERE("alias = #{alias}");
                }
                if (type != StorageTypes.ALL) {
                    AND();
                    WHERE("type = " + type.value());
                }
                ORDER_BY("original_name desc");
                OFFSET("#{offset}");
                LIMIT("#{limit}");
            }
        }.toString();
    }

    public static String count(
            @Param("flagPrivate") boolean flagPrivate,
            @Param("alias") String alias,
            @Param("type") StorageTypes type
    ) {

        return new SQL() {
            {
                SELECT("coalesce(count(*), 0) ");
                FROM("cms.ow_storage ");
                WHERE("is_private = #{flagPrivate}");
                if (!StringUtils.isBlank(alias)) {
                    AND();
                    WHERE("alias = #{alias}");
                }
                if (type != StorageTypes.ALL) {
                    AND();
                    WHERE("type = " + type.value());
                }
            }
        }.toString();
    }
}
