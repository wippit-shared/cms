package com.wippit.cms.backend.services.email;

import com.wippit.cms.backend.data.bean.site.CountryCode;
import com.wippit.cms.backend.types.ApiEmailTypes;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Mail {

    private String mailFrom = "";
    private String mailTo = "";
    private String mailCc = "";
    private String mailBcc = "";
    private String mailSubject = "";
    private String mailContent = "";
    private String contentType = "text/plain";
    private Map< String, Object > model = new HashMap<>();
    private ApiEmailTypes type = ApiEmailTypes.UNKNOWN;
    private String emailID = "";
    private String templateID = "";
    private String iso = CountryCode.US.getAlpha2();
    private List<EmailAttachment> attachments = new ArrayList<>();

    public Mail(){
    }

    public Mail(Mail original) {
        this.mailFrom = original.mailFrom;
        this.mailTo = original.mailTo;
        this.mailCc = original.mailCc;
        this.mailBcc = original.mailBcc;
        this.mailSubject = original.mailSubject;
        this.mailContent = original.mailContent;
        this.contentType = original.contentType;
        this.attachments.addAll(original.attachments);
        this.model.putAll(original.model);
        this.type = original.type;
        this.templateID = original.templateID;
        this.emailID = original.emailID;
        this.iso = original.iso;
    }

    public void addToModel(String key, Object item) {
        model.put(key, item);
    }

    public void addAttachment(EmailAttachment attachment) {
        attachments.add(attachment);
    }

}