package com.wippit.cms.backend.services.app;

import com.wippit.cms.AppConstants;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.site.*;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.dao.SystemDao;
import com.wippit.cms.backend.data.properties.AppProperties;
import com.wippit.cms.backend.data.services.KeyDataService;
import com.wippit.cms.backend.data.services.SiteDataService;
import com.wippit.cms.backend.security.AppSecure;
import com.wippit.cms.backend.security.OWSecureKey;
import com.wippit.cms.backend.security.SecurityUtils;
import com.wippit.cms.backend.services.ActivityService;
import com.wippit.cms.backend.services.email.EmailService;
import com.wippit.cms.backend.types.LogTypes;
import com.wippit.cms.backend.types.RoleTypes;
import com.wippit.cms.backend.types.SectionTypes;
import com.wippit.cms.backend.types.StatusTypes;
import com.wippit.cms.backend.utils.ResourceFile;
import com.wippit.cms.backend.utils.cache.FileCacheItem;
import com.wippit.cms.frontend.open.controllers.CMSWriter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.buf.HexUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.ApplicationContext;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class AppServiceSetup {

    private final ApplicationContext applicationContext;
    private boolean _configureSiteRequired = false;
    private final String profile;
    private String template;
    private final KeyDataService keyDataService;

    public AppServiceSetup(ApplicationContext applicationContext, String profile) {
        this.applicationContext = applicationContext;
        this.profile = profile;

        SystemDao systemDao = applicationContext.getBean(SystemDao.class);
        keyDataService = new KeyDataService(systemDao);
    }

    public void doSetup() throws Exception {
        log.info("Server setup");

        SiteDataService siteDataService = applicationContext.getBean(SiteDataService.class);
        if (!createSecureKeys(siteDataService)) {
            serverCriticalError("Server can't be secured", -12060);
            return;
        }

        SiteDataService siteService = applicationContext.getBean(SiteDataService.class);
        setupServer(siteService);

        if (_configureSiteRequired) {
            ConfigureNewSite newSite = new ConfigureNewSite(siteDataService);
            newSite.start();
        }

        setupSite(siteDataService);

        log.info("Server setup completed");
    }

    private void setupServer(@NotNull SiteDataService siteService) throws IOException {

        ApplicationArguments applicationArguments = applicationContext.getBean(ApplicationArguments.class);
        AppProperties appProperties = applicationContext.getBean(AppProperties.class);

        this.template = getTemplate(applicationArguments, appProperties, siteService);
        log.info("Template: {}", template);

        SystemDao systemDao = applicationContext.getBean(SystemDao.class);
        KeyDataService keyDataService = new KeyDataService(systemDao);

        //---
        //Email service
        //---
        EmailSettings emailSettings = siteService.getEmailSettings();
        if (!StringUtils.isBlank(emailSettings.getHost())) {
            EmailService.getCurrentInstance().setup(emailSettings);
        }

        SecurityUtils securityUtils = applicationContext.getBean(SecurityUtils.class);

        //---
        //Crypto Key
        //---
        String cryptoKeyHex = keyDataService.getKey("crypto.key");
        String cryptoKey = "";
        if (!StringUtils.isBlank(cryptoKeyHex)) {
            cryptoKey = new String(HexUtils.fromHexString(cryptoKeyHex), StandardCharsets.UTF_8);
        }
        if (StringUtils.isBlank(cryptoKey)) {
            cryptoKey = securityUtils.createLongSecretCode();
            cryptoKeyHex = HexUtils.toHexString(cryptoKey.getBytes(StandardCharsets.UTF_8));
            keyDataService.setKey("crypto.key", cryptoKeyHex);
        }
        AppGlobal.getInstance().setCryptoKey(cryptoKey);

        //---
        //Admin account
        //---
        Account currentAdminAccount = siteService.getAccount("admin");
        boolean flagAdminSetupRequired = (keyDataService.getKeyAsInt("setup.admin", 0) == 1);

        if (currentAdminAccount != null && flagAdminSetupRequired) {
            String accessCode = "cmsadmin";
            String encodedPassword = securityUtils.encryptPassword(accessCode);
            currentAdminAccount.setSecret(encodedPassword);

            siteService.save(currentAdminAccount);
            keyDataService.setKey("setup.admin", 0);
            ActivityService.addLog(AppConstants.SYSTEM_USER,"Admin password updated", LogTypes.SYSTEM);
        }

        if (currentAdminAccount != null) {

            String path = getPath(applicationArguments, appProperties);
            String url = getUrl(applicationArguments, appProperties);
            AppGlobal.setup(path, url, siteService);
            AppGlobal.saveSettings(siteService);

            String message = "Server (" + url + ") started with working path: " + AppGlobal.getInstance().getPath();
            log.info(message);
            ActivityService.addLog(AppConstants.SYSTEM_USER,message, LogTypes.SYSTEM);
            return;
        }

        ActivityService.addLog(AppConstants.SYSTEM_USER, "Configuring server", LogTypes.SYSTEM);

        log.info("Admin account not found. Setup required.");

        String accessCode = "cmsadmin";
        String encodedPassword = securityUtils.encryptPassword(accessCode);

//        String deviceAccessCode = securityUtils.createSecretCode();
//        String deviceEncodedPassword = securityUtils.encryptPassword(deviceAccessCode);

        Account newAdminAccount = new Account();
        newAdminAccount.setAlias("admin");
        newAdminAccount.setStatus(StatusTypes.ACTIVE.value());
        newAdminAccount.setFull_name("Admin user");
        newAdminAccount.setAvatar_url("");
        newAdminAccount.setBanner_url("");
        newAdminAccount.setEmail("");
        newAdminAccount.setAbout("CMS Administrator");
        newAdminAccount.setColor("#123456");
        newAdminAccount.getRoles().add(RoleTypes.ADMIN);
        newAdminAccount.setSecret(encodedPassword);

        siteService.save(newAdminAccount);

//        Device device = new Device();
//        device.setDeviceID(AppUtils.createDeviceID());
//        device.setUsername(newAdminAccount.getAlias());
//        systemDataService.addDevice(device, deviceEncodedPassword);

        String path = getPath(applicationArguments, appProperties);
        String url = getUrl(applicationArguments, appProperties);
        AppGlobal.setup(path, url, siteService);
        AppGlobal.saveSettings(siteService);

        keyDataService.setKey("setup.admin", 0);

//        String sb = "account: admin" +
//                "\n" +
//                "Secret: " + accessCode +
////                "\n" +
////                "device ID: " + device.getDeviceID() +
////                "\n" +
////                "device secret: " + deviceAccessCode +
//                "\n";
//
//        log.info("-------------- Admin account created ---------------------");
//        log.info("\n" + sb);
//        log.info("----------------------------------------------------------");
//        log.info("Working path: {}", AppGlobal.getInstance().getPath());
//
//        String filePath = AppGlobal.getInstance().getPath() + "/device.txt";
//        FileUtils.writeByteArrayToFile(new File(filePath), sb.getBytes(StandardCharsets.UTF_8));
//        log.info("Device details in file: {}", filePath);

        ActivityService.addLog(AppConstants.SYSTEM_USER, "Server started first time on path: " + AppGlobal.getInstance().getPath(), LogTypes.SYSTEM);
        _configureSiteRequired = true;
    }

    private String getPath(ApplicationArguments applicationArguments, AppProperties appProperties) {
        String path = "";
        if (applicationArguments.containsOption("app.path")) {
            List<String> pathList = applicationArguments.getOptionValues("app.path");
            path = pathList.get(0).trim();
            if (!StringUtils.isBlank(path)) {
                return path;
            }
        }

        if (appProperties.getDebug().isEnabled()) {
            path = appProperties.getDebug().getPath().trim();
            if (!StringUtils.isBlank(path)) {
                return path;
            }
        }

        path = appProperties.getPath().trim();
        return path;
    }

    private String getTemplate(ApplicationArguments applicationArguments, AppProperties appProperties, SiteDataService siteDataService) {
        String value = "";

        //---
        //Get template from command line
        //---
        if (applicationArguments.containsOption("app.template")) {
            List<String> pathList = applicationArguments.getOptionValues("app.template");
            value = pathList.get(0);
            if (!StringUtils.isBlank(value)) {
                return value;
            }
        }

        //---
        //Get template from debug app.properties
        //---
        if (appProperties.getDebug().isEnabled()) {
            value = appProperties.getDebug().getTemplate();
            if (!StringUtils.isBlank(value)){
                return value;
            }
        }

        //---
        //Get template from app.properties
        //---
        value = appProperties.getTemplate();
        if (!StringUtils.isBlank(value)) {
            return value;
        }

        //---
        //Get template from database or default
        //---
        value = keyDataService.getKey("site.template", AppConstants.TEMPLATE_DEFAULT);
        return value;
    }

    private String getUrl(ApplicationArguments applicationArguments, AppProperties appProperties) {

        String url = appProperties.getUrl();

        log.debug("URL from properties: {}", url);

        if (applicationArguments.containsOption("app.url")) {
            List<String> urlList = applicationArguments.getOptionValues("app.url");
            url = urlList.get(0);
            log.info("URL from arguments: {}", url);
            return url;
        }

        if (appProperties.getDebug().isEnabled()) {
            if (!StringUtils.isBlank(appProperties.getDebug().getUrl())) {
                url = appProperties.getDebug().getUrl();
                log.info("URL debug: {}", url);
            }
        }

        if (!StringUtils.isBlank(url)) {
            return addPortsToUrl(url.trim());
        }

        log.info("URL is blank");
        return addPortsToUrl("");
    }

    private String addPortsToUrl(String url) {

        log.debug("Configuring url: {}", url);

        if (profile.equalsIgnoreCase("production")) {
            if (StringUtils.isBlank(url)) {
                serverCriticalError("Url parameter required in production mode", -748);
            }
        }
        else if (profile.equalsIgnoreCase("beta")) {
            if (StringUtils.isBlank(url)) {
                url = "https://localhost";
            }
            url = url + ":8443";
        }
        else {
            if (StringUtils.isBlank(url)) {
                url = "https://localhost";
            }
            url = url + ":8443";
        }

        log.debug("URL configured: {}", url);
        return url;
    }

    private boolean createSecureKeys(SiteDataService siteService) {

        if (!readServerKeys(siteService)) {
            if (!createServerKeys(siteService)) {
                serverCriticalError("Cant create server keys", -12048);
                return false;
            }
        }

        if (!serverKeyTest()) {
            serverCriticalError("Server keys test failed", -12050);
            return false;
        }

        return true;
    }

    private boolean createServerKeys(SiteDataService siteService)  {
        try {
            log.debug("Creating RSA Keys");
            AppSecure coreSecure = new AppSecure();
            KeyPair pair = coreSecure.generateRSAKeys();
            PrivateKey privateKey = pair.getPrivate();
            PublicKey publicKey = pair.getPublic();

            String hexPublic = Hex.encodeHexString(publicKey.getEncoded());
            String hexPrivate = Hex.encodeHexString(privateKey.getEncoded());

            String encodedPublic = OWSecureKey.encode(hexPublic);
            String encodedPrivate = OWSecureKey.encode(hexPrivate);

            AppSecure.setServerKeys(publicKey, encodedPublic, privateKey);

            ServerKeys serverKeys = new ServerKeys();
            serverKeys.setPrivateKey(encodedPrivate);
            serverKeys.setPublicKey(encodedPublic);
            serverKeys.setClientPublicKey(encodedPublic);

            siteService.save(serverKeys);
        }
        catch (Exception ex) {
            log.error("On create server Keys: {}", ex.toString());
            return false;
        }

        log.info("Server keys created");
        return true;
    }

    private boolean readServerKeys(SiteDataService siteService)  {
        try {
            log.debug("Reading RSA Keys");

            ServerKeys serverKeys = siteService.getServerKeys();
            if (serverKeys == null || serverKeys.isEmpty()) {
                log.warn("No server keys in db");
                return false;
            }

            String decodedPublic = OWSecureKey.decode(serverKeys.getPublicKey());
            String decodedPrivate = OWSecureKey.decode(serverKeys.getPrivateKey());
            byte[] publicKeyBytes = Hex.decodeHex(decodedPublic);
            byte[] privateKeyBytes = Hex.decodeHex(decodedPrivate);

            KeyFactory keyFactory = KeyFactory.getInstance("RSA");

            EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
            PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
            PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

            String hexPublic = Hex.encodeHexString(publicKey.getEncoded());
            AppSecure.setServerKeys(publicKey, hexPublic, privateKey);
        }
        catch (Exception ex) {
            serverCriticalError("Cant load server keys", 12049);
            return false;
        }

        log.info("Server keys loaded");
        return true;
    }

    private void serverCriticalError(String error, int errorCode) {
        ActivityService.addLog(AppConstants.SYSTEM_USER, String.valueOf(errorCode) + "|" + error, LogTypes.CRITICAL_ERROR);
        log.error("*********************************************************\n{}: {}\n*********************************************************", error, errorCode);
        System.exit(errorCode);
    }

    private boolean serverKeyTest() {
        AppSecure coreSecure = new AppSecure();

        final String messageTest = RandomStringUtils.random(1256);

        try {
            String encryptedMessage = coreSecure.encryptWithServerKey(messageTest);
            String decryptedMessage = coreSecure.decryptWithServerKey(encryptedMessage);

            if (decryptedMessage.contentEquals(messageTest)) {
                log.debug("Server key test Ok");
                return true;
            }

            log.error("Server key test failed");
        }
        catch (Exception ex) {
            log.error("Server key test failed: {}", ex.toString());
        }

        return false;
    }

    private boolean templateIsValid(String template) {
        String templateRoot = AppGlobal.getInstance().getTemplatePath() +
                AppConstants.PATH_SEPARATOR +
                template;

        log.debug("Testing directory: {}", templateRoot);

        return FileUtils.isDirectory(new File(templateRoot));
    }

    private String getNewTemplateName(String templateSuggestedName) {

        String dirPath = AppGlobal.getInstance().getTemplatePath() + AppConstants.PATH_SEPARATOR + templateSuggestedName;
        String templateName = templateSuggestedName;
        int index = 1;
        while (Files.exists(Path.of(dirPath))) {
            templateName = templateSuggestedName + String.valueOf(index);
            dirPath = AppGlobal.getInstance().getTemplatePath() + AppConstants.PATH_SEPARATOR + templateName;
            index++;
        }

        return templateName;
    }

    private class ConfigureNewSite extends Thread {

        private final SiteDataService siteDataService;
        public ConfigureNewSite(SiteDataService siteDataService) {
            this.siteDataService = siteDataService;
        }

        public void run() {

            log.info("Configuring new site");
            try {
                Thread.sleep(500);
                addDefaultTemplate();

                Thread.sleep(500);
                setupSite(siteDataService);
                _configureSiteRequired = false;
            }
            catch (Exception ex) {
                log.error("Configuring new site: {}", ex.toString());
            }
        }

        private void addDefaultTemplate() {
            log.debug("Adding default template");

            Path defaultPath = Paths.get(AppGlobal.getInstance().getTemplatePath() + "/default");
            File defaultTemplateDir = defaultPath.toFile();
            if (defaultTemplateDir.exists()) {
                log.debug("Default template already exists");
                return;
            }

            //---
            //Create default template directory & index file
            //---
            try {

                Files.createDirectories(defaultPath);

                ResourceFile indexFile = new ResourceFile("index.html");
                Path indexPath = Paths.get(AppGlobal.getInstance().getTemplatePath() + "/default/index.html");
                FileUtils.writeByteArrayToFile(indexPath.toFile(), indexFile.getContentAsBytes());

            } catch (Exception ex) {
                log.error("Error creating default template: {}", ex.toString());
            }

            String templatePath = AppGlobal.getInstance().getTemplatePath() + AppConstants.PATH_SEPARATOR;
            AppTemplateSetup appTemplateSetup = new AppTemplateSetup("default", templatePath);
            appTemplateSetup.doScan();
        }

    }

    private void setupSite(SiteDataService siteService) {

        log.debug("Setup site");

        boolean flagAddSections = false;

        SiteInfo siteInfo = AppGlobal.getInstance().getSiteInfo();

        log.debug("Setup site - template: {} / {}", template, siteInfo.getTemplate());
        if (StringUtils.isBlank(siteInfo.getTemplate())) {
            siteInfo.setTemplate(this.template);
        }
        else if (!siteInfo.getTemplate().equalsIgnoreCase(this.template) && !this.template.equalsIgnoreCase("default")) {
            siteInfo.setTemplate(this.template);
        }
        log.info("Setup site - template: {} / {}", template, siteInfo.getTemplate());

        if (!templateIsValid(siteInfo.getTemplate())) {
            log.debug("Changing template to default");
            SiteTemplate siteTemplate = new SiteTemplate();
            siteTemplate.setName(AppConstants.TEMPLATE_DEFAULT);
            siteTemplate.setPage_root(AppConstants.TEMPLATE_ROOT + AppConstants.TEMPLATE_DOC1);
            siteInfo.setSiteTemplate(siteTemplate);
            siteInfo.setTemplate(siteTemplate.getName());
        }

        log.info("Setup site - template: {} ", siteInfo.getTemplate());
        SiteTemplate configuredSiteTemplate = siteService.getSiteTemplate(siteInfo.getTemplate());
        if (configuredSiteTemplate == null) {
            log.info("Setup site - configuring template: {} ", siteInfo.getTemplate());
            String templatePath = AppGlobal.getInstance().getTemplatePath() + AppConstants.PATH_SEPARATOR;
            AppTemplateSetup appTemplateSetup = new AppTemplateSetup(siteInfo.getTemplate(), templatePath);
            appTemplateSetup.doScan();
            appTemplateSetup.display();

            CMSWriter.getCurrent().setPageIndex(appTemplateSetup.getPageIndex());
            CMSWriter.getCurrent().setPageAuthor(appTemplateSetup.getPageAuthor());
            CMSWriter.getCurrent().setPageSearch(appTemplateSetup.getPageSearch());
            CMSWriter.getCurrent().setPageUser(appTemplateSetup.getPageUser());
            CMSWriter.getCurrent().setPageBlog(appTemplateSetup.getPageBlog());
            CMSWriter.getCurrent().setPageBlogPage(appTemplateSetup.getPageBlogPage());
            CMSWriter.getCurrent().setPageError400(appTemplateSetup.getPageError400());
            CMSWriter.getCurrent().setPageError500(appTemplateSetup.getPageError500());
            CMSWriter.getCurrent().setTemplate(siteInfo.getTemplate());

            configuredSiteTemplate = new SiteTemplate();
            configuredSiteTemplate.setName(siteInfo.getTemplate());
            FileCacheItem rootItem = CMSWriter.getCurrent().getFileTreeCache().getIndexItem();
            if (rootItem != null) {
                configuredSiteTemplate.setPage_root(rootItem.getName());
                configuredSiteTemplate.setPage_blog(appTemplateSetup.getPageBlog());
                configuredSiteTemplate.setPage_item(appTemplateSetup.getPageBlogPage());
                configuredSiteTemplate.setPage_user(appTemplateSetup.getPageUser());
                configuredSiteTemplate.setPage_search(appTemplateSetup.getPageSearch());
                configuredSiteTemplate.setPage_author(appTemplateSetup.getPageAuthor());
                configuredSiteTemplate.setPage_404(appTemplateSetup.getPageError400());
                configuredSiteTemplate.setPage_500(appTemplateSetup.getPageError500());
                flagAddSections = true;
            }

            siteInfo.setSiteTemplate(configuredSiteTemplate);
        }
        else {
            siteInfo.setSiteTemplate(configuredSiteTemplate);
            CMSWriter.getCurrent().setTemplate(siteInfo.getTemplate());

            if (CMSWriter.getCurrent().getFileTreeCache().setIndex(configuredSiteTemplate.getPage_root())) {
                FileCacheItem rootItem = CMSWriter.getCurrent().getFileTreeCache().getIndexItem();
                if (rootItem != null) {
                    siteInfo.getSiteTemplate().setPage_root(rootItem.getName());
                }
            }
        }

        LocalCountry localCountry = siteService.getCountry(siteInfo.getCountry_iso());
        if (localCountry != null) {
            AppUtils.updateDateFormatter(localCountry.getDate_format());
            log.info("Country: {} - {} - {}", localCountry.getIso(), localCountry.getName(), localCountry.getDate_format());
        }

        FileCacheItem pageRoot = CMSWriter.getCurrent().getFileTreeCache().getIndexItem();
        if (pageRoot != null) {
            ActivityService.addLog(AppConstants.SYSTEM_USER, "Template: " + siteInfo.getTemplate() + ", Root: " + pageRoot.getName(), LogTypes.SYSTEM);
        }
        else {
            ActivityService.addLog(AppConstants.SYSTEM_USER, "Template: " + siteInfo.getTemplate() + ", Root not found", LogTypes.SYSTEM);
        }

        log.info("AppGlobal siteInfo: {}", AppGlobal.getInstance().getSiteInfo());
        siteService.save(siteInfo);
        siteService.save(siteInfo.getSiteTemplate());

        if (flagAddSections) {
            for (LocalCountry country : AppUtils.getCountries()) {
                siteService.saveTemplateSection(siteInfo.getTemplate(), "blog", SectionTypes.HOME, country.getIso(), configuredSiteTemplate.getPage_blog(), configuredSiteTemplate.getPage_item());
            }
        }
    }

}
