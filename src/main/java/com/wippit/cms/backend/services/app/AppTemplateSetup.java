package com.wippit.cms.backend.services.app;

import com.wippit.cms.backend.utils.cache.FileCacheItem;
import com.wippit.cms.backend.utils.cache.FileTreeCache;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class AppTemplateSetup {
    private final String name;
//    private final ApplicationContext applicationContext;
    private final FileTreeCache fileTreeCache;

    @Getter
    private String pageIndex;
    @Getter
    private String pageAuthor;
    @Getter
    private String pageSearch;
    @Getter
    private String pageUser;
    @Getter
    private String pageError400;
    @Getter
    private String pageError500;
    @Getter
    private String pageBlogPage;
    @Getter
    private String pageBlog;

//    public AppTemplateSetup(ApplicationContext applicationContext) {
//        name = AppConstants.TEMPLATE_DEFAULT;
//        this.applicationContext = applicationContext;
//        String templateRoot = AppGlobal.getInstance().getTemplatePath() +
//                AppConstants.PATH_SEPARATOR +
//                name;
//
//        fileTreeCache =  new FileTreeCache(templateRoot);
//    }

    public AppTemplateSetup(String name, String templatePath) {
        this.name = name;
        //this.applicationContext = applicationContext;
        String templateRoot = templatePath + name;

        fileTreeCache =  new FileTreeCache(templateRoot);
        fileTreeCache.reload();
    }

    public void doScan() {

        //---
        //Index
        //---
        pageIndex = getPage("index");
        pageAuthor = getPageOrSimilar("author");
        pageSearch = getPageOrSimilar("search");
        pageUser = getPageOrSimilar("user");
        pageError400 = getPageOrSimilar("400");
        pageError500 = getPageOrSimilar("500");

        pageBlog = getPageOrSimilar("blog");
        if (StringUtils.isBlank(pageBlog)) {
            pageBlog = getSimilarPageVariances("blog-home");
        }
        if (StringUtils.isBlank(pageBlog)) {
            pageBlog = getSimilarPageVariances("blog-index");
        }

        pageBlogPage = getSimilarPageVariances("blog*item");
        if (StringUtils.isBlank(pageBlogPage)) {
            pageBlogPage = getSimilarPageVariances("blog*page");
        }
        if (StringUtils.isBlank(pageBlogPage)) {
            pageBlogPage = getSimilarPageVariances("blog*detail");
        }
        if (StringUtils.isBlank(pageBlogPage)) {
            pageBlogPage = getSimilarPageVariances("blog*post");
        }
    }

    private String getPageOrSimilar(String name) {
        String pageName = getPage(name);
        if (!StringUtils.isBlank(pageName)) {
            return pageName;
        }
        pageName = getSimilarPage(name);
        if (!StringUtils.isBlank(pageName)) {
            return pageName;
        }

        return "";
    }

    private String getPage(String name) {
        FileCacheItem item = fileTreeCache.getItem("/" + name + ".html");
        if (item != null) {
            return item.getName();
        }
        item = fileTreeCache.getItem("/" +"_" + name + ".html");
        if (item != null) {
            return item.getName();
        }
        item = fileTreeCache.getItem("/" +"_" + name + ".ftl");
        if (item != null) {
            return item.getName();
        }
        item = fileTreeCache.getItem("/" +"." + name + ".html");
        if (item != null) {
            return item.getName();
        }
        item = fileTreeCache.getItem("/" +"." + name + ".ftl");
        if (item != null) {
            return item.getName();
        }

        return "";
    }

    private String getSimilarPage(String name) {
        FileCacheItem item = fileTreeCache.similarItem(name ,".html");
        if (item != null) {
            return item.getName();
        }
        item = fileTreeCache.similarItem(name ,".ftl");
        if (item != null) {
            return item.getName();
        }
        item = fileTreeCache.similarItem( "_" + name, ".html");
        if (item != null) {
            return item.getName();
        }
        item = fileTreeCache.similarItem("_" + name, ".ftl");
        if (item != null) {
            return item.getName();
        }
        item = fileTreeCache.similarItem("." + name, ".html");
        if (item != null) {
            return item.getName();
        }
        item = fileTreeCache.similarItem("." + name, ".ftl");
        if (item != null) {
            return item.getName();
        }

        return "";
    }

    private String getSimilarPageVariances(String name) {
        String foundPage = getPageOrSimilar(name.replace("*", "-"));
        if (!StringUtils.isBlank(foundPage)) {
            return foundPage;
        }
        foundPage = getPageOrSimilar(name.replace("*", "_"));
        if (!StringUtils.isBlank(foundPage)) {
            return foundPage;
        }
        foundPage = getPageOrSimilar(name.replace("*", "."));
        if (!StringUtils.isBlank(foundPage)) {
            return foundPage;
        }
        return "";
    }

//    private void saveTemplateSetup() {
//        SystemDao dao = applicationContext.getBean(SystemDao.class);
//        dao.updateTemplatePages(name, pageIndex, pageError400, pageError500, pageAuthor, pageSearch, pageUser);
//        dao.updateTemplateSection(name, AppGlobal.getInstance().getSiteInfo().getCountry_iso(),"blog", pageBlog, pageBlogPage);
//
//    }

    public void display() {
        StringBuffer sb = new StringBuffer();
        sb.append("\n------------------------------------------------");
        sb.append("\nTemplate: \t" + name);
        sb.append("\n\tIndex: \t" + pageIndex);
        sb.append("\n\tAuthor: \t" + pageAuthor);
        sb.append("\n\tSearch: \t" + pageSearch);
        sb.append("\n\tUser: \t" + pageUser);
        sb.append("\n\t400: \t" + pageError400);
        sb.append("\n\t500: \t" + pageError500);
        sb.append("\n\tBlog: \t" + pageBlog);
        sb.append("\n\tPage: \t" + pageBlogPage);
        sb.append("\n------------------------------------------------");

        log.debug(sb.toString());
    }
}
