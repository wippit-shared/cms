package com.wippit.cms.backend.services.email;

import lombok.Data;

@Data
public class EmailAttachment {
    private String fid = "";
    private String cid = "";
}
