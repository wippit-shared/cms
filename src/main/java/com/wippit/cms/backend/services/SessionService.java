package com.wippit.cms.backend.services;

import com.wippit.cms.AppConstants;
import com.wippit.cms.backend.data.bean.api.SessionItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SessionService {

    @Autowired
    private ApplicationContext applicationContext;


    public SessionItem getSessionItem(String key) {
        log.trace("Searching session item with key: " + key);
        CacheManager cacheManager = applicationContext.getBean(CacheManager.class);
        Cache cache = cacheManager.getCache(AppConstants.CACHE_SESSIONS);
        return cache.get(key, SessionItem.class);
    }

    public SessionItem getSessionItem(String sessionID, String token) {
        String key = SessionItem.buildKey(sessionID, token);
        return getSessionItem(key);
    }

    public void updateSession(SessionItem sessionItem) {
        log.trace("Updating session item with key: " + sessionItem.getKey());
        CacheManager cacheManager = applicationContext.getBean(CacheManager.class);
        Cache cache = cacheManager.getCache(AppConstants.CACHE_SESSIONS);
        cache.putIfAbsent(sessionItem.getKey(), sessionItem);
    }



}
