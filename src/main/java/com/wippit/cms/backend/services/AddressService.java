package com.wippit.cms.backend.services;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;
import com.wippit.cms.AppConstants;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.backend.data.bean.geo.CountryLocation;
import com.wippit.cms.backend.data.dao.SystemDao;
import com.wippit.cms.backend.data.services.KeyDataService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.Cache;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;

import java.io.File;
import java.net.InetAddress;

@Slf4j
public class AddressService {

    private DatabaseReader dbReader;
    private final ConcurrentMapCacheManager cacheManager;
    private boolean flagSetupRequired;
    private SystemDao dao = null;
    private boolean status = false;

    private static AddressService service = new AddressService();

    private AddressService(){
        dbReader = null;
        flagSetupRequired = true;
        cacheManager = new ConcurrentMapCacheManager(AppConstants.CACHE_IP_LOCATION);
    }

    public static AddressService currentInstance() {
        return service;
    }

    public static void setDao(SystemDao systemDao) {
        currentInstance().dao = systemDao;
    }

    public static void reload() {

        if (currentInstance().dao == null) {
            log.warn("DAO setup required");
            service.status = false;
            return;
        }

        KeyDataService keyDataService = new KeyDataService(currentInstance().dao);

        try {

            String dbPath = AppGlobal.getInstance().getPath() + "/geo/" + keyDataService.getKey("geo.db");
            currentInstance().dbReader = new DatabaseReader.Builder(new File(dbPath)).build();
            currentInstance().flagSetupRequired = false;

            StringBuilder sb = new StringBuilder();
            sb.append("\n").append("------------------------------------------------------------------------");
            sb.append("\n").append("Geo db path: ").append(dbPath);
            sb.append("\n").append("------------------------------------------------------------------------");

            log.debug(sb.toString());
            keyDataService.setKey("geo.status","Ok" );
            service.status = true;
        }
        catch (Exception ex) {
            log.error("Can't open GEO database: {}", ex.toString());
            keyDataService.setKey("geo.status", ex.toString());
            service.status = false;
        }
    }

    public static boolean status() {
        return service.status;
    }

    public static CountryLocation getCountryFromAddress(String address) {

        if (StringUtils.isBlank(address)
                || address.equalsIgnoreCase("127.0.0.1")
                || address.startsWith("10.")
//                || address.startsWith("172.")     //Requires 20 bits to validate
                || address.startsWith("192.168.")
        ) {
            return new CountryLocation();
        }

        Cache cache = currentInstance().cacheManager.getCache(AppConstants.CACHE_IP_LOCATION);
        if (cache != null) {
            CountryLocation cachedLocation = cache.get(address, CountryLocation.class);
            if (cachedLocation != null && cachedLocation.getAddress().equalsIgnoreCase(address)) {
                //log.debug("Country from cached [{}] address: {}/{}", cachedLocation.getAddress(), cachedLocation.getCountry(), cachedLocation.getCity());
                return cachedLocation;
            }
        }
        else {
            log.warn("Cache location is null");
        }

        CountryLocation location = new CountryLocation();
        if (service.flagSetupRequired){
            log.warn("Setup is required for AddressService");
            return location;
        }

        try {
            log.debug("Searching Country from address: {}", address);
            InetAddress ipAddress = InetAddress.getByName(address);
            CityResponse response;
            synchronized (service) {
                response = currentInstance().dbReader.city(ipAddress);
            }
            Country country = response.getCountry();

            location.setCountry(country.getIsoCode());
            location.setCity(response.getCity().getName());
            location.setAddress(address);

            log.debug("Country from request[{}]: {}/{}", location.getAddress(), location.getCountry(), location.getCity());
            if (cache != null) {
                cache.putIfAbsent(location.getAddress(), location);
            }
            else {
                log.warn("Cache put location is null");
            }
            return location;
        }
        catch (Exception ex) {
            log.warn("Cant query location database: {}", ex.toString());
        }

        return location;
    }

}
