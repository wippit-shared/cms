package com.wippit.cms.backend.services.storage;

import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.types.StorageTypes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Slf4j
@Service("storage")
public class StorageService extends StorageManager {

    private static StorageService _storageService;
    private static int timeout = 10;

    public StorageService(){
        super();
        _storageService = this;
        log.info("Storage setup");
    }

    @Async
    private Future<Storage> _upload(String alias, byte[] fileData, String fileMime, String fileName, StorageTypes type, boolean flag_private, String properties) throws Exception {

        try {
            Storage storage = addStorage(alias, fileData.length, fileMime, fileName, type, flag_private, properties);
            if (storage != null) {
                _storageService.uploadStorage(storage, fileData);
                return new AsyncResult<Storage>(storage);
            }
        } catch (Exception ex) {
            log.error("Uploading storage: {}", ex.toString());
        }

        return new AsyncResult<Storage>(null);
    }

    /**
     * Upload a file in async mode
     * @param alias   account
     * @param fileData  file bytes[]
     * @param fileMime file mime
     * @param fileName file name
     * @param type  type: AVATAR, TASK, IMAGE THUMBNAIL, WIP, RESOURCE IMAGE, RESOURCE DOCUMENT
     * @param flag_private true for private access, false for public access
     * @return Future Storage if uploaded, null otherwise
     * @throws Exception When something was wrong
     */
    @Async
    public Future<Storage> upload(String alias, byte[] fileData, String fileMime, String fileName, StorageTypes type, boolean flag_private, String properties) throws Exception {
        return _storageService._upload(alias, fileData, fileMime, fileName, type, flag_private, properties);
    }

    private Storage _uploadNow(String alias, byte[] fileData, String fileMime, String fileName, StorageTypes type, boolean flag_private, String properties) throws Exception {

        try {
            Storage storage = addStorage(alias, fileData.length, fileMime, fileName, type, flag_private, properties);
            if (storage != null) {
                _storageService.uploadStorageNow(storage, fileData);
                return storage;
            }
        } catch (Exception ex) {
            log.error("Uploading storage: {}", ex.toString());
        }

        return null;
    }

    /**
     * Upload a file now
     * @param alias   account AID
     * @param fileData  file bytes[]
     * @param fileMime file mime
     * @param fileName file name
     * @param type  type: AVATAR, TASK, IMAGE THUMBNAIL, WIP, RESOURCE IMAGE, RESOURCE DOCUMENT
     * @param flag_private true for private access, false for public access
     * @return Storage if uploaded, null if not
     * @throws Exception When something was wrong
     */
    public Storage uploadNow(String alias, byte[] fileData, String fileMime, String fileName, StorageTypes type, boolean flag_private, String properties) throws Exception {
        synchronized (_storageService) {
            return _storageService._uploadNow(alias, fileData, fileMime, fileName, type, flag_private, properties);
        }
    }

}