package com.wippit.cms.backend.services.email;

import lombok.Data;

@Data
public class EmailPoolItem {
    private Mail email = null;
    private long userID = 0;

    public EmailPoolItem(){
    }

    public EmailPoolItem(Mail email, long userID) {
        this.email = new Mail(email);
        this.userID = userID;
    }
}
