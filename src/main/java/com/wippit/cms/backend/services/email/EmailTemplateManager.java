package com.wippit.cms.backend.services.email;

import com.wippit.cms.backend.data.services.EmailTemplateDataService;
import freemarker.cache.StringTemplateLoader;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
public class EmailTemplateManager {

    private final AtomicBoolean flagReload = new AtomicBoolean(true);
    public static final EmailTemplateManager current = new EmailTemplateManager();

    private EmailTemplateManager() {
    }

    public StringTemplateLoader getTemplates(EmailTemplateDataService dataService){

        synchronized (current) {

            int total = 0;
            log.debug("Loading templates");

            StringTemplateLoader templateLoader = new StringTemplateLoader();

            List<EmailTemplate> templateList = dataService.getAll();
            for (EmailTemplate emailTemplate : templateList) {

                log.debug("Loading template: {}", emailTemplate.getEtid());
                templateLoader.putTemplate(emailTemplate.getEtid()+ "-html", emailTemplate.getHtml());
                templateLoader.putTemplate(emailTemplate.getEtid()+ "-plain", emailTemplate.getPlain());

                total++;
            }

            flagReload.set(true);
            log.info("Loaded {} email templates", total);
            return templateLoader;
        }
    }

    public void clearFlag() {
        synchronized (current) {
            flagReload.set(false);
        }
    }

    public void setFlag() {
        synchronized (current) {
            flagReload.set(true);
        }
    }

    public boolean getFlag() {
        return flagReload.get();
    }

}
