package com.wippit.cms.backend.services.email;

import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.types.ApiEmailTypes;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class EmailTemplate {
    private long id = 0;
    private String etid = "";
    private String name = "";
    private int type = ApiEmailTypes.UNKNOWN.getType();
    private String page = "";
    private String html = "";
    private String plain = "";
    private String comments = "";
    private String data = "";
    private Timestamp created_date;

    public EmailTemplate() {
        etid = AppUtils.createID("ET");
    }

    public String toString() {
        return name;
    }
}
