package com.wippit.cms.backend.services.storage;

import com.wippit.cms.AppConstants;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.storage.Storage;
import com.wippit.cms.backend.data.services.ResourceDataService;
import com.wippit.cms.backend.types.StorageTypes;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@Service
public class StorageManager {

    @Autowired
    private ResourceDataService dataService;

    private final int timeout = 30;

    private String[] paths = {
            "aad",
            "anc",
            "def",
            "zyx",
            "pod",
            "uye",
            "cui",
            "znn",
            "nat",
            "snb",
            "pps",
            "uko",
            "spl",
            "iuy",
            "xnm",
            "moq",
            "zcx",
            "diu",
            "ddi",
            "xmn",
            "xxv"
    };

    protected Storage addStorage(String alias, long size, String fileMime, String fileName, StorageTypes type, boolean flag_private, String properties) {

        long nextID = dataService.nextID();
        Storage storage = createStorageRecord(alias, fileName, fileMime, size, type, flag_private);
        storage.setId(nextID);
        storage.setProperties(properties);
        dataService.add(storage);
        return storage;

    }

    protected void uploadStorage(Storage storage, byte[] data) {
        Uploader uploader = new Uploader(storage, data);
        uploader.start();
    }

    protected Storage uploadStorageNow(Storage storage, byte[] data) {
        try {
            String fullPath = storage.getCompletePath();

            Path path = Paths.get(storage.getCompletePath());
            String filename = path.getFileName().toFile().getName();
            String dirs = fullPath.replace(filename, "");
            log.debug("Dirs: {}", dirs);
            log.debug("File: {}", filename);

            Files.createDirectories(Path.of(dirs));
            File file = new File(storage.getCompletePath());
            FileUtils.writeByteArrayToFile(file, data);
            return storage;
        }
        catch (Exception ex) {
            log.error("While uploading storage[{}]: {}", storage.getCompletePath(), ex.toString());
        }

        return null;
    }

    private Storage createStorageRecord(String alias, String originalFilename, String mime, long byteSize, StorageTypes type, boolean flag_private) {

        String fid = AppUtils.createLocalStorageID(flag_private);
        String newName = buildName(originalFilename);

        Storage storage = new Storage();
        storage.setAlias(alias);
        storage.setUrl(buildURL(newName));
        storage.setPath(buildPath(newName));
        storage.setType(type.value());
        storage.set_private(flag_private);
        storage.setOriginal_name(originalFilename);
        storage.setMime(mime);
        storage.setByte_size(byteSize);
        storage.setFid(fid);

        log.trace("Storage record: " + storage.toString());

        return storage;
    }

    private String buildPath(String filename) {
        StringBuilder sb = new StringBuilder();

        int index = RandomUtils.nextInt(0, paths.length);
        sb.append(AppConstants.PATH_SEPARATOR);
        sb.append(paths[index]);
        sb.append(AppConstants.PATH_SEPARATOR);
        sb.append(filename);
        return sb.toString();
    }

    private String buildURL(String filename) {
        StringBuilder sb = new StringBuilder();

        int index = RandomUtils.nextInt(0, paths.length);
        sb.append(AppConstants.PATH_SEPARATOR);
        sb.append(paths[index]);
        sb.append(AppConstants.PATH_SEPARATOR);
        sb.append(filename);
        return sb.toString();
    }


    private String buildName(String filename) {
        String fileUID = AppUtils.timestampNow().getTime() + RandomStringUtils.randomAlphanumeric(5);
        String newFilename = fileUID + "." + FilenameUtils.getExtension(filename);
        return newFilename;
    }

    class Uploader extends Thread {
        private Storage storage;
        private byte[] fileData;

        public Uploader(Storage storage, byte[] fileData) {
            this.storage = storage;
            this.fileData = fileData;
        }

        public void run(){
            uploadStorageNow(storage, fileData);
        }
    }
}
