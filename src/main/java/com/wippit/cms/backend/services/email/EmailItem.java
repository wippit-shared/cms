package com.wippit.cms.backend.services.email;

import com.wippit.cms.backend.types.ApiEmailTypes;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class EmailItem {
    private String emailID = "";
    private int type = ApiEmailTypes.UNKNOWN.value();
    private String emailTemplateID = "";
    private int status = 0;
    private String email = "";
    private String info = "";
    private String from_address = "";
    private String confirm_address = "";
    private Timestamp start_date;
    private Timestamp status_date;
    private Timestamp expire_date;

}
