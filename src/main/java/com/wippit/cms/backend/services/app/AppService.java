/*
 * Copyright (c) 2019-2020. Onewip Corp. All rights reserved.
 * onewip.com/terms
 * contact@onewip.com
 */

package com.wippit.cms.backend.services.app;

import com.wippit.cms.AppConstants;
import com.wippit.cms.AppGlobal;
import com.wippit.cms.AppUtils;
import com.wippit.cms.backend.data.bean.geo.CountryLocation;
import com.wippit.cms.backend.data.dao.SystemDao;
import com.wippit.cms.backend.data.services.StatsDataService;
import com.wippit.cms.backend.services.ActivityService;
import com.wippit.cms.backend.services.AddressService;
import com.wippit.cms.backend.services.StatsService;
import com.wippit.cms.backend.services.email.EmailService;
import com.wippit.cms.backend.types.LogTypes;
import com.wippit.cms.backend.utils.cache.FileTree;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.File;
import java.nio.file.Path;
import java.util.List;

@Slf4j
@Service
@EnableScheduling
public class AppService {

    @Value("${spring.profiles.active}")
    private String activeProfile;

    @Autowired
    private StatsService statsService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ApplicationArguments applicationArguments;

    @Getter
    @Setter
    private static boolean _cleanFlag = true;

    private boolean _setupDone = false;

    @Autowired
    private EmailService emailService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private StatsDataService statsDataService;

    private int emailCount;

    @Autowired
    private SystemDao systemDao;

    @EventListener(ApplicationReadyEvent.class)
    private void setup() {

        if (_setupDone) {
            return;
        }

        AppConstants.about();

        log.debug("Configuring app");
        AppServiceSetup appServiceSetup = new AppServiceSetup(applicationContext, activeProfile);
        try {
            appServiceSetup.doSetup();
            emailCount = 0;

            updateHitLog();
        }
        catch (Exception ex) {
            log.error("Setup error: {}", ex.toString());
        }
        log.debug("Configuring app - done");

        AddressService.setDao(systemDao);
        AddressService.reload();

        _setupDone = true;
    }

    @Scheduled(fixedDelay = AppConstants.SERVICE_INTERVAL_STATS, initialDelay = AppConstants.SERVICE_INITIAL_DELAY_STATS)
    public void doStatsService(){
        statsService.doService();
        activityService.doService();
        clearTempDirectory();
    }

    @Scheduled(fixedDelay = AppConstants.SERVICE_INTERVAL_EMAIL, initialDelay = AppConstants.SERVICE_INITIAL_DELAY_EMAIL)
    public void sendEmails(){
        EmailService.sendEmails();

        emailCount++;
        if (emailCount>10) {
            EmailService.expireEmails();
            emailCount = 0;
        }
    }

    @PreDestroy
    public void onShutDown() {
//
//        appWebsocketsService.stop();
//
        SystemDao systemDao = applicationContext.getBean(SystemDao.class);
        systemDao.addLog("system", LogTypes.SYSTEM.value(), "Server shutdown");
        log.info("Server shutdown");
    }

    private void clearTempDirectory() {

        if (!_cleanFlag) {
            return;
        }

        long limit = AppUtils.timestampNow().toInstant().toEpochMilli() - 300000;
        FileTree fileTree = new FileTree(AppGlobal.getInstance().getTempPath());
        try {

            //Removing old dirs
            for (Path fileItem : fileTree.getDirectories()) {

                if (fileTree.isRoot(fileItem)) {
                    continue;
                }

                File item = fileItem.toFile();
                if (item.lastModified() < limit) {
                    //log.debug("Removing DIR: {}", item.);
                    FileUtils.deleteDirectory(item);
                }
            }

            //Removing old files
            for (Path fileItem : fileTree.getFiles()) {
                File item = fileItem.toFile();
                if (item.lastModified() < limit) {
                    //log.debug("Removing FILE: {}", fileItem.toAbsolutePath());
                    FileUtils.delete(item);
                }
            }

        }
        catch (Exception ex) {
            log.error("Removing tmp: {}", ex.toString());
        }
    }


    private void updateHitLog() {

        log.debug("Updating empty hit log - start");
        List<String> addressToUpdate = statsDataService.getAddressForEmptyHitLog();
        if (addressToUpdate == null || addressToUpdate.isEmpty()) {
            return;
        }

        for (String address : addressToUpdate) {
            CountryLocation location = AddressService.getCountryFromAddress(address);
            if (location.getCountry().isEmpty()) {
                continue;
            }

            String country = location.getCountry();
            String city = location.getCity();
            if (StringUtils.isBlank(country)) {
                country = "Unknown";
            }
            if (StringUtils.isBlank(city)) {
                city = "Unknown";
            }
            statsDataService.updateHitLogForAddress(address, country, city);
        }

        log.debug("Updating empty hit log - end");
    }

}
