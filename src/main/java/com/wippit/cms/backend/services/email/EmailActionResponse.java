package com.wippit.cms.backend.services.email;

import com.wippit.cms.backend.data.bean.api.ContactMessageRequest;
import com.wippit.cms.backend.data.bean.model.ContactRequestModel;
import com.wippit.cms.backend.data.services.EmailDataService;
import com.wippit.cms.backend.exceptions.ApiException;
import com.wippit.cms.backend.types.ApiEmailRequestStatus;
import com.wippit.cms.backend.types.ApiEmailTypes;
import com.wippit.cms.backend.types.ApiErrorTypes;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Slf4j
@Service
public class EmailActionResponse {

    @Autowired
    private EmailDataService dataService;

    public HashMap<String, Object> createResponse(HttpServletRequest request, EmailItem emailItem) {

        HashMap<String, Object> dataModel = null;
        ApiEmailTypes emailType = ApiEmailTypes.getType(emailItem.getType());
        switch (emailType) {
            case CONTACT_REQUEST:
                dataModel = actionContactRequest(request, emailItem);
                break;

            case UNKNOWN:
                break;

            case NEWSLETTER:
                dataModel = actionNewsletter(request, emailItem);
                break;

            case PROMOTION:
                dataModel = actionPromotion(request, emailItem);
                break;

            case NOTIFICATION:
                dataModel = actionNotification(request, emailItem);
                break;

            case EMAIL_CONFIRM:
                dataModel = actionEmailConfirm(request, emailItem);
                break;

            case NEWSLETTER_SIGNUP:
                dataModel = actionNewsletterSignup(request, emailItem);
                break;
        }

        return dataModel;
    }

    private HashMap<String, Object> actionContactRequest(HttpServletRequest request, EmailItem emailItem) {
        HashMap<String, Object> dataModel = new HashMap<>();

        log.debug("Email item: {}", emailItem);

        ContactMessageRequest contactMessageRequest = ContactMessageRequest.fromJSON(emailItem.getInfo());
        if (contactMessageRequest == null) {
            throw new ApiException(ApiErrorTypes.INVALID_MESSAGE, "Invalid message");
        }

        ContactRequestModel model = new ContactRequestModel(contactMessageRequest);
        ApiEmailRequestStatus status = ApiEmailRequestStatus.getType(emailItem.getStatus());
        switch (status) {
            case UNKNOWN:
            case CREATED:
            case EXPIRED:
                throw new ApiException(ApiErrorTypes.BAD_REQUEST, "Email expired");

            case CONFIRMED:
                log.debug("ApiOpenPost. Confirmed email: {}", emailItem.getEmailID());
                model.setConfirmed(true);
                break;

            case SENT:
                log.debug("ApiOpenPost. Confirm email: {}", emailItem.getEmailID());
                model.setConfirmed(false);
                dataService.confirmEmailRequest(emailItem.getEmailID(), request.getRemoteAddr());
                emailItem.setConfirm_address(request.getRemoteAddr());
                dataService.addContactRequest(emailItem, contactMessageRequest);
                break;
        }

        dataModel.put("REQUEST", model);
        return dataModel;
    }

    private HashMap<String, Object> actionNewsletter(HttpServletRequest request, EmailItem emailItem) {
        HashMap<String, Object> dataModel = new HashMap<>();
        return dataModel;
    }

    private HashMap<String, Object> actionNotification(HttpServletRequest request, EmailItem emailItem) {
        HashMap<String, Object> dataModel = new HashMap<>();
        return dataModel;
    }

    private HashMap<String, Object> actionEmailConfirm(HttpServletRequest request, EmailItem emailItem) {
        HashMap<String, Object> dataModel = new HashMap<>();
        return dataModel;
    }

    private HashMap<String, Object> actionNewsletterSignup(HttpServletRequest request, EmailItem emailItem) {
        HashMap<String, Object> dataModel = new HashMap<>();
        return dataModel;
    }

    private HashMap<String, Object> actionPromotion(HttpServletRequest request, EmailItem emailItem) {
        HashMap<String, Object> dataModel = new HashMap<>();
        return dataModel;
    }






}
