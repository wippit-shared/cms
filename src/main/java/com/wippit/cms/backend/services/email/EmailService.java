package com.wippit.cms.backend.services.email;

import com.wippit.cms.AppGlobal;
import com.wippit.cms.backend.data.bean.model.TranslationMethod;
import com.wippit.cms.backend.data.bean.site.EmailSettings;
import com.wippit.cms.backend.data.services.EmailDataService;
import com.wippit.cms.backend.data.services.EmailTemplateDataService;
import com.wippit.cms.backend.data.services.LanguageDataService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import jakarta.mail.internet.MimeMessage;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.File;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Service
public class EmailService {

    private final AtomicInteger _counter = new AtomicInteger(0);
    private final AtomicBoolean flagBusy;


    private EmailConfig config = null;

    @Autowired
    private EmailDataService emailDataService;

    @Autowired
    private EmailTemplateDataService templateDataService;

    @Autowired
    private LanguageDataService languageDataService;

    @Autowired
    private Configuration _freemarkerConfig;

    private static final LinkedBlockingQueue<EmailPoolItem> _pool = new LinkedBlockingQueue<>();

    @Getter
    private static EmailService currentInstance = null;

    public EmailService(){
        currentInstance = this;
        flagBusy = new AtomicBoolean(false);
    }

    public void  setup(EmailSettings settings) {
        config = new EmailConfig(settings);
        log.info("Email service setup: Done");
    }

    public static void addEmail(Mail mail, long userID) {
        currentInstance._addEmail(mail, userID);
    }

    public static void sendEmails(){
        currentInstance._sendEmails();
    }

    public static void expireEmails() {
        currentInstance.emailDataService.expireEmailRequests();
    }

    private void _addEmail(Mail mail, long userID) {
        try {
            synchronized (this) {
                _pool.put(new EmailPoolItem(mail, userID));
                log.trace("Email pool size: " + _pool.size());
            }
            log.trace("Added email to pool from user(" + userID + ")");
        }
        catch (Exception ex) {
            log.error("On adding email to pool: " + ex.toString());
        }
    }

    private void _sendEmails(){

        if (flagBusy.get()) {
            return;
        }

        flagBusy.set(true);

        if (EmailTemplateManager.current.getFlag()) {
            _freemarkerConfig.setTemplateLoader(EmailTemplateManager.current.getTemplates(templateDataService));
            EmailTemplateManager.current.clearFlag();
        }

        int size = _pool.size();
        if (size < 1) {
            flagBusy.set(false);
            return;
        }

        log.trace("Sending " + size + " emails.");
        EmailPoolItem emailPoolItem = _pool.poll();

        while (emailPoolItem != null) {

            try {
                Mail mail = emailPoolItem.getEmail();
                if (_sendEmail(mail, emailPoolItem.getUserID())) {
                    emailDataService.updateEmailAsSent(mail.getEmailID());
                    log.trace("Email sent");
                }
            }
            catch (Exception ex) {
                log.error("On sending email: " + ex.toString());
            }

            emailPoolItem = _pool.poll();
            if (emailPoolItem != null) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                }catch (Exception ex) {
                    log.error("On sleep: " + ex.toString());
                }
            }

        }

        flagBusy.set(false);
    }

    private boolean _sendEmail(Mail mail, long id_user) throws Exception {

        if (config == null) {
            log.warn("Email requires configuration");
            return false;
        }

        TranslationMethod translationMethod = new TranslationMethod(languageDataService, "email", mail.getTemplateID(), mail.getIso());
        mail.getModel().put("TRANSLATE", translationMethod);

        String nameHTML = mail.getTemplateID() + "-html";
        String namePlain = mail.getTemplateID() + "-plain";
        Template templateHTML = _freemarkerConfig.getTemplate(nameHTML);
        Template templatePlain = _freemarkerConfig.getTemplate(namePlain);

        return _sendEmailWithTemplate(mail, templatePlain, templateHTML, id_user);
    }

    private boolean _sendEmailWithTemplate(Mail mail, Template templatePlain, Template templateHTML, long id_user) throws Exception {

        if (config == null) {
            log.warn("Email requires configuration");
            return false;
        }

        String msd_id = mail.getEmailID() + "-" + _counter.incrementAndGet();
        if (config.getSettings().isDebug()) {
            log.warn("Debug mode. NOT Sending email: " + msd_id);
            return true;
        }
        else {
            log.trace("Sending email: " + msd_id);
        }

        mail.getModel().put("msg_id", msd_id);
        mail.setMailFrom(config.getSettings().getFrom());

        String textHTML = FreeMarkerTemplateUtils.processTemplateIntoString(templateHTML, mail.getModel());
        String textPlain = FreeMarkerTemplateUtils.processTemplateIntoString(templatePlain, mail.getModel());

        MimeMessage message = config.getMailSender().createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(mail.getMailTo());
        helper.setText(textPlain, textHTML);
        helper.setFrom(config.getSettings().getFrom());

        if (config.getSettings().getPrefix().isEmpty()) {
            helper.setSubject(mail.getMailSubject());
        }
        else {
            helper.setSubject(config.getSettings().getPrefix() + " " + mail.getMailSubject());
        }

        //---
        //Attachments
        //---
        String attachmentDir = AppGlobal.getInstance().getEmailPath() + "/" + mail.getType().getTemplate();
        String attachmentPath;
        for (EmailAttachment attachment: mail.getAttachments()) {
            attachmentPath = attachmentDir + "/" + attachment.getCid();
            FileSystemResource file = new FileSystemResource(new File(attachmentPath));
            helper.addAttachment(attachment.getCid(), file);
            if (!StringUtils.isBlank(attachment.getCid())) {
                helper.addInline(attachment.getCid(), file);
            }
        }

        try {
            config.getMailSender().send(message);
            log.debug("Sending email( " + mail.getMailTo() + " ): " + msd_id + " -> done");
        }
        catch (Exception ex) {
            log.error("Sending email: " + ex.toString());
            return false;
        }
        return true;
    }

}