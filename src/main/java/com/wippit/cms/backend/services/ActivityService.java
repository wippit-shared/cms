package com.wippit.cms.backend.services;

import com.wippit.cms.backend.data.bean.log.ActivityItem;
import com.wippit.cms.backend.data.dao.SystemDao;
import com.wippit.cms.backend.types.LogTypes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
@Slf4j
public class ActivityService {

    private SystemDao systemDao;

    private LinkedBlockingQueue<ActivityItem> _activityItems;
    private static ActivityService _thisInstance;
    private static AtomicBoolean flagBusy = new AtomicBoolean(false);

    public ActivityService(@Autowired SystemDao systemDao) {
        this.systemDao = systemDao;
        _activityItems = new LinkedBlockingQueue<>();
        _thisInstance = this;
    }

    public static void addLog(String alias, String detail, LogTypes type) {
        ActivityItem item = new ActivityItem();
        try {
            item.setAlias(alias);
            item.setType(type.value());
            item.setDetail(detail);
            _thisInstance._addLog(item);
        }
        catch (Exception ex) {
            log.error("Cant add log: {}", ex.toString());
        }
    }

    public static void addLogNow(String alias, String detail, LogTypes type) {
        ActivityItem item = new ActivityItem();
        try {
            item.setAlias(alias);
            item.setType(type.value());
            item.setDetail(detail);
            _thisInstance._addLog(item);
            _thisInstance.doService();
        }
        catch (Exception ex) {
            log.error("Cant add log: {}", ex.toString());
        }
    }

    private void _addLog(ActivityItem item) {
        _activityItems.add(item);
    }

    public void doService(){

        if (flagBusy.get()) {
            return;
        }

        _saveLogs();
    }

    private void _saveLogs() {

        flagBusy.set(true);

        if (_activityItems.size() < 1) {
            flagBusy.set(false);
            return;
        }

        ActivityItem item = _activityItems.poll();

        while (item != null) {

            try {
                systemDao.addLog(item.getAlias(), item.getType(), item.getDetail());
            }
            catch (Exception ex) {
                log.error("On log service: {}", ex.toString());
            }

            item = _activityItems.poll();
        }

        flagBusy.set(false);
    }
}
