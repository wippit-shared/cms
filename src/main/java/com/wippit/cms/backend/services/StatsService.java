package com.wippit.cms.backend.services;

import com.wippit.cms.backend.data.bean.geo.CountryLocation;
import com.wippit.cms.backend.data.bean.stats.HitItem;
import com.wippit.cms.backend.data.dao.SystemDao;
import com.wippit.cms.backend.types.CMSHitTypes;
import com.wippit.cms.backend.types.CMSItemTypes;
import com.wippit.cms.backend.utils.ResourceSection;
import com.wippit.cms.backend.utils.ResourceStorage;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
@Slf4j
public class StatsService {

    private final SystemDao systemDao;
    private final LinkedBlockingQueue<HitItem> _hits;
    private static StatsService _thisInstance;
    private static AtomicBoolean flagBusy;

    public StatsService(@Autowired SystemDao systemDao) {
        this.systemDao = systemDao;
        _hits = new LinkedBlockingQueue<>();
        _thisInstance = this;
        flagBusy  = new AtomicBoolean(false);
    }

    /**
     * Add hit to stats
     * @param request HttpServletRequest
     */
    public static void addHit(HttpServletRequest request) {
        HitItem item = new HitItem();
        try {

            String agent = request.getHeader(HttpHeaders.USER_AGENT);
            String address = request.getRemoteAddr();
            String url = request.getRequestURI();
            CMSItemTypes type = CMSItemTypes.getTypeByURI(url);

            CountryLocation location = AddressService.getCountryFromAddress(address);

            item.setUrl(url);
            item.setType(type.value());
            item.setHit(CMSHitTypes.OK.value());
            item.setAddress(address);
            item.setAgent(agent);
            item.setCountry(location.getCountry());
            item.setCity(location.getCity());

            _thisInstance._addHit(item);
        }
        catch (Exception ex) {
            log.error("Cant add hit: {}", ex.toString());
        }
    }

    /**
     * Add secID hit to stats
     * @param section ResourceSection requested
     * @param request HttpServletRequest
     */
    public static void addSectionHit(ResourceSection section, HttpServletRequest request) {
        HitItem item = new HitItem();
        try {

            String agent = request.getHeader(HttpHeaders.USER_AGENT);
            String address = request.getRemoteAddr();
            String url = request.getRequestURI();
            CMSItemTypes type = CMSItemTypes.getType(section.getResource(), section.getSection());

            CountryLocation location = AddressService.getCountryFromAddress(address);

            item.setUrl(url);
            item.setType(type.value());
            item.setHit(CMSHitTypes.OK.value());
            item.setAddress(address);
            item.setAgent(agent);
            item.setSection(section.getSection());
            item.setCountry(location.getCountry());
            item.setCity(location.getCity());

            _thisInstance._addHit(item);
        }
        catch (Exception ex) {
            log.error("Cant add hit: {}", ex.toString());
        }
    }

    public static void addErrorHit(int error, HttpServletRequest request) {
        HitItem item = new HitItem();
        try {

            String agent = request.getHeader(HttpHeaders.USER_AGENT);
            String address = request.getRemoteAddr();
            String url = (String) request.getAttribute(RequestDispatcher.ERROR_REQUEST_URI);
            if (url == null) {
                url = request.getRequestURL().toString();
            }

            CMSItemTypes type = CMSItemTypes.getTypeByURI(url);

            CountryLocation location = AddressService.getCountryFromAddress(address);

            item.setCountry(location.getCountry());
            item.setCity(location.getCity());
            item.setUrl(url);
            item.setType(type.value());

            if (error == 404)
                item.setHit(CMSHitTypes.ERROR_NOT_FOUND.value());
            else if (error == 500)
                item.setHit(CMSHitTypes.ERROR_INTERNAL.value());
            else
                item.setHit(CMSHitTypes.ERROR_OTHER.value());

            item.setAddress(address);
            item.setAgent(agent);

            _thisInstance._addHit(item);
        }
        catch (Exception ex) {
            log.error("Cant add error hit: {}", ex.toString());
        }
    }

    /**
     * Add resource hit to stats
     * @param storage ResourceStorage requested
     * @param request HttpServletRequest
     */
    public static void addResourceHit(ResourceStorage storage, HttpServletRequest request) {
        HitItem item = new HitItem();
        try {

            String agent = request.getHeader(HttpHeaders.USER_AGENT);
            String address = request.getRemoteAddr();
            CMSItemTypes type = CMSItemTypes.RESOURCE;

            CountryLocation location = AddressService.getCountryFromAddress(address);

            item.setUrl(storage.getUrl());
            item.setType(type.value());
            item.setHit(CMSHitTypes.OK.value());
            item.setAddress(address);
            item.setAgent(agent);
            item.setCountry(location.getCountry());
            item.setCity(location.getCity());

            _thisInstance._addHit(item);
        }
        catch (Exception ex) {
            log.error("Cant add hit: {}", ex.toString());
        }
    }

    /**
     * Add redirect hit to stats
     * @param url redirec requested
     * @param request HttpServletRequest
     */
    public static void addRedirectHit(String iso, String name, String url, HttpServletRequest request) {
        HitItem item = new HitItem();
        try {

            String agent = request.getHeader(HttpHeaders.USER_AGENT);
            String address = request.getRemoteAddr();
            CMSItemTypes type = CMSItemTypes.REDIRECT;

            CountryLocation location = AddressService.getCountryFromAddress(address);

            item.setUrl(url);
            item.setType(type.value());
            item.setHit(CMSHitTypes.OK.value());
            item.setAddress(address);
            item.setAgent(agent);
            item.setIso(iso);
            item.setCountry(location.getCountry());
            item.setCity(location.getCity());

            item.setSection(name); //redirect name

            _thisInstance._addHit(item);

        }
        catch (Exception ex) {
            log.error("Cant add hit: {}", ex.toString());
        }
    }

    /**
     * Add hit to stats
     * @param path Request URI
     * @param itemType CMS Item type requested
     * @param hitType Hit type
     * @param agent Request agent
     * @param address Request address
     */
    public static void addHit(String path, CMSItemTypes itemType, CMSHitTypes hitType, String agent, String address) {
        HitItem item = new HitItem();
        try {

            CountryLocation location = AddressService.getCountryFromAddress(address);

            item.setUrl(path);
            item.setType(itemType.value());
            item.setHit(hitType.value());
            item.setAddress(address);
            item.setAgent(agent);
            item.setCountry(location.getCountry());
            item.setCity(location.getCity());

            _thisInstance._addHit(item);
        }
        catch (Exception ex) {
            log.error("Cant add hit: {}", ex.toString());
        }
    }

    /**
     * Add hit to secID stats
     * @param section Section
     * @param path Request URI
     * @param itemType CMS Item type requested
     * @param hitType Hit type
     * @param agent Request agent
     * @param address Request address
     */
    public static void addSectionHit(String section, String path, CMSItemTypes itemType, CMSHitTypes hitType, String agent, String address) {
        HitItem item = new HitItem();
        try {

            CountryLocation location = AddressService.getCountryFromAddress(address);

            item.setSection(section);
            item.setUrl(path);
            item.setType(itemType.value());
            item.setHit(hitType.value());
            item.setAddress(address);
            item.setAgent(agent);
            item.setCountry(location.getCountry());
            item.setCity(location.getCity());

            _thisInstance._addHit(item);
        }
        catch (Exception ex) {
            log.error("Cant add hit: {}", ex.toString());
        }
    }

    /**
     * Add hit to Article stats
     * @param cid Article CID
     * @param path Request URI
     * @param itemType CMS Item type requested
     * @param hitType Hit type
     * @param agent Request agent
     * @param address Request address
     */
    public static void addArticleHit(String cid, String path, CMSItemTypes itemType, CMSHitTypes hitType, String agent, String address) {
        HitItem item = new HitItem();
        try {
            CountryLocation location = AddressService.getCountryFromAddress(address);

            item.setCid(cid);
            item.setUrl(path);
            item.setType(itemType.value());
            item.setHit(hitType.value());
            item.setAddress(address);
            item.setAgent(agent);
            item.setCountry(location.getCountry());
            item.setCity(location.getCity());

            _thisInstance._addHit(item);
        }
        catch (Exception ex) {
            log.error("Cant add hit: {}", ex.toString());
        }
    }

    private void _addHit(HitItem item) {
        _hits.add(item);
    }

    public void doService(){

        if (flagBusy.get()) {
            return;
        }

        _saveHits();
    }

    private void _saveHits() {

        flagBusy.set(true);

        if (_hits.isEmpty()) {
            flagBusy.set(false);
            return;
        }

        HitItem item = _hits.poll();

        while (item != null) {

            try {
                int added = systemDao.updateURLHitCount(item.getUrl());
                if (added == 0) {
                    systemDao.addUrlToHitCount(item.getUrl(), item.getType());
                }

                String city = item.getCity();
                if (city == null) {
                    city = "";
                }
                String country = item.getCountry();
                if (country==null) {
                    country = "";
                }
                systemDao.addHitLog(item.getUrl(), item.getType(), item.getHit(), item.getAgent(), item.getAddress(), country, city);

                CMSItemTypes itemTypes = CMSItemTypes.getType(item.getType());
                switch (itemTypes) {
                    case FTL_ARTICLE -> {
                        systemDao.updateCIDHitCount(item.getCid());
                    }
                    case FTL_SECTION -> {
                        systemDao.updateSectionHitCount(item.getIso(), item.getSection());
                    }
                    case REDIRECT -> {
                        systemDao.addRedirectHit(item.getIso(), item.getSection());
                    }
                }
            }
            catch (Exception ex) {
                log.error("On stats: {}", ex.toString());
            }

            item = _hits.poll();
        }

        flagBusy.set(false);
    }
}
