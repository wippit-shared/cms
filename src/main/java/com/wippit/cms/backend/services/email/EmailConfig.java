package com.wippit.cms.backend.services.email;

import com.wippit.cms.backend.data.bean.site.EmailSettings;
import lombok.Getter;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Getter
public class EmailConfig {

    private final EmailSettings settings;
    private final JavaMailSender mailSender;

    public EmailConfig(EmailSettings settings) {
        this.settings = new EmailSettings(settings);
        this.mailSender = createSender();
    }

    private JavaMailSender createSender() {

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setHost(settings.getHost());
        mailSender.setPort(settings.getPort());
        mailSender.setUsername(settings.getUsername());
        mailSender.setPassword(settings.getPassword());

        Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail.smtp.starttls.enable", settings.isTls());
        javaMailProperties.put("mail.smtp.ssl.enable", settings.isSsl());
        javaMailProperties.put("mail.smtp.auth", settings.isAuth());
        javaMailProperties.put("mail.transport.protocol", "smtp");
        javaMailProperties.put("mail.debug", settings.isDebug());

        mailSender.setJavaMailProperties(javaMailProperties);

        return mailSender;
    }
}