package com.wippit.cms.backend.security;

import com.wippit.cms.AppGlobal;
import com.wippit.cms.backend.utils.ResourceFile;
import com.wippit.cms.backend.utils.crypto.ChaChaLib;
import com.wippit.cms.backend.utils.crypto.KeyAndNonce;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.text.RandomStringGenerator;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ralmeida on 3/27/17.
 */
@Slf4j
public class AppSecure {

    @Getter
    public static PrivateKey privateKey;
    @Getter
    public static PublicKey publicKey;
    @Getter
    @Setter
    public static PublicKey clientPublicKey;
    @Getter
    private static String shareablePublicKey = "";
    @Getter
    private static String certificatePEM = "";

    private final int ENC_LEN = 64;

    private final SecureRandom random;

    public AppSecure(){
        random = new SecureRandom();

        Security.addProvider(
                new org.bouncycastle.jce.provider.BouncyCastleProvider()
        );
    }

    public  String createSalt(){
        byte[] bytes = new byte[24];
        random.nextBytes(bytes);
        return Hex.encodeHexString(bytes);
    }

    public synchronized String createSecret(){
        return generatePassword();
    }

    public synchronized String generateRandomSpecialCharacters(int length) {

        RandomStringGenerator pwdGenerator = new RandomStringGenerator.Builder()
                .withinRange(35, 45)
                //.usingRandom(stp)
                .build();
        return pwdGenerator.generate(length);
    }

    public synchronized String generateCustomLenPassword(int len) {

        final int LEN = 40;
        if (len <= LEN) {
            return generatePassword();
        }

        String upperCaseLetters = RandomStringUtils.random(8, 65, 90, true, true);
        String lowerCaseLetters = RandomStringUtils.random(8, 97, 122, true, true);
        String numbers = RandomStringUtils.randomNumeric(8);
        String specialChar = generateRandomSpecialCharacters(len - LEN);
        String totalChars = RandomStringUtils.randomAlphanumeric(8);
        String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
                .concat(numbers)
                .concat(specialChar)
                .concat(totalChars);
        List<Character> pwdChars = combinedChars.chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.toList());
        Collections.shuffle(pwdChars);
        String password = pwdChars.stream()
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
        return password;
    }

    public synchronized String generatePassword() {
        String upperCaseLetters = RandomStringUtils.random(8, 65, 90, true, true);
        String lowerCaseLetters = RandomStringUtils.random(8, 97, 122, true, true);
        String numbers = RandomStringUtils.randomNumeric(8);
        String specialChar = generateRandomSpecialCharacters(8);
        String totalChars = RandomStringUtils.randomAlphanumeric(8);
        String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
                .concat(numbers)
                .concat(specialChar)
                .concat(totalChars);
        List<Character> pwdChars = combinedChars.chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.toList());
        Collections.shuffle(pwdChars);
        String password = pwdChars.stream()
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
        return password;
    }

    public String generateSmallPassword() {
        String upperCaseLetters = RandomStringUtils.random(3, 65, 90, true, true);
        String lowerCaseLetters = RandomStringUtils.random(3, 97, 122, true, true);
        String numbers = RandomStringUtils.randomNumeric(3);
        String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
                .concat(numbers);
        List<Character> pwdChars = combinedChars.chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.toList());
        Collections.shuffle(pwdChars);
        String password = pwdChars.stream()
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
        return password;
    }

    public static void setServerKeys(PublicKey publicKey, String shareablePublicKey, PrivateKey privateKey) {
        AppSecure.publicKey = publicKey;
        AppSecure.privateKey = privateKey;
        AppSecure.shareablePublicKey = shareablePublicKey;
        AppSecure.certificatePEM = loadStoreCertificatePEM(); //Load certificate from keystore
    }

    /**
     * Create a RSA Key pair
     * @return RSA Key pair
     * @throws Exception if RSA generator is wrong or not available
     */
    public KeyPair generateRSAKeys() throws Exception {
        final int KEY_LEN = 2048;
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(KEY_LEN);
        KeyPair pair = generator.generateKeyPair();
        return pair;
    }

    /**
     * Encrypt message with RSA Public key
     * @param message   Message to encrypt
     * @param publicKey RSA PublicKey
     * @return  String with encrypted message
     * @throws Exception if invalid data or key
     */
    public String encryptWithPublicKey(String message, PublicKey publicKey) throws Exception {
        String key = RandomStringUtils.randomAlphanumeric(ENC_LEN);
        OWSecure secure = new OWSecure(key);
        String securedMessage = secure.encode(message);

        String secureKey = _encryptSecretWithPublicKey(key, publicKey);
        String encodedMessage = secureKey + securedMessage;
        return encodedMessage;
    }

    public byte[] encodeWithRSAAndChaCha20(String message, PublicKey publicKey) throws Exception {

        KeyAndNonce kn = new KeyAndNonce();
        ChaChaLib coder = new ChaChaLib(kn);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] dataKeyCoded = _encryptSecretWithPublicKey(kn.getKey(), publicKey);
        byte[] dataMessageCoded = coder.encode(message.getBytes(StandardCharsets.UTF_8));
        bos.write(dataKeyCoded);
        bos.write(dataMessageCoded);
        byte[] data = bos.toByteArray();
        return data;
    }


    /**
     * Encrypt with server RSA public key
     * @param message   Message to encrypt
     * @return Encrypted message
     * @throws Exception if invalid data or key
     */
    public String encryptWithServerKey(String message) throws Exception{

        String key = RandomStringUtils.randomAlphanumeric(ENC_LEN);
        OWSecure secure = new OWSecure(key);
        String securedMessage = secure.encode(message);

        String secureKey = _encryptSecretWithServerKey(key);
        return secureKey + securedMessage;
    }

    /**
     * Decrypt message using server RSA private key
     * @param encryptedMessage  String with encrypted message
     * @return decrypted message
     * @throws Exception if wrong encryption or invalid data
     */
    public String decryptWithServerKey(String encryptedMessage) throws  Exception {

        int index = ENC_LEN*8;
        String keyEncoded = encryptedMessage.substring(0, index);
        String messageEncoded = encryptedMessage.substring(index);
        String key = _decryptSecretWithServerKey(keyEncoded);
        OWSecure secure = new OWSecure(key);
        return secure.decode(messageEncoded);
    }

    /**
     * Get a RSA PublicKey from X509 hex encoded string
     * @param x509 string with X509 key spec
     * @return  RSA PublicKey
     * @throws Exception if data is wrong or missing
     */
    public PublicKey getPublicKeyFromX509(String x509) throws Exception {
        byte[] encoded = Hex.decodeHex(x509);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encoded);
        PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
        return publicKey;
    }

    private String _encryptSecretWithServerKey(String message) throws Exception {
        Cipher encryptCipher = Cipher.getInstance("RSA");
        encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);

        byte[] secretMessageBytes = message.getBytes(StandardCharsets.UTF_8);
        byte[] encryptedMessageBytes = encryptCipher.doFinal(secretMessageBytes);
        return Hex.encodeHexString(encryptedMessageBytes);
    }

    private String _decryptSecretWithServerKey(String encryptedMessage) throws Exception {
        Cipher decryptCipher = Cipher.getInstance("RSA");
        decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);

        byte[] encryptedMessageBytes = Hex.decodeHex(encryptedMessage);
        byte[] decryptedMessageBytes = decryptCipher.doFinal(encryptedMessageBytes);
        return new String(decryptedMessageBytes, StandardCharsets.UTF_8);
    }

    private String _encryptSecretWithPublicKey(String message, PublicKey publicKey) throws Exception {
        byte[] dataBytes = _encryptSecretWithPublicKey(message.getBytes(StandardCharsets.UTF_8), publicKey);
        return Hex.encodeHexString(dataBytes);
    }

    private byte[] _encryptSecretWithPublicKey(byte[] message, PublicKey publicKey) throws Exception {
        Cipher encryptCipher = Cipher.getInstance("RSA");
        encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return encryptCipher.doFinal(message);
    }

    /**
     * Load a X509 certificate from KeyStore and returns in PEM format
     * @return PEM String from X509 Certificate
     */
    private static String loadStoreCertificatePEM() {
        StringWriter sw = new StringWriter();
        try {
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            ResourceFile resourceFile = new ResourceFile("wippitkeystore.p12");
            InputStream stream = resourceFile.getContentAsStream();
            char[] pass = ("wippitcms").toCharArray();
            keyStore.load(stream, pass);

            X509Certificate certificate = (X509Certificate)keyStore.getCertificate("wippitcms");
            try (PEMWriter pw = new PEMWriter(sw)) {
                pw.writeObject(certificate);
            } catch (Exception ex) {
                log.error("Cant convert key to PEM: {}", ex.toString());
                return "";
            }

        }
        catch (Exception ex) {
            log.error("Cant open keystore");
        }

        return sw.toString();
    }

    /**
     * Creates a X509 certificate from dynamic private/public keys and returns in PEM format
     * @return PEM String from X509 Certificate
     */
    private static String createCertificatePEM() {

        StringWriter sw = new StringWriter();
        try {
            KeyPair keyPair = new KeyPair(AppSecure.getPublicKey(), AppSecure.getPrivateKey());
            X509Certificate certificate = createX509Certificate(keyPair, "CN=" + AppGlobal.getInstance().getDomain());

            try (PEMWriter pw = new PEMWriter(sw)) {
                pw.writeObject(certificate);
            } catch (Exception ex) {
                log.error("Cant convert key to PEM: {}", ex.toString());
                return "";
            }
        }
        catch (Exception ex) {
            log.error("Cant create X509 certificate: {}", ex.toString());
        }

        return sw.toString();
    }

    /**
     * Creates a X509 certificate from KeyPair and a subject DN
     * @param keyPair   KeyPair
     * @param subjectDN Subject DN (String "CN=xxxxxxx")
     * @return X509 Certificate
     * @throws OperatorCreationException Operator creation exception
     * @throws CertificateException Certificate exception
     * @throws IOException IO exception
     */
    public static X509Certificate createX509Certificate(KeyPair keyPair, String subjectDN) throws OperatorCreationException, CertificateException, IOException {
        Provider bcProvider = new BouncyCastleProvider();
        Security.addProvider(bcProvider);

        long now = System.currentTimeMillis();
        Date startDate = new Date(now);

        X500Name dnName = new X500Name(subjectDN);
        BigInteger certSerialNumber = new BigInteger(Long.toString(now)); // <-- Using the current timestamp as the certificate serial number

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.YEAR, 1); // <-- 1 Yr validity

        Date endDate = calendar.getTime();

        String signatureAlgorithm = "SHA256WithRSA"; // <-- Use appropriate signature algorithm based on your keyPair algorithm.

        ContentSigner contentSigner = new JcaContentSignerBuilder(signatureAlgorithm).build(keyPair.getPrivate());

        JcaX509v3CertificateBuilder certBuilder = new JcaX509v3CertificateBuilder(dnName, certSerialNumber, startDate, endDate, dnName, keyPair.getPublic());

        // Extensions --------------------------

        // Basic Constraints
        BasicConstraints basicConstraints = new BasicConstraints(true); // <-- true for CA, false for EndEntity

        certBuilder.addExtension(new ASN1ObjectIdentifier("2.5.29.19"), true, basicConstraints); // Basic Constraints is usually marked as critical.

        // -------------------------------------

        return new JcaX509CertificateConverter().setProvider(bcProvider).getCertificate(certBuilder.build(contentSigner));
    }

}
