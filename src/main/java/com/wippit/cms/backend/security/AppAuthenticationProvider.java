package com.wippit.cms.backend.security;

import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.bean.user.AccountExtended;
import com.wippit.cms.backend.data.properties.AppProperties;
import com.wippit.cms.backend.data.services.UserDataService;
import com.wippit.cms.backend.types.AccountTypes;
import com.wippit.cms.backend.types.RoleTypes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class AppAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserDataService userDataService;

    @Autowired
    private AppProperties appProperties;

    @Autowired
    private SecurityUtils securityUtils;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String username = authentication.getName();
        String code = authentication.getCredentials().toString();

        log.debug("Authenticate: {} / {}", username, code);

        Account account = userDataService.getAccountByAlias(username);
        if (account == null) {
            log.error("User not found: {}", username);
            throw new UsernameNotFoundException("No user present with username: " + username);
        }

        boolean validateMatch = securityUtils.validateMatch(code, account.getSecret());

        if (!validateMatch) {
            log.error("Invalid credentials: {}", username);
            throw new UsernameNotFoundException("Invalid credentials: " + username);
        }
        log.debug("\n========================================\nAuthenticated: {}\nRoles: {} \n========================================", username, account.getRoles());

        List<SimpleGrantedAuthority> roles = new ArrayList<>();
        for (RoleTypes role : account.getRoles()) {
            roles.add(new SimpleGrantedAuthority("ROLE_" + role.getCode().toUpperCase()));
        }

        if (account.getAccountType() != AccountTypes.SYSTEM) {
            AccountExtended extended = userDataService.getAccountExtendedByAlias(account.getAlias());
            account.setExtended(extended);
        }

        return new UsernamePasswordAuthenticationToken(
                username,
                account.getSecret(),
                roles);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}