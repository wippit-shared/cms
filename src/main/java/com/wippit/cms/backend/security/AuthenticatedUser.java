package com.wippit.cms.backend.security;

import com.vaadin.flow.spring.security.AuthenticationContext;
import com.wippit.cms.backend.data.bean.user.Account;
import com.wippit.cms.backend.data.services.UserDataService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
public class AuthenticatedUser {

    private final UserDataService userDataService;
    private final AuthenticationContext authenticationContext;

    public AuthenticatedUser(AuthenticationContext authenticationContext, UserDataService userDataService) {
        this.userDataService = userDataService;
        this.authenticationContext = authenticationContext;
    }

    @Transactional
    public Optional<Account> get() {
        if (!authenticationContext.isAuthenticated() || authenticationContext.getPrincipalName().isEmpty()) {
            return Optional.empty();
        }

        Account account = userDataService.getAccountByAlias(authenticationContext.getPrincipalName().get());
        if (account == null) {
            return Optional.empty();
        }

        account.setRoles(userDataService.getUserRoles(account.getAlias()));

        return Optional.of(account);
    }

    public void logout() {
        authenticationContext.logout();
    }

}
