package com.wippit.cms.backend.security;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;

@Component
@Slf4j
public class SecurityUtils {

    private static final int LEN_PASSWD = 24;
    private static final int LEN_LONG_PASSWD = 72;
    private static final String LOGOUT_SUCCESS_URL = "/";

    private final BCryptPasswordEncoder passwordEncoder;
    private SecureRandom random;

    public SecurityUtils() {
        random = new SecureRandom();
        passwordEncoder = new BCryptPasswordEncoder();
//        java.security.Security.addProvider(
//                new org.bouncycastle.jce.provider.BouncyCastleProvider()
//        );
    }

//    public boolean isAuthenticated() {
//        VaadinServletRequest request = VaadinServletRequest.getCurrent();
//        return request != null && request.getUserPrincipal() != null;
//    }
//
//    public boolean authenticateRemoteSession(String username, String code) {
//
//        VaadinServletRequest request = VaadinServletRequest.getCurrent();
//        if (request == null) {
//            // This is in a background thread and we cannot access the request to
//            // log in the user
//            return false;
//        }
//        try {
//            request.login(username, code);
//            return true;
//        } catch (Exception e) {
//            // login exception handle code omitted
//            log.warn("Cant validate remote device: {}", e.toString());
//            return false;
//        }
//    }
//
//    public static void logout() {
//        VaadinSession.getCurrent().getSession().invalidate();
//        UI.getCurrent().getPage().setLocation(LOGOUT_SUCCESS_URL);
//    }

    private  String createSalt(){
        byte bytes[] = new byte[24];
        random.nextBytes(bytes);
        return Hex.encodeHexString(bytes);
    }

    public String createSecretCode() {

        RandomStringGenerator generator = new RandomStringGenerator
                .Builder()
                .selectFrom("aAbBcCdDeEfFgGhHiJjJKmMnNLpPqQrRsStTuUvVxXyYzZ23456789-_#%!;:<>@=?*+${}[]()".toCharArray())
                .build();

        return generator.generate(LEN_PASSWD);
    }

    public String createLongSecretCode() {

        RandomStringGenerator generator = new RandomStringGenerator
                .Builder()
                .selectFrom("aAbBcCdDeEfFgGhHiJjJKmMnNLpPqQrRsStTuUvVxXyYzZ23456789-_#%!;:<>@=?*+${}[]()".toCharArray())
                .build();

        return generator.generate(LEN_LONG_PASSWD);
    }

    public  String encryptPassword(String password) {
        //log.debug("Encriptando: {}", password);
        return passwordEncoder.encode(password);
    }

    public  String encryptPassword(String password, String salt) {
        //log.debug("Encriptando: {} / {}", password, salt);
        return passwordEncoder.encode(password + salt);
    }

    public boolean validateMatch(String password, String encodedPassword) {
        //log.debug("Validando match: {}", password);
        return passwordEncoder.matches(password, encodedPassword);
    }

    public boolean validateMatch(String password, String salt, String encodedPassword) {
        //log.debug("Validando match: {} / {}", password, salt);
        return passwordEncoder.matches(password + salt, encodedPassword);
    }

}