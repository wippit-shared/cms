package com.wippit.cms.backend.types;

import org.apache.commons.lang3.StringUtils;

public enum PostSortTypes {

    DEFAULT(0, "Sort default"),
    START_DATE(100, "Start date"),
    START_DATE_ASC(110, "Start date asc"),
    EVENT_DATE(200, "Event date"),
    EVENT_DATE_ASC(210, "Event date asc"),
    TITLE(300, "Title"),
    TITLE_DESC(310, "Title desc");

    private final int type;
    private final String label;

    PostSortTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String toString(){
        return label;
    }

    public static PostSortTypes getType(int type) {

        for (PostSortTypes item : PostSortTypes.values()){
            if (type == item.type) {
                return item;
            }
        }
        return PostSortTypes.DEFAULT;
    }

    public static PostSortTypes getType(String type) {

        if (StringUtils.isBlank(type)) {
            return PostSortTypes.DEFAULT;
        }

        boolean isInverted = false;
        String originalType = type.trim();
        String sType = type.trim();
        if (originalType.contains("-")
                || originalType.contains("!")
                || originalType.contains("Not")
                || originalType.contains("NOT")
                || originalType.contains("not")) {
            isInverted = true;
            sType = originalType
                    .replace("-", "")
                    .replace("Not", "")
                    .replace("not", "")
                    .replace("NOT", "")
                    .replace("!", "");
        }

        if (sType.equalsIgnoreCase("default")
        || sType.equalsIgnoreCase("normal")
        || sType.equalsIgnoreCase("order")
        ) {
            return isInverted ? PostSortTypes.START_DATE_ASC : PostSortTypes.START_DATE;
        }

        if (sType.equalsIgnoreCase("title")
                || sType.equalsIgnoreCase("name")
                || sType.equalsIgnoreCase("label")
        ) {

            return isInverted ? PostSortTypes.TITLE_DESC : PostSortTypes.TITLE;
        }

        if (sType.equalsIgnoreCase("event")
                || sType.equalsIgnoreCase("action")
                || sType.equalsIgnoreCase("other")
        ) {
            return isInverted ? PostSortTypes.EVENT_DATE_ASC : PostSortTypes.EVENT_DATE;
        }

        return PostSortTypes.DEFAULT;
    }

}
