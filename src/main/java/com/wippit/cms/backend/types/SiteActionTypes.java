package com.wippit.cms.backend.types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum SiteActionTypes {

    NO_ACTION(4000, "No action"),
    SITE_RELOAD(4010, "Site reload");

    private final int type;
    private final String label;

    SiteActionTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String toString(){
        return label;
    }

    public static SiteActionTypes getType(int type) {

        for (SiteActionTypes item : SiteActionTypes.values()){
            if (type == item.type) {
                return item;
            }
        }
        return SiteActionTypes.NO_ACTION;
    }

    public static List<SiteActionTypes> toList() {
        List<SiteActionTypes> list = new ArrayList<>();
        Collections.addAll(list, SiteActionTypes.values());
        return list;
    }

}
