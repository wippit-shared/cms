package com.wippit.cms.backend.types;

public enum SendCodeStatusTypes {

    UNKNOWN(0, "Unknown"),
    WAITING(15000, "Waiting"),
    CONFIRMED(15001, "Confirmed"),
    EXPIRED(15002, "Expired");

    private final int type;
    private final String label;

    SendCodeStatusTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String toString(){
        return label;
    }

    public static SendCodeStatusTypes getType(int type) {

        for (SendCodeStatusTypes status : values()) {
            if (status.type == type) {
                return status;
            }
        }

        return SendCodeStatusTypes.UNKNOWN;
    }

}
