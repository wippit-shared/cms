package com.wippit.cms.backend.types;

import lombok.Getter;

public enum UploadCommandTypes {

    FILE_INFO(13000, "File info"),
    TEMPLATE_INFO(13001, "Template info"),
    PRODUCT_INFO(13002, "Product info"),
    POST_INFO(13003, "Post info"),
    TABLE_INFO(13200, "Table info"),
    TABLE_PRODUCT_INFO(13201, "Table product info"),
    APPEND(13500, "Append segment"),
    ADD(13600, "Add resource"),
    ADD_UPDATE(13601, "Add and update file on user account"),
    ABORT(13998, "Abort command"),
    UNKNOWN(13999, "Unknown command");

    private final int type;
    @Getter
    private final String label;

    UploadCommandTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String toString(){
        return label;
    }

    public static UploadCommandTypes getType(int type) {

        for (UploadCommandTypes item : UploadCommandTypes.values()){
            if (item.value() == type) {
                return item;
            }
        }

        return UploadCommandTypes.UNKNOWN;
    }

    public static UploadCommandTypes getType(String command) {

        for (UploadCommandTypes item : UploadCommandTypes.values()){
            if (("UPLO" + item.value()).equalsIgnoreCase(command)) {
                return item;
            }
        }

        return UploadCommandTypes.UNKNOWN;
    }
}
