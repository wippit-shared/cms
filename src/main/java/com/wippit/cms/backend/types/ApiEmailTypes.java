package com.wippit.cms.backend.types;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public enum ApiEmailTypes {

    ALL(-1, "All", "", ""),
    UNKNOWN(0, "Unknown", "", ""),
    CONTACT_REQUEST(10, "Contact request", "contact_request", "_contact_request"),
    EMAIL_CONFIRM(20, "Email confirm", "email_confirm", "_email_confirm"),
    SEND_CODE(22, "Send code", "send_code", ""),
    NEWSLETTER_SIGNUP(30, "Newsletter signup", "news_signup", "_news_signup"),
    NEWSLETTER(40, "Newsletter", "newsletter",  "_newsletter"),
    NOTIFICATION(50, "Notification", "notification", "_notification"),
    PROMOTION(60, "Promotion", "promo", "_promo"),
    TEST(100, "Test", "test", "");

    private final int type;
    private final String label;
    private final String template;
    private final String page;

    ApiEmailTypes(int type, String label, String template, String page) {
        this.type = type;
        this.label = label;
        this.template = template;
        this.page = page;
    }

    public int value() {
        return type;
    }

    public String toString(){
        return label;
    }

    public String key() {
        return "template-" + template + ".default";
    }

    public static ApiEmailTypes getType(int type) {

        for (ApiEmailTypes item : ApiEmailTypes.values()){
            if (type == item.type) {
                return item;
            }
        }
        return ApiEmailTypes.UNKNOWN;
    }

    public static List<ApiEmailTypes> toList() {
        List<ApiEmailTypes> list = new ArrayList<>();
        list.add(ALL);
        list.add(CONTACT_REQUEST);
        list.add(EMAIL_CONFIRM);
        list.add(NEWSLETTER_SIGNUP);
        list.add(NEWSLETTER);
        list.add(NOTIFICATION);
        list.add(SEND_CODE);
        list.add(PROMOTION);
        list.add(TEST);
        return list;
    }

    public static List<ApiEmailTypes> toEditList() {
        List<ApiEmailTypes> list = new ArrayList<>();
        list.add(CONTACT_REQUEST);
        list.add(EMAIL_CONFIRM);
        list.add(NEWSLETTER_SIGNUP);
        list.add(NEWSLETTER);
        list.add(NOTIFICATION);
        list.add(SEND_CODE);
        list.add(PROMOTION);
        return list;
    }

}
