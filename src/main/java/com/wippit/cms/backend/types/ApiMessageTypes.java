package com.wippit.cms.backend.types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum ApiMessageTypes {

    UNKNOWN(0, "Unknown", "XXXX"),
    HELLO(10, "Hello", "HELO"),
    SIGN_IN(20, "Sign-in", "SIGI"),
    SITE(30, "Site-action", "SITE"),
    ERROR(500, "Error", "ERRO"),;

    private final int type;
    private final String label;
    private final String prefix;

    ApiMessageTypes(int type, String label, String prefix) {
        this.type = type;
        this.label = label;
        this.prefix = prefix;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String toString(){
        return label;
    }

    public String getPrefix() {
        return prefix;
    }

    public static ApiMessageTypes getType(int type) {

        for (ApiMessageTypes item : ApiMessageTypes.values()){
            if (type == item.type) {
                return item;
            }
        }
        return ApiMessageTypes.UNKNOWN;
    }

    public static List<ApiMessageTypes> toList() {
        List<ApiMessageTypes> list = new ArrayList<>();

        Collections.addAll(list, ApiMessageTypes.values());

        return list;
    }

}
