package com.wippit.cms.backend.types;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public enum RoleTypes {

    ALL(-1, "---", "All"),
    GUEST(0, "guest", "Guest"),
    ADMIN(10, "admin","System Admin"),
    EDITOR(20, "editor","CMS Editor"),
    AUTHOR(30, "author","CMS Author"),
    DEVELOPER(40, "developer","CMS Developer");

    @Getter
    private final int type;
    @Getter
    private final String code;
    @Getter
    private final String label;

    RoleTypes(int type, String code, String style) {
        this.type = type;
        this.code = code;
        this.label = style;
    }

    public static RoleTypes getType(int type) {

        for (RoleTypes item : RoleTypes.values()){
            if (item.type == type) {
                return item;
            }
        }

        return RoleTypes.GUEST;
    }

    public static RoleTypes getType(String code) {

        for (RoleTypes item : RoleTypes.values()){
            if (item.code.equalsIgnoreCase(code)) {
                return item;
            }
        }

        return RoleTypes.GUEST;
    }

    public static List<RoleTypes> toList() {
        List<RoleTypes> roles = new ArrayList<>();
        roles.add(ADMIN);
        roles.add(EDITOR);
        roles.add(AUTHOR);
        roles.add(DEVELOPER);
        roles.add(GUEST);
        return roles;
    }

    public static List<RoleTypes> toFilterList() {
        List<RoleTypes> roles = new ArrayList<>();
        roles.add(ADMIN);
        roles.add(EDITOR);
        roles.add(AUTHOR);
        roles.add(DEVELOPER);
        roles.add(ALL);
        return roles;
    }

    public String toString() {
        return label;
    }

}
