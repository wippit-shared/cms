package com.wippit.cms.backend.types;

import lombok.Getter;

@Getter
public enum AccountTypes {

    SYSTEM(0, "System account"),
    EXTERNAL(100, "External guest"),
    EXTENDED(200, "Extended account");

    private final int type;
    private final String label;

    AccountTypes(int type, String style) {
        this.type = type;
        this.label = style;
    }

    public static AccountTypes getType(int type) {

        for (AccountTypes item : AccountTypes.values()){
            if (type == item.type) {
                return item;
            }
        }
        return AccountTypes.SYSTEM;
    }
}
