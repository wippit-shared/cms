package com.wippit.cms.backend.types;

import lombok.Getter;

public enum QueryCommandTypes {

    QUERY_SELECT(17000, "Query select"),
    QUERY_DELETE(17001, "Query delete"),
    QUERY_INSERT(17002, "Query insert"),
    QUERY_GET(17003, "Query get"),
    UNKNOWN(17999, "Unknown query command");

    private final int type;
    @Getter
    private final String label;

    QueryCommandTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String toString(){
        return label;
    }

    public static QueryCommandTypes getType(int type) {

        for (QueryCommandTypes item : QueryCommandTypes.values()){
            if (item.value() == type) {
                return item;
            }
        }

        return QueryCommandTypes.UNKNOWN;
    }

    public static QueryCommandTypes getType(String command) {

        for (QueryCommandTypes item : QueryCommandTypes.values()){
            if (("QERY" + item.value()).equalsIgnoreCase(command)) {
                return item;
            }
        }

        return QueryCommandTypes.UNKNOWN;
    }
}
