/*
 * Copyright (c) 2019-2020. Onewip Corp. All rights reserved.
 * onewip.com/terms
 * contact@onewip.com
 */

package com.wippit.cms.backend.types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum ContentStatusTypes {

    NOT_PUBLISHED(StatusTypes.INACTIVE.value(), "Not published"),
    PUBLISHED(StatusTypes.ACTIVE.value(), "Published"),
    ANY(StatusTypes.ALL.value(), "Any");

    private final int type;
    private final String label;

    ContentStatusTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String toString(){
        return label;
    }

    public StatusTypes toStatus() {
        return StatusTypes.getType(value());
    }

    public static ContentStatusTypes getType(int type) {

        for (ContentStatusTypes item : ContentStatusTypes.values()){
            if (item.value() == type) {
                return item;
            }
        }
        return ContentStatusTypes.NOT_PUBLISHED;
    }

    public static List<ContentStatusTypes> toList() {
        return new ArrayList<>(Arrays.asList(ContentStatusTypes.values()));
    }

    public static List<ContentStatusTypes> toEditList() {
        List<ContentStatusTypes> list = new ArrayList<>();
        list.add(PUBLISHED);
        list.add(NOT_PUBLISHED);
        return list;
    }

}
