package com.wippit.cms.backend.types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum SiteActionResultTypes {

    NO_ACTION(5000, "No action"),
    OK(5010, "Ok"),
    ERROR_INVALID_ACTION(5100, "Invalid action"),
    ERROR_INVALID_PARAMS(5110, "Invalid params"),
    ERROR_UNKNOWN(5199, "Unknown error");

    private final int type;
    private final String label;

    SiteActionResultTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String toString(){
        return label;
    }

    public static SiteActionResultTypes getType(int type) {

        for (SiteActionResultTypes item : SiteActionResultTypes.values()){
            if (type == item.type) {
                return item;
            }
        }
        return SiteActionResultTypes.NO_ACTION;
    }

    public static List<SiteActionResultTypes> toList() {
        List<SiteActionResultTypes> list = new ArrayList<>();
        Collections.addAll(list, SiteActionResultTypes.values());
        return list;
    }

}
