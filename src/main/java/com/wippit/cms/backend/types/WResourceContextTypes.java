package com.wippit.cms.backend.types;

public enum WResourceContextTypes {

    DEFAULT(10, "Default"),
    CONTENT(20, "Content"),
    RESOURCE(30, "Resource");

    private final int type;
    private final String label;

    WResourceContextTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

}
