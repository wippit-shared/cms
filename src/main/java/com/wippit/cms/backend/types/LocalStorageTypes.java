package com.wippit.cms.backend.types;

public enum LocalStorageTypes {

    PUBLIC(0, "Public"),
    PRIVATE(10, "Private");

    private final int type;
    private final String label;

    LocalStorageTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public static LocalStorageTypes getType(int type) {

        for (LocalStorageTypes item : LocalStorageTypes.values()){
            if (item.value() == type) {
                return item;
            }
        }
        return LocalStorageTypes.PUBLIC;
    }


}
