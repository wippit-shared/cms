package com.wippit.cms.backend.types;

import com.wippit.cms.AppConstants;
import lombok.Getter;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@Getter
public enum CMSItemTypes {

    ALL(0, "HTML document"),
    HTML(100, "HTML document"),
    FTL(200, "FTL Document"),
    FTL_SECTION(210, "Section document"),
    FTL_ARTICLE(220, "Article document"),
    RESOURCE(300, "Resource"),
    RESOURCE_INTERNAL(400, "Wippit resource"),
    REDIRECT(500, "Redirect"),
    UNKNOWN(0, "Unknown");

    private final int type;
    private final String label;

    CMSItemTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public static CMSItemTypes getType(int type) {

        for (CMSItemTypes item : CMSItemTypes.values()){
            if (item.type == type) {
                return item;
            }
        }

        return UNKNOWN;
    }

    public static CMSItemTypes getType(String filename, String section) {
        CMSItemTypes type = getType(filename);
        if (Objects.requireNonNull(type) == CMSItemTypes.FTL) {
            return FTL_SECTION;
        } else {
            return type;
        }
    }

    public static CMSItemTypes getTypeByURI(String url) {
        String validURL = url.trim();
        if (validURL.length() > 1024) {
            return UNKNOWN;
        }

        Path path = Paths.get(validURL.trim());
        Path filename = path.getFileName();
        if (filename != null) {
            return CMSItemTypes.getType(filename.toString());
        }

        return UNKNOWN;
    }

    public static CMSItemTypes getType(String filename) {

        if (StringUtils.isBlank(filename)) {
            return CMSItemTypes.UNKNOWN;
        }

        String ext = FilenameUtils.getExtension(filename);

        if (("." + ext).equalsIgnoreCase(AppConstants.TEMPLATE_EXT)) {
            return CMSItemTypes.FTL;
        }
        else if (("." + ext).equalsIgnoreCase(AppConstants.TEMPLATE_DOC1)) {
            return CMSItemTypes.HTML;
        }
        else if (("." + ext).equalsIgnoreCase(AppConstants.TEMPLATE_DOC2)) {
            return CMSItemTypes.HTML;
        }
        else if (StringUtils.isBlank(ext)) {
            return CMSItemTypes.FTL;
        }

        return CMSItemTypes.RESOURCE;
    }

    public String toString() {
        return label;
    }
}
