package com.wippit.cms.backend.types;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public enum HomeTypes {

    ALL(-1, "All"),
    NOT_IN_HOME(0, "Not in Home Page"),
    IN_HOME(1, "In Home Page");

    private final int type;
    private final String label;

    HomeTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public static List<HomeTypes> toList() {
        List<HomeTypes> list = new ArrayList<>();

        list.add(NOT_IN_HOME);
        list.add(IN_HOME);

        return list;
    }

    public static List<HomeTypes> toFilterList() {
        List<HomeTypes> list = new ArrayList<>();

        list.add(ALL);
        list.add(NOT_IN_HOME);
        list.add(IN_HOME);

        return list;
    }

    public static HomeTypes getType(int type) {

        for (HomeTypes item : HomeTypes.values()){
            if (item.type == type) {
                return item;
            }
        }

        return HomeTypes.NOT_IN_HOME;
    }

    @Override
    public String toString() {
        return label;
    }
}
