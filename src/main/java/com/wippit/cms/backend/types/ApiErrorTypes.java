package com.wippit.cms.backend.types;

import java.util.ArrayList;
import java.util.List;

public enum ApiErrorTypes {

    NO_ERROR(3000, "No error"),
    INVALID_SIZE(3010, "Invalid size"),
    INVALID_CHECKSUM(3020, "Invalid checksum"),
    INVALID_KEYS(3030, "Invalid keys"),
    CODING_ERROR(3040, "Coding error"),
    INVALID_KEY_NONCE(3050, "Invalid key and nonce"),
    INVALID_MESSAGE_TYPE(3060, "Invalid message type"),
    INVALID_MESSAGE(3061, "Invalid message"),
    INVALID_COMMAND(3062, "Invalid command"),
    CRYPTO_KEY_ERROR(3070, "Crypto key error"),
    ACTION_ERROR(3080, "Action error"),
    EXPIRED_TOKEN(3090, "Expired token"),
    INVALID_TOKEN(3091, "Invalid token"),

    BAD_REQUEST(3400, "Bad request"),
    NOT_AUTHORIZED(3401, "Not authorized"),
    FORBIDDEN(3403, "Forbidden"),
    NOT_FOUND(3404, "Not found"),
    INTERNAL_ERROR(3500, "Internal error"),
    UPGRADE_REQUIRED(3600, "Upgrade required"),

    //---
    //3900 a 3998 reservados
    //---
    UNKNOWN(3999, "Unknown error");

    private final int type;
    private final String label;

    ApiErrorTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String toString(){
        return label;
    }

    public static ApiErrorTypes getType(int type) {

        for (ApiErrorTypes item : ApiErrorTypes.values()){
            if (type == item.type) {
                return item;
            }
        }
        return ApiErrorTypes.UNKNOWN;
    }

    public static List<ApiErrorTypes> toList() {
        List<ApiErrorTypes> list = new ArrayList<>();

        for (ApiErrorTypes item : ApiErrorTypes.values()){
            list.add(item);
        }

        return list;
    }

}
