package com.wippit.cms.backend.types;


import com.wippit.cms.backend.utils.Size;

import java.util.ArrayList;
import java.util.List;

public enum CropTypes {

    BOX_400(100, "Box 400x400", 400, 400),
    BOX_800(110, "Box 800x800", 800, 800),
    BOX_1600(120, "Box 1600x1600", 1600, 1600),
    CIRCLE_400(200, "Circle 400x400", 400, 400),
    CIRCLE_800(210, "Circle 800x800", 800, 800),
    RECT_800x450(300, "Horizontal rect 800x450", 800, 450),
    RECT_1600x900(310, "Horizontal rect 1600x900", 1600, 900),
    RECT_450x800(400, "Vertical rect 450x800", 450, 800),
    RECT_900x1600(410, "Vertical rect 900x1600", 900, 1600),
    NO_CROP(1000, "Original size", 1000, 1000)
    ;

    private final int type;
    private final int width;
    private final int height;
    private final String label;

    CropTypes(int type, String label, int width, int height) {
        this.type = type;
        this.label = label;
        this.width = width;
        this.height = height;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String toString(){
        return label;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public static CropTypes getType(int type) {

        for (CropTypes item : CropTypes.values()){
            if (item.value() == type) {
                return item;
            }
        }

        return CropTypes.BOX_800;
    }

    public static List<CropTypes> toList() {
        List<CropTypes> list = new ArrayList<>();

        for (CropTypes item : CropTypes.values()){
            list.add(item);
        }

        return list;
    }

    public Float getRatio() {
        return Float.valueOf(width)/Float.valueOf(height);
    }

    public String toProperties() {
        Size size = new Size(width, height);
        return size.toJSON();
    }
}
