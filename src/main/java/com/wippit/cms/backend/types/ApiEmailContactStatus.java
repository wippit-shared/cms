package com.wippit.cms.backend.types;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
public enum ApiEmailContactStatus {

    UNKNOWN(0, "Unknown"),
    CREATED(10, "Created"),
    IGNORED_SPAM(20, "Request ignored by spam"),
    IGNORED_BOT(30, "Request ignored by bot"),
    IGNORED_NOT_VALID(40, "Request not valid"),
    IGNORED_OTHER(50, "Request ignored by other"),
    CONTACT_SENT(100, "Request contact sent"),
    CONTACT_FAIL_NOT_RESPONDED(110, "Request contact failed - no respond"),
    CONTACT_FAIL_RETURNED(120, "Request contact failed - returned"),
    CONTACT_SUCCESS(200, "Request contact success"),;

    private final int type;
    private final String label;

    ApiEmailContactStatus(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String toString(){
        return label;
    }

    public static ApiEmailContactStatus getType(int type) {

        for (ApiEmailContactStatus item : ApiEmailContactStatus.values()){
            if (type == item.type) {
                return item;
            }
        }
        return ApiEmailContactStatus.UNKNOWN;
    }

    public static List<ApiEmailContactStatus> toList() {
        List<ApiEmailContactStatus> list = new ArrayList<>();
        Collections.addAll(list, ApiEmailContactStatus.values());
        return list;
    }

}
