package com.wippit.cms.backend.types;

import java.util.ArrayList;
import java.util.List;

public enum CMSHitTypes {

    OK(200, "Found"),
    ERROR_NOT_FOUND(404, "Not found"),
    ERROR_INTERNAL(500, "Internal error"),
    ERROR_OTHER(1000, "Other error");

    private final int type;
    private final String label;

    CMSHitTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String toString(){
        return label;
    }

    public static CMSHitTypes getType(int type) {

        for (CMSHitTypes item : CMSHitTypes.values()){
            if (type == item.type) {
                return item;
            }
        }
        return CMSHitTypes.ERROR_OTHER;
    }

    public static List<CMSHitTypes> toList() {
        List<CMSHitTypes> list = new ArrayList<>();

        for (CMSHitTypes item : CMSHitTypes.values()){
            list.add(item);
        }

        return list;
    }

    public boolean isError() {
        if (this == OK) {
            return false;
        }
        return true;
    }

    public boolean isSuccess() {
        return !isError();
    }

}
