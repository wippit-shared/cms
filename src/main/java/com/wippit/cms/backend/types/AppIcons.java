package com.wippit.cms.backend.types;

import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.SvgIcon;
import com.vaadin.flow.server.StreamResource;

public enum AppIcons {
    ABOUT("about"),
    ADD("add"),
    ADD_DIRECTOR("add-director"),
    ADD_LOCATION("add-location"),
    ADD_SCHOOL("add-school"),
    ADD_TEACHER("add-teacher"),
    ADD_USER("add-user"),
    ARTICLES("articles"),
    AUDIO("audio"),
    AVATAR("avatar"),
    BADGE_APPROVED("badge_approved"),
    BADGE_NOT_APPROVED("badge_not_approved"),
    BADGE_PENDING("badge_pending"),
    BALANCE("balance"),
    BANNER("banner"),
    CLOUD_DONE("cloud-done"),
    CLOUD_DOWNLOAD("cloud-download"),
    CLOUD_UPLOAD("cloud-upload"),
    CONNECTIONS("connections"),
    DASHBOARD("dashboard"),
    DOCUMENT("document"),
    DOCUMENT_TRASH("document-trash"),
    EDIT("edit"),
    EDIT_HTML("edit-html"),
    EDIT_PLAIN("edit-plain"),
    EMAIL_CONFIG("email-config"),
    ERROR("error"),
    EXCHANGE("exchange"),
    FILE("file"),
    FILE_COPY("file-copy"),
    FILE_DELETE("file-delete"),
    FILE_NEW("file-new"),
    FILE_SAVE("file-save"),
    FOLDER("folder"),
    FOLDER_NEW("folder-new"),
    FORWARDS("forwards"),
    HISTORY("history"),
    HOME("home"),
    ID("id"),
    IMAGE("image"),
    IMAGE_SELECT("image-select"),
    LOGOUT("logout"),
    OK("ok"),
    PAGE_LOGS("page-logs"),
    PAGE_STATS("page-stats"),
    PAGE_TRASH("page-trash"),
    PASSWORD("password"),
    QUESTION("question"),
    RELOAD("reload"),
    RENAME("rename"),
    RESOURCES("resources"),
    SCHOOL("school"),
    SELECT("select"),
    SETTINGS("settings"),
    STUDENT("student"),
    TEACHER("teacher"),
    TEACHERS("teachers"),
    TEMPLATES("templates"),
    TEXT("text"),
    TRANSLATIONS("translations"),
    TRASH("trash"),
    TUITION("tuition"),
    USER_LOGS("user-logs"),
    USERS("users"),
    VIDEO("video"),
    WIPPS("wipps"),
    WIPPS_CONFIG("wipps-config"),
    ZIP("zip");
    private final String svg;

    AppIcons(String svg) {
        this.svg = svg;
    }

    public Image image() {
        return new Image(new StreamResource(svg + ".svg", () -> getClass().getResourceAsStream("/icons/" + svg + ".svg")), svg);
    }

    public Image image(String filter) {
        Image image = image();
        image.getStyle().set("filter", filter);
        return image;
    }

    public Image image(String filter, int size) {
        Image image = image(filter);
        image.getStyle().set("width", size + "px");
        image.getStyle().set("height", size + "px");
        return image;
    }

    public Image image(String filter, int size, int padding) {
        int newSize = size - 2*padding;
        Image image = image(filter, newSize);
        image.getStyle().set("padding", padding + "px");
        return image;
    }

    public SvgIcon icon() {
        StreamResource iconResource = new StreamResource(svg + ".svg", () -> getClass().getResourceAsStream("/icons/" + svg +".svg"));
        return new SvgIcon(iconResource);
    }

    public SvgIcon icon(int size) {
        SvgIcon icon = icon();
        icon.setSize(size + "px");
        return icon;
    }

    public SvgIcon icon(int size, int padding) {
        SvgIcon icon = icon(size);
        icon.getStyle().set("padding", padding + "px");
        return icon;
    }

}