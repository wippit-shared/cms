package com.wippit.cms.backend.types;

import java.util.ArrayList;
import java.util.List;

public enum ContentSearchTypes {

    ALL(0, "All"),
    SECTION(100, "Section"),
    HOME(200, "Home");

    private final int type;
    private final String label;

    ContentSearchTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String toString(){
        return label;
    }

    public static ContentSearchTypes getType(int type) {

        for (ContentSearchTypes item : ContentSearchTypes.values()){
            if (type == item.type) {
                return item;
            }
        }
        return ContentSearchTypes.ALL;
    }

    public static List<ContentSearchTypes> toList() {
        List<ContentSearchTypes> list = new ArrayList<>();

        for (ContentSearchTypes item : ContentSearchTypes.values()){
            list.add(item);
        }

        return list;
    }

}
