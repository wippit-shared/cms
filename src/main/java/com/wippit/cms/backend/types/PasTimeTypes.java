package com.wippit.cms.backend.types;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
public enum PasTimeTypes {

    BEGINNING(0, "Since beginning"),
    YESTERDAY(1, "Since yesterday"),
    WEEK(7, "Last 7 days"),
    WEEK2(14, "Last 14 days"),
    MONTH(30, "Last 30 days"),
    MONTH2(60, "Last 60 days"),
    YEAR(360, "Last 360 days");

    private final int days;
    private final String label;

    PasTimeTypes(int days, String style) {
        this.days = days;
        this.label = style;
    }

    List<PasTimeTypes> toList() {
        return new ArrayList<>(Arrays.asList(PasTimeTypes.values()));
    }

}
