package com.wippit.cms.backend.types;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
public enum ApiEmailRequestStatus {

    UNKNOWN(0, "Unknown"),
    CREATED(10, "Created"),
    SENT(20, "Sent"),
    CONFIRMED(30, "Confirmed"),
    EXPIRED(-1, "Expired");

    private final int type;
    private final String label;

    ApiEmailRequestStatus(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String toString(){
        return label;
    }

    public static ApiEmailRequestStatus getType(int type) {

        for (ApiEmailRequestStatus item : ApiEmailRequestStatus.values()){
            if (type == item.type) {
                return item;
            }
        }
        return ApiEmailRequestStatus.UNKNOWN;
    }

    public static List<ApiEmailRequestStatus> toList() {
        List<ApiEmailRequestStatus> list = new ArrayList<>();
        Collections.addAll(list, ApiEmailRequestStatus.values());
        return list;
    }

}
