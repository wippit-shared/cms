package com.wippit.cms.backend.types;

import java.util.ArrayList;
import java.util.List;

public enum LogTypes {

    CRITICAL_ERROR(-20, "Critical error"),
    ERROR(-10, "Error"),
    INFORMATION(0, "Information"),
    SYSTEM(10, "System"),
    DESIGN(20, "Design"),
    EDITION(30, "Edition"),
    SETTINGS(40, "Settings"),
    ALL(1000, "All");

    private final int type;
    private final String label;

    LogTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String toString(){
        return label;
    }

    public static LogTypes getType(int type) {

        for (LogTypes item : LogTypes.values()){
            if (type == item.type) {
                return item;
            }
        }
        return LogTypes.INFORMATION;
    }

    public static List<LogTypes> toList() {
        List<LogTypes> list = new ArrayList<>();

        for (LogTypes item : LogTypes.values()){
            list.add(item);
        }

        return list;
    }

}
