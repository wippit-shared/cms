package com.wippit.cms.backend.types;

import lombok.Getter;

@Getter
public enum DialogTypes {

    MESSAGE(100, "Message"),
    YES_NO(110, "Yes/No"),
    CONFIRM(120, "Confirm"),
    DELETE(130, "Delete");

    private final int type;
    private final String label;

    DialogTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public static DialogTypes getType(int type) {

        for (DialogTypes item : DialogTypes.values()){
            if (type == item.type) {
                return item;
            }
        }
        return DialogTypes.MESSAGE;
    }
}
