package com.wippit.cms.backend.types;

public enum ToolbarTypes {

    PRIMARY(0, "toolbar_primary"),
    SECONDARY(10, "toolbar_secondary"),
    HEADER(20, "toolbar_header"),
    FOOTER(30, "toolbar_footer");

    private final int type;
    private final String style;

    ToolbarTypes(int type, String style) {
        this.type = type;
        this.style = style;
    }

    public int value() {
        return type;
    }

    public String getStyle() {
        return style;
    }

}
