package com.wippit.cms.backend.types;

import com.vaadin.flow.component.html.Image;
import com.wippit.cms.AppConstants;
import com.wippit.cms.backend.utils.Size;
import com.wippit.cms.backend.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

public enum StorageTypes {

    IMAGE_AVATAR(12000, "Avatar image"),
    IMAGE_BANNER(12001, "Banner image"),
    IMAGE_PRODUCT(12002, "Product image"),
    IMAGE_POST(12003, "Post image"),
    IMAGE_THUMBNAIL(12004, "Thumbnail"),
    IMAGE_OTHER(12005, "Box image"),
    DOCUMENT(12006, "Document"),
    VIDEO(12007, "Video"),
    AUDIO(12009, "Audio"),
    ZIP(12010, "Compressed file"),
    TEXT(12011, "Text"),
    OTHER(12012, "Other"),
    ALL(0, "All");

    private final int type;
    private final String label;

    StorageTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String toString(){
        Size size = getCropSize();
        if (size != null) {
            return label + " (" + size.toDisplay() + ")";
        }

        return label;
    }

    public static StorageTypes getType(int type) {

        for (StorageTypes item : StorageTypes.values()){
            if (item.value() == type) {
                return item;
            }
        }

        return StorageTypes.OTHER;
    }

    public static List<StorageTypes> toLinkList() {
        List<StorageTypes> list = new ArrayList<>();
        list.add(IMAGE_POST);
        list.add(IMAGE_BANNER);
        list.add(IMAGE_PRODUCT);
        list.add(IMAGE_OTHER);
        list.add(DOCUMENT);
        list.add(VIDEO);
        list.add(AUDIO);
        list.add(ZIP);
        list.add(TEXT);
        list.add(OTHER);
        list.add(ALL);
        return list;
    }

    public static List<StorageTypes> toUploadList() {
        List<StorageTypes> list = new ArrayList<>();
        list.add(IMAGE_AVATAR);
        list.add(IMAGE_POST);
        list.add(IMAGE_BANNER);
        list.add(IMAGE_PRODUCT);
        list.add(IMAGE_OTHER);
        list.add(DOCUMENT);
        list.add(VIDEO);
        list.add(AUDIO);
        list.add(ZIP);
        list.add(TEXT);
        list.add(OTHER);
        return list;
    }

    public Size getCropSize(){
        return switch (StorageTypes.getType(type)) {
            case IMAGE_PRODUCT -> new Size(1200, 1200);
            case IMAGE_POST -> new Size(1200, 675);
            case IMAGE_AVATAR -> new Size(800, 800);
            case IMAGE_BANNER -> new Size(1600, 900);
            case IMAGE_OTHER -> new Size(900, 900);
            case IMAGE_THUMBNAIL -> new Size(320, 320);
            default -> null;
        };
    }

    public boolean isImage() {
        switch (StorageTypes.getType(type)) {
            case IMAGE_PRODUCT:
            case IMAGE_POST:
            case IMAGE_AVATAR:
            case IMAGE_BANNER:
            case IMAGE_OTHER:
            case IMAGE_THUMBNAIL:
                return true;
        };

        return false;
    }

    public List<String> getMimeList() {
        List<String> mime = new ArrayList<>();
        switch (StorageTypes.getType(type)) {
            case IMAGE_PRODUCT:
            case IMAGE_POST:
            case IMAGE_AVATAR:
            case IMAGE_BANNER:
            case IMAGE_OTHER:
            case IMAGE_THUMBNAIL:
                mime.add("image/*");
                break;

            case DOCUMENT:
                mime.add("application/*");
                break;

            case VIDEO:
                mime.add("video/*");
                break;

            case AUDIO:
                mime.add("audio/*");
                break;

            case ZIP:
                mime.add("application/gzip");
                mime.add("application/zip");
                mime.add("application/vnd.rar");
                mime.add("application/x-tar");
                break;

            case TEXT:
                mime.add("text/*");
                break;

            case ALL: mime.add("*/*");
                break;

            default: break;
        };

        return mime;
    }

    public Image getImage() {

        String colorFilter = ViewUtils.getColorFilter(AppConstants.COLOR_WHITE);

        if (isImage()) {
            return AppIcons.IMAGE.image(colorFilter);
        }

        return switch (this) {
            case ZIP -> AppIcons.ZIP.image(colorFilter);
            case DOCUMENT -> AppIcons.DOCUMENT.image(colorFilter);
            case VIDEO -> AppIcons.VIDEO.image(colorFilter);
            case AUDIO -> AppIcons.AUDIO.image(colorFilter);
            case TEXT -> AppIcons.TEXT.image(colorFilter);
            default -> AppIcons.FILE.image(colorFilter);
        };
    }

}
