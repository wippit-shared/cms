package com.wippit.cms.backend.types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum StatusTypes {

    INACTIVE(15000, "Inactive"),
    ACTIVE(15001, "Active"),
    ALL(0, "All");

    private final int type;
    private final String label;

    StatusTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int value() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String toString(){
        return label;
    }

    public static StatusTypes getType(int type) {

        if (type <= 0) {
            return StatusTypes.INACTIVE;
        }

        if (type == StatusTypes.ACTIVE.value()) {
            return ACTIVE;
        }

        return StatusTypes.INACTIVE;
    }

    public static StatusTypes getType(String status) {
        if (status.equalsIgnoreCase("1")
                || status.equalsIgnoreCase("10")
                || status.equalsIgnoreCase("100")
                || status.equalsIgnoreCase("+")
                || status.equalsIgnoreCase("true")
                || status.equalsIgnoreCase("yes")) {
            return StatusTypes.ACTIVE;
        }

        return StatusTypes.INACTIVE;
    }

    public static StatusTypes getType(boolean status) {
        if (status) return StatusTypes.ACTIVE;
        return StatusTypes.INACTIVE;
    }

    public static List<StatusTypes> toList() {
        List<StatusTypes> list = new ArrayList<>();

        Collections.addAll(list, StatusTypes.values());

        return list;
    }

    public static List<StatusTypes> toEditList() {
        List<StatusTypes> list = new ArrayList<>();

        list.add(ACTIVE);
        list.add(INACTIVE);

        return list;
    }

}
