package com.wippit.cms.backend.types;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
public enum AccountSetupTypes {

    SETUP_REQUIRED(0, "Confirmation required"),
    SETUP_REJECTED(50, "Rejected"),
    SETUP_VALIDATED(100, "Accepted");

    private final int type;
    private final String label;

    AccountSetupTypes(int type, String style) {
        this.type = type;
        this.label = style;
    }

    public static List<AccountSetupTypes> toList() {
        return new ArrayList<>(Arrays.asList(AccountSetupTypes.values()));
    }

    public static AccountSetupTypes getType(int type) {

        for (AccountSetupTypes item : AccountSetupTypes.values()){
            if (item.type == type) {
                return item;
            }
        }

        return AccountSetupTypes.SETUP_REQUIRED;
    }

    public String toString() {
        return label;
    }
}
