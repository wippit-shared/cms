package com.wippit.cms.backend.types;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public enum SectionTypes {

    HOME(100, "Home"),
    ARTICLES(200, "Article"),
    NOTES(300, "Note");

    private final int type;
    private final String label;

    SectionTypes(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public static List<SectionTypes> toList() {
        List<SectionTypes> list = new ArrayList<>();

        list.add(ARTICLES);
        list.add(NOTES);

        return list;
    }

    public static SectionTypes getType(int type) {

        for (SectionTypes item : SectionTypes.values()){
            if (item.type == type) {
                return item;
            }
        }

        return SectionTypes.ARTICLES;
    }
}
