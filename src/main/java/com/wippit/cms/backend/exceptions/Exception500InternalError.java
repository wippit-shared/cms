package com.wippit.cms.backend.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class Exception500InternalError extends RuntimeException {
    public Exception500InternalError(String message) {
        super(message);
    }
}


