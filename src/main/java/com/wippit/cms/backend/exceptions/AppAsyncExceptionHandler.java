package com.wippit.cms.backend.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

import java.lang.reflect.Method;

@Slf4j
public class AppAsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

    @Override
    public void handleUncaughtException(Throwable throwable, Method method, Object... objects) {
        log.error("Async problem: " + throwable.getMessage());
        log.error("\tMethod: " + method.getName());
        for (Object obj : objects) {
            log.error("\t\tParam: " + obj);
        }

    }
}
