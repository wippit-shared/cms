package com.wippit.cms.backend.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class Exception403Forbidden extends RuntimeException {
    public Exception403Forbidden(String message) {
        super(message);
    }
}


