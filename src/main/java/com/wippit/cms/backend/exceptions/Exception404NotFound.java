package com.wippit.cms.backend.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class Exception404NotFound extends RuntimeException {
    public Exception404NotFound(String message) {
        super(message);
    }
}


