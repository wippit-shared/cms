package com.wippit.cms.backend.exceptions;

import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import lombok.extern.slf4j.Slf4j;

import java.io.Writer;

@Slf4j
public class SiteTemplateExceptionHandler implements TemplateExceptionHandler {


    public SiteTemplateExceptionHandler(){
    }

    @Override
    public void handleTemplateException(
            TemplateException e,
            freemarker.core.Environment environment,
            Writer writer) throws TemplateException {

        try {

            String templateSource = e.getTemplateSourceName();
            String msgError = e.getMessageWithoutStackTop();

            log.error("Template error (" + templateSource + "): " + e.getMessageWithoutStackTop());

            StringBuilder sb = new StringBuilder();
            sb.append("\n=================================");
            sb.append("\n" + msgError);
            sb.append("\n=================================");
            sb.append("\n");

            writer.write(sb.toString());

        }
        catch (Exception ex) {
            log.error("Cant log template error(" + e.getTemplateSourceName() + "): " + ex.toString());
        }
    }

    private String extractPagenameFromTemplate(String original) {

        String pageName = original.replace(".ftl", "");
        if (pageName.indexOf("_") > 0) {
            pageName = pageName.substring(pageName.indexOf("_")+1);
        }
        pageName = pageName.replace("_","/");

        return pageName;
    }

    private String extractTemplate(String original) {
        String pageName = "";
        if (original.indexOf("_") > 0) {
            pageName = original.substring(0, original.indexOf("_"));
        }
        return  pageName;
    }

}