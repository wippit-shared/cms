package com.wippit.cms.backend.exceptions;

import com.wippit.cms.backend.types.ApiErrorTypes;
import lombok.Getter;

public class ApiException extends RuntimeException {
    @Getter
    private final ApiErrorTypes errorType;

    public ApiException(ApiErrorTypes errorType, String message) {
        super(message);
        this.errorType = errorType;
    }

    public ApiException(ApiErrorTypes errorType) {
        super(errorType.getLabel());
        this.errorType = errorType;
    }
}


