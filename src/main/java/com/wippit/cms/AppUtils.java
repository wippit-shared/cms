/***********************************************************************
 Copyright 2024 Wippit.Systems

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ************************************************************************/

package com.wippit.cms;

import com.vaadin.flow.component.html.Image;
import com.wippit.cms.backend.data.bean.geo.CountryLocation;
import com.wippit.cms.backend.data.bean.site.LocalCountry;
import com.wippit.cms.backend.services.AddressService;
import com.wippit.cms.backend.types.AppIcons;
import com.wippit.cms.backend.utils.ResourceSection;
import com.wippit.cms.backend.utils.ResourceStorage;
import com.wippit.cms.backend.utils.ViewUtils;
import com.wippit.cms.backend.utils.Zipper;
import com.wippit.cms.backend.utils.crypto.ChaChaLib;
import com.wippit.cms.backend.utils.crypto.KeyAndNonce;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.buf.HexUtils;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.vaadin.olli.FileDownloadWrapper;

import java.io.File;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class AppUtils {

    public static final int SHORT_CODE = 6;
    public static final int REMOTE_CODE = 8;
    public static final int MEDIUM_CODE = 16;
    public static final int NORMAL_CODE = 24;
    public static final int LONG_CODE = 72;

    private final static NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
    private final static DecimalFormat decimalFormat = new DecimalFormat("####.#");
    private final static ConcurrentHashMap<String, LocalCountry> countries = new ConcurrentHashMap<>();
    private final String redColorFilter;
    private final String blueColorFilter;

    private AppUtils(){
        redColorFilter = ViewUtils.getColorFilter(AppConstants.COLOR_RED);
        blueColorFilter = ViewUtils.getColorFilter(AppConstants.COLOR_BLUE);
    }

    private static final AppUtils _thisInstance = new AppUtils();

    public static AppUtils sharedInstance() {
        return _thisInstance;
    }

    private static String getMilisHex() {
        return Long.toHexString(System.currentTimeMillis());
    }

    private static String createCode() {
        return getMilisHex() + RandomStringUtils.randomAlphanumeric(NORMAL_CODE);
    }

    public static String createLongCode() {
        return getMilisHex() + RandomStringUtils.randomAlphanumeric(LONG_CODE);
    }

    public static String createContentID() {
        return "CI" + createCode();
    }

    public static String createTokenID() {
        return "TK" + createCode();
    }

    //Create ID
    public static String createID(String prefix) {
        return prefix.toUpperCase() + createCode();
    }

    //Create ID
    public static String createDeviceID() {
        return createID("DI");
    }

    public static String createLocalStorageID(boolean isPrivate) {
        if (isPrivate) {
            return "LS0" + createCode();
        }
        else {
            return "LS1" + createCode();
        }
    }

    public static boolean isPrivateStorage(String lsID) {
        return lsID.startsWith("LS0");
    }

    public static String createConfirmationCode() {
        StringBuilder sb = new StringBuilder();
        for (int index=0;index < 12;index++) {
            switch (RandomUtils.nextInt(0,20)) {
                case 0: sb.append("F");break;
                case 1: sb.append("1");break;
                case 2: sb.append("2");break;
                case 3: sb.append("3");break;
                case 4: sb.append("4");break;
                case 5: sb.append("5");break;
                case 6: sb.append("6");break;
                case 7: sb.append("7");break;
                case 8: sb.append("8");break;
                case 9: sb.append("9");break;
                case 10: sb.append("A");break;
                case 11: sb.append("B");break;
                case 12: sb.append("C");break;
                case 13: sb.append("X");break;
                case 14: sb.append("P");break;
                case 15: sb.append("K");break;
                case 16: sb.append("T");break;
                case 17: sb.append("H");break;
                case 18: sb.append("R");break;
                case 19: sb.append("U");break;
            }
        }
        return sb.toString();
    }

    public static String createShortConfirmationCode() {
        StringBuilder sb = new StringBuilder();
        for (int index=0;index < 6;index++) {
            switch (RandomUtils.nextInt(0,20)) {
                case 0: sb.append("F");break;
                case 1: sb.append("1");break;
                case 2: sb.append("2");break;
                case 3: sb.append("3");break;
                case 4: sb.append("4");break;
                case 5: sb.append("5");break;
                case 6: sb.append("6");break;
                case 7: sb.append("7");break;
                case 8: sb.append("8");break;
                case 9: sb.append("9");break;
                case 10: sb.append("A");break;
                case 11: sb.append("B");break;
                case 12: sb.append("C");break;
                case 13: sb.append("X");break;
                case 14: sb.append("P");break;
                case 15: sb.append("K");break;
                case 16: sb.append("T");break;
                case 17: sb.append("H");break;
                case 18: sb.append("R");break;
                case 19: sb.append("U");break;
            }
        }
        return sb.toString();
    }

    public static String createHelloID() {
        return "HE" + createCode();
    }

    /**
     * Get max local date time for articles: 99 years in advance
     * @return current LocalDateTime + 99 years
     */
    public static LocalDateTime getLocalDateMax() {
        Timestamp ts = Timestamp.from(Instant.now().plus(99, ChronoUnit.YEARS));
        LocalDateTime ldt = AppUtils.timestampToLocalDateTime(ts, AppGlobal.getInstance().getSiteInfo().getTime_offset());
        return ldt;
    }

    /**
     * Convert timestamp in UTC to String in ISO format
     * @param timestamp Timestamp
     * @return String in iso format
     */
    public static String timestampToISOString(Timestamp timestamp) {
        long millisSinceEpoch = timestamp.getTime();
        Instant instant = Instant.ofEpochMilli(millisSinceEpoch);
        OffsetDateTime dt = OffsetDateTime.ofInstant(instant, ZoneId.of("UTC"));
        return offsetDateTimeToZonedDateTimeString(dt);
    }

    /**
     * Convert timestamp + offset to String in ISO format
     * @param timestamp Timestamp
     * @param offset  offset in minutes
     * @return String in ISO format
     */
    public static String timestampToISOString(Timestamp timestamp, int offset) {
        OffsetDateTime dt = timestampToOffsetDateTime(timestamp, offset);
        return offsetDateTimeToZonedDateTimeString(dt);
    }

    /**
     * Convert OffsetDateTime to String in ISO format
     * @param dateTime OffsetDateTime
     * @return String in ISO format
     */
    public static String offsetDateTimeToZonedDateTimeString(OffsetDateTime dateTime) {
        return dateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    /**
     * Convert timestamp with offset to ZonedDateTime
     * @param timestamp timestamp
     * @param offset offset in minutes
     * @return ZonedDateTime
     */
    public static ZonedDateTime timestampToZonedDateTime(Timestamp timestamp, int offset) {
        ZoneId zoneId = ZoneId.of(timeOffsetToString(offset));
        return timestamp.toLocalDateTime().atZone(zoneId);
    }

    /**
     * Convert timestamp with offset to OffsetDateTime
     * @param timestamp timestamp
     * @param offset offset in minutes
     * @return OffsetDateTime
     */
    public static OffsetDateTime timestampToOffsetDateTime(Timestamp timestamp, int offset) {
        ZoneOffset zoneOffSet = ZoneOffset.of(timeOffsetToString(offset));
        OffsetDateTime date = OffsetDateTime.of (timestamp.toLocalDateTime(),zoneOffSet);
        return  date;
    }

    /**
     * Get current timestamp converted to site local date with offset
     * @return LocalDateTime with offset
     */
    public static LocalDateTime nowToSiteLocalDate() {
        LocalDateTime ldt = AppUtils.offsetNowToLocalDateTime(AppGlobal.getInstance().getSiteInfo().getTime_offset());
        return ldt;
    }

    /**
     * Convert timestamp to Site LocalDateTime
     * @param timestamp timestamp to convert
     * @return LocalDateTime with Site offset
     */
    public static LocalDateTime timestampToSiteDateTime(Timestamp timestamp) {
        return AppUtils.timestampToLocalDateTime(timestamp, AppGlobal.getInstance().getSiteInfo().getTime_offset());
    }

    /**
     * Converts a timestamp to SiteDate to formatted string
     * @param timestamp Timestamp to convert
     * @return String with formatted SiteDate
     */
    public static String timestampToSiteDateString(Timestamp timestamp) {
        LocalDateTime ldt = AppUtils.timestampToSiteDateTime(timestamp);
        return AppUtils.localDateTimeToSiteFormattedDate(ldt);
    }

    /**
     * Converts a timestamp to SiteDate to formatted string
     * @param timestamp Timestamp to convert
     * @return String with formatted SiteDate
     */
    public static String timestampSiteDateTimeString(Timestamp timestamp) {
        LocalDateTime ldt = AppUtils.timestampToSiteDateTime(timestamp);
        return AppUtils.localDateTimeToFormattedDateTime(ldt);
    }

    /**
     * Convert SiteDate to timestamp
     * @param ldt LocalDateTime with SiteTime to convert to timestamp
     * @return timestamp with site offset
     */
    public static Timestamp siteDateToTimestamp(LocalDateTime ldt) {
        return AppUtils.localDateTimeToTimestamp(ldt, AppGlobal.getInstance().getSiteInfo().getTime_offset());
    }

    /**
     * Convert current instant with offset to LocalDateTime
     * @param offset offset in minutes
     * @return LocalDateTime
     */
    public static LocalDateTime offsetNowToLocalDateTime(int offset) {
        Instant instant = Instant.now().plus(offset, ChronoUnit.MINUTES);
        LocalDateTime date = LocalDateTime.ofInstant(instant, ZoneId.of("UTC"));
        return  date;
    }

    /**
     * Convert LocalDatetime with offset to timestamp in UTC
     * @param offset offset in minutes
     * @return Timestamp in UTC
     */
    public static Timestamp localDateTimeToTimestamp(LocalDateTime ldt, int offset) {
        Instant instant = ldt.toInstant(ZoneOffset.UTC).plusSeconds(-offset* 60L);
        return Timestamp.from(instant);
    }

    /**
     * Convert timestamp with offset to LocalDateTime
     * @param offset offset in minutes
     * @return LocalDateTime
     */
    public static LocalDateTime timestampToLocalDateTime(Timestamp timestamp, int offset) {
        Instant instant = timestamp.toInstant().plus(offset, ChronoUnit.MINUTES);
        return LocalDateTime.ofInstant(instant, ZoneId.of("UTC"));
    }

    /**
     * Get current timestamp
     * @return Timestamp current on server
     */
    public static Timestamp timestampNow() {
        return  Timestamp.from(Instant.now());
    }

    public static Timestamp getNowPlusSeconds(long seconds) {
        LocalDateTime selectDate = LocalDateTime.now(ZoneId.systemDefault());
        LocalDateTime selectDateNext = selectDate.plusSeconds(seconds);
        return Timestamp.valueOf(selectDateNext);
    }

    public static Timestamp getNowPlusMinutes(int minutes) {
        LocalDateTime selectDate = LocalDateTime.now(ZoneId.systemDefault());
        LocalDateTime selectDateNext = selectDate.plusMinutes(minutes);
        return Timestamp.valueOf(selectDateNext);
    }

    public static Timestamp getNowPlusHours(int hours) {
        LocalDateTime selectDate = LocalDateTime.now();
        LocalDateTime selectDateNext = selectDate.plusHours(hours);
        return Timestamp.valueOf(selectDateNext);
    }

    public static Timestamp getNowPlusDays(int days) {
        LocalDateTime selectDate = LocalDateTime.now();
        LocalDateTime selectDateNext = selectDate.plusDays(days);
        return Timestamp.valueOf(selectDateNext);
    }

    public static Timestamp getNowPlusWeeks(int weeks) {
        LocalDateTime selectDate = LocalDateTime.now();
        LocalDateTime selectDateNext = selectDate.plusWeeks(weeks);
        return Timestamp.valueOf(selectDateNext);
    }

    public static Timestamp getNowPlusMonths(int months) {
        LocalDateTime selectDate = LocalDateTime.now();
        LocalDateTime selectDateNext = selectDate.plusMonths(months);
        return Timestamp.valueOf(selectDateNext);
    }

    public static boolean isTimestampValid(Timestamp startTimestamp, Timestamp endTimestamp) {
        long now = timestampNow().toInstant().toEpochMilli();
        long start = startTimestamp.toInstant().toEpochMilli();
        long end = endTimestamp.toInstant().toEpochMilli();

        return (now >= start && now <= end);
        //log.debug("Start: {} \tNow: {} \tEnd:{} => Result: {}", start, now, end, result);
        //return result;
    }

    /**
     * Convert offset in minutes to String offset ex. "+02:00", "-05:00"
     * @param offset offset in minutes
     * @return String offset
     */
    public static String timeOffsetToString(int offset) {
        String startTime = "00:00";
        int minutes = offset;
        boolean flagNeg = false;
        if (offset<0) {
            minutes = Math.abs(offset);
            flagNeg = true;
        }

        int h = minutes / 60 + Integer.parseInt(startTime.substring(0,1));
        int m = minutes % 60 + Integer.parseInt(startTime.substring(3,4));
        String sh = "";
        String sm = "";
        if (Math.abs(h)<10) {
            sh = "0" + h;
        }
        else {
            sh = String.valueOf(h);
        }
        if (Math.abs(m) < 10) {
            sm = "0" + m;
        }
        else {
            sm = String.valueOf(m);
        }
        String newTime;
        if (flagNeg) {
            newTime = "-" + sh + ":" + sm;
        }
        else {
            if (h == 0 && m == 0) {
                newTime = "+0";
            }
            else {
                newTime = "+" + sh + ":" + sm;
            }
        }
        return newTime;
    }

    private static DateTimeFormatter siteDateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MMM/dd HH:mm");
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MMM/dd HH:mm");

    public static void updateDateFormatter(String patter) {
        AppUtils.siteDateTimeFormatter = DateTimeFormatter.ofPattern(patter);
    }

    public static String localDateTimeToSiteFormattedDate(LocalDateTime localDateTime) {
        return AppUtils.siteDateTimeFormatter.format(localDateTime);
    }

    public static String localDateTimeToFormattedDateTime(LocalDateTime localDateTime) {
        return AppUtils.dateTimeFormatter.format(localDateTime);
    }

    public static String getFancySize(long size) {
        final double KB = 1014;
        final double MB = KB*KB;

        if (size < KB ) {
            return size + " b";
        }
        else if (size < MB) {
            double sizeKB = (double)(size)/KB;
            return decimalFormat.format(sizeKB) + " Kb";
        }
        else {
            double sizeMB = (double)(size)/MB;
            return decimalFormat.format(sizeMB) + " Mb";
        }
    }

    public static String getFancyCurrency(int value) {
        return currencyFormat.format(value);
    }

    public static ResourceSection getBlogResource(String path) {
        //---
        //Remove prefix
        //---
        ResourceSection resourceSection = new ResourceSection();
        resourceSection.setSection("blog");

        String[] segments = path.split("/");

        //---
        //Extract secID
        //---
        String sub;
        String page = "";
        for (String segment : segments) {
            sub = segment.trim();
            if (StringUtils.isBlank(sub)) {
                continue;
            }
            if (sub.startsWith(".") || sub.startsWith("_")) {
                continue;
            }
            if (StringUtils.isBlank(resourceSection.getSection())) {
                if (sub.contains(".")) {
                    continue;
                }
                resourceSection.setSection(sub);
            }
            else {
                page = sub;
            }
        }
        if (!StringUtils.isBlank(page)) {
            resourceSection.setResource(page);
        }

        return resourceSection;
    }

    public static ResourceSection getResourceSection(String path) {
        //---
        //Remove prefix
        //---
        ResourceSection resourceSection = new ResourceSection();
        String[] segments = path.split("/");
        //log.debug("Path: {} \nSegments: {}", path, segments.length);
        if (segments.length == 0) {
            String newPath = path.replace("/", "");
            if (StringUtils.isBlank(newPath)) {
                resourceSection.setSection("blog");
                //log.debug("Resource secID: {}", resourceSection);
                return resourceSection;
            }
        }

        //---
        //Extract secID
        //---
        String sub;
        String page = "";
        for (String segment : segments) {
            sub = segment.trim();
            if (StringUtils.isBlank(sub)) {
                continue;
            }
            if (sub.startsWith(".") || sub.startsWith("_")) {
                continue;
            }
            if (StringUtils.isBlank(resourceSection.getSection())) {
                if (sub.contains(".")) {
                    continue;
                }
                if (sub.equalsIgnoreCase("content")) {
                    continue;
                }
                resourceSection.setSection(sub);
            }
            else {
                page = sub;
            }
        }
        if (!StringUtils.isBlank(page)) {
            resourceSection.setResource(page);
        }

        if (StringUtils.isBlank(resourceSection.getSection())) {
            resourceSection.setSection("blog");
        }

        return resourceSection;
    }

    public static ResourceStorage getResourceStorage(String path) {
        //---
        //Remove prefix
        //---
        ResourceStorage resourceStorage = new ResourceStorage();
        String[] segments = path.split("/");
        if (segments.length == 0) {
            return resourceStorage;
        }

        //---
        //Extract secID
        //---
        String sub;
        String page = "";
        StringBuilder sb = new StringBuilder();
        for (String segment : segments) {
            sub = segment.trim();
            if (StringUtils.isBlank(sub)) {
                continue;
            }
            if (sub.startsWith(".") || sub.startsWith("_") || sub.equalsIgnoreCase("resource")) {
                continue;
            }

            sb.append("/");
            sb.append(sub);

        }
        String url = sb.toString();
        if (!StringUtils.isBlank(url)) {
            resourceStorage.setUrl(url);
        }

        return resourceStorage;
    }

    /**
     * Padding int into byte array filled with character
     * @param value long value to strore in padding string
     * @param arraySize Size for padding string
     * @param filler Char for fill padding string
     * @return byte array with value and padded with char
     */
    public static byte[] paddingIntIntoByteArray(long value, int arraySize, char filler) {
        String fill = String.valueOf(filler).repeat(Math.max(0, arraySize));
        String toPad = (fill + value);
        String padded = toPad.substring(toPad.length() - arraySize);
        return padded.getBytes(StandardCharsets.UTF_8);
    }

    /**
     * Encode a string as URL
     * @param value text to encode
     * @return value encoded as URL
     */
    public static String encodeAsUrl(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8);
        }
        catch (Exception ex) {
            log.error("Encoding url[{}]: {}", value, ex.toString());
            return "";
        }
    }

    /**
     * Creates a server URL with encoded params
     * @param partialUrl Query url
     * @param params Params
     * @return Url
     */
    public static String createServerUrl(String partialUrl, Map<String, String> params) {

        StringBuilder sb = new StringBuilder();
        sb.append(AppGlobal.getInstance().getUrl());
        if (!(sb.toString().endsWith("/") || partialUrl.startsWith("/")) && !partialUrl.trim().isEmpty()) {
            sb.append("/");
        }
        sb.append(partialUrl);

        if (!params.isEmpty()) {
            StringBuilder sbParams = new StringBuilder();

            List<String> paramList = params.keySet().stream().toList();
            for (String s : paramList) {
                String encodedVal = encodeAsUrl(params.get(s));
                if (!StringUtils.isBlank(encodedVal)) {
                    sbParams.append(s);
                    sbParams.append("=");
                    sbParams.append(encodedVal);
                }
            }

            String queryParams = sbParams.toString();
            if (!StringUtils.isBlank(queryParams)) {
                sb.append("?");
                sb.append(queryParams);
            }
        }

        //log.info("Server url: {}", sb.toString());
        return sb.toString();
    }

    /**
     * Creates a server URL
     * @param partialUrl Query url
     * @return Url
     */
    public static String createServerUrl(String partialUrl) {
        return createServerUrl(partialUrl, new HashMap<>());
    }

    public static String createServerURL() {
        return createServerUrl("", new HashMap<>());
    }

    public static String simpleEncoding(String message) {
        try {
            KeyAndNonce keyAndNonce = new KeyAndNonce(AppGlobal.getInstance().getCryptoKey());

            ChaChaLib chaChaLib = new ChaChaLib(keyAndNonce);
            byte[] data = chaChaLib.encode(RandomStringUtils.randomAlphabetic(12) + message);
            return HexUtils.toHexString(data);
        }
        catch (Exception ex) {
            log.error("Encoding: " + ex);
            return "";
        }
    }

    public static String simpleDecoding(String encodedMessage) {

        try {
            byte[] data = HexUtils.fromHexString(encodedMessage);
            KeyAndNonce keyAndNonce = new KeyAndNonce(AppGlobal.getInstance().getCryptoKey());
            ChaChaLib chaChaLib = new ChaChaLib(keyAndNonce);
            String decodedMessageWithRandom = new String(chaChaLib.decode(data));
            return StringUtils.substring(decodedMessageWithRandom, 12);
        }
        catch (Exception ex) {
            log.error("Decoding: " + ex);
            return "";
        }
    }

    //----------------------------------------------------------------------------------------------------//
    /**
     * Get country for this request
     * @param request HttpServlet request
     * @return Country for request
     */
    public static String getCountry(HttpServletRequest request) {

        String selectedCountry =  (String) request.getSession().getAttribute(CookieLocaleResolver.DEFAULT_COOKIE_NAME);
        log.debug("Get country from request.session: {}", selectedCountry);

        if (StringUtils.isBlank(selectedCountry)) {
            Optional<String> cookieOp = getCookie(CookieLocaleResolver.DEFAULT_COOKIE_NAME, request);
            if (cookieOp.isPresent()) {
                selectedCountry = cookieOp.get();
            }
            log.debug("Get country from request.cookie: {}", selectedCountry);
        }

        if (StringUtils.isBlank(selectedCountry)) {
            CountryLocation location = AddressService.getCountryFromAddress(request.getRemoteAddr());
            selectedCountry = location.getCountry();
            log.debug("Get country from request.address: {}", selectedCountry);
        }

        LocalCountry localCountry = countries.get(selectedCountry);
        if (localCountry != null) {
            log.debug("Country set to: {}", localCountry.getIso());
            return localCountry.getIso();
        }

        log.debug("No country found. Country set to default: {}", AppGlobal.getInstance().getSiteInfo().getCountry_iso());
        return AppGlobal.getInstance().getSiteInfo().getCountry_iso();
    }

//    public static CountryLocation getCountryFromAddress(String address) {
//
//        if (StringUtils.isBlank(address) || address.equalsIgnoreCase("127.0.0.1")) {
//            return new CountryLocation();
//        }
//
//        Cache cache = sharedInstance().getCacheManager().getCache(AppConstants.CACHE_IP_LOCATION);
//        if (cache != null) {
//            CountryLocation cachedLocation = cache.get(address, CountryLocation.class);
//            if (cachedLocation != null && cachedLocation.getAddress().equalsIgnoreCase(address)) {
//                //log.debug("Country from cached [{}] address: {}/{}", cachedLocation.getAddress(), cachedLocation.getCountry(), cachedLocation.getCity());
//                return cachedLocation;
//            }
//        }
//        else {
//            log.warn("Cache location is null");
//        }
//
//        CountryLocation location = new CountryLocation();
//        try {
//            log.debug("Searching Country from address: {}", address);
//            InetAddress ipAddress = InetAddress.getByName(address);
//            CityResponse response;
//            synchronized (dbReader) {
//                response = dbReader.city(ipAddress);
//            }
//            Country country = response.getCountry();
//
//            location.setCountry(country.getIsoCode());
//            location.setCity(response.getCity().getName());
//            location.setAddress(address);
//
//            log.debug("Country from request[{}]: {}/{}", location.getAddress(), location.getCountry(), location.getCity());
//            if (cache != null) {
//                cache.putIfAbsent(location.getAddress(), location);
//            }
//            else {
//                log.warn("Cache put location is null");
//            }
//            return location;
//        }
//        catch (Exception ex) {
//            log.warn("Cant query location database: {}", ex.toString());
//        }
//
//        return location;
//    }

    //----------------------------------------------------------------------------------------------------//
    /**
     * Get cookie from request
     * @param cookieName Cookie name
     * @param request HttpServlet request
     * @return cookie value
     */
    public static Optional<String> getCookie(String cookieName, HttpServletRequest request) {
        if (request == null) {
            return Optional.empty();
        }
        if (request.getCookies() == null) {
            return Optional.empty();
        }

        return Arrays.stream(request.getCookies())
                .filter(c -> cookieName.equals(c.getName()))
                .map(Cookie::getValue)
                .findAny();
    }

    //----------------------------------------------------------------------------------------------------//
    /**
     * Get parameter value from query
     * @param request HttpServletRequest
     * @param param param name
     * @param defaultValue default value
     * @return parameter value
     */
    public static String getSecureParameter(HttpServletRequest request, String param, String defaultValue) {
        String sParam = request.getParameter(param);
        if (StringUtils.isBlank(sParam)) {
            return defaultValue;
        }

        return sParam.trim();
    }

    public static int getSecureParameterAsInt(HttpServletRequest request, String param, int defaultValue) {
        try {
            String sParam = getSecureParameter(request, param, "0");
            return Integer.parseInt(sParam);
        }
        catch (Exception ex) {
            log.warn("Invalid parameter: {}", ex.toString());
        }

        return 0;
    }

    public static void loadCountries(List<LocalCountry> localCountries) {
        countries.clear();
        for (LocalCountry country : localCountries) {
            countries.put(country.getIso(), country);
            log.debug("Country: {}", country);
        }
    }

    public static List<LocalCountry> getCountries() {
        List<LocalCountry> countryList = new ArrayList<>();
        countryList.addAll(countries.values());
        return countryList;
    }

    public static Image getMenuImage(AppIcons icon) {
        return icon.image(AppUtils.sharedInstance().blueColorFilter, 32, 4);
    }

    public static Image getToolbarImage(AppIcons icon) {
        return icon.image(AppUtils.sharedInstance().blueColorFilter, 24, 2);
    }

    public static String validData(String value) {
        if (StringUtils.isBlank(value)) {
            return "";
        }

        return value;
    }

//    public static List<EmailAttachment> getAttachments(ApiEmailTypes emailTypes) {
//        List<EmailAttachment> attachments = new ArrayList<>();
//
//        String dir = AppGlobal.getInstance().getEmailPath() + "/" + emailTypes.getTemplate();
//        try {
//
//            Collection<File> files = FileUtils.listFiles(new File(dir), null, false);
//            if (files == null) {
//                return attachments;
//            }
//
//            for (File file : files) {
//                EmailAttachment attachment = new EmailAttachment();
//                attachment.setName(file.getName());
//                attachment.setPath(file.getAbsolutePath());
//                String cid = FileUtil.getNameOnly(file);
//                attachment.setCid(cid);
//
//                attachments.add(attachment);
//
//                log.debug("Set attachment: {}", attachment);
//            }
//        }
//        catch (Exception ex) {
//            log.warn("Getting attachments: {}", ex.toString());
//        }
//
//        return  attachments;
//    }

    public static String fixAccountName(String original) {
        String data = StringUtils.stripAccents(original).toLowerCase();
        StringBuilder account = new StringBuilder();
        String validCharacters = "abcdefghijklmnopqrstuvwxyz1234567890_.";

        for (int index = 0; index < data.length(); index++) {
            if (validCharacters.contains(String.valueOf(data.charAt(index)))) {
                account.append(data.charAt(index));
            }
        }

        return account.toString();
    }

    public static String fixPathName(String original) {
        String data = StringUtils.stripAccents(original).toLowerCase();
        StringBuilder account = new StringBuilder();
        String validCharacters = "abcdefghijklmnopqrstuvwxyz1234567890_";

        for (int index = 0; index < data.length(); index++) {
            if (validCharacters.contains(String.valueOf(data.charAt(index)))) {
                account.append(data.charAt(index));
            }
        }

        return account.toString();
    }

    public static String fixURLFromName(String original) {
        String data = StringUtils.stripAccents(original).toLowerCase();
        StringBuilder account = new StringBuilder();
        String validCharacters = "abcdefghijklmnopqrstuvwxyz1234567890_";

        for (int index = 0; index < data.length(); index++) {
            if (validCharacters.contains(String.valueOf(data.charAt(index)))) {
                account.append(data.charAt(index));
            }
            else {
                account.append("_");
            }
        }

        return account.toString();
    }

    public static class DownloadProvider implements FileDownloadWrapper.DownloadBytesProvider {
        private String file = "";

        public DownloadProvider(String source) {
            this.file = source;
        }

        @Override
        public byte[] getBytes() {
            try {
                return FileUtils.readFileToByteArray(new File(file));
            }
            catch (Exception ex) {
                log.error("Can't get data from file: {}", file);
                return "".getBytes();
            }
        }
    }

    public static class ZipDirAndDownloadProvider implements FileDownloadWrapper.DownloadBytesProvider {

        private String source = "";

        public ZipDirAndDownloadProvider(String source) {
            this.source = source;
        }

        private String  makeZip(){
            String tmp = RandomStringUtils.randomAlphabetic(10) + "-template.zip";
            Path outputPath = Path.of(AppGlobal.getInstance().getTempPath() + "/" + tmp);

            try {
                Zipper zipper = new Zipper();
                zipper.zipDirectory(source, outputPath.toString());
                return outputPath.toString();
            }
            catch (Exception ex) {
                return "";
            }
        }


        @Override
        public byte[] getBytes() {
            log.debug("Making zip to download: {}", source);
            String zipFile = makeZip();
            log.debug("Download zip: {}", zipFile);

            try {
                return FileUtils.readFileToByteArray(new File(zipFile));
            }
            catch (Exception ex) {
                log.error("From[{}] can't get data: {}", source, ex.toString());
                return "".getBytes();
            }
        }
    }
}
