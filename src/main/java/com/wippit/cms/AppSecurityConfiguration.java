/***********************************************************************
 Copyright 2024 Wippit.Systems

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ************************************************************************/

package com.wippit.cms;

import com.vaadin.flow.spring.security.VaadinWebSecurity;
import com.wippit.cms.frontend.open.views.login.LoginView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Slf4j
@EnableWebSecurity
@Configuration
@EnableCaching
@EnableAsync
public class AppSecurityConfiguration extends VaadinWebSecurity {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(WebSecurity web) throws Exception {
        web.ignoring().requestMatchers(
                "/*",
                "/assets/**",
                "/js/**",
                "/css/**",
                "/webfonts/**",
                "/static/**",
                "/images/**",
                "/go/**",
                "/do/**",
                "/wres/**",
                "/blog/**",
                "/content/**",
                "/resource/**",
                "/app/wipp/**",
                "/app/VAADIN/**",
                "/VAADIN/**"
        );
        super.configure(web);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        log.debug("Configuring");
        http.authorizeHttpRequests(authorize -> {
//            authorize.requestMatchers(new AntPathRequestMatcher("/*")).anonymous();
//            authorize.requestMatchers(new AntPathRequestMatcher("/js/**")).anonymous();
//            authorize.requestMatchers(new AntPathRequestMatcher("/css/**")).anonymous();
//            authorize.requestMatchers(new AntPathRequestMatcher("/assets/**")).anonymous();
//            authorize.requestMatchers(new AntPathRequestMatcher("/static/**")).anonymous();
//            authorize.requestMatchers(new AntPathRequestMatcher("/webfonts/**")).anonymous();
//            authorize.requestMatchers(new AntPathRequestMatcher("/images/**")).anonymous();
//            authorize.requestMatchers(new AntPathRequestMatcher("/blog/**")).anonymous();
//            authorize.requestMatchers(new AntPathRequestMatcher("/content/**")).anonymous();
//            authorize.requestMatchers(new AntPathRequestMatcher("/go/**")).anonymous();
//            authorize.requestMatchers(new AntPathRequestMatcher("/do/**")).anonymous();
//            authorize.requestMatchers(new AntPathRequestMatcher("/wres/**")).anonymous();
//            authorize.requestMatchers(new AntPathRequestMatcher("/app/wipp/**")).anonymous();
//            authorize.requestMatchers(new AntPathRequestMatcher("/app/VAADIN/**")).permitAll();

            authorize.requestMatchers(new AntPathRequestMatcher("/app/line-awesome/**/*.svg")).permitAll();
        });

        super.configure(http);

        setLoginView(http, LoginView.class);
    }

    private CacheManager cacheManager = null;

    @Bean
    public CacheManager cacheManager() {
        if (cacheManager == null) {
            cacheManager = new ConcurrentMapCacheManager(AppConstants.CACHE_SESSIONS);
        }
        return cacheManager;
    }

}
