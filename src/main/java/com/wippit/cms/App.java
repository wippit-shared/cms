/***********************************************************************
Copyright 2024 Wippit.Systems

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
************************************************************************/

package com.wippit.cms;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import com.wippit.cms.backend.utils.crypto.KeyAndNonce;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.TimeZone;

@SpringBootApplication
@Theme(value = "cms", variant = Lumo.LIGHT)
//@Push
@MapperScan("com.wippit.cms.backend.data")
public class App implements AppShellConfigurator {

    public static void main(String[] args) {

        //---
        //Set Timezone default to UTC
        //---
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

        //---
        //To serialize ZonedDateTime to ISO
        //---
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        //---
        //About
        //---
        AppConstants.about();

        //---
        //Setup
        //---
        KeyAndNonce.setKeySource("Salmo119");

        SpringApplication.run(App.class, args);
    }

}
