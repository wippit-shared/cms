function getCookie(name) {
    function escape(s) { return s.replace(/([.*+?\^$(){}|\[\]\/\\])/g, '\\$1'); }
    var match = document.cookie.match(RegExp('(?:^|;\\s*)' + escape(name) + '=([^;]*)'));
    return match ? decodeURIComponent(match[1]) : null;
}

function setCookie(name, value) {
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + ";SameSite=Strict";
}

function setCookieDays(name, value, days) {
    var expire = days*24*60*60;
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + ";SameSite=Strict;max-age="+expire;
}

function openURL(url) {
  var link = document.createElement("a")
  link.href = url;
  link.target = "_blank"
  link.click()
}s