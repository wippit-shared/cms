
set search_path to cms, public;

-- ============================================================================================================
-- team
-- ============================================================================================================

-- ------------------------------------------------------
CREATE TABLE if not exists cms.ow_team (
    id              serial PRIMARY KEY,
    oid             varchar NOT NULL,
    owner           varchar not null default '',
    name            varchar not null default '',
    commercial_name varchar not null default '',
    type            int not null default 0,
    about           varchar not null default '',
    color           varchar not null default '',
    site_url        varchar not null default '',
    avatar_url      varchar not null default '',
    banner_url      varchar not null default '',
    email           varchar not null default '',
    phone           varchar not null default '',
    contact         varchar not null default '',
    address         varchar not null default '',
    work_hours      varchar not null default '',
    created_date    timestamp with time zone NOT NULL DEFAULT now(),
    unique(oid)
);

-- ============================================================================================================
-- Account
-- ============================================================================================================

-- ------------------------------------------------------
CREATE TABLE if not exists cms.ow_account (
    id              serial PRIMARY KEY,
    alias           varchar NOT NULL DEFAULT '',
    status          integer NOT NULL DEFAULT 10,
    full_name       varchar NOT NULL DEFAULT '',
    email           varchar NOT NULL DEFAULT '',
    avatar_url      varchar NOT NULL DEFAULT '',
    banner_url      varchar NOT NULL DEFAULT '',
    about           varchar NOT NULL DEFAULT '',
    color           varchar NOT NULL DEFAULT '',
    secret          varchar not null default '',
    type            int not null default 0,
    created_date    timestamp with time zone NOT NULL DEFAULT now(),
    unique(alias),
    unique(email)
);

create index  if not exists i1_account on cms.ow_account(full_name, email);

-- ------------------------------------------------------
create table if not exists cms.ow_account_role (
    id              serial PRIMARY KEY,
    alias           varchar NOT NULL DEFAULT '',
    role            varchar NOT NULL DEFAULT '',
    created_date    timestamp with time zone NOT NULL DEFAULT now(),
    unique (alias, role)
);

-- ------------------------------------------------------
create table if not exists cms.ow_account_signup (
    id              serial PRIMARY KEY,
    sgid            varchar NOT NULL DEFAULT '' unique,
    alias           varchar NOT NULL DEFAULT '',
    full_name       varchar NOT NULL DEFAULT '',
    email           varchar NOT NULL DEFAULT '',
    code            varchar NOT NULL DEFAULT '',
    status          int not null default 0,
    info            varchar not null default '',
    created_date    timestamp with time zone NOT NULL DEFAULT now(),
    expire_date     timestamp with time zone NOT NULL DEFAULT now(),
    status_date     timestamp with time zone NOT NULL DEFAULT now()
);

create index  if not exists i1_account_signup on cms.ow_account_signup(alias, email, status, expire_date);

-- ============================================================================================================
-- site
-- ============================================================================================================

-- ------------------------------------------------------
CREATE TABLE if not exists cms.ow_site_setting (
    id                  serial PRIMARY KEY,
    key                 varchar NOT NULL default '',
    value               varchar NOT NULL default '',
    created_date        timestamp with time zone NOT NULL DEFAULT now(),
    unique(key)
);

-- ============================================================================================================
-- Storage
-- ============================================================================================================

-- ------------------------------------------------------
CREATE TABLE if not exists cms.ow_storage (
    id              serial PRIMARY KEY,
    url             varchar NOT NULL DEFAULT '',
    fid             varchar NOT NULL DEFAULT '',
    alias           varchar NOT NULL DEFAULT '',
    type            integer NOT NULL DEFAULT 0,
    path            varchar NOT NULL DEFAULT '',
    is_private      boolean NOT NULL DEFAULT false,
    original_name   varchar NOT NULL DEFAULT '',
    byte_size       integer NOT NULL DEFAULT 0,
    mime            varchar NOT NULL DEFAULT '',
    properties      varchar NOT NULL DEFAULT '',
    created_date    timestamp with time zone NOT NULL DEFAULT now(),
    unique(url),
    unique(fid)
);

create index if not exists i1storage on cms.ow_storage (alias, type, original_name);

-- ============================================================================================================
-- template
-- ============================================================================================================

-- ------------------------------------------------------
CREATE TABLE if not exists cms.ow_template (
    id              serial PRIMARY KEY,
    name            varchar not null default '',
    page_root       varchar not null default '',
    page_404        varchar not null default '',
    page_500        varchar not null default '',
    page_author     varchar not null default '',
    page_search     varchar not null default '',
    page_user       varchar not null default '',
    page_blog       varchar not null default '',
    page_item       varchar not null default '',
    created_date    timestamp with time zone NOT NULL DEFAULT now(),
    unique (name)
);

-- ------------------------------------------------------
CREATE TABLE if not exists cms.ow_template_section (
    id              serial PRIMARY KEY,
    secID           varchar not null default '',
    iso             varchar not null default '',
    name            varchar not null default '',
    section_page    varchar not null default '',
    item_page       varchar not null default '',
    created_date    timestamp with time zone NOT NULL DEFAULT now(),
    unique (name, iso, secID)
);

-- ------------------------------------------------------
CREATE TABLE if not exists cms.ow_template_index (
    id              serial PRIMARY KEY,
    iso             varchar not null default '',
    template        varchar not null default '',
    page            varchar not null default '',
    created_date    timestamp with time zone NOT NULL DEFAULT now(),
    unique (iso, template)
);

-- ============================================================================================================
-- content
-- ============================================================================================================

-- ------------------------------------------------------
CREATE TABLE if not exists cms.ow_country (
    id              serial PRIMARY KEY,
    iso             varchar not null default '',
    iso2            varchar not null default '',
    iso3            varchar not null default '',
    name            varchar not null default '',
    flag            varchar not null default '',
    locale          varchar not null default '',
    display         varchar not null default '',
    currency        varchar not null default '',
    currency_prefix varchar not null default '',
    date_format     varchar not null default '',
    phone_code      varchar not null default '',
    created_date    timestamp with time zone NOT NULL DEFAULT now(),
    unique (iso)
);

-- ------------------------------------------------------
CREATE TABLE if not exists cms.ow_section (
    id              serial PRIMARY KEY,
    iso             varchar not null default '',
    secID           varchar not null default '',
    name            varchar not null default '',
    type            int not null default 0,
    hit_count       int not null default 0,
    created_date    timestamp with time zone NOT NULL DEFAULT now(),
    unique (iso, secID)
);

create index  if not exists i1_section on cms.ow_section (iso, name);

-- ------------------------------------------------------
CREATE TABLE if not exists cms.ow_content (
    id              serial PRIMARY KEY,
    cid             varchar not null default '',
    ocid            varchar not null default '',
    iso             varchar not null default '',
    url             varchar not null default '',
    secID           varchar not null default '',
    section_type    int not null default 0,
    home            int not null default 0,
    title           varchar not null default '',
    tags            varchar not null default '',
    image_fid       varchar not null default '',
    image_url       varchar not null default '',
    content_intro   varchar not null default '',
    content_complete varchar not null default '',
    author          varchar not null default '',
    status          int not null default 0,
    start_date      timestamp with time zone NOT NULL DEFAULT now(),
    end_date        timestamp with time zone NOT NULL DEFAULT now(),
    event_date      timestamp with time zone not null default now(),
    hit_count       int not null default 0,
    created_date    timestamp with time zone NOT NULL DEFAULT now(),
    unique (cid),
    unique (iso, url)
);

create index  if not exists i1_content on cms.ow_content (iso, url, secID, home, status, start_date, end_date);
create index  if not exists i2_content on cms.ow_content (iso, author);
create index  if not exists i3_content on cms.ow_content (ocid, iso);

-- ------------------------------------------------------
CREATE TABLE if not exists cms.ow_content_home (
    id              serial PRIMARY KEY,
    iso             varchar not null default '',
    cid             varchar not null default '',
    index           int not null default 999,
    created_date    timestamp with time zone NOT NULL DEFAULT now(),
    unique (iso, cid)
);

create index i1_content_home on cms.ow_content_home (iso, index);

-- ============================================================================================================
-- Stats
-- ============================================================================================================

-- ------------------------------------------------------
drop table if exists cms.ow_hit cascade;

create table cms.ow_hit (
    id                  serial PRIMARY KEY,
    url                 varchar not null default '',
    type                int not null default 0,
    count               int not null default 0,
    created_date        timestamp with time zone NOT NULL DEFAULT now(),
    unique (url)
);

create index i1_hit on cms.ow_hit (url, type, count);

-- ------------------------------------------------------
drop table if exists cms.ow_hit_log cascade;

create table cms.ow_hit_log (
    id                  serial PRIMARY KEY,
    url                 varchar not null default '',
    type                int not null default 0,
    hit                 int not null default 0,
    address             varchar not null default '',
    agent               varchar not null default '',
    country             varchar not null default '',
    city                varchar not null default '',
    created_date        timestamp with time zone NOT NULL DEFAULT now()
);

create index i1_hit_log on cms.ow_hit_log (created_date, url, hit);
create index i2_hit_log on cms.ow_hit_log (url, country, city, agent);

-- ============================================================================================================
-- Log
-- ============================================================================================================

drop table if exists cms.ow_log cascade;

create table cms.ow_log (
    id                  serial PRIMARY KEY,
    alias               varchar not null default '',
    type                int not null default 0,
    detail              varchar not null default '',
    created_date        timestamp with time zone NOT NULL DEFAULT now()
);

create index i1log on cms.ow_log(alias, type, created_date);

-- ============================================================================================================
-- Devices
-- ============================================================================================================

drop table if exists cms.ow_device cascade;

create table cms.ow_device (
    id                  serial PRIMARY KEY,
    deviceID            varchar not null default '',
    secret              varchar not null default '',
    username            varchar not null default '',
    app                 varchar not null default '',
    os                  varchar not null default '',
    model               varchar not null default '',
    pub_key             varchar not null default '',
    jwt                 varchar not null default '',
    jwt_start_date      timestamp with time zone NOT NULL DEFAULT now(),
    jwt_end_date        timestamp with time zone NOT NULL DEFAULT now(),
    created_date        timestamp with time zone NOT NULL DEFAULT now(),
    unique (deviceID)
);

create index i1device on cms.ow_device(username);
create index i2device on cms.ow_device(jwt);


-- ============================================================================================================
-- redirect
-- ============================================================================================================

-- ------------------------------------------------------
CREATE TABLE if not exists cms.ow_redirect (
    id              serial PRIMARY KEY,
    iso             varchar not null default '',
    name            varchar not null default '',
    url             varchar not null default '',
    hit_count       int not null default 0,
    status          int not null default 0,
    start_date      timestamp with time zone NOT NULL DEFAULT now(),
    end_date        timestamp with time zone NOT NULL DEFAULT now(),
    created_date    timestamp with time zone NOT NULL DEFAULT now(),
    unique (iso, name)
);

create index if not exists i1redirect on cms.ow_redirect (iso, name, status, start_date);

-- ============================================================================================================
-- emails
-- ============================================================================================================

-- ------------------------------------------------------------------------------------------------------------

drop table if exists cms.ow_email_pool cascade;

CREATE TABLE if not exists cms.ow_email_pool (
    id              serial PRIMARY KEY,
    emailID         varchar not null default '' unique,
    etid            varchar not null default '',
    type            int not null default 0,
    status          int not null default 0,
    email           varchar not null default '',
    info            varchar not null default '',
    from_address    varchar not null default '',
    confirm_address  varchar not null default '',
    start_date      timestamp with time zone NOT NULL DEFAULT now(),
    status_date     timestamp with time zone NOT NULL DEFAULT now(),
    expire_date     timestamp with time zone NOT NULL DEFAULT now() + interval ' 60 min'
);

create index if not exists i1email_pool on cms.ow_email_pool (type, status, expire_date);

-- ------------------------------------------------------------------------------------------------------------

drop table if exists cms.ow_email_template cascade;

create table if not exists cms.ow_email_template (
        id              serial PRIMARY KEY,
        etid            varchar not null default '' unique,
        name            varchar not null default '',
        type            int not null default 0,
        page            varchar not null default '',
        html            varchar not null default '',
        plain           varchar not null default '',
        data            varchar not null default '', --json
        comments        varchar not null default '',
        created_date    timestamp with time zone NOT NULL DEFAULT now()
);

create unique index if not exists i1email_template on cms.ow_email_template (type, name);

-- ------------------------------------------------------------------------------------------------------------
create table if not exists cms.ow_email_template_attachment (
        id              serial PRIMARY KEY,
        etid            varchar not null default '',
        fid             varchar not null default '',
        cid             varchar not null default '',
        created_date    timestamp with time zone NOT NULL DEFAULT now(),
        unique (etid, cid)
);

create index if not exists i1_email_template_attachment on cms.ow_email_template_attachment(fid);

-- ------------------------------------------------------------------------------------------------------------
create table if not exists cms.ow_email_contact_request (
    id              serial PRIMARY KEY,
    emailID         varchar not null default '' unique,
    name            varchar not null default '',
    email           varchar not null default '',
    phone           varchar not null default '',
    company         varchar not null default '',
    message         varchar not null default '',
    status          int not null default 0,
    from_address    varchar not null default '',
    confirm_address varchar not null default '',
    alias           varchar not null default '',
    response        varchar not null default '',
    created_date    timestamp with time zone NOT NULL DEFAULT now(),
    status_date     timestamp with time zone NOT NULL DEFAULT now()
);


-- ============================================================================================================
-- Translation
-- ============================================================================================================

create table IF NOT EXISTS cms.ow_languages (
    id              serial PRIMARY KEY,
    template        varchar not null default '',
    page            varchar not null default '',
    iso             varchar not null default '',
    key             varchar not null default '',
    content         varchar not null default '',
    unique (template, page, iso, key)
);

create index if not exists i1language on cms.ow_languages (template, page, iso, key);

