-- --------------------------------------------------------------------------------
-- SETUP STEP1: in TEMPLATE1 database
-- --------------------------------------------------------------------------------
--create user cms_user with createdb;
--alter user cms_user with password 'cms_user';
--
--create database cms with owner cms_user;
--

set search_path to public;

--create extension IF NOT EXISTS hstore;
--create extension IF NOT EXISTS "uuid-ossp";
--create extension IF NOT EXISTS pgcrypto;

create schema if not exists cms;

