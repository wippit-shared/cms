# Wippit CMS

This is just another Java CMS. Simple like that.

Current version is 1.1.

## What makes it different

Just another Java CMS plus:
+ Vaadin as frontend for management    
+ Freemarker for web macros & includes
+ Localization support 
+ Translation support
+ GeoDB support
+ Web redirections 
+ Redirection from common port to secure port  

## Tech stack

- Java 17
- Spring Boot 3.2.x 
- Vaadin 24.x 
- PostgreSql 16.x 
- Freemarker 2.3.x

## Local mode 

For development we recommend:
- Intellij Idea as your IDE
- Firefox as your browser

### Configure 

Adjust `src/main/resources/application.properties` to local.

Adjust `src/main/resources/application-local.properties` as you requires.

### Compile 

The project is a standard Maven project. To compile it from the command line:

`
mvn clean package 
`

### Run 

To run it from the command line:

`
mvn spring-boot:run 
`

Go to http://localhost:8080 in your browser. It will be redirected to https://localhost:8443. 

### Management

Go to https://localhost:8443/app 

Default admin account / password:
- Admin / cmsadmin 

## Production mode 

Adjust `src/main/resources/application.properties` to production.

Adjust `src/main/resources/application-production.properties` as you requires.

To compile it from the command line:

`
mvn clean package -Pproduction
`

To run it from the command line:

`
java -jar target/cms-1.0.jar
`

## Project structure

- App: `src/main/java/com/wippit/cms`
- Backend: `src/main/java/com/wippit/cms/backend` 
- Frontend: `src/main/java/com/wippit/cms/frontend`

## About Wippit.Systems 

Visit our site:  https://wippit.systems 

## Links

- Vaadin: https://vaadin.com 
- Freemarker: https://freemarker.apache.org
- SpringBoot: https://spring.io/projects/spring-boot
- GeoDB: https://www.maxmind.com/en/solutions/ip-geolocation-databases-api-services
- DeepL: https://www.deepl.com/en/pro-api?cta=header-pro-api 
- PostgreSql: https://www.postgresql.org

